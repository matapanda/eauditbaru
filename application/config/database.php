<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$active_group = 'sia';
$query_builder = TRUE;
$db['sia'] = array(
	'dsn'	=> '',
	'hostname' => '127.0.0.1',
    'username' => '1',
    'password' => '1',
	'database' => '1',
	'dbdriver' => 'postgre',
	'dbprefix' => '',
	'pconnect' => FALSE,
	'db_debug' => (ENVIRONMENT !== 'production'),
	'cache_on' => FALSE,
	'cachedir' => '',
	'char_set' => 'utf8',
	'dbcollat' => 'utf8_general_ci',
	'swap_pre' => '',
	'encrypt' => FALSE,
	'compress' => FALSE,
	'stricton' => FALSE,
	'failover' => array(),
	'save_queries' => TRUE
);
