<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$hook['post_controller_constructor'] = function()
{
//    $_GET=str_replace("'","",$_GET);
    $CI = & get_instance();
    if($CI->router->fetch_class()!='api'){
        $config=$CI->session->userdata('config');
        $whitelist=array('maintenance','init');
        if(!isset($config['id']) && !in_array(strtolower($CI->router->fetch_class()),$whitelist)){
            redirect(base_url('init'));
        }elseif($CI->session->userdata('locked') && strtolower($CI->router->fetch_class())!='gateway'){
            redirect(base_url('gateway/locked'));
        }elseif($config['maintenance']=='t' && !in_array(strtolower($CI->router->fetch_class()),$whitelist)){
            redirect(base_url('maintenance'));
        }elseif($CI->session->userdata('user')){
            $user=$CI->session->userdata('user');
            $CI->db->select('rolemenu.menu,rolemenu.c,rolemenu.r,rolemenu.u,rolemenu.d');
            $CI->db->join('role','role.id=rolemenu.role and role.status=\'t\'');
            $CI->db->join('roleusers','roleusers.role=rolemenu.role and roleusers.user=\''.$user['id'].'\'');
            $raw=$CI->db->get('rolemenu')->result_array();
            $role=array();
            foreach ($raw as $r){
                foreach ($r as $i=>$s){
                    if($i=='menu' || $s!='t'){
                        continue;
                    }
                    $role[$r['menu']][$i]=true;
                }
            }
            $CI->session->set_userdata('role',$role);
        }
    }
};