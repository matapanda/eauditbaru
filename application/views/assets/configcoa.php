<table class="table table-bordered table-striped">
    <thead>
    <tr>
        <th class="col-xs-4">Keterangan</th>
        <th class="center col-xs-4">Debet</th>
        <th class="center col-xs-4">Kredit</th>
    </tr>
    </thead>
    <tbody>
    <?php
    foreach($data as $g) {
    ?>
    <tr class="data" style="letter-spacing: .2em;">
        <td><?= $g['keterangan'] ?></td>
        <td colspan="2" style="text-align: <?= $g['dk'] == 'D' ? 'left' : 'right' ?>">
            <?php
            if($g['dk']!='0'){
            ?>
            <select name="coa[]" class="select2" required>
                <option value=''><?= str_pad(' PILIH COA ', 20, '-', STR_PAD_BOTH) ?></option>
                <?php
                if ($g['detail'] == 'f'){
                    foreach ($coa as $r) {
                        echo "<option value='$g[id];$g[no];$r[id]' " . ($r['id'] == $g['coa'] ? 'selected=""' : "") . ">" . str_pad(format_coa($r['kode']), 12, '-', STR_PAD_RIGHT) . " $r[nama]</option>";
                    }
                }else {
                        $last = array();
                        $last_id = array();
                        $group = 0;
                        foreach ($coa as $r) {
                            if ($r['level'] == 1) {
                                $last_id[1] = $r['k1'];
                                $last[1] = $r['nama'];
                                continue;
                            } elseif ($r['level'] == 2 && "$r[k1]" == "$last_id[1]") {
                                $last_id[2] = $r['k2'];
                                $last[2] = $r['nama'];
                                continue;
                            } elseif ($r['level'] == 3 && "$r[k1]$r[k2]" == "$last_id[1]$last_id[2]") {
                                $last_id[3] = $r['k3'];
                                $last[3] = $r['nama'];
                                if ($group > 0) {
                                    echo "</optgroup>";
                                }
                                $group++;
                                echo "<optgroup label=\"" . implode(" - ", $last) . "\">";
                                continue;
                            } elseif ($r['level'] == 4 && "$r[k1]$r[k2]$r[k3]" == implode("", $last_id)) {
                                echo "<option value='$g[id];$g[no];$r[id]' ".($r['id']==$g['coa']?'selected=""':"").">" . format_coa($r['kode']) . " $r[nama]</option>";
                            }
                        }
                        echo "</optgroup>";
                    }
                    ?>
                </select>
                <?php
            }
            ?>
            </td>
        </tr>
        <?php
    }
    ?>
    </tbody>
</table>