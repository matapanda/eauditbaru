<div class="row">
    <div class="col-sm-12">
        <div class="card-box row">
            <?php
            include VIEWPATH.'alert.php';
            ?>
            <div class="col-md-6 col-md-offset-3">
                <form role="form" method="post">
                    <div class="form-group row">
                        <label for="kode" class="col-sm-4 form-control-label">NIK<span class="text-danger">*</span></label>
                        <div class="col-sm-7">
                            <input type="text" name="nik" required parsley-type="text" class="form-control" id="nik" value="<?=$kar['nik']?>">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="nama" class="col-sm-4 form-control-label">Nama<span class="text-danger">*</span></label>
                        <div class="col-sm-7">
                            <input type="text" name="nama" required parsley-type="text" class="form-control" id="nama" value="<?=$kar['nama']?>">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="alamat" class="col-sm-4 form-control-label">Alamat</label>
                        <div class="col-sm-7">
                            <textarea name="alamat" parsley-type="text" class="form-control" id="alamat"><?=$kar['alamat']?></textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="npwp" class="col-sm-4 form-control-label">NPWP<span class="text-danger">*</span></label>
                        <div class="col-sm-7">
                            <input type="text" name="npwp" required parsley-type="text" class="form-control" id="npwp" value="<?=$kar['npwp']?>">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="email" class="col-sm-4 form-control-label">Kontak</label>
                        <div class="col-sm-7">
                            <input type="kontak" name="kontak" parsley-type="text" class="form-control" id="kontak" value="<?=$kar['telp']?>">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="cpnama" class="col-sm-4 form-control-label">Tempat Lahir <span class="text-danger">*</span></label>
                        <div class="col-sm-7">
                            <input type="text" name="tempatlahir" required parsley-type="text" class="form-control" id="tempatlahir" value="<?=$kar['tempatlahir']?>">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="cpnama" class="col-sm-4 form-control-label">Tanggal Masuk <span class="text-danger">*</span></label>
                        <div class="col-sm-7">
                            <input type="text" style="background-color: lightgrey" name="tglmasuk" required parsley-type="text" class="form-control" id="tglmasuk" readonly value="<?=$kar['tglmasuk']?>">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-8 col-sm-offset-4">
                            <button type="submit" class="btn btn-primary waves-effect waves-light">Save</button>
                            <a href="?" class="btn btn-default waves-effect m-l-5">Cancel</a>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-md-12">
                <hr>
            </div>
        </div>
    </div>
</div>
<link href="<?=base_url()?>assets/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="<?=base_url()?>assets/plugins/select2/js/select2.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/plugins/parsleyjs/parsley.min.js"></script>
<script type="text/javascript">
    $(function () {
        $('.select2').select2();
        $('form').parsley();
    });
</script>