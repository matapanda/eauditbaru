<div class="row">
    <div class="col-sm-12">
        <div class="card-box row">
            <?php
            show_alert();
            ?>
            <div class="col-md-10 col-md-offset-3">
                <form role="form" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="id" value="<?= @$id ?>">
                    <div class="form-group row">
                        <label for="inputName" class="col-sm-2 form-control-label">Kode<span class="text-danger">*</span></label>
                        <div class="col-sm-4">
                            <div class="row">
                                <div class="col-sm-4">
                                    <p style="padding: .75em">KK<?=$header['kode']?></p>
                                </div>
                                <div class="col-sm-8">
                                    <input type="text" name="kertas" required class="form-control" id="kertas"
                                           placeholder="Kode Kertas Kerja" value="<?= @$user['kertas_kerja'] ?>">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputName" class="col-sm-2 form-control-label">Kertas Kerja<span class="text-danger">*</span></label>
                        <div class="col-sm-10">
                            <input type="file" name="file_download" id="file_download" value="<?= @$user['file_download'] ?>" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="kode" class="col-sm-2 form-control-label">Langkah Kerja<span
                                class="text-danger">*</span></label>
                        <div class="col-sm-4">
                            <input type="text" name="langkah" required class="form-control" id="langkah"
                                   placeholder="Langkah Kerja" value="<?= @$user['langkah'] ?>">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-8 col-sm-offset-2">
                            <br>
                            <button type="submit" class="btn btn-inverse waves-effect waves-light">Simpan</button>
                            <a href="javascript:window.history.back();" class="btn btn-default waves-effect m-l-5">Batal</a>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-md-12">
                <hr>
            </div>
        </div>
    </div>
</div>
<link href="<?= base_url() ?>assets/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="<?= base_url() ?>assets/plugins/select2/js/select2.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/plugins/parsleyjs/parsley.min.js"></script>
<script src="<?= base_url() ?>assets/plugins/tinymce/jquery.tinymce.min.js"></script>
<script src="<?= base_url() ?>assets/plugins/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
    tinymce.init({
        selector: 'textarea',
        height: 200,
        menubar: false,
        plugins: [
            'advlist autolink lists link image charmap print preview anchor',
            'searchreplace visualblocks code fullscreen',
            'insertdatetime media table contextmenu paste code'
        ],
        toolbar: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
        content_css: [
            '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
            '//www.tinymce.com/css/codepen.min.css']
    });
    $(function () {
        $('.select2').select2({
            maximumSelectionLength: 1,
        });
        $('form').parsley();
        $("form").submit(function (event) {
            $(this).parsley().validate();
            if (!$(this).parsley().isValid()) {
                event.preventDefault();
            }
        });

    });
</script>