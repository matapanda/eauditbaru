<div class="row">
    <div class="col-sm-12">
        <div class="card-box">
            <?php
            show_alert();
            ?>
            <div class="row">
                <div class="col-md-4">
                    <a href="?i=true" class="<?= is_authority(@$access['c']) ?> btn btn-inverse"><i
                            class="fa fa-plus"></i> TAMBAH KERTAS KERJA</a>
                </div>
                <div class="col-md-8 right">
                    <a href="?" class="btn btn-inverse"><i class="fa fa-refresh"></i></a>
                    <a href="<?= base_url('master/tao') ?>" class="btn btn-inverse"><i class="fa fa-backward"></i></a>
                </div>
            </div>
            <div class="table-responsive">
                <br>
                <table class="table table-bordered table-striped table-hover">
                    <thead>
                    <tr>
                        <th class="center col-xs-2">Kode</th>
                        <th class="center">Langkah Kerja</th>
                        <th class="center col-xs-2">Kertas Kerja</th>
                        <th class="center col-xs-1">Status</th>
                        <th class="<?= is_authority(@$access['u']) ?> center col-xs-1"></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    foreach ($detail as $g) {
                        ?>
                        <tr>
                            <td class="center">
                                KK<?=$header['kode']?> - <a href="#" id="<?= isset($access['u']) ? "kertas_kerja$g[id]" : '' ?>"><?= $g['kertas_kerja'] ?></a>
                                <script>
                                    $(function () {
                                        $('#kertas_kerja<?=$g['id']?>').editable({
                                            type: 'number',
                                            name: 'kertas_kerja',
                                            pk: '<?=$g['id']?>',
                                            url: '?',
                                            title: 'Kertas Kerja'
                                        });
                                    });
                                </script>
                            </td>
                            <td class="">
                                <a href="#"
                                   id="<?= isset($access['u']) ? "langkah$g[id]" : '' ?>"><?= $g['langkah'] ?></a>
                                <script>
                                    $(function () {
                                        $('#langkah<?=$g['id']?>').editable({
                                            type: 'text',
                                            name: 'langkah',
                                            pk: '<?=$g['id']?>',
                                            url: '?',
                                            title: 'Langkah - Langkah'
                                        });
                                    });
                                </script>
                            </td>
                            <td class="center">
                                <a href="<?= (@$g['file_download'] ? base_url("img/$g[file_download]") : "#") ?>"
                                   class="btn btn-sm btn-primary" target="_blank"><i class="fa fa-file-text"></i></a>
                            </td>
                            <td class="center <?= $g['id'] ?> hand-cursor" <?= isset($access['u']) ? "onclick=\"setStatusActive('$g[id]')\"" : '' ?>><?= getLabelStatus($g['status']) ?></td>
                            <td class="<?= is_authority(@$access['u']) ?> center">
                                <a href="?e=<?= $g['id'] ?>" class="btn btn-sm btn-inverse"><i class="fa fa-pencil"></i></a>
                            </td>
                        </tr>
                        <?php
                    }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<link href="<?= base_url() ?>assets/plugins/bootstrap3-editable/css/bootstrap-editable.css" rel="stylesheet">
<script src="<?= base_url() ?>assets/plugins/bootstrap3-editable/js/bootstrap-editable.min.js"></script>
<script>
    $.fn.editable.defaults.mode = 'popup';
    function setStatusActive(_i) {
        $('.' + _i).html('<img src="<?=base_url('assets/loading.gif')?>">');
        $.post('?', {status: _i}, function (data, status) {
            $('.' + _i).html(data);
        });
    }
</script>