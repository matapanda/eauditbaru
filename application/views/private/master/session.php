<div class="row">
    <div class="col-sm-12">
        <div class="card-box">
            <a href="?clear=true" class="btn btn-inverse">CLEAR ALL SESSION</a>
            <hr>
            <div class="table-responsive">
                <table class="table table-bordered table-striped table-hover">
                    <thead>
                    <tr>
                        <th class="center col-xs-1">#</th>
                        <th>IP</th>
                        <th class="center col-xs-2">Time</th>
                        <th class="center col-xs-1">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    foreach($session as $no=>$g) {
                        ?>
                        <tr>
                            <td scope="row" class="center"><?=$no+1?></td>
                            <td><?=$g['ip_address']?></td>
                            <td class="center"><?=date('d/m/Y H:i:s',$g['timestamp'])?></td>
                            <td class="center"><a href="?c=<?=$g['id']?>" class="btn btn-xs btn-block btn-inverse">REMOVE</a></td>
                        </tr>
                        <?php
                    }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>