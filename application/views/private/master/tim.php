<style>
    .select2-selection.select2-selection--single {
        width: 300px;
    }
    #select2-ketua-e7-container {
        text-align: left;
    }
    #select2-wilayah-2i-container {
        text-align: left;
    }
    #select2-dalnis-v3-container {
        text-align: left;
    }
    .left-select{
        text-align: left;
    }

</style><div class="row">
    <div class="col-sm-12">
        <div class="card-box">
            <?php
            show_alert();
            ?>
            <a href="#new-group" class="<?=is_authority(@$access['c'])?> btn btn-inverse" data-animation="blur" data-plugin="custommodal" data-overlayspeed="100" data-overlaycolor="#36404a"><i class="fa fa-plus"></i> TAMBAH DATA TIM</a>
            <a href="?" class="btn btn-inverse"><i class="fa fa-refresh"></i></a>
            <hr>
            <form method="get" class="row" action="<?=base_url('master/tim')?>">
                <div class="col-md-6">
                    <div class="dataTables_wrapper form-inline">
                        <select name="closed" onchange="this.form.submit()" class="form-control input-sm">
                            <option value="" <?=$closed==''?'selected':''?>>Semua Status</option>
                            <option value="t" <?=$closed=='t'?'selected':''?>>Status Aktif</option>
                            <option value="f" <?=$closed=='f'?'selected':''?>>Status Non Aktif</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="dataTables_wrapper form-inline right">
                        <label class="hidden-xs">Search: &nbsp;</label><input type="search" name="search" class="form-control input-sm" autocomplete="off" value="<?=@$search?>">
                    </div>
                </div>
            </form>
            <div class="table-responsive">
                <br> <table class="table table-bordered table-striped table-hover">
                    <thead>
                    <tr>
                        <th class="center col-xs-1">#</th>
                        <th class="center col-xs-2">Wilayah</th>
                        <th class="center col-xs-2">Nama Tim</th>
                        <th class="center col-xs-2">Ketua Tim</th>
                        <th class="center col-xs-2">Dalnis</th>
                        <th class="center col-xs-1">Status</th>
                        <th class="center col-xs-2">Action</th>
                        <!--                        <th class="center col-xs-2 --><?//=is_authority(@$access['u'])?><!--">Action</th>-->
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $no=1;
                    foreach($user as $g) {
                        ?>
                        <tr>
                            <td class="center">
                             <?=$no;?>
                            </td><td class="center">
                                <a href='#' id='wilayah<?=$g['id']?>' data-type='select2' data-pk='<?=$g['id']?>' data-value='<?=$g['id']?>'><?=$g['wilayah']?></a>
                                <script>
                                    $(function(){
                                        $('#wilayah<?=$g['id']?>').editable({
                                            url: '?',
                                            mode:'inline',
                                            name: 'wilayah',
                                            title:'Nama Wilayah',
                                            source:<?=json_encode($wilayah)?>
                                        });
                                        $("#wilayah<?=$g['id']?>").editable('setValue', '<?=$g['wk']?>');
                                    });
                                </script>
                            </td><td class="center">
                                <a href="#" id="<?=isset($access['u'])?"tim$g[id]":''?>"><?=$g['tim']?></a>
                                <script>
                                    $(function () {
                                        $('#tim<?=$g['id']?>').editable({
                                            type: 'text',
                                            name: 'tim',
                                            pk:'<?=$g['id']?>',
                                            url: '?',
                                            title: 'Nama Tim'
                                        });
                                    });
                                </script>
                            </td>
                            <td class="center">
                                <a href='#' id='ketua<?=$g['id']?>' data-type='select2' data-pk='<?=$g['id']?>' data-value='<?=$g['id']?>'></a>
                                <script>
                                    $(function(){
                                        $('#ketua<?=$g['id']?>').editable({
                                            url: '?',
                                            mode:'inline',
                                            name: 'ketua',
                                            title:'Nama Ketua',
                                            source:<?=json_encode($ketua)?>
                                        });
                                        $("#ketua<?=$g['id']?>").editable('setValue', '<?=$g['tid1']?>');
                                    });
                                </script>
                            </td>
                            <td class="center">
                                <a href='#' id='dalnis<?=$g['id']?>' data-type='select2' data-pk='<?=$g['id']?>' data-value='<?=$g['id']?>'></a>
                                <script>
                                    $(function(){
                                        $('#dalnis<?=$g['id']?>').editable({
                                            url: '?',
                                            mode:'inline',
                                            name: 'dalnis',
                                            title:'Nama Dalnis',
                                            source:<?=json_encode($dalnis)?>
                                        });
                                        $("#dalnis<?=$g['id']?>").editable('setValue', '<?=$g['tid2']?>');
                                    });
                                </script>
                            </td>
                            <td class="center <?=isset($access['u'])?"$g[id] hand-cursor\" onclick=\"setStatusActive('$g[id]')\"":''?>"><?=getLabelStatus($g['status'])?></td>
                            <td class="center <?=is_authority(@$access['u'])?>"><a href="?e=<?=$g['id']?>" class="btn btn-xs btn-block btn-inverse">Edit Tim</a></td>
                        </tr>
                        <?php
                        $no++;}
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<link href="<?=base_url()?>assets/plugins/custombox/css/custombox.min.css" rel="stylesheet">
<link href="<?=base_url()?>assets/plugins/bootstrap3-editable/css/bootstrap-editable.css" rel="stylesheet">
<script src="<?=base_url()?>assets/plugins/bootstrap3-editable/js/bootstrap-editable.min.js"></script>
<script src="<?=base_url()?>assets/plugins/custombox/js/custombox.min.js"></script>
<script src="<?=base_url()?>assets/plugins/custombox/js/legacy.min.js"></script>

<div id="new-group" class="modal-gradin">
    <button type="button" class="close" onclick="Custombox.close();">
        <span>&times;</span><span class="sr-only">Close</span>
    </button>
    <h4 class="custom-modal-title">Tambah Tim</h4>
    <div class="custom-modal-text">
        <form action="?" method="post">
            <div class="form-group row">
                <div class="col-sm-12">
                    <br><input type="hidden" name="new" value="true">
                    <input type="text" name="tim" required class="form-control" placeholder="Nama Tim">
                    <br></div>

                <div class="col-sm-12 left-select">
                    <br><select class="form-control modal-select2 select2" name="wilayah" ng-model="wilayah" required>
                        <option value="">
                            Pilih Wilayah..
                        </option> <?php
                        foreach ($wilayahnew as $r){
                            echo "<option value='$r[id]'>$r[wilayah]</option>";
                        }
                        ?>
                    </select><br>
                </div>
                <div class="col-sm-12 left-select">
                    <br><select class="form-control modal-select2 select2" name="ketua" ng-model="ketua" required>
                        <option value="">
                            Pilih Ketua Tim..
                        </option><?php
                        foreach ($ketuanew as $r){
                            echo "<option value='$r[id]'>$r[name]</option>";
                        }
                        ?>
                    </select><br>
                </div>
                <div class="col-sm-12 left-select">
                    <br> <select class="form-control modal-select2 select2" name="dalnis" ng-model="dalnis" required>
                        <option value="">
                            Pilih Dalnis..
                        </option><?php
                        foreach ($dalnisnew as $r){
                            echo "<option value='$r[id]'>$r[name]</option>";
                        }
                        ?>
                    </select><br>
                </div>
                <div class="col-sm-12 right" style="padding-top: 1em">
                    <br><button type="button" onclick="Custombox.close();" class="btn btn-default">BATAL</button>
                    <button type="submit" class="btn btn-inverse">SIMPAN</button>
                </div>
            </div>
        </form>
    </div>
</div>


<link href="<?=base_url()?>assets/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="<?=base_url()?>assets/plugins/select2/js/select2.full.min.js"></script>
<script>
    $.fn.editable.defaults.mode = 'popup';
    function setStatusActive(_i) {
        $('.'+_i).html('<img src="<?=base_url('assets/loading.gif')?>">');
        $.post('?',{status:_i},function (data,status) {
            $('.'+_i).html(data);
        });
    }
    $('.select2').select2({});
    $('.modal-select2').select2({
        dropdownParent: $('.modal-gradin')
    });

    $(" #ketua-n, #wilayah-n, #dalnis-n").select2({
        maximumSelectionLength: 1,
        minimumResultsForSearch: Infinity,
        dropdownParent: $('.modal-gradin')
    });
    $(function(){
        <?php
        if(!isset($access['u'])){
        ?>
        $(".emjes-editable").editable('option', 'disabled', true);
        <?php
        }
        ?>
    });
</script>