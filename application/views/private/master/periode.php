<div class="row">
    <div class="col-sm-12">
        <div class="card-box">
            <?php
            show_alert();
            ?>
            <a onclick="tmbh()" class="<?=is_authority(@$access['c'])?> btn btn-inverse" data-animation="blur"data-overlayspeed="100" data-overlaycolor="#36404a"><i class="fa fa-plus"></i> TAMBAH DATA PERIODE</a>
            <a href="?" class="btn btn-inverse"><i class="fa fa-refresh"></i></a>
            <hr>
            <form method="get" class="row" action="<?=base_url('master/periode')?>">
                <div class="col-md-6">
                    <div class="dataTables_wrapper form-inline">
                        <select name="closed" onchange="this.form.submit()" class="form-control input-sm">
                            <option value="" <?=$closed==''?'selected':''?>>Semua Status</option>
                            <option value="t" <?=$closed=='t'?'selected':''?>>Status Aktif</option>
                            <option value="f" <?=$closed=='f'?'selected':''?>>Status Non Aktif</option>
                        </select>
                    </div>
                </div>
            </form>
            <div class="table-responsive">
                <br><table class="table table-bordered table-striped table-hover">
                    <thead>
                    <tr>
                        <th class="center col-xs-1">#</th>
                        <th class="center col-xs-6">Nama Periode</th>
                        <th class="center col-xs-2">Mulai - Akhir Periode</th>
                        <th class="center col-xs-1">Status</th>
                        <th class="center col-xs-1"></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $no=1;
                    foreach($periode as $g) {
                        ?>
                        <tr>
                            <td scope="row" class="center"><?=$no?></td>
                            <td class=""><?=$g['nama']?><input type="hidden" id="nama" value="<?=$g['nama']?>"><input type="hidden" id="start" value="<?=format_waktu($g['start'])?>"><input type="hidden" id="end" value="<?=format_waktu($g['end'])?>"></td>
                            <td class="center"><?=format_waktu($g['start']).' - '.format_waktu($g['end'])?></td>
                            <td class="center <?= $g['id'] ?> hand-cursor" <?= isset($access['u']) ? "onclick=\"setStatusActive('$g[id]')\"" : '' ?>><?= getLabelStatus($g['status']) ?></td>
                            <td class="center hand-cursor"><a onclick="edit_m('<?=$g['id']?>')"><label class="hand-cursor btn btn-sm btn-inverse "><i class="fa fa-edit"></i></label></a><a href="?e=<?=$g['id']?>"><label class="hand-cursor btn btn-sm btn-default "><i class="fa fa-cog"></i></label></a></td>
                        </tr>
                        <?php
                        $no++;}
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="uploadModal" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-large" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Tambah Periode</h4>
            </div>
            <div class="modal-body">
                <form action="?" method="post">
                    <div class="form-group row">
                        <label for="inputEmail" class="col-sm-4 form-control-label">Nama<span class="text-danger">*</span></label>
                        <div class="col-sm-7">
                            <input type="hidden" name="id" id="id" value="tambah">
                            <select name="nama" id="nama_m" class="select2 form-control" onchange="ganti_tgl()" required>
                                <?php
                                for($i=date('Y')+5;$i>(date('Y')-5);$i--){
                                    echo "<option value='$i' ".($i==@$edit['nama']?'selected=""':"").">$i</option>";
                                }
                                ?>
                            </select>
<!--                            <input type="text" name="nama" id="nama_m" required class="form-control" placeholder="Nama" value="--><?//=@$edit['nama']?><!--">-->
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputName" class="col-sm-4 form-control-label">Periode<span class="text-danger">*</span></label>
                        <div class="col-sm-7">
                            <input type="text" name="daterange" readonly class="form-control" value="<?=@$daterange?>"/>
                            <input type="hidden" name="sd" id="sd" value="<?=@$starts?>"/>
                            <input type="hidden" name="ed" id="ed" value="<?=@$ends?>"/>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-offset-4" style="padding-top: 1em">
                            <button type="submit" class="btn btn-inverse">SIMPAN</button>
                            <button type="button" data-dismiss="modal" class="btn btn-default">BATAL</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<link href="<?=base_url()?>assets/plugins/custombox/css/custombox.min.css" rel="stylesheet">
<link href="<?=base_url()?>assets/plugins/bootstrap3-editable/css/bootstrap-editable.css" rel="stylesheet">
<script src="<?=base_url()?>assets/plugins/bootstrap3-editable/js/bootstrap-editable.min.js"></script>
<script src="<?=base_url()?>assets/plugins/custombox/js/custombox.min.js"></script>
<script src="<?=base_url()?>assets/plugins/custombox/js/legacy.min.js"></script>
<script src="<?= base_url() ?>assets/plugins/moment/moment.js"></script>
<script src="<?= base_url() ?>assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
<script src="<?= base_url() ?>assets/plugins/fullcalendar/js/fullcalendar.min.js"></script>
<script>

    function tmbh(){
        $('#uploadModal').modal('show');
    }
    function edit_m(i){
       var s= $('#start').val();
        var e= $('#end').val();
        var n=$('#nama').val();
        $('#nama_m').val(n);
        $('#sd').val(s);
        $('#ed').val(e);
        $('#id').val(i);
        $('input[name="daterange"]').val(s+' - '+e)
        $('#uploadModal').modal('show');
    }function ganti_tgl(){
        var nama_m=$('#nama_m').val();
        $('#start').val('01/01/'+nama_m);
        $('#end').val('31/12/'+nama_m);
       var s= $('#start').val();
        var e= $('#end').val();
        var n=$('#nama').val();
        $('#sd').val(s);
        $('#ed').val(e);
//        $('#id').val(i);
        $('input[name="daterange"]').val(s+' - '+e)
        $('#uploadModal').modal('show');
    }
    $.fn.editable.defaults.mode = 'popup';
    function setStatusActive(_i) {
        $('.'+_i).html('<img src="<?=base_url('assets/loading.gif')?>">');
        $.post('?',{status:_i},function (data,status) {
            $('.'+_i).html(data);
        });
    }
    $(function(){
        $('input[name="daterange"]').daterangepicker({locale: {
            format: 'DD/MM/YYYY'
        },});
        $('input[name="daterange"]').on('apply.daterangepicker', function(ev, picker) {
            var _sd =picker.startDate.format('YYYY-MM-DD');
            var _ed= picker.endDate.format('YYYY-MM-DD');
            $('#sd').val(_sd);
            $('#ed').val(_ed);
            $('input[name="daterange"]').val(picker.startDate.format('DD/MM/YYYY')+' - '+picker.endDate.format('DD/MM/YYYY'));
            $.post('?', {start: _sd,end: _ed}, function (data, status) {
                $('.liburan').html(data);
                $('.select2').select2();
            });
        });
        <?php
        if(!isset($access['u'])){
        ?>
        $(".emjes-editable").editable('option', 'disabled', true);
        <?php
        }
        ?>
    });
</script>