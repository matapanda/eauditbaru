<div class="row">
    <div class="col-sm-12">
        <div class="card-box row">
            <?php
            show_alert();
            ?>
            <div class="col-md-6 col-md-offset-3">
                <form role="form" method="post" action="?">
                    <div class="form-group row">
                        <label for="inputName" class="col-sm-4 form-control-label">Name<span class="text-danger">*</span></label>
                        <div class="col-sm-7">
                            <input type="text" name="nama" required parsley-type="text" class="form-control" id="inputName" placeholder="Name" value="<?=@$user['name']?>">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-4 form-control-label">NIP</label>
                        <div class="col-sm-7">
                            <input type="text" name="alamat" placeholder="NIP" value="<?=@$user['nip']?>" class="form-control">
                        </div>
                    </div><div class="form-group row">
                        <label class="col-sm-4 form-control-label">Alamat</label>
                        <div class="col-sm-7">
                            <input type="text" name="alamat" placeholder="Alamat" value="<?=@$user['alamat']?>" class="form-control">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="hori-pass1" class="col-sm-4 form-control-label">No. Telp</label>
                        <div class="col-sm-7">
                            <input id="hori-pass1" name="telp" type="text" value="<?=@$user['telp']?>" placeholder="No Telpon" class="form-control">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-8 col-sm-offset-4">
                            <button type="submit" class="btn btn-primary waves-effect waves-light"> Save</button>
                            <a href="?" class="btn btn-default waves-effect m-l-5">Cancel</a>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-md-12">
                <hr>
            </div>
        </div>
    </div>
</div>
<link href="<?=base_url()?>assets/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="<?=base_url()?>assets/plugins/select2/js/select2.min.js"></script>