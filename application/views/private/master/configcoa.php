<link href="<?=base_url()?>assets/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css"/>
<div class="row">
    <form method="post" class="row">
        <div class="col-sm-12">
            <?php
            include VIEWPATH.'alert.php';
            ?>
                <div class="card-box">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped table-hover">
                            <thead>
                                <tr>
                                    <th class="col-xs-6">Transaksi</th>
                                    <th class="center col-xs-6" colspan="2">COA</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php
                            foreach($data as $g) {
                                ?>
                                <tr class="data" style="letter-spacing: .2em;">
                                    <td class="coa-<?=$g['id']?>"><?=strtoupper($g['transaksi'])?></td>
                                    <td>
                                        <?php
                                        if($g['detail']=='f'){
                                        ?>
                                        <select name="coa[]" class="select2 form-control" required>
                                            <?php
                                            foreach ($coa as $r){
                                                echo "<option value='$g[id];$r[id]' ".($r['id']==$g['coa']?'selected=""':"").">" . str_pad(format_coa($r['kode']), 20, '-',STR_PAD_RIGHT) . " $r[nama]</option>";
                                            }
                                            ?>
                                        </select>
                                            <?php
                                        }
                                        ?>
                                    </td>
                                    <td>
                                        <?php
                                        if($g['detail']=='t'){
                                            ?>
                                            <button type="button" data-coa="<?=$g['id']?>" class="btn btn-sm btn-primary btn-block"><i class="fa fa-search"></i></button>
                                            <?php
                                        }
                                        ?>
                                    </td>
                                </tr>
                                <?php
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                    <button type="submit" class="btn btn-inverse">SIMPAN</button>
                    <a href="?" class="btn btn-default">KEMBALI</a>
                </div>
        </div>
    </form>
</div>
<div id="formedit" class="modal fade" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-title">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3></h3>
            </div>
            <div class="modal-body">
                <form method="post">
                    <div class="form-data"></div>
                    <div style="text-align: right">
                        <button type="submit" class="btn btn-primary waves-effect waves-light">SIMPAN</button>
                        <button type="button" data-dismiss="modal" class="btn btn-default waves-effect waves-light">BATAL</button>
                    </div>
                </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<style>
    .select2-container,.select2{
        font-family: monospace!important;
    }
</style>
<script type="text/javascript" src="<?=base_url()?>assets/plugins/select2/js/select2.min.js"></script>
<script>
    $(function () {
        $('.select2').select2();
        $('[data-coa]').on('click',function () {
            var _id=$(this).data('coa');
            var _transaksi=$('td.coa-'+_id).html();
            $('.modal-title h3','#formedit').html(_transaksi);
            $.post('?config=true',{detail:_id},function (data,status) {
                $('form div.form-data','#formedit').html(data);
                $('#formedit').modal('show');
                $('.select2','#formedit').select2();
            });
        });
    });
</script>