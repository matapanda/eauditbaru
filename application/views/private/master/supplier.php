<div class="row">
    <div class="col-sm-12">
        <div class="card-box">
            <a href="?i=true" class="<?=is_authority(@$access['c'])?> btn btn-inverse"><i class="fa fa-plus"></i> ADD NEW DATA</a>
            <a href="?" class="btn btn-primary"><i class="fa fa-refresh"></i> RESET</a>
            <hr>
            <form method="get" class="row" action="<?=base_url('master/supplier')?>">
                <div class="col-md-6">
                    <div class="dataTables_wrapper form-inline">
                        <label>Filter: </label>
                        <select name="status" onchange="this.form.submit()" class="form-control input-sm">
                            <option value="">Semua Supplier</option>
                            <option value="t" <?=$status=='t'?'selected':''?>>Supplier aktif</option>
                            <option value="f" <?=$status=='f'?'selected':''?>>Supplier non-aktif</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="dataTables_wrapper form-inline right">
                        <label class="hidden-xs">Search: &nbsp;</label><input type="search" name="search" class="form-control input-sm" autocomplete="off" value="<?=$search?>">
                    </div>
                </div>
            </form>
            <div class="table-responsive">
                <table class="table table-bordered table-striped table-hover">
                    <thead>
                    <tr>
                        <th class="center col-xs-1">#</th>
                        <th class="center col-xs-2">ID / NPWP</th>
                        <th class="col-xs-3">Supplier</th>
                        <th class="center col-xs-2">Kontak</th>
                        <th class="center col-xs-2 ini-haram">Utang/Piutang</th>
                        <th class="center col-xs-1">Status</th>
                        <th class="<?=is_authority(@$access['u'])?> center col-xs-1">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $no=$page;
                    foreach($data as $g) {
                        ?>
                        <tr class="data<?=$g['id']?> hand-cursor">
                            <td scope="row" class="center"><?=++$no?></td>
                            <td class="center" onmouseenter="$(this).html('<?=htmlveryspecialchars($g['npwp']);?>')" onmouseleave="$(this).html('<?=htmlveryspecialchars($g['kode']);?>')"><?=$g['kode']?></td>
                            <td class="hand-cursor" onmouseenter="$(this).html('<?=htmlveryspecialchars($g['alamat'])?>')" onmouseleave="$(this).html('<?=htmlveryspecialchars($g['nama'])?>')"><?=$g['nama']?></td>
                            <td class="center" onmouseenter="$(this).html('<?=htmlveryspecialchars($g['telp']);?>')" onmouseleave="$(this).html('<?=htmlveryspecialchars($g['email']);?>')"><?=$g['email']?></td>
                            <td class="right ini-haram">
                                <a href="?saldo=<?=$g['id']?>" target="_blank"><?=format_uang($g['hutangpiutang'])?></a>
                            </td>
                            <td class="center <?=$g['id']?>" onclick="<?=isset($access['d'])?"setStatusActive('$g[id]')":''?>"><?=getLabelStatus($g['status'])?></td>
                            <td class="<?=is_authority(@$access['u'])?> center">
                                <a href="?e=<?=$g['id']?>" class="btn btn-xs btn-block btn-orange"><i class="fa fa-pencil"></i></a>
                            </td>
                        </tr>
                        <?php
                    }
                    ?>
                    </tbody>
                </table>
            </div>
            <?=$pagination?>
        </div>
    </div>
</div>
<script>
    function setStatusActive(_i) {
        $('.'+_i).html('<img src="<?=base_url('assets/loading.gif')?>">');
        $.post('?e=true',{status:_i},function (data,status) {
            $('.'+_i).html(data);
        });
    }
</script>