<div class="row">
    <div class="col-sm-12">
        <div class="card-box">
            <?php
            show_alert();
            ?>
            <a href="#new-group" class="<?=is_authority(@$access['c'])?> btn btn-inverse" data-animation="blur" data-plugin="custommodal" data-overlayspeed="100" data-overlaycolor="#36404a"><i class="fa fa-plus"></i> ADD NEW PERAN</a>
            <a href="?" class="btn btn-primary"><i class="fa fa-refresh"></i> RELOAD</a>
            <hr>
            <div class="table-responsive">
                <table class="table table-bordered table-striped table-hover">
                    <thead>
                    <tr>
                        <th class="center col-xs-2">#</th>
                        <th class="center col-xs-3">Keterangan</th>
                        <th class="center col-xs-3">Order</th>
                        <th class="center col-xs-2">Status</th>
                        <!--                        <th class="center col-xs-2 --><?//=is_authority(@$access['u'])?><!--">Action</th>-->
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $no=1;
                    foreach($peran as $g) {
                        ?>
                        <tr>
                            <td scope="row" class="center"><?=$no?></td>
                            <td>
                                <a href="#" id="<?=isset($access['u'])?"ket$g[id]":''?>"><?=$g['ket']?></a>
                                <script>
                                    $(function () {
                                        $('#ket<?=$g['id']?>').editable({
                                            type: 'text',
                                            name: 'ket',
                                            pk:'<?=$g['id']?>',
                                            url: '?',
                                            title: 'Keterangan Peran'
                                        });
                                    });
                                </script>
                            </td><td class="center">
                                <a href="#" id="<?=isset($access['u'])?"ord$g[id]":''?>"><?=$g['order']?></a>
                                <script>
                                    $(function () {
                                        $('#ord<?=$g['id']?>').editable({
                                            type: 'text',
                                            name: 'order',
                                            pk:'<?=$g['id']?>',
                                            url: '?',
                                            title: 'Order Peran'
                                        });
                                    });
                                </script>
                            </td>
                            <td class="center <?=isset($access['d'])?"$g[id] hand-cursor\" onclick=\"setStatusActive('$g[id]')\"":''?>><?=getLabelStatus($g['status'])?></td>
<!--                        <td class="--><?//=is_authority(@$access['u'])?><!-- center"><a href="?u=--><?//=$g['id']?><!--" class="btn btn-xs btn-block btn-inverse">UPDATE DATA</a></td>-->
                        </tr>
                        <?php
                        $no++;}
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<link href="<?=base_url()?>assets/plugins/custombox/css/custombox.min.css" rel="stylesheet">
<link href="<?=base_url()?>assets/plugins/bootstrap3-editable/css/bootstrap-editable.css" rel="stylesheet">
<script src="<?=base_url()?>assets/plugins/bootstrap3-editable/js/bootstrap-editable.min.js"></script>
<script src="<?=base_url()?>assets/plugins/custombox/js/custombox.min.js"></script>
<script src="<?=base_url()?>assets/plugins/custombox/js/legacy.min.js"></script>
<div id="new-group" class="modal-gradin">
    <button type="button" class="close" onclick="Custombox.close();">
        <span>&times;</span><span class="sr-only">Close</span>
    </button>
    <h4 class="custom-modal-title">Tambah Peran</h4>
    <div class="custom-modal-text">
        <form action="?" method="post">
            <div class="form-group row">
                <div class="col-sm-12">
                    <input type="text" name="ket" required class="form-control" placeholder="Nama Peran">
                </div>
                <div class="col-sm-12 right" style="padding-top: 1em">
                    <button type="button" onclick="Custombox.close();" class="btn btn-default">BATAL</button>
                    <button type="submit" class="btn btn-primary">SIMPAN</button>
                </div>
            </div>
        </form>
    </div>
</div>
<script>
    $.fn.editable.defaults.mode = 'popup';
    function setStatusActive(_i) {
        $('.'+_i).html('<img src="<?=base_url('assets/loading.gif')?>">');
        $.post('?',{status:_i},function (data,status) {
            $('.'+_i).html(data);
        });
    }
</script>