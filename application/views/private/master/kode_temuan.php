<div class="row">
    <div class="col-sm-12">
        <div class="card-box">
            <a href="?i=true" class="<?=is_authority(@$access['c'])?> btn btn-primary fa fa-plus"></a>
<!--            <a href="?config=true" class="--><?//=is_authority(@$access['u'])?><!-- btn btn-primary fa fa-cog"></a>-->
            <a href="?" class="btn btn-default fa fa-refresh"></a>
            <hr class="nomargin">
            <form method="get" class="row" action="<?=base_url('master/kode_temuan')?>">
                <div class="col-md-6">
                    <div class="dataTables_wrapper form-inline">
                        <label>Filter: </label>
                        <select name="status" onchange="this.form.submit()" class="form-control input-sm">
                            <option value="">no-filter</option>
                            <option value="t" <?=$status=='t'?'selected':''?>>aktif</option>
                            <option value="f" <?=$status=='f'?'selected':''?>>non-aktif</option>
                        </select>
                    </div>
                </div>
            </form>
            <div class="table-responsive">
                <table class="table table-bordered table-striped table-hover">
                    <thead>
                    <tr>
                        <th class="center col-xs-2">Kode</th>
                        <th class="col-xs-5">Keterangan</th>
                        <th class="<?=is_authority(@$access['u'])?> center col-xs-1">Status</th>
                        <th class="<?=is_authority(@$access['u'])?> center col-xs-2">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $no=$page;
                    foreach($data as $g) {
                        ?>
                        <tr class="data<?=$g['id']?>" style="<?=$g['level']==4?'':'font-weight:bold;'?>letter-spacing: .2em;">
                            <td><?=format_coa($g['kode'])?></td>
                            <td><?php
                                for($i=1;$i<$g['level'];$i++){
                                    echo " - ";
                                }
                                echo $g['nama'];
                                ?></td>
                            <td class="<?=is_authority(@$access['u'])?> center <?=$g['id']?> hand-cursor" onclick="setStatusActive('<?=$g['id']?>')"><?=getLabelStatus($g['status'])?></td>
                            <td class="<?=is_authority(@$access['u'])?> center">
                                <a href="?e=<?=$g['id']?>" class="btn btn-xs btn-primary fa fa-pencil nomargin"></a>
                                <?php
                                if(isset($access['d'])){
                                    ?>
                                    <button onclick="hapus('<?=$g['id']?>')" class="btn nomargin btn-xs btn-danger fa fa-remove"></button>
                                    <?php
                                }
                                ?>
                            </td>
                        </tr>
                        <?php
                    }
                    ?>
                    </tbody>
                </table>
            </div>
            <?=$pagination?>
        </div>
    </div>
</div>
<script>
    function hapus(_i) {
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this data!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD5555",
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel plx!",
            closeOnConfirm: true,
            closeOnCancel: true
        }, function (isConfirm) {
            if (isConfirm) {
                $.post('?',{delete:_i},function (data) {
                    if(data.status==200){
                        $('.data'+_i).remove();
                        toastr["success"](data.message);
                    }else{
                        toastr.options = {
                            "closeButton": false,
                            "debug": false,
                            "newestOnTop": false,
                            "progressBar": false,
                            "positionClass": "toast-bottom-right",
                            "preventDuplicates": false,
                            "onclick": null,
                            "showDuration": "300",
                            "hideDuration": "1000",
                            "timeOut": "5000",
                            "extendedTimeOut": "1000",
                            "showEasing": "swing",
                            "hideEasing": "linear",
                            "showMethod": "fadeIn",
                            "hideMethod": "fadeOut"
                        }
                        toastr["error"](data.message);
                    }
                },'json');
            }
        });
    }
    function setStatusActive(_i) {
        $('.'+_i).html('<img src="<?=base_url('assets/loading.gif')?>">');
        $.post('?e=true',{status:_i},function (data,status) {
            $('.'+_i).html(data);
        });
    }
</script>