<div class="row">
    <div class="col-sm-12">
        <div class="card-box">
            <a href="?i=true" class="<?=is_authority(@$access['c'])?> btn btn-inverse"><i class="fa fa-plus"></i> ADD NEW DATA</a>
            <a href="?" class="btn btn-primary"><i class="fa fa-refresh"></i> RESET</a>
            <hr>
            <form method="get" class="row" action="<?=base_url('master/karyawan')?>">
                <div class="col-md-6">
                    <div class="dataTables_wrapper form-inline">
                        <label>Filter: </label>
                        <select name="status" onchange="this.form.submit()" class="form-control input-sm">
                            <option value="">Semua Karyawan</option>
                            <option value="t" <?=$status=='t'?'selected':''?>>Karyawan aktif</option>
                            <option value="f" <?=$status=='f'?'selected':''?>>Karyawan non-aktif</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="dataTables_wrapper form-inline right">
                        <label class="hidden-xs">Search: &nbsp;</label><input type="search" name="search" class="form-control input-sm" autocomplete="off" value="<?=$search?>">
                    </div>
                </div>
            </form>
            <div class="table-responsive">
                <table class="table table-bordered table-striped table-hover">
                    <thead>
                    <tr>
                        <th class="center col-xs-1">#</th>
                        <th class="center col-xs-2">NIK</th>
                        <th class="col-xs-3">Nama</th>
                        <th class="">Alamat</th>
                        <th class="center col-xs-2">TTL</th>
                        <th class="center col-xs-1">Status</th>
                        <th class="<?=is_authority(@$access['u'])?> center col-xs-1">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $no=$page;
                    foreach($data as $g) {
                        ?>
                        <tr class="data<?=$g['id']?>">
                            <td scope="row" class="center"><?=++$no?></td>
                            <td class="center"><?=$g['nik']?></td>
                            <td><?=$g['nama']?></td>
                            <td><?=$g['alamat']?></td>
                            <td class="center"><?=$g['tempatlahir'].", ".format_waktu($g['tgllahir'])?></td>
                            <td class="center <?=$g['id']?> hand-cursor" onclick="<?=isset($access['d'])?"setStatusActive('$g[id]')":''?>"><?=getLabelStatus($g['status'])?></td>
                            <td class="<?=is_authority(@$access['u'])?> center">
                                <a href="?e=<?=$g['id']?>" class="btn btn-xs btn-block btn-inverse"><i class="fa fa-search"></i></a>
                            </td>
                        </tr>
                        <?php
                    }
                    ?>
                    </tbody>
                </table>
            </div>
            <?=$pagination?>
        </div>
    </div>
</div>
<script>
    function setStatusActive(_i) {
        $('.'+_i).html('<img src="<?=base_url('assets/loading.gif')?>">');
        $.post('?e=true',{status:_i},function (data,status) {
            $('.'+_i).html(data);
        });
    }
</script>