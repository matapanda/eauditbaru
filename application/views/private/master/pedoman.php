<style>
    .select2-selection.select2-selection--single {
        width: 300px;
    }

</style><div class="row">
    <div class="col-sm-12">
        <div class="card-box">
            <a href="?i=true" class="<?=is_authority(@$access['c'])?> btn btn-inverse"><i class="fa fa-plus"></i> TAMBAH DATA PEDOMAN</a>
            <a href="?" class="btn btn-inverse"><i class="fa fa-refresh"></i></a>
            <hr>
            <form method="get" class="row" action="<?=base_url('master/pedoman')?>">
                <div class="col-md-6">
                    <div class="dataTables_wrapper form-inline">
                        <select name="closed" onchange="this.form.submit()" class="form-control input-sm">
                            <option value="" <?=$closed==''?'selected':''?>>Semua Status</option>
                            <option value="t" <?=$closed=='t'?'selected':''?>>Status Aktif</option>
                            <option value="f" <?=$closed=='f'?'selected':''?>>Status Non Aktif</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="dataTables_wrapper form-inline right">
                        <label class="hidden-xs">Search: &nbsp;</label><input type="search" name="search" class="form-control input-sm" autocomplete="off" value="<?=@$search?>">
                    </div>
                </div>
            </form>
            <div class="table-responsive">
                <br><table class="table table-bordered table-striped table-hover">
                    <thead>
                    <tr>
                        <th class="center">#</th>
                        <th class="center col-xs-3">Kode</th>
                        <th class="center col-xs-3">Jenis Audit</th>
                        <th class="center col-xs-3">Sasaran Audit</th>
                        <th class="center col-xs-2">Status</th>
                        <th class="center <?=is_authority(@$access['u'])?> col-xs-1">Action</th>
                    </tr>
                    </thead>
                    <tbody>

                    <?php
                    $no=1;
                    foreach($pedoman as $g) {
                        ?>
                        <tr>
                            <td class="center"><?=$no;?></td>
                            <td class="center">
                                <a href="#" class="emjes-editable" id="<?=isset($access['u'])?"kode$g[id]":''?>"><?=$g['kode']?></a>
                                <script>
                                    $(function () {
                                        $('#kode<?=$g['id']?>').editable({
                                            type: 'text',
                                            name: 'kode',
                                            pk:'<?=$g['id']?>',
                                            url: '?',
                                            title: 'Kode Pedoman'
                                        });
                                    });
                                </script>
                            </td>
                            <td class="center">
                                <a href='#' class="emjes-editable" id='jenis<?=$g['id']?>' data-type='select2' data-pk='<?=$g['id']?>' data-value='<?=$g['id']?>'></a>
                                <script>
                                    $(function(){
                                        $('#jenis<?=$g['id']?>').editable({
                                            url: '?',
                                            mode:'inline',
                                            name: 'jenis',
                                            title:'Jenis Audit',
                                            source:<?=json_encode($jenis)?>
                                        });
                                        $("#jenis<?=$g['id']?>").editable('setValue', '<?=$g['idj']?>');
                                    });
                                </script>
                            </td>
                            <td class="center">
                                <a href='#' class="emjes-editable" id='sasaran<?=$g['id']?>' data-type='select2' data-pk='<?=$g['id']?>' data-value='<?=$g['id']?>'></a>
                                <script>
                                    $(function(){
                                        $('#sasaran<?=$g['id']?>').editable({
                                            url: '?',
                                            mode:'inline',
                                            name: 'sasaran',
                                            title:'Sasaran Audit',
                                            source:<?=json_encode($sasaran)?>
                                        });
                                        $("#sasaran<?=$g['id']?>").editable('setValue', '<?=$g['ids']?>');
                                    });
                                </script>
                            </td>
                            <td class="center <?=$g['id']?> hand-cursor" <?=isset($access['u'])?"onclick=\"setStatusActive('$g[id]')\"":''?>><?=getLabelStatus($g['status'])?></td>
                            <td class="<?=is_authority(@$access['u'])?> center">
                                <a href="<?=(@$g['file']?base_url("img/$g[file]"):"#")?>" class="btn btn-sm btn-primary" target="_blank"><i class="fa fa-file-text"></i></a>
                                <a href="?e=<?=$g['id']?>" class="btn btn-sm btn-inverse"><i class="fa fa-pencil"></i></a>
                            </td>
                        </tr>
                        <?php
                        $no++; }
                    ?>
                    </tbody>

                </table>
            </div>
        </div>
    </div>
</div>
<style>
    .custom-file-upload > input {
        position: absolute;
        top: 0;
        right: 0;
        margin: 0;
        opacity: 0;
        filter: alpha(opacity=0);
        font-size: 23px;
        height: 100%;
        width: 100%;
        direction: ltr;
        cursor: pointer;
    }
    input[type="file"] {
        display: block;
    }
</style>
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/3.1.3/css/jasny-bootstrap.min.css">
<script src="//cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/3.1.3/js/jasny-bootstrap.min.js"></script>
<link href="<?=base_url()?>assets/plugins/custombox/css/custombox.min.css" rel="stylesheet">
<link href="<?=base_url()?>assets/plugins/bootstrap3-editable/css/bootstrap-editable.css" rel="stylesheet">
<script src="<?=base_url()?>assets/plugins/bootstrap3-editable/js/bootstrap-editable.min.js"></script>
<script src="<?=base_url()?>assets/plugins/custombox/js/custombox.min.js"></script>
<script src="<?=base_url()?>assets/plugins/custombox/js/legacy.min.js"></script>
<link href="<?=base_url()?>assets/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="<?=base_url()?>assets/plugins/select2/js/select2.full.min.js"></script>
<script>
    $.fn.editable.defaults.mode = 'popup';
    $('.select2').select2({});
    function setStatusActive(_i) {
        $('.'+_i).html('<img src="<?=base_url('assets/loading.gif')?>">');
        $.post('?',{status:_i},function (data,status) {
            $('.'+_i).html(data);
        });
    }
    $(function(){
        <?php
        if(!isset($access['u'])){
        ?>
        $(".emjes-editable").editable('option', 'disabled', true);
        <?php
        }
        ?>
    });
</script>