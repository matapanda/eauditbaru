<div class="row">
    <div class="col-sm-12">
        <div class="card-box row">
            <?php
            show_alert();
            ?>
            <div class="col-md-6 col-md-offset-3">
                <form role="form" method="post" enctype="multipart/form-data">
                    <div class="form-group row">
                        <label for="tim" class="col-sm-4 form-control-label">Kode Aturan<span class="text-danger">*</span></label>
                        <div class="col-sm-7">
                            <input type="text" name="kode" required class="form-control" id="kode" placeholder="Kode Aturan" value="<?=@$user['kode']?>">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="inputName" class="col-sm-4 form-control-label">Sasaran Audit<span class="text-danger">*</span></label>
                        <div class="col-sm-7">
                            <select name="sasaran" class="select2 form-control" ng-model="sasaran" required>
                                <option value="">
                                    Pilih Sasaran Audit
                                </option> <?php
                                foreach ($sasaran as $r){
                                    echo "<option value='$r[id]' ".($r['id']==@$user['ids']?'selected':'').">$r[text]</option>";
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputName" class="col-sm-4 form-control-label">Jenis Audit<span class="text-danger">*</span></label>
                        <div class="col-sm-7">
                            <select class="form-control select2" name="jenis" ng-model="jenis" required>
                                <option value="">
                                    Pilih Jenis Audit
                                </option>   <?php
                                foreach ($jenis as $r){
                                    echo "<option value='$r[id]' ".($r['id']==@$user['idj']?'selected':'').">$r[text]</option>";
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputName" class="col-sm-4 form-control-label">File<span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                           <?php
                           if($jumlah>0){
                           $no=1;
                           for($i=0;$jumlah>$i;$i++){
                               echo $no.". <a href='".base_url('img/'.@$user['file'])."'>".@$user['file']."</a><br> <br>";
                           $no++;
                           }
                               ?>
                               Edit file <span class="text-danger">*</span><br> <br> <input type="hidden" name="id_file" value="<?=$user['id']?>">
                               <input type="file" name="file" id="file-upload" value="<?=@$user['file']?>" >
                               <?php
                           }else{
                           ?>
                              <input type="hidden" name="id_file" value="<?=$user['id']?>">
                                <input type="file" name="file" id="file-upload" value="<?=@$user['file']?>" required><?php }?>
                         </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-sm-8 col-sm-offset-4">
                           <br> <button type="submit" class="btn btn-inverse waves-effect waves-light">Simpan</button>
                            <a href="?" class="btn btn-default waves-effect m-l-5">Batal</a>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-md-12">
                <hr>
            </div>
        </div>
    </div>
</div>
<link href="<?=base_url()?>assets/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="<?=base_url()?>assets/plugins/select2/js/select2.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/plugins/parsleyjs/parsley.min.js"></script>
<script type="text/javascript">
    $(function () {
        $('.select2').select2({});
        $('form').parsley();
        $("form").submit(function(event) {
            $(this).parsley().validate();
            if (!$(this).parsley().isValid()){
                event.preventDefault();
            }
        });

    });
</script>