<div class="row">
    <div class="col-sm-12">
        <div class="card-box row">
            <?php
            show_alert();
            ?>
            <div class="col-md-6 col-md-offset-3">
                <form role="form" method="post">
                    <div class="form-group row">
                        <label for="tim" class="col-sm-4 form-control-label">Nama Tim<span class="text-danger">*</span></label>
                        <div class="col-sm-7">
                            <input type="text" name="tim" required class="form-control" id="tim" placeholder="tim" value="<?=$user['tim']?>">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="inputName" class="col-sm-4 form-control-label">Wilayah<span class="text-danger">*</span></label>
                        <div class="col-sm-7">
                            <select class="form-control select2" name="wilayah" ng-model="wilayah" required>
                                <option value="">
                                    Pilih Wilayah..
                                </option>      <?php
                                foreach ($wilayah as $r){
                                    echo "<option value='$r[id]' ".($r['id']==$user['wilayah']?'selected':'').">$r[text]</option>";
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputName" class="col-sm-4 form-control-label">Dalnis<span class="text-danger">*</span></label>
                        <div class="col-sm-7">
                            <select class="form-control select2" name="dalnis" ng-model="dalnis" required>
                                <option value="">
                                    Pilih Dalnis
                                </option>      <?php
                                foreach ($dalnis as $r){
                                    echo "<option value='$r[id]' ".($r['id']==$user['dalnis']?'selected':'').">$r[text]</option>";
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputName" class="col-sm-4 form-control-label">Ketua<span class="text-danger">*</span></label>
                        <div class="col-sm-7">
                            <select class="form-control select2" name="ketua" ng-model="ketua" required>
                                <option value="">
                                    Pilih Ketua Tim
                                </option>      <?php
                                foreach ($ketua as $r){
                                    echo "<option value='$r[id]' ".($r['id']==$user['ketua']?'selected':'').">$r[text]</option>";
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="hori-pass2" class="col-sm-4 form-control-label">Anggota</label>
                        <div class="col-sm-7">
                            <select name="anggota[]" id="anggota" class="select2 form-control select2-multiple" multiple="multiple" multiple data-placeholder="Pilih Anggota...">
                                <?php
                                foreach ($anggota as $r){
                                    echo "<option value='$r[id]' ".(in_array($r['id'],$anggotatim)?'selected':'').">$r[text]</option>";
                                }
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-sm-8 col-sm-offset-4">
                            <button type="submit" class="btn btn-inverse waves-effect waves-light">Simpan</button>
                            <a href="?" class="btn btn-default waves-effect m-l-5">Batal</a>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-md-12">
                <hr>
            </div>
        </div>
    </div>
</div>
<link href="<?=base_url()?>assets/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="<?=base_url()?>assets/plugins/select2/js/select2.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/plugins/parsleyjs/parsley.min.js"></script>
<script type="text/javascript">
    $(function () {
        $('.select2').select2({});
        $('#anggota').select2({
            maximumSelectionLength: 10,
        });
        $('form').parsley();
        $("form").submit(function(event) {
            $(this).parsley().validate();
            if (!$(this).parsley().isValid()){
                event.preventDefault();
            }else if($('#hori-pass1').val()!=$('#hori-pass2').val()){
                event.preventDefault();
                $('#hori-pass2').focus();
            }
        });

    });
</script>