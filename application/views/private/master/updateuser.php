<div class="row">
    <div class="col-sm-12">
        <div class="card-box row">
            <?php
            show_alert();
            ?>
            <div class="col-md-6 col-md-offset-3">
                <form role="form" method="post">
                    <div class="form-group row">
                        <label for="inputEmail" class="col-sm-4 form-control-label">Email<span class="text-danger">*</span></label>
                        <div class="col-sm-7">
                            <input type="email" name="email" required parsley-type="email" class="form-control" id="inputEmail" placeholder="Email" value="<?=$user['email']?>">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputName" class="col-sm-4 form-control-label">Name<span class="text-danger">*</span></label>
                        <div class="col-sm-7">
                            <input type="text" name="name" required parsley-type="text" class="form-control" id="inputName" placeholder="Name" value="<?=$user['name']?>">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-4 form-control-label">NIP</label>
                        <div class="col-sm-7">
                            <input type="text" value="<?=$user['nip']?>" name="nip"  placeholder="NIP" class="form-control">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-4 form-control-label">Alamat</label>
                        <div class="col-sm-7">
                            <input type="text" value="<?=$user['alamat']?>" name="alamat" placeholder="Alamat" class="form-control">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-4 form-control-label">Telephone</label>
                        <div class="col-sm-7">
                            <input type="text" value="<?=$user['telp']?>"  name="telp" placeholder="Telephone" class="form-control">
                        </div>
                    </div><div class="form-group row">
                        <label for="hori-pass2" class="col-sm-4 form-control-label">Jabatan</label>
                        <div class="col-sm-7">
                            <select name="jabatan" class="form-control">
                                <option value="">Pilih Jabatan</option>
                                <?php
                                foreach ($jabatan as $r){
                                    echo "<option value='$r[id]' ".($r['id']==$user['jabatan']?'selected':'').">$r[ket]</option>";
                                }
                                ?>
                            </select>
                        </div>
                    </div><div class="form-group row">
                        <label for="hori-pass2" class="col-sm-4 form-control-label">Golongan</label>
                        <div class="col-sm-7">
                            <select name="golongan" class="form-control">
                                <option value="">Pilih Golongan</option>
                                <?php
                                foreach ($golongan as $r){
                                    echo "<option value='$r[id]' ".($r['id']==$user['golongan']?'selected':'').">$r[ket]</option>";
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-4 form-control-label">Username</label>
                        <div class="col-sm-7">
                            <input type="text" value="<?=$user['username']?>" class="form-control">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="hori-pass1" class="col-sm-4 form-control-label">Password</label>
                        <div class="col-sm-7">
                            <input id="hori-pass1" name="password" type="password" placeholder="Password" class="form-control">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="hori-pass2" class="col-sm-4 form-control-label">Confirm Password</label>
                        <div class="col-sm-7">
                            <input data-parsley-equalto="#hori-pass1" name="konfirm" type="password" placeholder="Password" class="form-control" id="hori-pass2">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="hori-pass2" class="col-sm-4 form-control-label">Role</label>
                        <div class="col-sm-7">
                            <select name="role[]" class="select2 form-control select2-multiple" multiple="multiple" multiple data-placeholder="Choose Access Role...">
                                <?php
                                foreach ($role as $r){
                                    echo "<option value='$r[id]' ".(in_array($r['id'],$roleusers)?'selected':'').">$r[name]</option>";
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-8 col-sm-offset-4">
                            <button type="submit" class="btn btn-primary waves-effect waves-light">Save</button>
                            <a href="?" class="btn btn-default waves-effect m-l-5">Cancel</a>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-md-12">
                <hr>
            </div>
        </div>
    </div>
</div>
<link href="<?=base_url()?>assets/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="<?=base_url()?>assets/plugins/select2/js/select2.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/plugins/parsleyjs/parsley.min.js"></script>
<script type="text/javascript">
    $(function () {
        $('.select2').select2();
        $('form').parsley();
        $("form").submit(function(event) {
            $(this).parsley().validate();
            if (!$(this).parsley().isValid()){
                event.preventDefault();
            }else if($('#hori-pass1').val()!=$('#hori-pass2').val()){
                event.preventDefault();
                $('#hori-pass2').focus();
            }
        });

    });
</script>