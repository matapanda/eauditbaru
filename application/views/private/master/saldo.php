<div class="row">
    <div class="col-sm-12">
        <div class="card-box">
            <?php
            include VIEWPATH.'alert.php';
            ?>
            <form method="get" class="row" action="?">
                <div class="col-md-6">
                    <div class="dataTables_wrapper form-inline">
                        <label>Filter: </label>
                        <div class="input-daterange input-group" id="date-range">
                            <input type="text" readonly class="form-control input-sm" name="start" onchange="this.form.submit()" value="<?=$start?>">
                            <span class="input-group-addon input-sm">~</span>
                            <input type="text" readonly class="form-control input-sm" name="end" onchange="this.form.submit()" value="<?=$end?>">
                        </div>
                        <input type="hidden" name="saldo" value="<?=$id?>">
                    </div>
                </div>
                <div class="col-md-6 right">
                    <h3><?=format_uang($saldo)?></h3>
                </div>
            </form>
            <div class="table-responsive">
                <table class="table table-bordered table-striped table-hover">
                    <thead>
                    <tr>
                        <th class="center col-xs-1">#</th>
                        <th class="center col-xs-3">Waktu</th>
                        <th class="center col-xs-2">Jumlah</th>
                        <th class="center">Note</th>
                        <th class="center col-xs-1">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $no=0;
                    foreach($data as $g) {
                        ?>
                        <tr class="data<?=$g['id']?>">
                            <td scope="row" class="center"><?=++$no?></td>
                            <td class="center"><?=date('d/m/Y H:i:s',strtotime($g['created_at']))?></td>
                            <td class="center"><?=format_uang($g['jumlah'])?></td>
                            <td><?=$g['note']?></td>
                            <td class="center">
                                <button onclick="rincian('<?=$g['refid']?>','<?=$g['reftrans']?>')" class="btn btn-xs btn-block btn-inverse"><i class="fa fa-search"></i></button>
                            </td>
                        </tr>
                        <?php
                    }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<div id="detailpenerimaan" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<link href="<?=base_url()?>assets/plugins/custombox/css/custombox.min.css" rel="stylesheet">
<script src="<?=base_url()?>assets/plugins/custombox/js/custombox.min.js"></script>
<script src="<?=base_url()?>assets/plugins/custombox/js/legacy.min.js"></script>
<link href="<?=base_url()?>assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css"/>
<link href="<?=base_url()?>assets/css/bootcomplete.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="<?=base_url()?>assets/plugins/select2/js/select2.min.js"></script>
<script src="<?=base_url()?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script src="<?=base_url()?>assets/js/jquery.bootcomplete.js"></script>
<script src="<?=base_url()?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script>
    $('input[name=nota_id]','#new-data').bootcomplete({
        url:'?opb=true',
        minLength : 2,
        method: 'post'
    });
    $('#date-range').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true,
        todayHighlight: true
    });
    function rincian(_i,_r) {
        $.post('?',{ri:_i,rn:_r},function (data,status) {
            $('h4.modal-title','#detailpenerimaan').html("Detail");
            $('.modal-body','#detailpenerimaan').html(data.data);
            $('#detailpenerimaan').modal('show');
        },'json');
    }
</script>