<div class="row">
    <div class="col-sm-12">
        <div class="card-box row">
            <?php
            include VIEWPATH.'alert.php';
            ?>
            <div class="col-md-6 col-md-offset-3">
                <form role="form" method="post">
                    <div class="form-group row">
                        <label for="kode" class="col-sm-4 col-xs-12 form-control-label">Kode<span class="text-danger">*</span></label>
                        <?php
                        $md=7;
                        $xs=12;
                        if($data['level']>1) {
                            ?>
                            <div class="col-sm-3 col-xs-6">
                                <input type="text" name="pre" class="form-control center" readonly value="<?php
                                echo $data['k1'];
                                if ($data['level'] > 2) {
                                    echo $data['k2'];
                                    if ($data['level'] > 3) {
                                        echo $data['k3'];
                                    }
                                }
                                ?>">
                            </div>
                            <?php
                            $md=4;
                            $xs=6;
                        }
                        ?>
                        <div class="col-sm-<?=$md?> col-xs-<?=$xs?>">
                            <input type="text" name="kode" required parsley-type="text" class="form-control center" id="kode" value="<?=$data['k'.$data['level']]?>">
                            <input type="hidden" name="level" value="<?=$data['level']?>">
                            <input type="hidden" name="id" value="<?=$data['id']?>">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="nama" class="col-sm-4 form-control-label">Keterangan<span class="text-danger">*</span></label>
                        <div class="col-sm-7">
                            <input type="text" name="nama" required parsley-type="text" class="form-control" id="nama" value="<?=$data['nama']?>">
                        </div>
                    </div>
<!--                    <div class="form-group row">-->
<!--                        <label for="barangjasa" class="col-sm-4 form-control-label"></label>-->
<!--                        <div class="col-sm-7">-->
<!--                            <div class="btn-switch btn-switch-inverse">-->
<!--                                <input type="checkbox" name="recursive" value="1" id="input-switch-recursive"/>-->
<!--                                <label for="input-switch-recursive" class="btn btn-sm btn-rounded btn-inverse waves-effect waves-light">-->
<!--                                    <em class="glyphicon glyphicon-ok"></em>-->
<!--                                    <strong> Rekursif</strong>-->
<!--                                </label>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->
                    <div class="form-group row">
                        <div class="col-sm-8 col-sm-offset-4">
                            <button type="submit" class="btn btn-primary waves-effect waves-light">Save</button>
                            <a href="?" class="btn btn-default waves-effect m-l-5">Cancel</a>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-md-12">
                <hr>
            </div>
        </div>
    </div>
</div>
<link href="<?=base_url()?>assets/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="<?=base_url()?>assets/plugins/select2/js/select2.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/plugins/parsleyjs/parsley.min.js"></script>
<script type="text/javascript">
    $(function () {
        $('.select2').select2();
        $('form').parsley();
    });
</script>