<div class="row">
    <div class="col-sm-12">
        <div class="card-box row">
            <?php
            show_alert();
            ?>
            <div class="col-md-12">
                <form role="form" method="get" action="?cek_periode=true">
                    <div class="form-group row">
                        <label for="hori-pass2" class="col-sm-1 form-control-label" style="padding-top: 10px">Periode</label>
                        <div class="col-sm-4">
                            <select name="periode" onchange="this.form.submit()" required class="select2 form-control select2-multiple" data-placeholder="Periode">
                                <?php
                                foreach ($periode as $r){
                                    echo "<option value='$r[id]' ".(@$per==$r['id']?'selected':'').">$r[nama]</option>";
                                }
                                ?>
                            </select>
                            <input type="hidden" name="<?=$tipe?>" value="<?=@$id_satker?>">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="hori-pass2" class="col-sm-1 form-control-label" style="padding-top: 10px">Pilih Satker</label>
                        <div class="col-sm-4">
                            <select required name="id_h" class="select2 form-control" >
                                <option value="">Pilih Satker</option>
                                <?php
                                foreach ($satker as $r){
                                    echo "<option value='$r[id]' ".(@$id_satker==$r['id']?'selected':'').">$r[ket]</option>";
                                }
                                ?>
                            </select>
                            <input type="hidden" name="periode_h" value="<?=@$per?>">
                            <input type="hidden" name="satker_h" value="<?=@$id_satker?>">
                        </div>
                    </div>
                </form>
                <form role="form" method="post" action="?">
                    <input type="hidden" name="satker" value="<?=@$id_satker?>">
                    <input type="hidden" name="periode_h" value="<?=@$per?>">
                    <div class="form-group">
                        <table class="table table-bordered table-striped table-hover">
                           <tr>
                               <td class="center" colspan="3"><b>TINGKAT  MATURITAS  SPIP  SKPD</b></td>
                           </tr>
                            <tr>
                                <td class="center">Pengendalian Intern</td>
                                <td class="center" colspan="2">Penilaian</td>
                            </tr>
                            <tr>
                                <td>Lingkungan Pengendalian</td>
                                <td class="center"><input type="radio" name="lp" onclick="cek_lp()" <?=@$edit['lp']==0?'checked':''?> value="0">Ya </td><td class="center"><input type="radio" name="lp" <?=@$edit['lp']==1?'checked':''?> onclick="cek_lp()" value="1">Tidak </td>
                            </tr>
                            <tr>
                                <td>Penilaian Resiko</td>
                                <td class="center"><input type="radio" name="lp2" onclick="cek_lp()" <?=@$edit['lp2']==0?'checked':''?> value="0">Ya </td><td class="center"><input type="radio" name="lp2" onclick="cek_lp()" <?=@$edit['lp2']==1?'checked':''?> value="1">Tidak </td>
                            </tr>
                            <tr>
                                <td>Kegiatan Pengendalian</td>
                                <td class="center"><input type="radio" name="lp3" onclick="cek_lp()" <?=@$edit['lp3']==0?'checked':''?> value="0">Ya </td><td class="center"><input type="radio" name="lp3" onclick="cek_lp()" <?=@$edit['lp3']==1?'checked':''?> value="1">Tidak </td>
                            </tr>
                            <tr>
                                <td>Informasi dan Komunikasi</td>
                                <td class="center"><input type="radio" name="lp4" onclick="cek_lp()" <?=@$edit['lp4']==0?'checked':''?> value="0">Ya </td><td class="center"><input type="radio" name="lp4" onclick="cek_lp()" <?=@$edit['lp4']==1?'checked':''?> value="1">Tidak </td>
                            </tr>
                            <tr>
                                <td>Pemantauan Pengendalian</td>
                                <td class="center"><input type="radio" name="lp5" onclick="cek_lp()" <?=@$edit['lp5']==0?'checked':''?> value="0">Ya </td><td class="center"><input type="radio" name="lp5" onclick="cek_lp()" <?=@$edit['lp5']==1?'checked':''?>  value="1">Tidak </td>
                            </tr>
                            <tr>
                                <td class="center"><b>Total Pengendalian Intern</b></td>
                                <td class="center" colspan="2"><label class="tot_lp"><b><?=@$edit['tot_lp']?$edit['tot_lp']:'0'?> </b></label><input type="hidden" name="tot_lp" value="<?=@$edit['tot_lp']?$edit['tot_lp']:'0'?>"></td>
                            </tr>
                        </table>
                    </div>

                    <div class="form-group">
                        <table class="table table-bordered table-striped table-hover">
                           <tr>
                               <td class="center" colspan="3"><b>Pemenuhan</b></td>
                           </tr>
                            <tr>
                                <td class="center">KM  (Kompetensi  Manajemen)</td>
                                <td class="center" colspan="2">Penilaian</td>
                            </tr>
                            <tr>
                                <td>Pola  Struktur  Organisasi</td>
                                <td class="center"><input type="radio" name="km" onclick="cek_km()" <?=@$edit['km']==0?'checked':''?>  value="0">Ya </td><td class="center"><input type="radio" name="km" onclick="cek_km()"  <?=@$edit['km']==1?'checked':''?> value="1">Tidak </td>
                            </tr>
                            <tr>
                                <td>Standar Kompetensi Pejabat</td>
                                <td class="center"><input type="radio" name="km2" onclick="cek_km()"  <?=@$edit['km2']==0?'checked':''?> value="0">Ya </td><td class="center"><input type="radio" name="km2" onclick="cek_km()"  <?=@$edit['km2']==1?'checked':''?> value="1">Tidak </td>
                            </tr>
                            <tr>
                                <td>Pemenuhan standar Diklat Jabatan</td>
                                <td class="center"><input type="radio" name="km3" onclick="cek_km()" <?=@$edit['km3']==0?'checked':''?> value="0">Ya </td><td class="center"><input type="radio" name="km3" onclick="cek_km()" <?=@$edit['km3']==1?'checked':''?> value="1">Tidak </td>
                            </tr>
                            <tr>
                                <td>Program Diklat  Kompetensi</td>
                                <td class="center"><input type="radio" name="km4" onclick="cek_km()" <?=@$edit['km4']==0?'checked':''?> value="0">Ya </td><td class="center"><input type="radio" name="km4" onclick="cek_km()" <?=@$edit['km4']==1?'checked':''?> value="1">Tidak </td>
                            </tr>
                            <tr>
                                <td>Program Diklat Teknis</td>
                                <td class="center"><input type="radio" name="km5" onclick="cek_km()" <?=@$edit['km5']==0?'checked':''?> value="0">Ya </td><td class="center"><input type="radio" name="km5" onclick="cek_km()" <?=@$edit['km5']==1?'checked':''?> value="1">Tidak </td>
                            </tr>
                            <tr>
                                <td class="center"><b>Total Kompetensi Manajemen</b></td>
                                <td class="center" colspan="2"><label class="tot_km"><b><?=@$edit['tot_km']?$edit['tot_km']:'0'?> </b></label><input type="hidden" name="tot_km" value="<?=@$edit['tot_km']?$edit['tot_km']:'0'?>"></td>
                            </tr>
                        </table>
                    </div>

                    <div class="form-group">
                        <table class="table table-bordered table-striped table-hover">
                           <tr>
                               <td class="center" colspan="3"><b>Pemenuhan</b></td>
                           </tr>
                            <tr>
                                <td class="center">IM  (Integritas  Manajemen)</td>
                                <td class="center" colspan="2">Penilaian</td>
                            </tr>
                            <tr>
                                <td>Sertifikat  ISSO</td>
                                <td class="center"><input type="radio" name="im" onclick="cek_im()" <?=@$edit['im']==0?'checked':''?> value="0">Ya </td><td class="center"><input type="radio" name="im" onclick="cek_im()" <?=@$edit['im']==1?'checked':''?> value="1">Tidak </td>
                            </tr>
                            <tr>
                                <td>Fakta Integriatas Pejabat/Staf</td>
                                <td class="center"><input type="radio" name="im2" onclick="cek_im()" <?=@$edit['im2']==0?'checked':''?> value="0">Ya </td><td class="center"><input type="radio" name="im2" onclick="cek_im()" <?=@$edit['im2']==1?'checked':''?> value="1">Tidak </td>
                            </tr>
                            <tr>
                                <td>Pemenuhan  Standar  Pelayanan</td>
                                <td class="center"><input type="radio" name="im3" onclick="cek_im()" <?=@$edit['im3']==0?'checked':''?> value="0">Ya </td><td class="center"><input type="radio" name="im3" onclick="cek_im()" <?=@$edit['im3']==1?'checked':''?> value="1">Tidak </td>
                            </tr>
                            <tr>
                                <td>Lingkungan  Budaya  Kerja</td>
                                <td class="center"><input type="radio" name="im4" onclick="cek_im()" <?=@$edit['im4']==0?'checked':''?> value="0">Ya </td><td class="center"><input type="radio" onclick="cek_im()" name="im4" <?=@$edit['im4']==1?'checked':''?> value="1">Tidak </td>
                            </tr>
                            <tr>
                                <td>Membuka Kotak Saran/Kritik</td>
                                <td class="center"><input type="radio" name="im5" onclick="cek_im()" <?=@$edit['im5']==0?'checked':''?> value="0">Ya </td><td class="center"><input type="radio" onclick="cek_im()" name="im5" <?=@$edit['im5']==1?'checked':''?> value="1">Tidak </td>
                            </tr>
                            <tr>
                                <td class="center"><b>Total Integritas Manajemen</b></td>
                                <td class="center" colspan="2"><label class="tot_im"><b><?=@$edit['tot_im']?$edit['tot_im']:'0'?></b></label><input type="hidden" name="tot_im" value="<?=@$edit['tot_im']?$edit['tot_im']:'0'?>"></td>
                            </tr>
                        </table>
                    </div>
                    <div class="form-group">
                        <table class="table table-bordered table-striped table-hover">
                           <tr>
                               <td class="center" colspan="3"><b>Pemenuhan</b></td>
                           </tr>
                            <tr>
                                <td class="center">KO  (Kompleksitas  Organisasi)</td>
                                <td class="center" colspan="2">Penilaian</td>
                            </tr>
                            <tr>
                                <td>Visi  dan  Misi  Organisasi</td>
                                <td class="center"><input type="radio" onclick="cek_ko()" name="ko" <?=@$edit['ko']==0?'checked':''?> value="0">Ya </td><td class="center"><input type="radio" onclick="cek_ko()" <?=@$edit['ko']==1?'checked':''?> name="ko" value="1">Tidak </td>
                            </tr>
                            <tr>
                                <td>Target  capaian  Tujuan Organisasi</td>
                                <td class="center"><input type="radio" onclick="cek_ko()" name="ko2" <?=@$edit['ko2']==0?'checked':''?> value="0">Ya </td><td class="center"><input type="radio" onclick="cek_ko()" <?=@$edit['ko2']==1?'checked':''?> name="ko2" value="1">Tidak </td>
                            </tr>
                            <tr>
                                <td>Target  capaian Program Kegiatan </td>
                                <td class="center"><input type="radio" onclick="cek_ko()" name="ko3" <?=@$edit['ko3']==0?'checked':''?> value="0">Ya </td><td class="center"><input type="radio" onclick="cek_ko()" <?=@$edit['ko3']==1?'checked':''?> name="ko3" value="1">Tidak </td>
                            </tr>
                            <tr>
                                <td>Target  Kinerja  Organisasi</td>
                                <td class="center"><input type="radio" onclick="cek_ko()" name="ko4" <?=@$edit['ko4']==0?'checked':''?> value="0">Ya </td><td class="center"><input type="radio" onclick="cek_ko()" <?=@$edit['ko4']==1?'checked':''?> name="ko4" value="1">Tidak </td>
                            </tr>
                            <tr>
                                <td>Target  Kinerja  Anggaran</td>
                                <td class="center"><input type="radio" onclick="cek_ko()" name="ko5" <?=@$edit['ko5']==0?'checked':''?> value="0">Ya </td><td class="center"><input type="radio" onclick="cek_ko()" name="ko5" <?=@$edit['ko5']==1?'checked':''?> value="1">Tidak </td>
                            </tr>
                            <tr>
                                <td class="center"><b>Total Kompleksitas Organisasi</b></td>
                                <td class="center" colspan="2"><label class="tot_ko"><b><?=@$edit['tot_ko']?$edit['tot_ko']:'0'?></b></label><input type="hidden" name="tot_ko" value="<?=@$edit['tot_ko']?$edit['tot_ko']:'0'?>"></td>
                            </tr>
                        </table>
                    </div>
                    <div class="form-group">
                        <table class="table table-bordered table-striped table-hover">
                           <tr>
                               <td class="center" colspan="3"><b>Pemenuhan</b></td>
                           </tr>
                            <tr>
                                <td class="center">SI  (Sistem  Informasi)</td>
                                <td class="center" colspan="2">Penilaian</td>
                            </tr>
                            <tr>
                                <td>Terbuka melalui Jaringan Internet</td>
                                <td class="center"><input type="radio" onclick="cek_si()" name="si" <?=@$edit['si']==0?'checked':''?> value="0">Ya </td><td class="center"><input type="radio" onclick="cek_si()" <?=@$edit['si']==1?'checked':''?> name="si" value="1">Tidak </td>
                            </tr>
                            <tr>
                                <td>Dapat diakses oleh publik</td>
                                <td class="center"><input type="radio" onclick="cek_si()" name="si2" <?=@$edit['si2']==0?'checked':''?>  value="0">Ya </td><td class="center"><input type="radio" onclick="cek_si()" <?=@$edit['si2']==1?'checked':''?> name="si2" value="1">Tidak </td>
                            </tr>
                            <tr>
                                <td>Terdapat ruang untuk kritik/saran </td>
                                <td class="center"><input type="radio" onclick="cek_si()" name="si3" <?=@$edit['si3']==0?'checked':''?>  value="0">Ya </td><td class="center"><input type="radio" onclick="cek_si()" <?=@$edit['si3']==1?'checked':''?> name="si3" value="1">Tidak </td>
                            </tr>
                            <tr>
                                <td>Respon terhadap kritik/saran</td>
                                <td class="center"><input type="radio" onclick="cek_si()" name="si4" <?=@$edit['si4']==0?'checked':''?>  value="0">Ya </td><td class="center"><input type="radio" onclick="cek_si()" <?=@$edit['si4']==1?'checked':''?> name="si4" value="1">Tidak </td>
                            </tr>
                            <tr>
                                <td>Publikasi  Capaian, Hambatan</td>
                                <td class="center"><input type="radio" onclick="cek_si()" name="si5" <?=@$edit['si5']==0?'checked':''?>  value="0">Ya </td><td class="center"><input type="radio" onclick="cek_si()" <?=@$edit['si5']==1?'checked':''?> name="si5" value="1">Tidak </td>
                            </tr>
                            <tr>
                                <td class="center"><b>Total Sistem Informasi</b></td>
                                <td class="center" colspan="2"><label class="tot_si"><b><?=@$edit['tot_si']?$edit['tot_si']:'0'?></b></label><input type="hidden" name="tot_si" value="<?=@$edit['tot_si']?$edit['tot_si']:'0'?>"></td>
                            </tr>
                        </table>
                    </div>
                    <div class="form-group">
                        <table class="table table-bordered table-striped table-hover">
                           <tr>
                               <td class="center" colspan="6"><b>Tingkat Resiko Lainya</b></td>
                           </tr>
                            <tr>
                                <td class="center">Pilihan</td>
                                <td class="center" colspan="5">Penilaian</td>
                            </tr>
                            <tr>
                                <td>Jumlah Anggaran</td>
                                <td class="center"><input type="radio" required name="ja" <?=@$edit['jumlah_anggaran']==0?'checked':''?> value="0"> < 1m</td>
                                <td class="center"><input type="radio" name="ja" <?=@$edit['jumlah_anggaran']==1?'checked':''?> value="1">1 s.d < 2 m </td>
                                <td class="center"><input type="radio" name="ja" <?=@$edit['jumlah_anggaran']==2?'checked':''?> value="2">2 s.d < 5 m </td>
                                <td class="center"><input type="radio" name="ja" <?=@$edit['jumlah_anggaran']==3?'checked':''?> value="3">5 s.d < 10 m </td>
                                <td class="center"><input type="radio" name="ja" <?=@$edit['jumlah_anggaran']==4?'checked':''?> value="4">10 keatas </td>
                            </tr>
                            <tr>
                                <td>Nilai Aset</td>
                                <td class="center"><input type="radio" required name="na" <?=@$edit['nilai_aset']==0?'checked':''?> value="0"> < 1m</td>
                                <td class="center"><input type="radio" name="na" <?=@$edit['nilai_aset']==1?'checked':''?> value="1">1 s.d < 2 m </td>
                                <td class="center"><input type="radio" name="na" <?=@$edit['nilai_aset']==2?'checked':''?> value="2">2 s.d < 5 m </td>
                                <td class="center"><input type="radio" name="na" <?=@$edit['nilai_aset']==3?'checked':''?> value="3">5 s.d < 10 m </td>
                                <td class="center"><input type="radio" name="na" <?=@$edit['nilai_aset']==4?'checked':''?> value="4">10 keatas </td>
                            </tr>
                            <tr>
                                <td>Jumlah Pegawai</td>
                                <td class="center"><input type="radio" required name="jp" <?=@$edit['jumlah_pegawai']==0?'checked':''?> value="0"> < 10</td>
                                <td class="center"><input type="radio" name="jp" <?=@$edit['jumlah_pegawai']==1?'checked':''?> value="1">10 s.d < 20 </td>
                                <td class="center"><input type="radio" name="jp" <?=@$edit['jumlah_pegawai']==2?'checked':''?> value="2">20 s.d < 50 </td>
                                <td class="center"><input type="radio" name="jp" <?=@$edit['jumlah_pegawai']==3?'checked':''?> value="3">50 s.d < 100 </td>
                                <td class="center"><input type="radio" name="jp" <?=@$edit['jumlah_pegawai']==4?'checked':''?> value="4">100 keatas </td>
                            </tr>
                            <tr>
                                <td>Jumlah Unit Kerja</td>
                                <td class="center"><input type="radio" required name="ju" <?=@$edit['jumlah_unit']==0?'checked':''?> value="0"> 1 </td>
                                <td class="center"><input type="radio" name="ju" <?=@$edit['jumlah_unit']==1?'checked':''?> value="1"> >1 s.d 5 </td>
                                <td class="center"><input type="radio" name="ju" <?=@$edit['jumlah_unit']==2?'checked':''?> value="2"> >5 s.d 10 </td>
                                <td class="center"><input type="radio" name="ju" <?=@$edit['jumlah_unit']==3?'checked':''?> value="3"> >10 s.d 20 </td>
                                <td class="center"><input type="radio" name="ju"  <?=@$edit['jumlah_unit']==4?'checked':''?> value="4"> >20 </td>
                            </tr>
                            <tr>
                                <td>Jumlah Jenis Kegiatan</td>
                                <td class="center"><input type="radio" required <?=@$edit['jumlah_jenis_keg']==0?'checked':''?>  name="jk" value="0"> >1 s.d 5 </td>
                                <td class="center"><input type="radio" name="jk" <?=@$edit['jumlah_jenis_keg']==1?'checked':''?> value="1"> >5 s.d 10 </td>
                                <td class="center"><input type="radio" name="jk" <?=@$edit['jumlah_jenis_keg']==2?'checked':''?> value="2"> >10 s.d 15 </td>
                                <td class="center"><input type="radio" name="jk" <?=@$edit['jumlah_jenis_keg']==3?'checked':''?> value="3"> >15 s.d 20 </td>
                                <td class="center"><input type="radio" name="jk" <?=@$edit['jumlah_jenis_keg']==4?'checked':''?> value="4"> >20 </td>
                            </tr>
                            <tr>
                                <td>Jumlah Pemeriksaan 5 Tahun Terakhir</td>
                                <td class="center"><input type="radio" required <?=@$edit['jumlah_pemeriksaan']==0?'checked':''?>  name="jp5" value="0"> 5 </td>
                                <td class="center"><input type="radio" name="jp5" <?=@$edit['jumlah_pemeriksaan']==1?'checked':''?> value="1"> 4 </td>
                                <td class="center"><input type="radio" name="jp5" <?=@$edit['jumlah_pemeriksaan']==2?'checked':''?> value="2"> 3 </td>
                                <td class="center"><input type="radio" name="jp5" <?=@$edit['jumlah_pemeriksaan']==3?'checked':''?> value="3"> 2 </td>
                                <td class="center"><input type="radio" name="jp5" <?=@$edit['jumlah_pemeriksaan']==4?'checked':''?> value="4"> 1 </td>
                            </tr>
                            <tr>
                                <td>Nilai SAKIP</td>
                                <td class="center"><input type="radio" required <?=@$edit['nilai_sakip']==0?'checked':''?>  name="ns" value="0"> AA </td>
                                <td class="center"><input type="radio" name="ns" <?=@$edit['nilai_sakip']==1?'checked':''?> value="1"> A </td>
                                <td class="center"><input type="radio" name="ns" <?=@$edit['nilai_sakip']==2?'checked':''?>  value="2"> BB </td>
                                <td class="center"><input type="radio" name="ns" <?=@$edit['nilai_sakip']==3?'checked':''?> value="3"> B </td>
                                <td class="center"><input type="radio" name="ns" <?=@$edit['nilai_sakip']==4?'checked':''?> value="4"> C </td>
                            </tr>
                            <tr>
                                <td>Nilai SPIP</td>
                                <td class="center"><input type="radio" required <?=@$edit['nilai_spip']==0?'checked':''?>  name="nsp" value="0"> >3.5 </td>
                                <td class="center"><input type="radio" name="nsp" <?=@$edit['nilai_spip']==1?'checked':''?> value="1"> < 3.5 s.d 3 </td>
                                <td class="center"><input type="radio" name="nsp" <?=@$edit['nilai_spip']==2?'checked':''?> value="2"> < 3 s.d 2.5 </td>
                                <td class="center"><input type="radio" name="nsp" <?=@$edit['nilai_spip']==3?'checked':''?> value="3"> < 2.5 s.d 2 </td>
                                <td class="center"><input type="radio" name="nsp" <?=@$edit['nilai_spip']==4?'checked':''?> value="4"> < 2 </td>
                            </tr>
                            <tr>
                                <td>Tertib ADM</td>
                                <td class="center"><input type="radio" required  <?=@$edit['tertib_adm']==0?'checked':''?>  name="ta" value="0"> A </td>
                                <td class="center"><input type="radio" name="ta" <?=@$edit['tertib_adm']==1?'checked':''?> value="1"> B </td>
                                <td class="center"><input type="radio" name="ta"  <?=@$edit['tertib_adm']==2?'checked':''?> value="2"> C </td>
                                <td class="center"><input type="radio" name="ta" <?=@$edit['tertib_adm']==3?'checked':''?>  value="3"> D </td>
                                <td class="center"><input type="radio" name="ta" <?=@$edit['tertib_adm']==4?'checked':''?>  value="4"> E </td>
                            </tr>
                            <tr>
                                <td>Jumlah Pengaduan Masyarakat</td>
                                <td class="center"><input type="radio" required  <?=@$edit['jumlah_pengaduan']==0?'checked':''?> name="jpm" value="0"> 1 </td>
                                <td class="center"><input type="radio" name="jpm" <?=@$edit['jumlah_pengaduan']==1?'checked':''?> value="1"> 2 </td>
                                <td class="center"><input type="radio" name="jpm" <?=@$edit['jumlah_pengaduan']==2?'checked':''?> value="2"> 3 </td>
                                <td class="center"><input type="radio" name="jpm" <?=@$edit['jumlah_pengaduan']==3?'checked':''?> value="3"> 4 </td>
                                <td class="center"><input type="radio" name="jpm" <?=@$edit['jumlah_pengaduan']==4?'checked':''?> value="4"> >4 </td>
                            </tr>
                            <tr>
                                <td>Jumlah Jabatan Kosong</td>
                                <td class="center"><input type="radio" required  <?=@$edit['jumlah_jabatan']==0?'checked':''?> name="jj" value="0"> 0 </td>
                                <td class="center"><input type="radio" name="jj" <?=@$edit['jumlah_jabatan']==1?'checked':''?> value="1"> 1 </td>
                                <td class="center"><input type="radio" name="jj" <?=@$edit['jumlah_jabatan']==2?'checked':''?> value="2"> 2 </td>
                                <td class="center"><input type="radio" name="jj" <?=@$edit['jumlah_jabatan']==3?'checked':''?> value="3"> 3 </td>
                                <td class="center"><input type="radio" name="jj" <?=@$edit['jumlah_jabatan']==4?'checked':''?> value="4"> >3 </td>
                            </tr>
                            <tr>
                                <td>Hasil Monitoring Berkala</td>
                                <td class="center"><input type="radio" required <?=@$edit['hasil_monitoring']==0?'checked':''?> name="hm" value="0"> A </td>
                                <td class="center"><input type="radio" name="hm" <?=@$edit['hasil_monitoring']==1?'checked':''?> value="1"> B </td>
                                <td class="center"><input type="radio" name="hm" <?=@$edit['hasil_monitoring']==2?'checked':''?> value="2"> C </td>
                                <td class="center"><input type="radio" name="hm" <?=@$edit['hasil_monitoring']==3?'checked':''?> value="3"> D </td>
                                <td class="center"><input type="radio" name="hm" <?=@$edit['hasil_monitoring']==4?'checked':''?> value="4"> E </td>
                            </tr>
                        </table>
                    </div>
<!--                    --><?//=json_encode($edit)?>
                    <div class="form-group row">
                        <div class="col-sm-8 col-sm-offset-4">
                            <button type="submit" class="btn btn-primary waves-effect waves-light">Save</button>
                            <a href="?" class="btn btn-default waves-effect m-l-5">Cancel</a>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-md-12">
                <hr>
            </div>
        </div>
    </div>
</div>
<link href="<?=base_url()?>assets/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="<?=base_url()?>assets/plugins/select2/js/select2.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/plugins/parsleyjs/parsley.min.js"></script>
<script type="text/javascript">
    function cek_lp(){
        var lp=$('input[name=lp]:checked').val();
        var lp2=$('input[name=lp2]:checked').val();
        var lp3=$('input[name=lp3]:checked').val();
        var lp4=$('input[name=lp4]:checked').val();
        var lp5=$('input[name=lp5]:checked').val();
        var tot=parseFloat(lp)+parseFloat(lp2)+parseFloat(lp3)+parseFloat(lp4)+parseFloat(lp5);
        $('input[name=tot_lp]').val(tot);
        $('.tot_lp').html('<b>'+tot+'</b>');
    }
    function cek_im(){
        var lp=$('input[name=im]:checked').val();
        var lp2=$('input[name=im2]:checked').val();
        var lp3=$('input[name=im3]:checked').val();
        var lp4=$('input[name=im4]:checked').val();
        var lp5=$('input[name=im5]:checked').val();
        var tot=parseFloat(lp)+parseFloat(lp2)+parseFloat(lp3)+parseFloat(lp4)+parseFloat(lp5);
        $('input[name=tot_im]').val(tot);
        $('.tot_im').html('<b>'+tot+'</b>');
    }
    function cek_km(){
        var lp=$('input[name=km]:checked').val();
        var lp2=$('input[name=km2]:checked').val();
        var lp3=$('input[name=km3]:checked').val();
        var lp4=$('input[name=km4]:checked').val();
        var lp5=$('input[name=km5]:checked').val();
        var tot=parseFloat(lp)+parseFloat(lp2)+parseFloat(lp3)+parseFloat(lp4)+parseFloat(lp5);
        $('input[name=tot_km]').val(tot);
        $('.tot_km').html('<b>'+tot+'</b>');
    }
    function cek_si(){
        var lp=$('input[name=si]:checked').val();
        var lp2=$('input[name=si2]:checked').val();
        var lp3=$('input[name=si3]:checked').val();
        var lp4=$('input[name=si4]:checked').val();
        var lp5=$('input[name=si5]:checked').val();
        var tot=parseFloat(lp)+parseFloat(lp2)+parseFloat(lp3)+parseFloat(lp4)+parseFloat(lp5);
        $('input[name=tot_si]').val(tot);
        $('.tot_si').html('<b>'+tot+'</b>');
    }
    function cek_ko(){
        var lp=$('input[name=ko]:checked').val();
        var lp2=$('input[name=ko2]:checked').val();
        var lp3=$('input[name=ko3]:checked').val();
        var lp4=$('input[name=ko4]:checked').val();
        var lp5=$('input[name=ko5]:checked').val();
        var tot=parseFloat(lp)+parseFloat(lp2)+parseFloat(lp3)+parseFloat(lp4)+parseFloat(lp5);
        $('input[name=tot_ko]').val(tot);
        $('.tot_ko').html('<b>'+tot+'</b>');
    }
    $(function () {
        $('.select2').select2();
        $('form').parsley();
        $("form").submit(function(event) {
            $(this).parsley().validate();
            if (!$(this).parsley().isValid()){
                event.preventDefault();
            }else if($('#hori-pass1').val()!=$('#hori-pass2').val()){
                event.preventDefault();
                $('#hori-pass2').focus();
            }
        });

    });
</script>