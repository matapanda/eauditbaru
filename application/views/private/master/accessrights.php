<div class="row">
    <div class="col-sm-12">
        <div class="card-box">
            <a href="?" class="btn btn-inverse"><i class="fa fa-angle-double-left"></i> BACK</a>
            <hr>
            <div class="table-responsive">
                <table class="table table-bordered table-striped table-hover">
                    <thead>
                    <tr>
                        <th class="center col-xs-1">#</th>
                        <th>Menu</th>
                        <th class="center col-xs-1">Create</th>
                        <th class="center col-xs-1">Read</th>
                        <th class="center col-xs-1">Update</th>
                        <th class="center col-xs-1">Delete</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $no=1;
                    foreach($menu[0] as $m) {
                        ?>
                        <tr>
                            <th scope="row" class="center"><?=$no++?></th>
                            <th><?=$m['name']?></th>
                            <th colspan="4" class="center r<?=$m['id']?> hand-cursor" onclick="setStatusActive('<?=$m['id']?>','r')"><?=getLabelAccessRights($m['r'])?></th>
                        </tr>
                        <?php
                        if(isset($menu[$m['id']])) {
                            foreach ($menu[$m['id']] as $s) {
                                ?>
                                <tr>
                                    <td></td>
                                    <td><?= $s['name'] ?></td>
                                    <td class="center c<?= $s['id'] ?> hand-cursor" onclick="setStatusActive('<?= $s['id'] ?>','c')"><?= getLabelAccessRights($s['c']) ?></td>
                                    <td class="center r<?= $s['id'] ?> hand-cursor" onclick="setStatusActive('<?= $s['id'] ?>','r')"><?= getLabelAccessRights($s['r']) ?></td>
                                    <td class="center u<?= $s['id'] ?> hand-cursor" onclick="setStatusActive('<?= $s['id'] ?>','u')"><?= getLabelAccessRights($s['u']) ?></td>
                                    <td class="center d<?= $s['id'] ?> hand-cursor" onclick="setStatusActive('<?= $s['id'] ?>','d')"><?= getLabelAccessRights($s['d']) ?></td>
                                </tr>
                                <?php
                            }
                        }
                    }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<script>
    function setStatusActive(_i,_a) {
        $('.'+_a+_i).html('<img src="<?=base_url('assets/loading.gif')?>">');
        $.post('?',{menu:_i,attribute:_a,role:'<?=$id?>'},function (data,status) {
            $('.'+_a+_i).html(data);
        });
    }
</script>