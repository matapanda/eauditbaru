<div class="row">
    <div class="col-sm-12">
        <div class="card-box">
            <?php
            show_alert();
            ?>
            <a onclick="tambah()" class="<?=is_authority(@$access['c'])?> btn btn-inverse" ><i class="fa fa-plus"></i> TAMBAH DATA AKIBAT</a>
            <a href="?" class="btn btn-inverse"><i class="fa fa-refresh"></i></a>
            <hr>
            <form method="get" class="row" action="<?=base_url('master/akibat')?>">
                <div class="col-md-6">
                    <div class="dataTables_wrapper form-inline">
                        <select name="closed" onchange="this.form.submit()" class="form-control input-sm">
                            <option value="" <?=$closed==''?'selected':''?>>Semua Status</option>
                            <option value="t" <?=$closed=='t'?'selected':''?>>Status Aktif</option>
                            <option value="f" <?=$closed=='f'?'selected':''?>>Status Non Aktif</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="dataTables_wrapper form-inline right">
                        <label class="hidden-xs">Search: &nbsp;</label><input type="search" name="search" class="form-control input-sm" autocomplete="off" value="<?=@$search?>">
                    </div>
                </div>

            </form>
            <div class="table-responsive">
               <br> <table class="table table-bordered table-striped table-hover">
                    <thead>
                    <tr>
                        <th class="center col-xs-1">#</th>
                        <th class="center col-xs-1">Kode Akibat</th>
                        <th class="center col-xs-9">Akibat</th>
                        <th class="center col-xs-1">Status</th>
<!--                        <th class="center col-xs-2 --><?//=is_authority(@$access['u'])?><!--">Action</th>-->
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $no=1;
                    foreach($user as $g) {
                        ?>
                        <tr>
                            <td scope="row" class="center"><?=$no?></td>
                            <td class="center">
                                <a href="#" class="emjes-editable" id="<?=isset($access['u'])?"kode$g[id]":''?>"><?=$g['kode']?></a>
                                <script>
                                    $(function () {
                                        $('#kode<?=$g['id']?>').editable({
                                            type: 'text',
                                            name: 'kode',
                                            pk:'<?=$g['id']?>',
                                            url: '?',
                                            title: 'kode Akibat'
                                        });
                                    });
                                </script>
                            </td><td>
                                <a href="#" class="emjes-editable" id="<?=isset($access['u'])?"nama$g[id]":''?>"><?=$g['nama']?></a>
                                <script>
                                    $(function () {
                                        $('#nama<?=$g['id']?>').editable({
                                            type: 'text',
                                            name: 'nama',
                                            pk:'<?=$g['id']?>',
                                            url: '?',
                                            title: 'Akibat'
                                        });
                                    });
                                </script>
                            </td>
                            <td class="center <?= $g['id'] ?> hand-cursor" <?= isset($access['u']) ? "onclick=\"setStatusActive('$g[id]')\"" : '' ?>><?= getLabelStatus($g['status']) ?></td>
                        </tr>
                        <?php
                    $no++;}
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<link href="<?=base_url()?>assets/plugins/custombox/css/custombox.min.css" rel="stylesheet">
<link href="<?=base_url()?>assets/plugins/bootstrap3-editable/css/bootstrap-editable.css" rel="stylesheet">
<script src="<?=base_url()?>assets/plugins/bootstrap3-editable/js/bootstrap-editable.min.js"></script>
<script src="<?=base_url()?>assets/plugins/custombox/js/custombox.min.js"></script>
<script src="<?=base_url()?>assets/plugins/custombox/js/legacy.min.js"></script>
<div id="new-group" class="modal-gradin">
    <button type="button" class="close" onclick="Custombox.close();">
        <span>&times;</span><span class="sr-only">Close</span>
    </button>
    <h4 class="custom-modal-title">Tambah Akibat</h4>
    <div class="custom-modal-text">
        <form action="?" method="post">
            <div class="form-group row">
                <div class=" form-group ">
                    <label for="inputName" class="col-sm-3 form-control-label">Kode Akibat<span
                                class="text-danger">*</span></label>
                    <div class="col-sm-9">
                        <input type="text" name="kode" required class="form-control" placeholder="Kode Akibat">
                    </div>
                    <br>
                </div>
                <div class="col-sm-12">
                    <input type="text" name="nama" required class="form-control" placeholder="Akibat">
                </div>
                <div class="col-sm-12 right" style="padding-top: 1em">
                    <button type="submit" class="btn btn-inverse">SIMPAN</button>
                    <button type="button" onclick="Custombox.close();" class="btn btn-default">BATAL</button>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="modal fade none-border" id="tambahModal">
    <div class="modal-dialog modal-md" style="overflow-y: scroll">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Tambah Akibat</h4>
            </div>
            <div class="modal-body p-20" style="height:100%;overflow-y: scroll" >
                <form method="post" id="tambah_hari">
                    <div class="form-group row">
                        <div class="form-group row">
                            <label for="inputName" class="col-sm-3 form-control-label">Kode Akibat<span
                                        class="text-danger">*</span></label>
                            <div class="col-sm-9">
                                <input type="text" name="kode" required class="form-control" placeholder="Kode Akibat">
                            </div>
                        </div>  <div class="form-group row">
                            <label for="inputName" class="col-sm-3 form-control-label">Nama Akibat<span
                                        class="text-danger">*</span></label>
                            <div class="col-sm-9">
                                <input type="text" name="nama" required class="form-control" placeholder="Akibat">
                            </div>
                        </div>

                    </div>

                    <div class="col-sm-offset-3">
                        <input type="submit" class="btn btn-success save-event" value="Simpan">
                        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Batal</button>
                    </div>
                    <!--                        </div>-->
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    $.fn.editable.defaults.mode = 'popup';
    function setStatusActive(_i) {
        $('.'+_i).html('<img src="<?=base_url('assets/loading.gif')?>">');
        $.post('?',{status:_i},function (data,status) {
            $('.'+_i).html(data);
        });
    }function tambah() {
        $('#tambahModal').modal('show');
    }
    $(function(){
        <?php
        if(!isset($access['u'])){
        ?>
        $(".emjes-editable").editable('option', 'disabled', true);
        <?php
        }
        ?>
    });
</script>