<div class="row">
    <div class="col-sm-12">
        <div class="card-box row">
            <?php
            show_alert();
            ?>
            <div class="col-md-10 col-md-offset-2">
                <form role="form" method="post" enctype="multipart/form-data">
                    <div class="form-group row">
                        <label for="kode" class="col-sm-2 form-control-label">Kode TAO<span class="text-danger">*</span></label>
                        <div class="col-sm-4">
                            <input type="text" name="kode" required class="form-control" id="kode" placeholder="Kode TAO" value="<?=@$user['kode']?>">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="langkah" class="col-sm-2 form-control-label">Keterangan TAO<span class="text-danger">*</span></label>
                        <div class="col-sm-4">
                            <input type="text" name="langkah" class="form-control" id="langkah" placeholder="Keterangan TAO" value="<?=@$user['langkah']?>">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputName" class="col-sm-2 form-control-label">Jenis Audit<span class="text-danger">*</span></label>
                        <div class="col-sm-5">
                            <select class="form-control select2" name="jenis" ng-model="jenis" required>
                            <option value="">
                                    Pilih Jenis Audit
                                </option>
                                <?php
                                foreach ($jenis as $r){
                                    echo "<option value='$r[id]' ".($r['id']==@$user['idj']?'selected':'').">$r[text]</option>";
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="tahapan" class="col-sm-2 form-control-label">Tahapan TAO<span class="text-danger">*</span></label>
                        <div class="col-sm-5">
                            <select class="form-control select2" name="tahapan" ng-model="tahapan" required>
                                <option value="">
                                    Pilih Tahapan Audit
                                </option><?php
                                foreach ($tahapan as $r){
                                    echo "<option value='$r[id]' ".($r['id']==@$user['kt']?'selected':'').">$r[text]</option>";
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputName" class="col-sm-2 form-control-label">Tujuan Audit<span class="text-danger">*</span></label>
                        <div class="col-sm-5">
                            <select name="tujuan" class="select2 form-control" ng-model="tujuan" required>
                                <option value="">
                                    Pilih Tujuan Audit
                                </option><?php
                                foreach ($tujuan as $r){
                                    echo "<option value='$r[id]' ".($r['id']==@$user['idt']?'selected':'').">$r[text]</option>";
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputName" class="col-sm-2 form-control-label">Sasaran Audit<span class="text-danger">*</span></label>
                        <div class="col-sm-5">
                            <select name="sasaran" class="select2 form-control" ng-model="sasaran" required>
                                <option value="">
                                    Pilih Sasaran Audit
                                </option><?php
                                foreach ($sasaran as $r){
                                    echo "<option value='$r[id]' ".($r['id']==@$user['ids']?'selected':'').">$r[text]</option>";
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-8 col-sm-offset-2">
                            <br> <button type="submit" class="btn btn-inverse waves-effect waves-light">Simpan</button>
                            <a href="?" class="btn btn-default waves-effect m-l-5">Batal</a>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-md-12">
                <hr>
            </div>
        </div>
    </div>
</div>
<link href="<?=base_url()?>assets/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="<?=base_url()?>assets/plugins/select2/js/select2.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/plugins/parsleyjs/parsley.min.js"></script>
<script src="<?=base_url()?>assets/plugins/tinymce/jquery.tinymce.min.js"></script>
<script src="<?=base_url()?>assets/plugins/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
    tinymce.init({
        selector: 'textarea',
        height: 200,
        menubar: false,
        plugins: [
            'advlist autolink lists link image charmap print preview anchor',
            'searchreplace visualblocks code fullscreen',
            'insertdatetime media table contextmenu paste code'
        ],
        toolbar: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
        content_css: [
            '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
            '//www.tinymce.com/css/codepen.min.css']
    });
    $(function () {

        $('.select2').select2();
        $('form').parsley();
        $("form").submit(function(event) {
            $(this).parsley().validate();
            if (!$(this).parsley().isValid()){
                event.preventDefault();
            }
        });

    });
</script>