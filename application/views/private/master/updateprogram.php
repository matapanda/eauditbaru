<div class="row">
    <div class="col-sm-12">
        <div class="card-box row">
            <?php
            show_alert();
            ?>
            <div class="col-sm-12">
                <form role="form" method="post">
                    <div class="form-group row">
                        <label for="inputName" class="col-sm-2 form-control-label">Kode Program<span
                                class="text-danger">*</span></label>
                        <div class="col-sm-5">
                            <input type="text" name="kode" required class="form-control" value="<?=@$keg['kode']?>">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputName" class="col-sm-2 form-control-label">Nama Program<span
                                class="text-danger">*</span></label>
                        <div class="col-sm-5">
                            <input type="text" name="nama" required class="form-control" value="<?=@$keg['nama']?>">
                            <input type="hidden" name="jml_program" required class="form-control" id="jml_program" value="<?=@$jml_kr?$jml_kr:1?>">
                            <?php
                            if(@$keg['id']){
                                ?>
                                <input type="hidden" name="ids" value="<?=$keg['id']?>">

                                <?php
                            }
                            ?>
                           </div>
                    </div>
                    <hr>
                    <?php
                    if(!@$resiko_kr){
                        ?>
                    <div class="col-md-12 tambahan">
                        <div class="form-group 1">
                            <h3>Resiko <button type="button" onclick="tambah_resiko(1)" class="btn btn-xs btn-inverse"><i class="fa fa-plus"></i></button></h3> <br>

                            <div class="form-group resiko1">

                                <div class="form-group row">
                                    <label for="inputName" class="col-sm-2 right form-control-label">Resiko<span
                                                class="text-danger">*</span></label>
                                    <div class="col-sm-6">
                                        <select class="select2 form-control select2-multiple" name="resiko1" data-placeholder="Pilih Tipe Resiko" required>
                                            <option value="">Pilih Tipe Resiko</option>
                                            <?php
                                            foreach ($tipe_resiko as $v):
                                                ?>
                                                <option value="<?= $v['id'] ?>" <?= @$user['resiko']==$v['id']?'selected':'' ?>><?= "$v[nama]" ?></option>
                                            <?php
                                            endforeach;
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputName" class="col-sm-2 right form-control-label">Penyebab Resiko<span
                                                class="text-danger">*</span></label>
                                    <div class="col-sm-6">
                                        <select class="select2 form-control select2-multiple" name="p_resiko1" data-placeholder="Pilih Penyebab Resiko" required>
                                            <option value="">Pilih Penyebab Resiko</option>
                                            <?php
                                            foreach ($pe_resiko as $v):
                                                ?>
                                                <option value="<?= $v['id'] ?>" <?= @$user['p_resiko']==$v['id']?'selected':'' ?>><?= "$v[nama]" ?></option>
                                            <?php
                                            endforeach;
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputName" class="col-sm-2 right form-control-label">Dampak Resiko<span
                                                class="text-danger">*</span></label>
                                    <div class="col-sm-6">
                                        <select class="select2 form-control select2-multiple" name="da_resiko1" data-placeholder="Pilih Dampak Resiko" required>
                                            <option value="">Pilih Dampak Resiko</option>
                                            <?php
                                            foreach ($da_resiko as $v):
                                                ?>
                                                <option value="<?= $v['id'] ?>" <?= @$r['da_resiko']==$v['id']?'selected':'' ?>><?= "$v[nama]" ?></option>
                                            <?php
                                            endforeach;
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group kendali1">
                                        <div class="form-group row">
                                   <label for="inputName" class="col-sm-2 right form-control-label">Kendali Seharusnya <span
                                               class="text-danger">*</span></label>
                                    <div class="col-sm-6">
                                        <select class="select2 form-control select2-multiple" multiple="multiple" multiple data-placeholder="Pilih Kendali Seharusnya" name="kendali_s1[]" data-placeholder="Pilih Kendali Seharusnya" required>
                                            <option value="">Pilih Kendali Seharusnya</option>
                                            <?php
                                            foreach ($kendali as $v):
                                                ?>
                                                <option value="<?= $v['id'] ?>" <?= @$user['kendali_s']==$v['id']?'selected':'' ?>><?= "$v[nama]" ?></option>
                                            <?php
                                            endforeach;
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                </div>
                        </div>
                    </div>
                    </div>
                        <?php
                    }else{
                        for($no=1;$no<=$jml_kr;$no++){
                        foreach($resiko_kr as $r){
                            ?>
                            <div class="col-md-12 tambahan">
                                <div class="form-group <?=$no?>">
                                    <h3>Resiko <button type="button" onclick="tambah_resiko(<?=$no?>)" class="btn btn-xs btn-inverse"><i class="fa fa-plus"></i></button></h3> <br>

                                    <div class="form-group resiko<?=$no?>">

                                        <div class="form-group row">
                                            <label for="inputName" class="col-sm-2 right form-control-label">Resiko<span
                                                        class="text-danger">*</span></label>
                                            <div class="col-sm-6">
                                                <select class="select2 form-control select2-multiple" name="resiko<?=$no?>" data-placeholder="Pilih Tipe Resiko" required>
                                                    <option value="">Pilih Tipe Resiko</option>
                                                    <?php
                                                    foreach ($tipe_resiko as $v):
                                                        ?>
                                                        <option value="<?= $v['id'] ?>" <?= @$r['resiko']==$v['id']?'selected':'' ?>><?= "$v[nama]" ?></option>
                                                    <?php
                                                    endforeach;
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="inputName" class="col-sm-2 right form-control-label">Penyebab Resiko<span
                                                        class="text-danger">*</span></label>
                                            <div class="col-sm-6">
                                                <select class="select2 form-control select2-multiple" name="p_resiko<?=$no?>" data-placeholder="Pilih Penyebab Resiko" required>
                                                    <option value="">Pilih Penyebab Resiko</option>
                                                    <?php
                                                    foreach ($pe_resiko as $v):
                                                        ?>
                                                        <option value="<?= $v['id'] ?>" <?= @$r['p_resiko']==$v['id']?'selected':'' ?>><?= "$v[nama]" ?></option>
                                                    <?php
                                                    endforeach;
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="inputName" class="col-sm-2 right form-control-label">Dampak Resiko<span
                                                        class="text-danger">*</span></label>
                                            <div class="col-sm-6">
                                                <select class="select2 form-control select2-multiple" name="da_resiko<?=$no?>" data-placeholder="Pilih Dampak Resiko" required>
                                                    <option value="">Pilih Dampak Resiko</option>
                                                    <?php
                                                    foreach ($da_resiko as $v):
                                                        ?>
                                                        <option value="<?= $v['id'] ?>" <?= @$r['da_resiko']==$v['id']?'selected':'' ?>><?= "$v[nama]" ?></option>
                                                    <?php
                                                    endforeach;
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                            <div class="form-group kendali<?=$no?>">
                                                <?php
                                                foreach($resiko_kr as $k) {
                                                    if($k['urutan']==$no){
                                                        ?>
                                                    <div class="form-group row">
                                                        <label for="inputName"
                                                               class="col-sm-2 right form-control-label">Kendali
                                                            Seharusnya <span
                                                                    class="text-danger">*</span></label>
                                                        <div class="col-sm-6">
                                                            <select class="select2 form-control select2-multiple" multiple="multiple" multiple data-placeholder="Pilih Kendali Seharusnya"
                                                                    name="kendali_s<?= $no ?>[]"
                                                                    required>
                                                                <?php
                                                                foreach ($kendali as $v):
                                                                    ?>
                                                                    <option value="<?= $v['id'] ?>" <?= in_array($v['id'],json_decode($k['kendali_s'],true))?'selected':''?>><?= "$v[nama]" ?></option>
                                                                <?php
                                                                endforeach;
                                                                ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                        <?php
                                                    }
                                                }
                                                    ?>
                                            </div>
                                    </div>
                                </div>
                            </div>
                            <?php
                        $no++;
                        }
                    }
                    }
                    ?>
                    <div class="form-group row">
                        <div class="col-sm-8 col-sm-offset-4">
                            <button type="submit" class="btn btn-inverse waves-effect waves-light">Simpan</button>
                            <a href="?" class="btn btn-default waves-effect m-l-5">Batal</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<link href="<?= base_url() ?>assets/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="<?= base_url() ?>assets/plugins/select2/js/select2.min.js"></script>
<script type="text/javascript">
    $('.select2').select2();
    function tambah_kendali(i){
        var no=i;
        $.post('?',{kendali:i},function (data,status) {
            $('.kendali'+i).append(data);
            $('.select2').select2();
        });
    }function tambah_resiko(i){
        var no=$('#jml_program').val();
        var akhir=parseFloat(no)+1;
        $.post('?',{resiko:akhir},function (data,status) {
            $('.tambahan').append(data);
            $('.select2').select2();
            $('#jml_program').val(akhir);
            $("input[name='resiko"+akhir+"']").focus();
        });
    }
</script>