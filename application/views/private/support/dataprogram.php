<div class="row">
    <div class="col-sm-12">
        <div class="card-box row">
            <?php
            show_alert();
            ?>
            <div class="col-md-6 col-md-offset-3">
                <form role="form" method="post">
                    <div class="form-group row">
                        <label for="inputEmail" class="col-sm-4 form-control-label">Email<span class="text-danger">*</span></label>
                        <div class="col-sm-7">
                            <input type="email" name="email" required parsley-type="email" class="form-control" id="inputEmail" placeholder="Email">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputName" class="col-sm-4 form-control-label">Name<span class="text-danger">*</span></label>
                        <div class="col-sm-7">
                            <input type="text" name="name" required parsley-type="text" class="form-control" id="inputName" placeholder="Name">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-4 form-control-label">Username<span class="text-danger">*</span></label>
                        <div class="col-sm-7">
                            <input type="text" name="username" autocomplete="off" required parsley-type="text" placeholder="Username" class="form-control">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="hori-pass1" class="col-sm-4 form-control-label">Password<span class="text-danger">*</span></label>
                        <div class="col-sm-7">
                            <input id="hori-pass1" name="password" type="password" required placeholder="Password" class="form-control">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="hori-pass2" class="col-sm-4 form-control-label">Confirm Password<span class="text-danger">*</span></label>
                        <div class="col-sm-7">
                            <input data-parsley-equalto="#hori-pass1" name="konfirm" required type="password" placeholder="Password" class="form-control" id="hori-pass2">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="hori-pass2" class="col-sm-4 form-control-label">Role</label>
                        <div class="col-sm-7">
                            <select name="role[]" class="select2 form-control select2-multiple" multiple="multiple" multiple data-placeholder="Choose Access Role...">
                                <?php
                                foreach ($role as $r){
                                    echo "<option value='$r[id]'>$r[name]</option>";
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-8 col-sm-offset-4">
                            <button type="submit" class="btn btn-primary waves-effect waves-light">Save</button>
                            <a href="?" class="btn btn-default waves-effect m-l-5">Cancel</a>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-md-12">
                <hr>
            </div>
        </div>
    </div>
</div>
<link href="<?=base_url()?>assets/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="<?=base_url()?>assets/plugins/select2/js/select2.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/plugins/parsleyjs/parsley.min.js"></script>
<script type="text/javascript">
    $(function () {
        $('.select2').select2();
        $('form').parsley();
        $("form").submit(function(event) {
            $(this).parsley().validate();
            if (!$(this).parsley().isValid()){
                event.preventDefault();
            }else if($('#hori-pass1').val()!=$('#hori-pass2').val()){
                event.preventDefault();
                $('#hori-pass2').focus();
            }else if($('#parsley-id-username').html().length>5){
                event.preventDefault();
                $('input[name=username]').focus();
            }
        });
        $('input[name=email]').focus();
        $('input[name=username]').on('keyup',function () {
            var _usn=$(this).val();
            if($('#parsley-id-username').length==0){
                $('<ul class="parsley-errors-list" id="parsley-id-username" style="display: inherit!important;"></ul>').insertAfter('input[name=username]');
            }
            if(_usn.length>4){
                $.post('?',{is_exist:_usn},function (data,status) {
                    if(data.jumlah>0){
                        $('#parsley-id-username').html('<li class="parsley-required">Username already exist!.</li>');
                    }else{
                        $('#parsley-id-username').html('');
                    }
                },'json');
            }
        });

    });
</script>