<div class="row">
    <div class="col-sm-12">
        <div class="card-box">
            <?php
            show_alert();
            ?>
            <a href="<?=base_url()."support/program?i=true"?>" class="<?=is_authority(@$access['c'])?> btn btn-inverse"><i class="fa fa-plus"></i> ADD NEW PROGRAM</a>
            <a href="?" class="btn btn-primary"><i class="fa fa-refresh"></i> RELOAD</a>
            <hr>
            <div class="table-responsive">
                <table class="table table-bordered table-striped table-hover dt">
                    <thead>
                    <tr>
                        <th class="center col-xs-2">Kode</th>
                        <th class="center col-xs-2">Jenis Audit</th>
                        <th class="center col-xs-2">Tahapan</th>
                        <th class="center col-xs-2">Tujuan</th>
                        <th class="center col-xs-2">Status</th>
                        <th class="center col-xs-2">Action</th>

                        <!--                        <th class="center col-xs-2 --><?//=is_authority(@$access['u'])?><!--">Action</th>-->
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $no=1;
                    foreach($user as $g) {
                        ?>
                        <tr>
                            <td scope="row" class="center"><?=$g['kode']?></td>
                            <td class="center"><?=$g['jenis']?></td>
                            <td class="center"><?=$g['tahapan']?></td>
                            <td class="center"><?=$g['tujuan']?></td>
                            <td class="center"><?=getLabelStatus($g['status'])?></td>
                            <td class="center"><a href="<?=base_url()."support/program?e=".$g['id']?>" class="btn btn-xs btn-block  btn-inverse">UPDATE DATA</a></td>
<!--                        <td class="--><?//=is_authority(@$access['u'])?><!-- center"><a href="?u=--><?//=$g['id']?><!--" class="btn btn-xs btn-block btn-inverse">UPDATE DATA</a></td>-->
                        </tr>
                        <?php
                        $no++;}
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<link href="<?=base_url()?>assets/plugins/custombox/css/custombox.min.css" rel="stylesheet">
<link href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css" rel="stylesheet">
<link href="<?=base_url()?>assets/plugins/bootstrap3-editable/css/bootstrap-editable.css" rel="stylesheet">
<script src="<?=base_url()?>assets/plugins/bootstrap3-editable/js/bootstrap-editable.min.js"></script>
<script src="<?=base_url()?>assets/plugins/custombox/js/custombox.min.js"></script>
<script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
<script src="<?=base_url()?>assets/plugins/custombox/js/legacy.min.js"></script>

<script>
    $.fn.editable.defaults.mode = 'popup';
    function setStatusActive(_i) {
        $('.'+_i).html('<img src="<?=base_url('assets/loading.gif')?>">');
        $.post('?',{status:_i},function (data,status) {
            $('.'+_i).html(data);
        });
    }
    $(document).ready(function() {
        $('.dt').DataTable();
    } );
</script>