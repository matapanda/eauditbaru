<?php
foreach ($notif as $n) {
    $icon = "";
    switch ($n['type']) {
        case 'rencana':
            $icon = "task";
            break;
    }
    ?>
    <div class="alert alert-<?=$n['readed_at']?'readed':'info'?> fade in hand-cursor" onclick="window.location='?id=<?=$n['id']?>'" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">×</span>
        </button>
        <strong><?= $n['message'] ?></strong>
        <p style="text-align: right"><?= format_waktu($n['created_at']) ?></p>
    </div>
    <?php
}
?>