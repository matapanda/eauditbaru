<div class="row">
    <div class="col-sm-12">
    <div class="card-box">
            <hr>
            <form method="get" class="row hidden-print" action="?">
                  <label class="col-sm-1">Filter: </label>
                        <div class="input-daterange input-group col-sm-4" id="date-range">
                            <input type="text" readonly class="form-control input-sm col-sm-3" name="start" onchange="this.form.submit()" value="<?=@$start?>">
                            <span class="input-group-addon input-sm">~</span>
                            <input type="text" readonly class="form-control input-sm col-sm-3" name="end" onchange="this.form.submit()" value="<?=@$end?>">
                        </div>
                <br>

                        <label class="col-sm-1">No SPT: </label>
                        <div class="input-daterange input-group">
                            <input type="text" class="form-control input-sm col-sm-3" name="search" value="<?=@$search?>">
                        </div>
                <input type="submit" class="hidden">
                </form>
        <br> <br>
            <div class="table-responsive">
                <table class="table table-bordered table-striped table-hover">
                    <thead>
                    <tr>
                        <th class="center col-xs-2">Tanggal SPT</th>
                        <th class="center col-xs-2">Nomor SPT</th>
                        <th class="center col-xs-3">Judul SPT</th>
                        <th class="center col-xs-4">Progress</th>
                        <th class="center"></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $act=array();

                    if(@$data){
                    foreach($laporan as $l){
                        if(isset($role[$l['id']]['r'])){
                            $act[]=$l;
                        }
                    }
                    $no=1;
                        foreach($data as $d):
                            $progress_disetujui=0;
                            if($d['disetujui']>0){
                                $progress_disetujui=($d['disetujui']/$d['total']*100);
                            }
                            $d['realisasi']=$d['realisasi']-$d['disetujui'];
                            $progress_realisasi=0;
                            if($d['realisasi']>0){
                                $progress_realisasi=($d['realisasi']/$d['total']*100);
                            }
                            ?>
                            <tr class="data<?=$d['id']?>">
                                <td class="center"><?=format_waktu($d['spt_date'])?></td>
                                <td class="center"><?=$d['spt_no']?></td>
                                <td class=""><?=$d['judul']?></td>
                                <td class="">
                                    <div class="progress">
                                        <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="<?=$d['disetujui']?>" aria-valuemin="0" aria-valuemax="<?=$d['total']?>" style="width: <?=$progress_disetujui?>%">
                                            <span class="sr-only"><?=$progress_disetujui?>% disetujui</span>
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="<?=$d['realisasi']?>" aria-valuemin="0" aria-valuemax="<?=$d['total']?>" style="width: <?=$progress_realisasi?>%">
                                            <span class="sr-only"><?=$progress_realisasi?>% realisasi</span>
                                        </div>
                                    </div>
                                </td>
                                <td class="center">
                                    <?php
                                    foreach($laporan as $l){
                                        if(isset($role[$l['id']]['r'])){
                                            ?>
                                            <a href="<?=base_url('laporan/export')?>/<?=$l['id']?>/<?=$d['id']?>/<?=rawurlencode($l['name'])?>" target="_blank" class="btn btn-sm btn-default btn-block"><?=$l['name']?></a>
                                            <?php
                                        }
                                    }
                                    ?>
                                </td>
                            </tr>
                        <?php
                        endforeach;
                    }
                    ?>
                    </tbody>

                </table>
            </div>
        </div>
    </div>
</div><link href="<?=base_url()?>assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css"/>
<script src="<?=base_url()?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script>
    $('#date-range').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true,
        todayHighlight: true
    });

</script>