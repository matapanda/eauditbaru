<style>
    td{
       vertical-align: top;
    }
</style>
<h1 style="text-align: center">LAPORAN PROSES PENYELESAIAN PENUGASAN</h1>
<table>
    <tr><td colspan="2">Nomor Laporan</td><td>:</td><td><?=null?></td></tr>
    <tr><td colspan="2">Tahun Laporan</td><td>:</td><td><?=null?></td></tr>
    <tr><td>1</td><td>Nomor dan Tanggal Kartu Penugasan</td><td>:</td><td><?=@$laporan['kp_no']?> <?=format_tanggal(@$laporan['kp_date'])?></td></tr>
    <tr><td>3</td><td>Nomor dan Tanggal Surat Penugasan</td><td>:</td><td><?=@$laporan['spt_no']?> <?=format_tanggal(@$laporan['spt_date'])?></td></tr>
    <tr><td>4</td><td>Judul Penugasan</td><td>:</td><td><?=@$laporan['judul']?></td></tr>
    <tr><td>5</td><td>Tujuan</td><td>:</td><td>
            <ol style="list-style-type: lower-alpha;">
            <?php
            foreach($tujuan as $t){
                echo "<li>$t[ket]</li>";
            }
            ?>
            </ol>
        </td></tr>
    <tr><td>6</td><td>Sasaran</td><td>:</td><td>
            <ol style="list-style-type: lower-alpha;">
                <?php
                foreach($sasaran as $s){
                    echo "<li>$s[ket]</li>";
                }
                ?>
            </ol>
        </td></tr>
    <tr><td>7</td><td>Program Kerja Audit</td><td></td><td></td></tr>
    <?php
    $temp_tahapan = "";
    $alp_tahapan=array('','A','B','C');
    foreach ($program as $t):
        if ($temp_tahapan != $t['tahapan']) {
            $temp_tahapan = $t['tahapan'];
            ?>
            <tr>
                <td></td><td colspan="3" style="font-weight: bold"><?=$alp_tahapan[$t['tahapan']]?>. Program Kerja <?= $t['ket_tahapan'] ?></td>
            </tr>
            <?php
            $no=1;
        }
        ?>
        <tr>
            <td class="center"></td>
            <td colspan="3">
                <table>
                    <tr><td rowspan="7">7.<?=$alp_tahapan[$t['tahapan']]?>.<?= $no ?></td><td>Kode TAO</td><td>:</td><td><?=$t['kode_tao']?></td></tr>
                    <tr><td>TAO</td><td>:</td><td><?=$t['tao']?></td></tr>
                    <tr><td>Kode langkah kerja</td><td>:</td><td><?=$t['kode_tao'].$no++?></td></tr>
                    <tr><td>Langkah kerja</td><td>:</td><td><?=$t['langkah']?></td></tr>
                    <tr><td>Dilaksanakan oleh</td><td>:</td><td><?=$t['realisasi_by']?></td></tr>
                    <tr><td>Tanggal</td><td>:</td><td><?=format_tanggal($t['realisasi_date'])?></td></tr>
                    <tr><td>Kesimpulan</td><td>:</td><td><?=str_replace(PHP_EOL,'<br>',@$t['kesimpulan'])?></td></tr>
                </table>
            </td>
        </tr>
        <?php
    endforeach;
    ?>
</table>