<div class="row">
    <div class="col-sm-12  col-print-12">
        <h1 class="center visible-print" style="margin:0">PKPT <?=@$tahun?></h1>
        <div class="card-box">
            <?php
            show_alert();
            ?>
            <hr>
            <form method="get" class="row hidden-print" action="<?=base_url('laporan/pr_p')?>">
                <div class="col-md-2">
                    <div class="dataTables_wrapper form-inline">
                        <h4 class="label-control">Periode : </h4>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="dataTables_wrapper form-inline">
                        <select name="periode" onchange="this.form.submit()"  class="form-control select2">
                            <option value="">Semua Periode</option>
                            <?php
                            foreach($list_periode as $k){
                                ?>
                                <option value="<?=$k['id']?>" <?=@$periode_s==$k['id']?'selected':''?>><?=$k['nama']?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                </div>

                <br>
                <br><div class="col-md-2">
                    <div class="dataTables_wrapper form-inline">
                        <h4 class="label-control">Program : </h4>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="dataTables_wrapper form-inline">
                        <select name="program" onchange="this.form.submit()"  class="form-control select2">
                            <option value="">Semua Program</option>
                            <?php
                            foreach($list_program as $k){
                                ?>
                                <option value="<?=$k['id']?>" <?=@$program_s==$k['id']?'selected':''?>><?=$k['nama_s']?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                </div>

                <br>
                <br>

                <div class="col-md-2">
                    <div class="dataTables_wrapper form-inline">
                        <h4 class="label-control">Kegiatan : </h4>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="dataTables_wrapper form-inline">
                        <select name="kegiatan" onchange="this.form.submit()"  class="form-control select2">
                            <option value="">Semua Kegiatan</option>
                            <?php
                            foreach($list_kegiatan as $k){
                                ?>
                                <option value="<?=$k['id']?>" <?=@$kegiatan_s==$k['id']?'selected':''?>><?=$k['nama_s']?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                </div>

            </form>
            <?php
            if(@$closed=='t'){
                ?>

                <div class="table-responsive">
                    <br> <table class="table table-bordered table-striped table-hover col-print-12">
                        <thead>
                        <tr>
                            <th class="center col-xs-1 col-print-1">#</th>
                            <th class="center col-xs-3 col-print-1">Periode</th>
                            <th class="center col-xs-3 col-print-2">Satker</th>
                            <th class="center col-xs-3 col-print-6">Nama Kegiatan</th>
                            <th class="center col-xs-3 col-print-1">Resiko</th>
                            <th class="center col-xs-1 col-print-1">Skala Dampak</th>
                            <th class="center col-xs-1 col-print-1">Skala Kemungkinan</th>
                            <th class="center col-xs-1 col-print-1">Score Resiko</th>
                            <th class="center col-xs-1 col-print-1">Tingkat Resiko</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $no=1;
                        $nama='';
                        $periode='';
                        $ket='';
                        foreach($jml as $g) {
                            ?>
                            <tr>
                                <td class="center"><?=$no?></td>
                                <td class="">
                                    <?php
                                    if($ket!=$g['ket']){
                                        echo @$g['periode'] ;
                                    }?>
                                </td>
                                <td class="">
                                    <?php
                                    if($ket!=$g['ket']){
                                        $ket=$g['ket'];
                                        echo @$g['ket'] ;
                                    }?>
                                </td><td class="">
                                    <?php
                                    if($nama!=$g['nama_s']){
                                        $nama=$g['nama_s'];
                                        echo @$g['nama_s'] ;
                                    }?>
                                </td>
                                <td class="">
                                    <?=@$g['nama_r']?>
                                </td>
                                <td class="right">
                                    <?=format_uang(@$g['sd'])?>
                                </td>
                                <td class="right">
                                    <?=format_uang(@$g['sk'])?>
                                </td>
                                <td class="right">
                                    <?=format_uang(@$g['sk']*$g['sd'])?>
                                </td>
                                <td class="right">
                                    <?php
                                    if($g['sk']*$g['sd']<5){
echo 'RENDAH';
                                    }
                                    elseif($g['sk']*$g['sd']>9){
                                        echo 'TINGGI';
                                    }
                                    else{
                                        echo 'SEDANG';

                                    }?>
                                </td>

                            </tr>
                            <?php
                            $no++;}
                        ?>
                        </tbody>
                    </table>
                </div>
                <?php
            }else{
                ?>

                <div id="container" style="min-width: 100%; height: 600px; margin: 0 auto"></div>
                <?php
            }
            ?>
        </div>
    </div>
</div>
<link href="<?=base_url()?>assets/plugins/custombox/css/custombox.min.css" rel="stylesheet">
<link href="<?=base_url()?>assets/plugins/bootstrap3-editable/css/bootstrap-editable.css" rel="stylesheet">
<script src="<?=base_url()?>assets/plugins/bootstrap3-editable/js/bootstrap-editable.min.js"></script>
<script src="<?=base_url()?>assets/plugins/custombox/js/custombox.min.js"></script>
<script src="<?=base_url()?>assets/plugins/custombox/js/legacy.min.js"></script>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<link href="<?=base_url()?>assets/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="<?=base_url()?>assets/plugins/select2/js/select2.full.min.js"></script>

<script>
    $.fn.editable.defaults.mode = 'inline';
    function setStatusActive(_i) {
        $('.'+_i).html('<img src="<?=base_url('assets/loading.gif')?>">');
        $.post('?',{status:_i},function (data,status) {
            $('.'+_i).html(data);
        });
    }
    $(function(){
        <?php
        if(!isset($access['u'])){
        ?>
        $(".emjes-editable").editable('option', 'disabled', true);
        <?php
        }
        ?>
    });
//samp sini belum
    $('.select2').select2();
    <?php
    if(@$kejadian){

    ?>
    Highcharts.chart('container', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Laporan Kegiatan'
        },
        xAxis: {
            categories: [
                <?php
                foreach ($kejadian as $k) {
                    echo $k['nama'] . ',';
                }
                ?>
            ],
            crosshair: true
        },
        yAxis: {
            min: 0,
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: [
            <?php
            if (@$kejadian) {

                foreach ($kejadian as $s) {
                    $this->db->where('kr.id', $s['id']);
                    $this->db->where('s.status', true);
                    $this->db->join('support.' . strtolower($title) . '_satker s', 'kr.id=s.id');
                    $this->db->join('support.setting_' . strtolower($title) . ' sp', 'sp.id=s.kegiatan', 'left');
                    $this->db->distinct();
                    $this->db->select('kr.*,sp.nama as nama_s');
                    $jml = $this->db->get('support.' . strtolower($title) . '_kr kr')->result_array();
                    echo "{name: '$s[nama_s]',
                    data:[";
                    foreach ($jml as $j) {
                        echo $j['sk'] * $j['sd'] . ',';
                    }
                    echo "]
                    },
                    ";
                }
            }
            ?>]
        //
        //     series: [{
        //         name: 'Tokyo',
        //     data: [49.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1, 95.6, 54.4]
        //
        // }, {
        //     name: 'New York',
        //         data: [83.6, 78.8, 98.5, 93.4, 106.0, 84.5, 105.0, 104.3, 91.2, 83.5, 106.6, 92.3]
        //
        // }, {
        //     name: 'London',
        //         data: [48.9, 38.8, 39.3, 41.4, 47.0, 48.3, 59.0, 59.6, 52.4, 65.2, 59.3, 51.2]
        //
        // }, {
        //     name: 'Berlin',
        //         data: [42.4, 33.2, 34.5, 39.7, 52.6, 75.5, 57.4, 60.4, 47.6, 39.1, 46.8, 51.1]
        //
        // }]
    });
    <?php
    }

    ?>
</script>