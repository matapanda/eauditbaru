<div class="row">
    <div class="col-sm-12">
        <div class="card-box row"><h2 class="visible-print center">Rekap Temuan Dan Tindak Lajut</h2>

            <form method="post" class="col-sm-8 col-xs-11 hidden-print">
                <div class="form-group row">


                    <div class="col-sm-2">
                        <label>Tahun Awal</label>
                        <select name="tahun_a" class="select2 form-control" required>
                            <?php
                            for($i=date('Y');$i>(date('Y')-5);$i--){
                                echo "<option value='$i' ".($i==@$tahun_as?'selected=""':"").">$i</option>";
                            }
                            ?>
                        </select>
                    </div>
                    <div class="col-sm-1">
                        <label>~</label>
                    </div>
                    <div class="col-sm-2">
                        <label>Tahun Akhir</label>
                        <select name="tahun_e" class="select2 form-control" required>
                            <?php
                            for($i=date('Y');$i>(date('Y')-5);$i--){
                                echo "<option value='$i' ".($i==$tahun_e?'selected=""':"").">$i</option>";
                            }
                            ?>
                        </select>
                    </div>
                    <div class="col-sm-4">
                        <input type="hidden" name="type">
                        <button type="submit" class="hidden">SUBMIT</button>
                        <button type="button" data-type="post" class="btn btn-submit-data btn-primary waves-effect waves-light">VIEW</button>
                        <button type="button" onclick="window.print()" class="btn btn-inverse hidden-print hidden-xs"><span class="ace-icon fa fa-print icon-on-right bigger-110"></span> PRINT</button>
                    </div>
                </div>
            </form>
            <div class="col-xs-12">
                <hr>
                <?php
                if(isset($data)){
                ?>
                <div class="table-responsive">
                    <table class="table table-bordered table-striped table-hover">
                        <thead>
                        <tr>
                            <th class="center hidden-print" rowspan="3">#</th>
                            <th class="center col-xs-1 hidden-print" rowspan="3">Tahun</th>
                            <th class="center col-xs-1 " rowspan="3">Kode Temuan</th>
                            <th class="center col-xs-1">HAPSEM</th>
                            <th class="center col-xs-1" colspan="3">SELESAI DITINDAKLANJUTI</th>
                            <th class="center col-xs-1" colspan="3">BELUM SESUAI REKOMENDASI</th>
                            <th class="center col-xs-1" colspan="3">BELUM DITINDAKLANJUTI</th>
                            <th class="center col-xs-1" colspan="3">TIDAK DAPAT DITINDAKLANJUTI</th>
                            <th class="center col-xs-1" rowspan="3">KETERANGAN</th>
                        </tr>
                        <tr>
                            <th class="center col-xs-1">Lingkup Pemeriksaan</th>
                            <th class="center col-xs-1" rowspan="2">Jml Temuan</th>
                            <th class="center col-xs-1" rowspan="2">Jml Saran</th>
                            <th class="center col-xs-1" rowspan="2">Nilai</th>
                            <th class="center col-xs-1" rowspan="2">Jml Temuan</th>
                            <th class="center col-xs-1" rowspan="2">Jml Saran</th>
                            <th class="center col-xs-1" rowspan="2">Nilai</th>
                            <th class="center col-xs-1" rowspan="2">Jml Temuan</th>
                            <th class="center col-xs-1" rowspan="2">Jml Saran</th>
                            <th class="center col-xs-1" rowspan="2">Nilai</th>
                            <th class="center col-xs-1" rowspan="2">Jml Temuan</th>
                            <th class="center col-xs-1" rowspan="2">Jml Saran</th>
                            <th class="center col-xs-1" rowspan="2">Nilai</th>
                        </tr>
                        <tr>
                            <th class="center col-xs-1">Kelompok Temuan</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $no=1;
                        foreach ($data as $i=>$r) {
                                $optgroup = null;
                                    if ($optgroup != $r[$i]['tahun']){
                                        $optgroup = $r[$i]['tahun'];
                                    ?>
                                        <tr><td colspan="17"><b>REKAPITULASI HASIL PEMERIKSAAN AUDIT PERIODE <?=$r[$i]['tahun']?>, TAHUN <?=$tahun_as?> - TAHUN  <?=$tahun_e?> </b></td></tr>
                            <?php
                            }
                            ?>
                            <tr>
                                <td class="center hidden-print"><?=$no;?></td>
                                <td class="center hidden-print"><?=$r[$i]['tahun']?></td>
                                <td class="center"><?=$r[$i]['kode']?></td>
                                <td class=""><?=$r[$i]['temuan']?></td>
                                <td class="center"><?=format_uang($r[$i]['jml_1'])?></td>
                                <td class="center"><?=format_uang($r[$i]['saran_1'])?></td>
                                <td class="right"><?=format_uang($r[$i]['nilai_1'])?></td>
                                <td class="center"><?=format_uang($r[$i]['jml_2'])?></td>
                                <td class="center"><?=format_uang($r[$i]['saran_2'])?></td>
                                <td class="right"><?=format_uang($r[$i]['nilai_2'])?></td>
                                <td class="center"><?=format_uang($r[$i]['jml_3'])?></td>
                                <td class="center"><?=format_uang($r[$i]['saran_3'])?></td>
                                <td class="right"><?=format_uang($r[$i]['nilai_3'])?></td>
                                <td class="center"><?=format_uang($r[$i]['jml_4'])?></td>
                                <td class="center"><?=format_uang($r[$i]['saran_4'])?></td>
                                <td class="right"><?=format_uang($r[$i]['nilai_4'])?></td>
                                <td class=""><?=$r[$i]['keterangan']?></td>
                            </tr>
                            <?php
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <?php
            }
            ?>
        </div>
    </div>
</div>
<div class="modal fade" id="uploadModal" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-large" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
            </div>
        </div>
    </div>
</div>
<link href="<?=base_url()?>assets/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css"/>
<link href="<?=base_url()?>assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="<?=base_url()?>assets/plugins/select2/js/select2.min.js"></script>
<script src="<?=base_url()?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script>
    function detail(_id) {
        var _modal=$('#uploadModal');
        var _title=$('tr#'+_id).children('td').eq(1).text()+' - '+$('tr#'+_id).children('td').eq(2).text();
        $('.modal-title',_modal).html(_title);
        $.post('?tao='+_id,function(data,status){
            $('.modal-body',_modal).html(data);
            _modal.modal('show');
        });
    }
    $('select').select2();
    $('#date-range').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true,
        todayHighlight: true
    });
    $('.btn-submit-data').on('click',function (e) {
        var _typ=$(this).data('type');
        if(_typ=='post'){
            $($(this).closest('form')).attr('target','_self');
            $($(this).closest('form')).attr('action','');
        }else{
            var _act=$(this).data('mode');
            $($(this).closest('form')).attr('target','_blank');
            $('[name=type]',$(this).closest('form')).val(_act);
            $($(this).closest('form')).attr('action',"<?=base_url('export/stok')?>");
        }
        $($(this).closest('form')).attr('method',_typ);
        $('[type=submit]',$(this).closest('form')).click();
    });
</script>