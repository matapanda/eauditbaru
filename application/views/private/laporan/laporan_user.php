<div class="row">
    <div class="col-sm-12">
        <div class="card-box">
            <h2 class="visible-print center">MONITORING PENUGASAN</h2>
            <?php
            show_alert();
            ?>

            <div class="col-md-6 hidden-print">
                <form method="get" class="row" action="<?= base_url('laporan/per_user') ?>">
                    <div class="col-md-6">
                        <div class="col-md-6">
                            <label>Tahun PKPT</label>
                        </div>
                        <div class="dataTables_wrapper form-inline">
                            <select name="tahun" onchange="this.form.submit()" class="form-control input-sm">
                                <?php
                                foreach ($tahun as $t) {
                                    ?>
                                    <option value="<?= $t['tahun'] ?>" <?=$selected_tahun==$t['tahun']?'selected=""':""?>><?= $t['tahun'] ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <a href="?tahun=<?=$selected_tahun?>" class="btn btn-primary btn-sm hidden-print"><i class="fa fa-refresh"></i> RELOAD</a>
                    <button type="button" onclick="window.print()" class="btn btn-sm btn-inverse hidden-print hidden-xs"><span class="ace-icon fa fa-print icon-on-right bigger-110"></span> PRINT</button>

                </form>
            </div>
            <div class="col-md-6" style="text-align: right">
                <label class="label label-warning">Realisasi</label>
                <label class="label label-success">Selesai</label>
            </div>
            <div style="clear: both">
                <div class="table-responsive">
                    <table class="table table-bordered table-striped table-hover">
                        <thead>
                        <tr>
                            <th class="center">#</th>
                            <th class="center col-xs-1">NIP</th>
                            <th class="center col-xs-2">Nama</th>
                            <th class="center col-xs-2">Penugasan</th>
                            <th class="center col-xs-2">Realisasi</th>
                            <th class="center col-xs-2">Selesai</th>
                            <th class="center col-xs-3">Kinerja</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $no = 1;
                        foreach ($user as $u) {
                            $realisasi=$u['realisasi'];
                            $realisasi2=$u['selesai'];
                            $disetujui = 0;
                            $direalisasi = 0;
                            if ($u['jumlah'] > 0) {
                                if ($u['selesai'] > 0) {
                                    $disetujui = ($u['selesai'] / $u['jumlah'] * 100);
                                }
                                $u['realisasi'] = $u['realisasi'] - $u['selesai'];
                                if ($u['realisasi'] > 0) {
                                    $direalisasi = ($u['realisasi'] / $u['jumlah'] * 100);
                                }
                            }
                            ?>
                            <tr>
                                <td class="center"><?= $no ?></td>
                                <td class="center"><?= $u['nip'] ?></td>
                                <td class=""><?= $u['nama'] ?></td>
                                <td class="center"><?= $u['jumlah'] ?></td>
                                <td class="center"><?= $realisasi ?></td>
                                <td class="center"><?= $realisasi2 ?></td>
                                <td class="center">
                                    <p class="visible-print"><?=round($disetujui)?>% disetujui</p>
                                    <p class="visible-print"><?=round($direalisasi  )?>% realisasi</p>
                                    <div class="progress">
                                        <div class="progress-bar progress-bar-success" role="progressbar"
                                             aria-valuenow="<?= $u['selesai'] ?>" aria-valuemin="0"
                                             aria-valuemax="<?= $u['jumlah'] ?>" style="width: <?= $disetujui ?>%">
                                            <span class="sr-only"><?= $disetujui ?>% disetujui</span>
                                        </div>
                                    </div>
                                    <div class="progress">

                                        <div class="progress-bar progress-bar-warning" role="progressbar"
                                             aria-valuenow="<?= $u['realisasi'] ?>" aria-valuemin="0"
                                             aria-valuemax="<?= $u['jumlah'] ?>" style="width: <?= $direalisasi ?>%">
                                            <span class="sr-only"><?= $direalisasi ?>% realisasi</span>
                                        </div>
                                    </div>

                                    <div class="progress">
                                        <div class="progress-bar progress-bar-success" role="progressbar"
                                             aria-valuenow="<?= $u['selesai'] ?>" aria-valuemin="0"
                                             aria-valuemax="<?= $u['jumlah'] ?>" style="width: <?= $disetujui ?>%">
                                            <span class="sr-only"><?= $disetujui ?>% disetujui</span>
                                        </div>
                                        <div class="progress-bar progress-bar-warning" role="progressbar"
                                             aria-valuenow="<?= $u['realisasi'] ?>" aria-valuemin="0"
                                             aria-valuemax="<?= $u['jumlah'] ?>" style="width: <?= $direalisasi ?>%">
                                            <span class="sr-only"><?= $direalisasi ?>% realisasi</span>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <?php
                            $no++;
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    function setStatusActive(_i) {
        $('.' + _i).html('<img src="<?=base_url('assets/loading.gif')?>">');
        $.post('?', {status: _i}, function (data, status) {
            $('.' + _i).html(data);
        });
    }
</script>