<div class="row">
    <div class="col-sm-12  col-print-12">
        <h1 class="center visible-print" style="margin:0">PKPT <?=@$tahun?></h1>
        <div class="card-box">
            <?php
            show_alert();
            ?>
            <hr>
            <form method="get" class="row hidden-print" action="<?=base_url('laporan/s_keg')?>">
                <div class="col-md-6">
                    <div class="dataTables_wrapper form-inline">
                        <select name="closed" onchange="this.form.submit()" class="form-control input-sm">
                            <option value="t" <?=@$closed=='t'?'selected':''?>>Tampilan Data</option>
                            <option value="f" <?=@$closed=='f'?'selected':''?>>Tampilan Grafik</option>
                        </select>
                    </div>
                </div>
                <br>
                <hr>
                <div class="col-md-2">
                    <div class="dataTables_wrapper form-inline">
                        <h4 class="label-control">Program : </h4>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="dataTables_wrapper form-inline">
                        <select name="program" onchange="this.form.submit()"  class="form-control select2">
                            <option value="">Semua Program</option>
                            <?php
                            foreach($list_program as $k){
                                ?>
                                <option value="<?=$k['id']?>" <?=@$program_s==$k['id']?'selected':''?>><?=$k['nama']?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                </div>

                <br>
                <br>

                <div class="col-md-2">
                    <div class="dataTables_wrapper form-inline">
                        <h4 class="label-control">Satker : </h4>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="dataTables_wrapper form-inline">
                        <select name="satker" onchange="this.form.submit()"  class="form-control select2">
                            <option value="">Semua Satker</option>
                            <?php
                            foreach($list_satker as $k){
                                ?>
                                <option value="<?=$k['id']?>" <?=@$satker_s==$k['id']?'selected':''?>><?=$k['nama']?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                </div>

            </form>
            <?php
            if(@$closed=='t'){
                ?>

                <div class="table-responsive">
                    <br> <table class="table table-bordered table-striped table-hover col-print-12">
                        <thead>
                        <tr>
                            <th class="center col-xs-1 col-print-1">#</th>
                            <th class="center col-xs-6 col-print-6">Nama Kegiatan</th>
                            <th class="center col-xs-1 col-print-1">Jumlah Resiko Kegiatan</th>
                            <th class="center col-xs-1 col-print-1">Jumlah Penyebab Resiko Kegiatan</th>
                            <th class="center col-xs-1 col-print-1">Jumlah Alat Kendali Seharusnya</th>
                            <th class="center col-xs-1 col-print-1">Jumlah Alat Kendali Yang Ada</th>
                            <th class="center col-xs-1 col-print-1">Jumlah Alat Kendali Blm Ada</th>
                            <th class="center col-xs-1 col-print-1">% Alat Kendali</th>
                            <th class="center col-xs-1 col-print-1">Pemetaan Jumlah Kejadian</th>
                            <th class="center col-xs-1 col-print-1">Realisasi Jumlah Kejadian</th>
                            <th class="center col-xs-1 col-print-1">% Kejadian</th>
                            <th class="center col-xs-1 col-print-1">Rata-rata % Skala Dampak</th>
                            <th class="center col-xs-1 col-print-1">Resiko Total</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $no=1;
                        foreach($satker as $g) {
                            ?>
                            <tr>
                                <td class="center"><?=$no?></td>
                                <td class="">
                                    <?=@$g['nama_keg']?>
                                </td>
                                <td class="right">
                                    <?=format_uang(@$g['jumlah'])?>
                                </td>
                                <td class="right">
                                    <?=format_uang(@$g['jumlah'])?>
                                </td>
                                <td class="right">
                                    <?=format_uang(@$g['jks'])?>
                                </td>
                                <td class="right">
                                    <?=format_uang(@$g['jka'])?>
                                </td>
                                <td class="right">
                                    <?=format_uang(@$g['jkb'])?>
                                </td>
                                <td class="right">
                                    <?=format_uang(@$g['jks']>0?@$g['jkb']/@$g['jks']:0)?>
                                </td>
                                <td class="right">
                                    <?=format_uang(@$g['jml'])?>
                                </td>
                                <td class="right">
                                    <?=format_uang(@$g['real'])?>
                                </td>
                                <td class="right">
                                    <?=format_uang(@$g['jml']>0?@$g['real']/@$g['jml']:0)?>
                                </td>
                                <td class="right">
                                    <?=format_uang(@$g['sd'])?>
                                </td>
                                <td class="right">
                                    <?=format_uang(@$g['sd']+(@$g['jks']>0?@$g['jkb']/@$g['jks']:0)+(@$g['jml']>0?@$g['real']/@$g['jml']:0))?>
                                </td>
                            </tr>
                            <?php
                            $no++;}
                        ?>
                        </tbody>
                    </table>
                </div>
                <?php
            }else{
                ?>

                <div id="container" style="min-width: 100%; height: 600px; margin: 0 auto"></div>
                <?php
            }
            ?>
        </div>
    </div>
</div>
<link href="<?=base_url()?>assets/plugins/custombox/css/custombox.min.css" rel="stylesheet">
<link href="<?=base_url()?>assets/plugins/bootstrap3-editable/css/bootstrap-editable.css" rel="stylesheet">
<script src="<?=base_url()?>assets/plugins/bootstrap3-editable/js/bootstrap-editable.min.js"></script>
<script src="<?=base_url()?>assets/plugins/custombox/js/custombox.min.js"></script>
<script src="<?=base_url()?>assets/plugins/custombox/js/legacy.min.js"></script>
<div id="new-group" class="modal-gradin">
<!--    <button type="button" class="close" onclick="Custombox.close();">-->
        <span>&times;</span><span class="sr-only">Close</span>
    </button>
    <h4 class="custom-modal-title">Tambah PKPT</h4>
    <div class="custom-modal-text">
        <form action="?" method="post">
            <div class="form-group row">
                <div class="col-sm-12">
                    <input type="text" name="no" required class="form-control" placeholder="No PKPT"><br>
                </div>
                <div class="col-sm-12">
                    <input type="text" name="tema" required class="form-control" placeholder="Tema PKPT"><br>
                </div>
                <div class="col-sm-12">
                    <input type="text" name="nama" required class="form-control" placeholder="Nama PKPT"><br>
                </div>
                <div class="col-sm-12">
                    <input type="text" name="kegiatan" required class="form-control" placeholder="Kegiatan PKPT"><br>
                </div>
                <div class="col-sm-12">
                    <input type="text" name="hp" required class="form-control" placeholder="HP"><br>
                </div>
                <div class="col-sm-12">
                    <input type="text" name="rmp" required class="form-control" placeholder="RMP"><br>
                </div>
                <div class="col-sm-12">
                    <input type="text" name="rpl" required class="form-control" placeholder="RPL"><br>
                </div>
                <div class="col-sm-12">
                    <input type="text" name="dana" required class="form-control" placeholder="Dana PKPT"><br>
                </div>
                <div class="col-sm-12">
                    <input type="text" name="risiko" required class="form-control" placeholder="Risiko PKPT"><br>
                </div>
                <div class="col-sm-12">
                    <input type="text" name="keterangan" required class="form-control" placeholder="Keterangan PKPT">
                </div>
                <div class="col-sm-12 right" style="padding-top: 1em">
                    <button type="button" onclick="Custombox.close();" class="btn btn-default">BATAL</button>
                    <button type="submit" class="btn btn-inverse">SIMPAN</button>
                </div>
            </div>
        </form>
    </div>
</div>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<link href="<?=base_url()?>assets/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="<?=base_url()?>assets/plugins/select2/js/select2.full.min.js"></script>

<script>
    $.fn.editable.defaults.mode = 'inline';
    function setStatusActive(_i) {
        $('.'+_i).html('<img src="<?=base_url('assets/loading.gif')?>">');
        $.post('?',{status:_i},function (data,status) {
            $('.'+_i).html(data);
        });
    }
    $(function(){
        <?php
        if(!isset($access['u'])){
        ?>
        $(".emjes-editable").editable('option', 'disabled', true);
        <?php
        }
        ?>
    });

    $('.select2').select2();
    Highcharts.chart('container', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Laporan Kegiatan'
        },
        xAxis: {
            categories: ["Jml Resiko", "Jml Penyebab Resiko", "Jml Alat Kendali Seharusnya", "Jml Alat Kendali Sudah ada", "Jumlah Alat Kendali Belum ada", "% Alat Kendali", "Pemetaan Jumlah Kejadian", "Realisasi Jumlah Kejadian", "% Kejadian", "Rata-rata Skala Dampak", "Resiko Total"],
            crosshair: true
        },
        yAxis: {
            min: 0,
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: [
            <?php
            if(@$satker){

                $jksab=0;
                $jmlreal=0;
                foreach ($satker as $s){
                    $jksab=@$s['jks']>0?@$s['jkb']/@$s['jks']:0;
                    $jmlreal=@$s['jml']>0?@$s['real']/@$s['jml']:0;
                    $tot=@$s['sd']+$jksab+$jmlreal;
                    $jumlah=null_is_nol($s['jumlah']);
                    $jks=null_is_nol($s['jks']);
                    $jka=null_is_nol($s['jka']);
                    $jkb=null_is_nol($s['jkb']);
                    $jml=null_is_nol($s['jml']);
                    $real=null_is_nol($s['real']);
                    $sd=null_is_nol($s['sd']);
                    echo"{name: '$s[nama_keg]',
                    data:[$jumlah,$jumlah,$jks,$jka,$jkb,$jksab,$jml,$real,$jmlreal,$sd,$tot]
                    },
                    ";
                }
            }
            ?>]
    //
    //     series: [{
    //         name: 'Tokyo',
    //     data: [49.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1, 95.6, 54.4]
    //
    // }, {
    //     name: 'New York',
    //         data: [83.6, 78.8, 98.5, 93.4, 106.0, 84.5, 105.0, 104.3, 91.2, 83.5, 106.6, 92.3]
    //
    // }, {
    //     name: 'London',
    //         data: [48.9, 38.8, 39.3, 41.4, 47.0, 48.3, 59.0, 59.6, 52.4, 65.2, 59.3, 51.2]
    //
    // }, {
    //     name: 'Berlin',
    //         data: [42.4, 33.2, 34.5, 39.7, 52.6, 75.5, 57.4, 60.4, 47.6, 39.1, 46.8, 51.1]
    //
    // }]
    });
</script>