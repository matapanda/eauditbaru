<div class="row">
    <div class="col-sm-12">
        <div class="card-box">
            <div class="table-responsive">
                <table class="table">
                    <tr>
                        <td colspan="2">Nomor Laporan</td>
                        <td>:</td>
                        <td><?= null ?></td>
                    </tr>
                    <tr>
                        <td colspan="2">Tahun Laporan</td>
                        <td>:</td>
                        <td><?= null ?></td>
                    </tr>
                    <tr>
                        <td>1</td>
                        <td>Nomor dan Tanggal Kartu Penugasan</td>
                        <td>:</td>
                        <td><?= @$laporan['kp_no'] ?> <?= format_tanggal(@$laporan['kp_date']) ?></td>
                    </tr>
                    <tr>
                        <td>3</td>
                        <td>Nomor dan Tanggal Surat Penugasan</td>
                        <td>:</td>
                        <td><?= @$laporan['spt_no'] ?> <?= format_tanggal(@$laporan['spt_date']) ?></td>
                    </tr>
                    <tr>
                        <td>4</td>
                        <td>Judul Penugasan</td>
                        <td>:</td>
                        <td><?= @$laporan['judul'] ?></td>
                    </tr>
                    <tr>
                        <td>5</td>
                        <td>Tujuan</td>
                        <td>:</td>
                        <td>
                            <ol style="list-style-type: lower-alpha;">
                                <?php
                                foreach ($tujuan as $t) {
                                    echo "<li>$t[ket]</li>";
                                }
                                ?>
                            </ol>
                        </td>
                    </tr>
                    <tr>
                        <td>6</td>
                        <td>Sasaran</td>
                        <td>:</td>
                        <td>
                            <ol style="list-style-type: lower-alpha;">
                                <?php
                                foreach ($sasaran as $s) {
                                    echo "<li>$s[ket]</li>";
                                }
                                ?>
                            </ol>
                        </td>
                    </tr>
                    <tr>
                        <td>7</td>
                        <td>Program Kerja Audit</td>
                        <td></td>
                        <td></td>
                    </tr>
                    <?php
                    $temp_tahapan = "";
                    $alp_tahapan = array('', 'A', 'B', 'C');
                    foreach ($program as $t):
                        if ($temp_tahapan != $t['tahapan']) {
                            $temp_tahapan = $t['tahapan'];
                            ?>
                            <tr>
                                <td></td>
                                <td colspan="3" style="font-weight: bold"><?= $alp_tahapan[$t['tahapan']] ?>. Program
                                    Kerja <?= $t['ket_tahapan'] ?></td>
                            </tr>
                            <?php
                            $no = 1;
                        }
                        $kode_kk=$no++;
                        ?>
                        <tr>
                            <td class="center"></td>
                            <td colspan="3">
                                <table class="table">
                                    <tr>
                                        <td style="width: 100px" rowspan="8">7.<?= $alp_tahapan[$t['tahapan']] ?>.<?= $no ?></td>
                                        <td style="width: 200px">Kode TAO</td>
                                        <td style="width: 1px">:</td>
                                        <td><?= $t['kode_tao'] ?></td>
                                    </tr>
                                    <tr>
                                        <td>TAO</td>
                                        <td>:</td>
                                        <td><?= $t['tao'] ?></td>
                                    </tr>
                                    <tr>
                                        <td>Kode langkah kerja</td>
                                        <td>:</td>
                                        <td><?= $t['kode_tao'].$kode_kk ?></td>
                                    </tr>
                                    <tr>
                                        <td>Langkah kerja</td>
                                        <td>:</td>
                                        <td><?= $t['langkah'] ?></td>
                                    </tr>
                                    <tr>
                                        <td>Kertas kerja</td>
                                        <td>:</td>
                                        <td>
                                            <?="KK" . $t['kode_tao'] . "-" . $kode_kk?>
                                            <?php
                                            if($t['isian']) {
                                                $kk=json_decode($t['isian'],TRUE);
                                                foreach($kk as $d):
                                                    echo '<br>';
                                                    echo '<a href= '.base_url('img/'.$d['file']).' class="btn btn-sm btn-primary" target="_blank"><i class="fa fa-file-text"></i> '.$d['nama'].'</a>';
                                                endforeach;
                                            }
                                            ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Dilaksanakan oleh</td>
                                        <td>:</td>
                                        <td><?= $t['realisasi_by'] ?></td>
                                    </tr>
                                    <tr>
                                        <td>Tanggal</td>
                                        <td>:</td>
                                        <td><?= format_tanggal($t['realisasi_date']) ?></td>
                                    </tr>
                                    <tr>
                                        <td>Kesimpulan</td>
                                        <td>:</td>
                                        <td style="white-space: pre-line"><?= @$t['kesimpulan'] ?></td>
                                    </tr>
                                    <tr>
                                        <td class="center"></td>
                                        <td colspan="3" style="font-weight: bold">Pemeriksaan Oleh Dalnis</td>
                                    </tr>
                                    <tr>
                                        <td class="center"></td>
                                        <td colspan="3">
                                            <table class="table">
                                                <tr>
                                                    <td class="col-xs-2">Nama</td>
                                                    <td class="col-xs-1">:</td>
                                                    <td class="col-xs-9"><?= $t['dalnis_name'] ?></td>
                                                </tr>
                                                <tr>
                                                    <td class="col-xs-2">Tanggal</td>
                                                    <td class="col-xs-1">:</td>
                                                    <td class="col-xs-9"><?= format_tanggal($t['app_sv1_tstamp']) ?></td>
                                                </tr>
                                                <tr>
                                                    <td class="col-xs-2">Kesimpulan</td>
                                                    <td class="col-xs-1">:</td>
                                                    <td class="col-xs-9" style="white-space: pre-line"><?= @$t['app_sv1_note'] ?></td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td></td>
                                        <td colspan="3" style="font-weight: bold">Pemeriksaan Oleh Irban</td>
                                    </tr>
                                    <tr>
                                        <td class="center"></td>
                                        <td colspan="3">
                                            <table class="table">
                                                <tr>
                                                    <td class="col-xs-2">Nama</td>
                                                    <td class="col-xs-1">:</td>
                                                    <td class="col-xs-9"><?= $t['irban_name'] ?></td>
                                                </tr>
                                                <tr>
                                                    <td class="col-xs-2">Tanggal</td>
                                                    <td class="col-xs-1">:</td>
                                                    <td class="col-xs-9"><?= format_tanggal($t['app_sv2_tstamp']) ?></td>
                                                </tr>
                                                <tr>
                                                    <td class="col-xs-2">Kesimpulan</td>
                                                    <td class="col-xs-1">:</td>
                                                    <td class="col-xs-9" style="white-space: pre-line"><?= @$t['app_sv2_note'] ?></td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <?php
                    endforeach;
                    ?>  <tr>
                        <td>8</td>
                        <td>Temuan & Tindak Lanjut</td>
                        <td></td>
                        <td></td>
                    </tr>
                    <?php
                    $urutan=1;
                    foreach ($temuan as $t){
                        ?>
                        <tr>
                            <td class="center"></td>
                            <td colspan="3">
                                <table class="table">
                                    <tr>
                                        <td style="width: 100px" rowspan="8">8.<?= $urutan ?></td>
                                        <td style="width: 200px">Judul Temuan</td>
                                        <td style="width: 1px">:</td>
                                        <td><?= $t['judul_temuan'] ?></td>
                                    </tr>
                                    <tr>
                                        <td>Kode Temuan</td>
                                        <td>:</td>
                                        <td><?= $t['kode_t'] ?> - <?= $t['temuan'] ?></td>
                                    </tr>
                                    <tr>
                                        <td>Uraian Temuan</td>
                                        <td>:</td>
                                        <td><?= $t['uraian_temuan'].$kode_kk ?></td>
                                    </tr>
                                    <tr>
                                        <td>Nilai Temuan</td>
                                        <td>:</td>
                                        <td><?= $t['nilai_temuan'] ?></td>
                                    </tr>
                                    <tr>
                                        <td>Kondisi</td>
                                        <td>:</td>
                                        <td><?= $t['kondisi'] ?></td>
                                    </tr>
                                    <tr>
                                        <td>Kriteria</td>
                                        <td>:</td>
                                        <td><?= $t['kriteria'] ?></td>
                                    </tr>
                                    <tr>
                                        <td colspan="3" style="font-weight: bold">Rekomendasi Temuan</td>
                                    </tr>
                                    <?php
                                    foreach ($tl_rekom as $t){
                                        if($t['urutan']==$urutan){
                                            ?>
                                            <tr>
                                                <td colspan="3">
                                                    <table class="table">
                                                        <tr>
                                                            <td class="col-xs-2">Kode Rekom</td>
                                                            <td class="col-xs-1">:</td>
                                                            <td class="col-xs-9"><?= $t['kode_rekoms'] ?> - <?= $t['nama_rekom'] ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="col-xs-2">Uraian Rekom</td>
                                                            <td class="col-xs-1">:</td>
                                                            <td class="col-xs-9"><?= $t['uraian_rekom'] ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="col-xs-2">No Tindak Lanjut</td>
                                                            <td class="col-xs-1">:</td>
                                                            <td class="col-xs-9"><?= $t['kode_tl'] ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="col-xs-2">Uraian Tindak Lanjut</td>
                                                            <td class="col-xs-1">:</td>
                                                            <td class="col-xs-9"><?= $t['uraian_tindakl'] ?></td>
                                                        </tr>

                                                    </table>
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                }

                                    foreach ($tl_penyebab as $t){
                                        if($t['urutan']==$urutan){
                                    ?>
                                    <tr>
                                        <td></td>
                                        <td colspan="3" style="font-weight: bold">Penyebab</td>
                                    </tr>
                                    <tr>
                                        <td class="center"></td>
                                        <td colspan="3">
                                            <table class="table">
                                                <tr>
                                                    <td class="col-xs-2">Kode Penyebab</td>
                                                    <td class="col-xs-1">:</td>
                                                    <td class="col-xs-9"><?= $t['kode'] ?> - <?= $t['nama'] ?></td>
                                                </tr>
                                                <tr>
                                                    <td class="col-xs-2">Uraian Penyebab</td>
                                                    <td class="col-xs-1">:</td>
                                                    <td class="col-xs-9"><?= $t['uraian_penyebab'] ?></td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                <?php
                                     }
                                    }
                                    $urutan++;}
                    ?>

                </table>
            </div>
        </div>
    </div>
</div>