<table style="width: 100%">
    <tr style="margin-bottom: 1cm">
        <td colspan="2" class="center">
            <h2 class="title center"><?=$laporan['judul']?></h2>
        </td>
    </tr>
    <tr>
        <td colspan="2"></td>
    </tr>
    <tr>
        <td class="double-line" colspan="2"></td>
    </tr>
</table>
<br>

<table width="100%" style="border: 1px solid;text-align: justify;vertical-align: text-top;">
    <tr >
        <td width="10%" style="border: 1px solid;text-align: center">
          NO
        </td>
        <td width="70%" style="border: 1px solid;text-align: center">
          URAIAN
        </td>
        <td width="20%" style="border: 1px solid;text-align: center">
          KOMENTAR
        </td>
    </tr>
    <?php
    $no=1;
    foreach($temuan as $t){
        ?>
        <tr>
            <td width="10%" style="border: 1px solid;text-align: center">
                <?=$no?>
            </td>
            <td width="70%" style="border: 1px solid;">
                <b><?=$t['judul_temuan']?></b>
                <br>
                <br>
                <?=$t['kondisi']?>
                <br>
                <br>
                <?=$t['kriteria']?>
                <br>
                <br>
                Akibatnya : <br>
                <ol>
                    <?php
                    $aki=json_decode($t['akibat'],true);

                    foreach($aki as $a){
                        $this->db->where('id',$a);
                        $aki = $this->db->get('master.akibat')->row_array();
                        ?>
                        <li><?=$aki['nama']?></li>
                        <?php
                    }
                    ?>
                </ol>
                <br>
                <br>
                Direkomendasikan <?=$t['uraian_rekom']?>
            </td>
            <td width="20%" style="border: 1px solid;text-align: center">

            </td>
        </tr>
        <?php
        $no++;
    }
    ?>
</table>
<style>
    @page {
        footer: html_footer;
        header: html_header;
    }

    @page

    * {
        margin-top: 1.8cm;
        margin-bottom: 2cm;
        margin-left: 2.5cm;
        margin-right: 5cm;
    }

    .list {
        width: 100%;
        margin-top: 20px;
        margin-bottom: 10px;
        border-bottom: 1px solid black;
    }

    th {
        margin: 10px;
        text-transform: uppercase;
    }

    .right {
        text-align: right;
    }

    .center {
        text-align: center;
    }

    th.line {
        border-bottom: 1px solid black;
    }

    .title {
        margin: 0px;
    }

    .bdr {
        border-bottom: 1px solid black;
    }

    .table tr td, .table tr th {
        border: 1px solid #000;
        padding: 3px;
    }

    .double-line {
        height: 5px !important;
        border-top: 3px solid #000;
        border-bottom: 1px solid #000;
    }
</style>