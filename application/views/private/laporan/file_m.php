<div class="row">
    <div class="col-sm-12">
        <div class="card-box row">
            <div class="col-md-8 col-md-offset-2">
                <form role="form" method="post">
                    <div class="form-group row">
                        <label class="col-sm-3 form-control-label">Periode<span
                                class="text-danger">*</span></label>
                        <div class="col-sm-7">
                            <select name="periode" id="periode" class="select2 form-control" required="">
                                <option value="">Pilih Periode</option>
                                <?php
                                foreach ($list_periode as $p) {
                                    echo "<option value='$p[nama]' " . ($p['nama'] == $periode ? 'selected=""' : "") . ">$p[nama]</option>";
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 form-control-label">PKPT</label>
                        <div class="col-sm-9">
                            <select name="pkpt" id="pkpt" class="select2 form-control">
                                <option value="">Semua PKPT</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 form-control-label">SPT</label>
                        <div class="col-sm-7">
                            <select name="spt" class="select2 form-control">
                                <option value="">Semua SPT</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-8 col-sm-offset-3">
                            <button type="submit" class="btn btn-inverse waves-effect waves-light">Cari File</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-xs-12" id="listfile">
            </div>
        </div>
    </div>
</div>
<link href="<?= base_url() ?>assets/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="<?= base_url() ?>assets/plugins/select2/js/select2.min.js"></script>
<script>
    $('select#periode').on('change', function () {
        $.post('?', {ajax: 'periode', val: $(this).val()}, function (data, status) {
            $('select#pkpt').html(data);
            $('select[name=spt]').html('<option value="">Semua SPT</option>');
            $('select').select2();
        });
    });
    $('select#pkpt').on('change', function () {
        $.post('?', {ajax: 'pkpt', val: $(this).val()}, function (data, status) {
            $('select[name=spt]').html(data);
            $('select').select2();
        });
    });
    $('form').on('submit', function (e) {
        e.preventDefault();
        $('#listfile').html('<img src="<?=base_url('assets/images/animat-search-color.gif')?>">');
        $.post('?', $(this).serialize(), function (data, status) {
            $('#listfile').html('');
            if(data.length>0) {
                $(data).each(function (index, item) {
                    let _obj = JSON.parse(item.file);
                    $(_obj).each(function (ix, it) {
                        $('#listfile').append('<div class="col-md-4" style="margin-bottom:1em;padding-right:1em"><a target="_blank" href="<?=base_url('img')?>/' + it.file + '" class="btn btn-default btn-lg btn-block" style="text-align:left;overflow:hidden" title="' + item.langkah +'"><h4>' + it.nama + '</h4><p class="details" style="overflow: hidden">' + item.nama + '</p><pre style="overflow: hidden">' + item.langkah + '</pre></a></li>');
                    });
                });
            }else{
                $('#listfile').html('<h1 style="padding:3em">File tidak ditemukan</h1>');
            }
        },'json');
    });
    $('select').select2();
</script>