<div class="row">
    <div class="col-sm-12">
        <div class="card-box row">
            <form method="post" class="col-sm-8 col-xs-11">
                <div class="form-group row">
                    <label class="col-sm-4 form-control-label">Range<span class="text-danger">*</span></label>
                    <div class="input-daterange input-group" id="date-range">
                        <input type="text" readonly class="form-control input-sm" name="start" onchange="this.form.submit()" value="<?=$start?>">
                        <span class="input-group-addon input-sm">~</span>
                        <input type="text" readonly class="form-control input-sm" name="end" onchange="this.form.submit()" value="<?=$end?>">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="hori-pass2" class="col-sm-4 form-control-label">COA<span class="text-danger">*</span></label>
                    <div class="col-sm-7" style="font-family: monospace!important;">
                        <select name="coa" class="select2 form-control" required>
                            <option value=''>-</option>
                            <?php
                            foreach ($listcoa as $r){
                                echo "<option value='$r[id]' ".($r['id']==$coa['id']?'selected=""':"").">" . str_pad(format_coa($r['kode']), 20, '-',STR_PAD_RIGHT) . " $r[nama]</option>";
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-8 col-sm-offset-4">
                        <input type="hidden" name="type">
                        <button type="submit" class="hidden">SUBMIT</button>
                        <button type="button" data-type="post" class="btn btn-submit-data btn-primary waves-effect waves-light">VIEW</button>
                        <button type="button" data-mode="pdf" data-type="get" class="btn btn-submit-data btn-danger waves-effect waves-light">PDF</button>
                        <button type="button" data-mode="excel" data-type="get" class="btn btn-submit-data btn-success waves-effect waves-light">EXCEL</button>
                    </div>
                </div>
            </form>
            <div class="col-xs-12">
                <hr>
                <?php
                if(isset($data)){
                ?>
                <div class="table-responsive">
                    <table class="table table-bordered table-striped table-hover">
                        <thead>
                            <tr>
                                <th class="no-sort col-xs-1 center">Tanggal</th>
                                <th class="no-sort col-xs-2 center">No Bukti</th>
                                <th class="no-sort col-xs-3 center">Keterangan</th>
                                <th class="no-sort col-xs-2 center">Debit</th>
                                <th class="no-sort col-xs-2 center">Kredit</th>
                                <th class="no-sort col-xs-2 center">Saldo</th>
                                <th class="no-sort col-xs-1"></th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                        foreach ($data as $d) {
                            ?>
                            <tr>
                                <td class="center"><?= format_waktu($d['tanggal']) ?></td>
                                <td class="center"><?= ($d['no']==0?"-":"$d[nobukti]-$d[no]")?></td>
                                <td><?= $d['uraian'] ?></td>
                                <td class="right"><?=format_uang($d['debet']) ?></td>
                                <td class="right"><?=format_uang($d['kredit']) ?></td>
                                <td class="right"><?=format_uang($d['saldo']) ?></td>
                                <td class="center"><a class="btn btn-info" href="#"><i class="fa fa-search"></i></a></td>
                            </tr>
                            <?php
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <?php
            }
            ?>
        </div>
    </div>
</div>
<link href="<?=base_url()?>assets/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css"/>
<link href="<?=base_url()?>assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="<?=base_url()?>assets/plugins/select2/js/select2.min.js"></script>
<script src="<?=base_url()?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script>
    $('select').select2();
    $('#date-range').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true,
        todayHighlight: true
    });
    function rincian(_i,_nota) {
        $.post('?',{d:_i},function (data,status) {
            $('h4.modal-title','#detailpenerimaan').html(_nota);
            $('tbody','#detailpenerimaan').html(data.data);
            $('#detailpenerimaan').modal('show');
        },'json');
    }
    $('.btn-submit-data').on('click',function (e) {
        var _typ=$(this).data('type');
        if(_typ=='post'){
            $($(this).closest('form')).attr('target','_self');
            $($(this).closest('form')).attr('action','');
        }else{
            var _act=$(this).data('mode');
            $($(this).closest('form')).attr('target','_blank');
            $('[name=type]',$(this).closest('form')).val(_act);
            $($(this).closest('form')).attr('action',"<?=base_url('export/bukubesar')?>");
        }
        $($(this).closest('form')).attr('method',_typ);
        $('[type=submit]',$(this).closest('form')).click();
    });
</script>