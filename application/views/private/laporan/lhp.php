<style>
    .table {
         margin-bottom: 0px;
    }
    @media (min-width: 768px) {
        .modal-xl {
            width: 90%;
            max-width:1200px;
        }
    }
    .custombox-modal-wrapper.custombox-modal-wrapper-blur.custombox-modal-open{
        z-index=0;!important;
    }
</style><div class="row">
    <div class="col-sm-12">
        <div class="card-box"><h2 class="visible-print center">LAPORAN LHP</h2>
            <a onclick="input_modal()" class="<?=is_authority(@$access['c'])?>  hidden-print btn btn-inverse"><i class="fa fa-plus"></i> TAMBAH DATA LHP</a>

            <button type="button" onclick="window.print()" class="btn btn-primary hidden-print hidden-xs"><span class="ace-icon fa fa-print icon-on-right bigger-110"></span> PRINT</button>
           <?php
            show_alert();
            ?>
            <form method="get" class="row hidden-print" action="<?=base_url('laporan/lhp')?>">
                <div class="col-md-6">
                    <div class="dataTables_wrapper form-inline">
                        <label>Filter: </label>
                        <div class="input-daterange input-group" id="date-range">
                            <input type="text" readonly class="form-control input-sm" name="start" onchange="this.form.submit()" value="<?=$start?>">
                            <span class="input-group-addon input-sm">~</span>
                            <input type="text" readonly class="form-control input-sm" name="end" onchange="this.form.submit()" value="<?=$end?>">
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="dataTables_wrapper form-inline right">
                        <label class="hidden-xs">Search: &nbsp;</label><input type="search" name="search" class="form-control input-sm" autocomplete="off" value="<?=@$search?>">
                        <input type="submit" hidden>
                    </div>
                </div>
            </form>
            <div class="table-responsive">
                <table class="table table-bordered table-striped table-hover">
                    <thead>
                    <tr>
                        <th class="center">#</th>
                        <th class="center col-xs-2">Nomor SPT</th>
                        <th class="center col-xs-3">Judul SPT</th>
                        <th class="center col-xs-2">Nomor LHP</th>
                        <th class="center col-xs-2">Satuan Kerja</th>
                        <th class="center col-xs-2">Jenis Audit</th>
                        <th class="center col-xs-1 hidden-print"></th>
                    </tr>
                    </thead>
                    <tbody>

                    <?php
                    $no=1;
                    foreach($lhp as $r) {
                        $tgl_start=explode('-',$r['tgl']);
                        $tgl_m=$tgl_start[2].'/'.$tgl_start[1].'/'.$tgl_start[0];
                        ?>
                        <tr class="data<?=$r['id']?>">
                            <td class="center"><?=$no;?></td>
                            <td class="center"><?=$r['spt_no']?></td>
                            <td class=""><?=$r['judul']?></td>
                            <td class="center"><?=$r['no']?></td>
                            <td class=""><?=$r['nama_satker']?>
                            <input type="hidden" id="nomer<?=$no?>" value="<?=$r['no']?>">
                            <input type="hidden" id="id_lhp<?=$no?>" value="<?=$r['id_lhp']?>">
                            <input type="hidden" id="tgl_lhp<?=$no?>" value="<?=$tgl_m?>">
                            <input type="hidden" id="id_r_lhp<?=$no?>" value="<?=$r['id_rencana']?>">
                            </td>
                            <td class=""><?=$r['nama_jenis']?></td>
                            <td class="center hidden-print">
                                <a onclick="edit_lhp(<?=$no?>)" class="btn btn-sm btn-inverse <?= is_authority(@$access['u']) ?>"><i class="fa fa-pencil"></i></a>
                            </td>
                        </tr>
                        <?php
                        $no++; }
                    ?>
                    </tbody>

                </table>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="edit_modal" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Edit No SPT</h4>
            </div>
            <form method="post" action="?edit=true">
                <div class="modal-body">
                    <table class='table'>
                        <tr>
                            <td class='right'>
                                <label>Nomor LHP</label>
                            </td>
                            <td>
                                <input type="hidden" name="id_lhp" id="id_hidden" required>
                                <input type="text" name="no_lhp" id="no_lhp_edit" required class="form-control" placeholder="No LHP">
                            </td>
                        </tr>
                        <tr>
                            <td class='right'>
                                <label>Pilih SPT</label>
                            </td>
                            <td>
                                <select class="select2 form-control" name="spt" id="spt_edit" data-placeholder="Pilih SPT" required>
                                    <option value="">Pilih SPT</option>
                                    <?php
                                    foreach ($rencana as $v):
                                        ?>
                                        <option value="<?= $v['id'] ?>"><?= "$v[spt_no]" ?></option>
                                    <?php
                                    endforeach;
                                    ?>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td class='right'>
                                <label>Tanggal LHP</label>
                            </td>
                            <td>
                                <input type="text" id="daterange" class="form-control input-sm" name="tgl_lhp" value="">
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                <input type="submit" value="SIMPAN" class="btn btn-inverse"> </td>
                        </tr>
                    </table>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="input_modal" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Tambah No LHP</h4>
            </div>
            <form method="post" action="?">
                <div class="modal-body">
                    <table class='table'>
                        <tr>
                            <td class='right'>
                                <label>Nomor LHP</label>
                            </td>
                            <td>
                                <input type="text" name="no_lhp" required class="form-control" placeholder="No LHP">
                            </td>
                        </tr>
                        <tr>
                            <td class='right'>
                                <label>Pilih SPT</label>
                            </td>
                            <td>
                                <select class="select2 form-control" name="spt" data-placeholder="Pilih SPT" required>
                                    <option value="">Pilih SPT</option>
                                    <?php
                                    foreach ($rencana as $v):
                                        ?>
                                        <option value="<?= $v['id'] ?>"><?= "$v[spt_no]" ?></option>
                                    <?php
                                    endforeach;
                                    ?>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td class='right'>
                                <label>Tanggal LHP</label>
                            </td>
                            <td>
                                <input type="text" id="daterange" class="form-control input-sm" name="tgl_lhp" value="<?=date('d/m/Y')?>">
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                <input type="submit" value="SIMPAN" class="btn btn-inverse"> </td>
                        </tr>
                    </table>
                </div>
            </form>
        </div>
    </div>
</div>
<link href="<?=base_url()?>assets/plugins/custombox/css/custombox.min.css" rel="stylesheet">
<script src="<?=base_url()?>assets/plugins/custombox/js/custombox.min.js"></script>
<script src="<?=base_url()?>assets/plugins/custombox/js/legacy.min.js"></script>

<link href="<?=base_url()?>assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css"/>
<script src="<?=base_url()?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.25.0/codemirror.min.css">
<script src="<?=base_url()?>assets/js/jquery.bootcomplete.js"></script>
<!-- Include Editor style. -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.8.5/css/froala_editor.pkgd.min.css" rel="stylesheet" type="text/css" />
<link href="https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.8.5/css/froala_style.min.css" rel="stylesheet" type="text/css" /><script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.25.0/codemirror.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.25.0/mode/xml/xml.min.js"></script>
<link href="<?=base_url()?>assets/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css"/>
<link href="<?=base_url()?>assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="<?=base_url()?>assets/plugins/select2/js/select2.min.js"></script>
<script src="<?=base_url()?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>

<!-- Include Editor JS files. -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.8.5/js/froala_editor.pkgd.min.js"></script>
<script>
function edit_lhp(i){
    var no=$('#nomer'+i).val();
    var id=$('#id_lhp'+i).val();
    var tgl=$('#tgl_lhp'+i).val();
    var id_r=$('#id_r_lhp'+i).val();
    $('#daterange').val(tgl);
    $('#id_hidden').val(id);
    $('#no_lhp_edit').val(no);
    $('#spt_edit').val(id_r); // Select the option with a value of '1'
    $('#spt_edit').trigger('change');
    $('#edit_modal').modal('show');
}function input_modal(){
    $('#input_modal').modal('show');
}
    $('.select2').select2();
    $('input[name=no_lhp]','#new-data').bootcomplete({
        url:'?kode=true',
        minLength : 2,
        method: 'post'
    });
    $('#mytextarea').froalaEditor();
    function edit_spt(i){
        $('#id_edit').val(i);
        $('input[name="lhp_edit"]').val($('#no_laporan_h').val());
        $('#edit_modal').modal('show');
    }
    function hapus(_i) {
        swal({
            title: "Apakah anda yakin?",
            text: "data yang terhapus tidak dapat dikembalikan!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD5555",
            confirmButtonText: "Lanjutkan!",
            cancelButtonText: "Batalkan!",
            closeOnConfirm: true,
            closeOnCancel: true
        }, function (isConfirm) {
            if (isConfirm) {
                $.post('?',{delete:_i},function () {
                    $('.data'+_i).remove();
                });
            }
        });
    }
    $('#date-range').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true,
        todayHighlight: true
    });$('#daterange').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true,
        todayHighlight: true
    });
    function rincian(_i,_r) {
        $.post('?',{ri:_i,rn:_r},function (data,status) {
            $('h4.modal-title','#detailpenerimaan').html("Detail");
            $('.modal-body','#detailpenerimaan').html(data.data);
            $('#detailpenerimaan').modal('show');
        },'json');
    }
    function setStatusActive(_i) {
        $('.'+_i).html('<img src="<?=base_url('assets/loading.gif')?>">');
        $.post('?',{status:_i},function (data,status) {
            $('.'+_i).html(data);
        });
    }
</script>