<div class="row">
    <div class="col-sm-12">
        <div class="card-box row">
            <form method="post" class="col-sm-8 col-xs-11">
                <div class="form-group row">

                </div>
                <div class="form-group row">
                    <div class="col-sm-3">
                        <select name="bulan" class="select2 form-control" required>
                            <?php
                            $list_bulan=array('-','Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember');
                            foreach ($list_bulan as $i=>$b){
                                echo "<option value='$i' ".($i==$bulan?'selected=""':"").">$b</option>";
                            }
                            ?>
                        </select>
                    </div>
                    <div class="col-sm-3">
                        <select name="tahun" class="select2 form-control" required>
                            <?php
                            for($i=date('Y');$i>(date('Y')-5);$i--){
                                echo "<option value='$i' ".($i==$tahun?'selected=""':"").">$i</option>";
                            }
                            ?>
                        </select>
                    </div>
                    <div class="col-sm-3">
                        <select name="detail" class="select2 form-control" required>
                            <option value="1">Summary</option>
                            <option value="0" <?=@$detail=='FALSE'?'selected':''?>>Detail</option>
                        </select>
                    </div>
                    <div class="col-sm-3">
                        <button type="submit" class="btn btn-primary waves-effect waves-light">VIEW</button>
                    </div>
                </div>
            </form>
            <div class="col-xs-12">
                <hr>
                <?php
                if(isset($data)){
                if ($bulan > 0) {
                    if($bulan==1){
                        $sebelum = $list_bulan[12] . " " . ($tahun - 1);
                        $sekarang = $list_bulan[1] . " " . $tahun;
                        $tahun=array(($tahun-1),($tahun));
                        $bulan=array((12),(1));
                    }else{
                        $sebelum = $list_bulan[$bulan - 1] . " " . $tahun;
                        $sekarang = $list_bulan[$bulan - 0] . " " . $tahun;
                        $tahun=array(($tahun),($tahun));
                        $bulan=array(($bulan - 1),($bulan));
                    }
                } else {
                    $bulan = 12;
                    $sebelum = $list_bulan[$bulan] . " " . ($tahun - 1);
                    $sekarang = $list_bulan[$bulan] . " " . $tahun;
                    $tahun=array(($tahun - 1),($tahun));
                    $bulan=array(($bulan),($bulan));
                }
                $sebelum = date("t", strtotime("$tahun[0]-$bulan[0]-1")) . " " . $sebelum;
                $sekarang = date("t", strtotime("$tahun[1]-$bulan[1]-1")) . " " . $sekarang;
                ?>
                <div class="table-responsive">
                    <div class="col-md-6">
                        <table class="table table-bordered table-striped table-hover">
                            <thead>
                                <tr>
                                    <th class="col-xs-4 center">Uraian</th>
                                    <th class="col-xs-4 center"><?php echo $sekarang;?></th>
                                    <th class="col-xs-4 center"><?php echo $sebelum;?></th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php
                            $temp="D";
                            foreach ($data as $d) {
                                $temp="D";
                                if($d['status']=='f'){
                                    ?>
                                    <tr style="font-size: larger;font-weight: bolder">
                                        <td colspan="3"><?= format_coa("$d[k1].$d[k2].$d[k3].$d[k4]")." - $d[dk]".$d['nama'] ?></td>
                                    </tr>
                                    <?php
                                }else{
                                    ?>
                                    <tr>
                                        <td><?= $d['nama'] ?></td>
                                        <td class="right"><?=isset($d['sebelum'])?format_uang($d['sebelum']):'' ?></td>
                                        <td class="right"><?=isset($d['sekarang'])?format_uang($d['sekarang']):'' ?></td>
                                    </tr>
                                    <?php
                                }
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <?php
            }
            ?>
        </div>
    </div>
</div>
<link href="<?=base_url()?>assets/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="<?=base_url()?>assets/plugins/select2/js/select2.min.js"></script>
<script>
    $('select').select2();
</script>