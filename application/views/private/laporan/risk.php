<div class="row">
    <div class="col-sm-12  col-print-12">
       <div class="card-box">
            <?php
            show_alert();
            ?>
            <hr>
           <form method="get" class="row hidden-print" action="<?=base_url('laporan/skala_resiko')?>">
               <div class="col-md-2">
                   <div class="dataTables_wrapper form-inline">
                       <h4 class="label-control">Satker : </h4>
                   </div>
               </div>
               <div class="col-md-6">
                   <div class="dataTables_wrapper form-inline">
                       <select name="satker" onchange="this.form.submit()"  class="form-control select2">
                           <option value="">Semua Satker</option>
                           <?php
                           foreach($list_satker as $k){
                               ?>
                               <option value="<?=$k['id']?>" <?=@$satker_s==$k['id']?'selected':''?>><?=$k['nama']?></option>
                               <?php
                           }
                           ?>
                       </select>
                   </div>
               </div>

           </form>

                <div id="container" style="min-width: 100%; height: 600px; margin: 0 auto"></div>
        </div>
    </div>
</div>
<link href="<?=base_url()?>assets/plugins/custombox/css/custombox.min.css" rel="stylesheet">
<link href="<?=base_url()?>assets/plugins/bootstrap3-editable/css/bootstrap-editable.css" rel="stylesheet">
<script src="<?=base_url()?>assets/plugins/bootstrap3-editable/js/bootstrap-editable.min.js"></script>
<script src="<?=base_url()?>assets/plugins/custombox/js/custombox.min.js"></script>
<script src="<?=base_url()?>assets/plugins/custombox/js/legacy.min.js"></script>

<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<link href="<?=base_url()?>assets/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="<?=base_url()?>assets/plugins/select2/js/select2.full.min.js"></script>

<script>
    $.fn.editable.defaults.mode = 'inline';
    function setStatusActive(_i) {
        $('.'+_i).html('<img src="<?=base_url('assets/loading.gif')?>">');
        $.post('?',{status:_i},function (data,status) {
            $('.'+_i).html(data);
        });
    }
    $(function(){
        <?php
        if(!isset($access['u'])){
        ?>
        $(".emjes-editable").editable('option', 'disabled', true);
        <?php
        }
        ?>
    });

    $('.select2').select2();
    Highcharts.chart('container', {
        chart: {
            type: 'scatter'
        },
        title: {
            text: 'Laporan Tipe Resiko'
        },
        xAxis: {
            categories: [
               'Tipe Resiko'
            ],
            crosshair: true
        },
        yAxis: {
            min: 0,
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: [
            <?php
                foreach ($risk as $s){
                    echo"{name: '$s[nama]',
                    data:[[".null_is_nol($s['tot']).",".null_is_nol($s['tot2'])."]]
                    },
                    ";
                }
            ?>]
    //
    //     series: [{
    //         name: 'Tokyo',
    //     data: [49.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1, 95.6, 54.4]
    //
    // }, {
    //     name: 'New York',
    //         data: [83.6, 78.8, 98.5, 93.4, 106.0, 84.5, 105.0, 104.3, 91.2, 83.5, 106.6, 92.3]
    //
    // }, {
    //     name: 'London',
    //         data: [48.9, 38.8, 39.3, 41.4, 47.0, 48.3, 59.0, 59.6, 52.4, 65.2, 59.3, 51.2]
    //
    // }, {
    //     name: 'Berlin',
    //         data: [42.4, 33.2, 34.5, 39.7, 52.6, 75.5, 57.4, 60.4, 47.6, 39.1, 46.8, 51.1]
    //
    // }]
    });
</script>