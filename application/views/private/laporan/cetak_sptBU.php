<table style="width: 100%">
    <tr style="margin-bottom: 1cm">
        <td style="width: 3cm"><img src="<?= base_url('assets/images/logo.png') ?>" style="width:20%;"></td>
        <td style="width: 15cm" class="center">
            <h2 class="title center">PEMERINTAH KABUPATEN BANYUWANGI</h2>
            <h2 class="title center">I N S P E K T O R A T</h2>
            <p class="title center">Jalan KH. Agus Salim Nomor 81 Banyuwangi 68425</p>
            <p class="title center">Telp. (0333) – 414240 Fax. (0333) - 412600</p>
            <p class="title center">Email: <a
                    href="mailto:inspektorat@banyuwangi.go.id">inspektorat@banyuwangi.go.id</a> <a
                    href="http://www.banyuwangikab.go.id">http://www.banyuwangikab.go.id</a></p>
        </td>
    </tr>
    <tr>
        <td class="double-line" colspan="2"></td>
    </tr>
</table>
<br>
<?php
$tahun = explode('-', $laporan['spt_date']);
?>
<table width="100%" style="text-align: justify;vertical-align: text-top">
    <tr>
        <td colspan="4" style="text-align: center">
            <h3 class="title" style="border-bottom: solid 3px #000">SURAT PERINTAH TUGAS</h3>
            <h3 class="title">NOMOR : 700/<?= $laporan['spt_no'] ?>/429.060/<?= $tahun[0] ?></h3>
        </td>
    </tr>
    <tr>
        <td colspan="4" style="text-align: center">&nbsp;</td>
    </tr>
    <?php
    foreach ($conf_spt as $i=>$c) {
        ?>
        <tr>
            <?php
            if ($i == 0) {
                ?>
                <td rowspan="<?= count($conf_spt) ?>" style="width: 3cm">Dasar</td>
                <td rowspan="<?= count($conf_spt) ?>" style="width: 1cm">:</td>
                <?php
            }
            ?>
            <td style="width: 1cm"><?= $i + 1 ?>.</td>
            <td style="width: 15cm"><?= $c['ket'] ?></td>
        </tr>
        <?php
    }
    ?>
    <tr>
        <td colspan="4" style="text-align: center">&nbsp;</td>
    </tr>
    <tr>
        <td colspan="4" style="text-align: center"><h3 class="title">MEMERINTAHKAN</h3></td>
    </tr>
    <tr>
        <td colspan="4" style="text-align: center">&nbsp;</td>
    </tr>
    <tr>
        <td rowspan="1" style="width: 3cm">Kepada</td>
        <td rowspan="1" style="width: 1cm">:</td>
        <td colspan="2">
            <table width="100%" class="table" cellpadding="0" cellspacing="0" style="vertical-align: text-top">
                <tr>
                    <th width="1cm" style="text-align: center">NO</th>
                    <th width="5cm" class="center">NAMA DAN NIP</th>
                    <th width="5cm" class="center">KETERANGAN</th>
                </tr>
                <tbody>
                <?php
                foreach ($tim as $i => $v) {
                    ?>
                    <tr>
                        <td style="text-align: center"><?= $i + 1 ?>.</td>
                        <td><?= $v['nama'] ?><br>NIP. <?= $v['nip'] ?></td>
                        <td><?= $v['jabatan'] ?></td>
                    </tr>
                    <?
                }
                ?>
                </tbody>
            </table>
        </td>
    </tr>
    <tr>
        <td colspan="4" style="text-align: center">&nbsp;</td>
    </tr>
    <tr>
        <?php
        $tujuan[] = array('ket' => $laporan['judul']);
        $tujuan[] = array('ket' => 'Membuat laporan kegiatan dimaksud');
        ?>
        <td rowspan="<?= count($tujuan) ?>" style="width: 3cm">Untuk</td>
        <td rowspan="<?= count($tujuan) ?>" style="width: 1cm">:</td>
        <?php
        foreach ($tujuan as $no => $t) {
            if ($no > 0) {
                echo "</tr><tr>";
            }
            ?>
            <td><?= $no + 1; ?>.</td>
            <td><?= $t['ket']; ?>.</td>
            <?php
        }
        ?>
    <tr>
</table>
<br>
<table>
    <tr>
        <td style="width: 65%"></td>
        <td style="width: 35%" style="background-image: url('<?= $laporan['app_sv2_status'] == 't' ? base_url('img/' . $inspektur['ttd']) : '' ?>');background-size:contain;background-repeat: no-repeat">
            <table>
                <tr>
                    <td><p>Dikeluarkan di : Banyuwangi<br>
                            Pada tanggal : <?= format_tanggal($laporan['spt_date']) ?>
                        </p></td>
                </tr>
                <tr>
                    <td><p>INSPEKTUR<br>KABUPATEN BANYUWANGI</td>
                    </p></tr>
                <tr>
                    <td>
                        <p><br><br><br><br></p>
                    </td>
                </tr>
                <tr>
                    <td>
                        <p>
                            <span
                                style="font-weight: bold;border-bottom: 1px solid #000;text-transform: uppercase"><?= $inspektur['nama'] ?></span><br>
                            <?= $inspektur['golongan'] ?><br>
                            NIP. <?= $inspektur['nip'] ?>
                        </p>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<htmlpagefooter name="footer">
</htmlpagefooter>
<style>
    @page {
        footer: html_footer;
        header: html_header;
    }

    @page

    * {
        margin-top: 1.8cm;
        margin-bottom: 2cm;
        margin-left: 2.5cm;
        margin-right: 5cm;
    }

    .list {
        width: 100%;
        margin-top: 20px;
        margin-bottom: 10px;
        border-bottom: 1px solid black;
    }

    th {
        margin: 10px;
        text-transform: uppercase;
    }

    .right {
        text-align: right;
    }

    .center {
        text-align: center;
    }

    th.line {
        border-bottom: 1px solid black;
    }

    .title {
        margin: 0px;
    }

    .bdr {
        border-bottom: 1px solid black;
    }

    .table tr td, .table tr th {
        border: 1px solid #000;
        padding: 3px;
    }

    .double-line {
        height: 5px !important;
        border-top: 3px solid #000;
        border-bottom: 1px solid #000;
    }
</style>