<div class="row">
    <div class="col-sm-12">
        <div class="card-box row"><h2 class="visible-print center">Rekap Temuan Dan Tindak Lajut</h2>

            <form method="post" action="?cek=true" class="col-sm-8 col-xs-11 hidden-print">
                <div class="form-group row">
                    <div class="col-sm-2">
                        <select name="periode" id="periode" class="select2 form-control" required>
                            <?php
                            foreach($periode as $p){
                                ?>
                                <option value="<?=$p['id']?>" <?=@$periode_s==$p['id']?'selected':''?>><?=$p['nama']?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                    <div class="col-sm-3">
                        <select class="select2 form-control select2-multiple" required onchange="tipes(this.value)" name="tipe" data-placeholder="Pilih Tipe Satker">
                            <option value="">Pilih Tipe Satker</option>
                            <option value="P" <?=@$tipe=='P'?'selected':''?>>Program Satker</option>
                            <option value="K" <?=@$tipe=='K'?'selected':''?>>Kegiatan Satker</option>
                        </select>
                    </div>

                    <div class="col-sm-3 hasil">
                        <?php
                        if(@$tipe!=''){
                            echo '<select class="select2 form-control select2-multiple" name="pk" data-placeholder="Pilih Program / Kegiatan Satker">
                                <option value="">Pilih Program / Kegiatan Satker</option>';
                            foreach ($kejadian as $v){
                                echo '<option value="'.$v['id'].'" '.(@$id_s==$v['id']?'selected':'').'>'.$v['nama_s'].'</option>';
                            }
                            echo'</select>';
                        }
                        ?>
                    </div>
                    <div class="col-sm-4">
                        <input type="hidden" name="type">
                        <input type="submit" class="btn btn-submit-data btn-primary" value="VIEW">
                        <button type="button" onclick="window.print()" class="btn btn-inverse hidden-print hidden-xs"><span class="ace-icon fa fa-print icon-on-right bigger-110"></span> PRINT</button>
                    </div>
                </div>
            </form>
            <div class="col-xs-12">
                <hr>
                <div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
        </div>
    </div>
</div>
<div class="modal fade" id="uploadModal" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-large" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
            </div>
        </div>
    </div>
</div>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/series-label.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>

<link href="<?=base_url()?>assets/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css"/>
<link href="<?=base_url()?>assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="<?=base_url()?>assets/plugins/select2/js/select2.min.js"></script>
<script src="<?=base_url()?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script>
// alert('hahaha');
<?php
if(@$kejadian){

?>
Highcharts.chart('container', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Laporan Kegiatan'
    },
    xAxis: {
        categories: [
            <?php
                foreach ($kejadian as $k){
                    echo $k['nama'].',';
                }
                ?>
        ],
        crosshair: true
    },
    yAxis: {
        min: 0,
    },
    tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
            '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
    },
    plotOptions: {
        column: {
            pointPadding: 0.2,
            borderWidth: 0
        }
    },
    series: [
        <?php
        if(@$kejadian){

            foreach ($kejadian as $s){
                $this->db->where('kr.id', $s['id']);
                $this->db->where('s.status', true);
                $this->db->join('support.'.strtolower($title).'_satker s', 'kr.id=s.id');
                $this->db->join('support.setting_'.strtolower($title).' sp', 'sp.id=s.kegiatan','left');
                $this->db->distinct();
                $this->db->select('kr.*,sp.nama as nama_s');
                $jml=$this->db->get('support.'.strtolower($title).'_kr kr')->result_array();
                echo"{name: '$s[nama_s]',
                    data:[";
                foreach ($jml as $j) {
                    echo $j['sk']*$j['sd'].',';
                }
                echo"]
                    },
                    ";
            }
        }
        ?>]
    //
    //     series: [{
    //         name: 'Tokyo',
    //     data: [49.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1, 95.6, 54.4]
    //
    // }, {
    //     name: 'New York',
    //         data: [83.6, 78.8, 98.5, 93.4, 106.0, 84.5, 105.0, 104.3, 91.2, 83.5, 106.6, 92.3]
    //
    // }, {
    //     name: 'London',
    //         data: [48.9, 38.8, 39.3, 41.4, 47.0, 48.3, 59.0, 59.6, 52.4, 65.2, 59.3, 51.2]
    //
    // }, {
    //     name: 'Berlin',
    //         data: [42.4, 33.2, 34.5, 39.7, 52.6, 75.5, 57.4, 60.4, 47.6, 39.1, 46.8, 51.1]
    //
    // }]
});

    <?php
    }
    if(@$kejadian){
    ?>
    $('.hasil').show();
    $('#container').show();
    <?php
    }else{
        ?>
    $('.hasil').hide();
    $('#container').hide();
    <?php
    }
    ?>
    function detail(_id) {
        var _modal=$('#uploadModal');
        var _title=$('tr#'+_id).children('td').eq(1).text()+' - '+$('tr#'+_id).children('td').eq(2).text();
        $('.modal-title',_modal).html(_title);
        $.post('?tao='+_id,function(data,status){
            $('.modal-body',_modal).html(data);
            _modal.modal('show');
        });
    }
    function tipes(i) {
        if(i!=''){
var periode=$('#periode').val();
            $.post('?tipes='+i+'&periode='+periode,function(data,status){
                $('.hasil').show();
                $('.hasil').html();
                $('.hasil').html(data);
                $('.select2').select2();
            });
        }
    }
    $('select').select2();
    $('#date-range').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true,
        todayHighlight: true
    });
</script>