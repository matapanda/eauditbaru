<table>
    <tr>
        <td><img src="<?=base_url('assets/images/logo.png')?>" style="width:20%;" ></td>
        <td class="center"><h1 class="title center">PEMERINTAH  KABUPATEN BANYUWANGI<br> I N S P E K T O R A T</h1><br>
            <h4 class="title center">Jalan  KH. Agus Salim Nomor  81 Banyuwangi 68425</h4><br>
            <h4 class="title center">Telp. (0333) – 414240 Fax. (0333) - 412600</h4><br>
            <h4 class="title center">Email: inspektorat@banyuwangi go.id http://www.banyuwangikab.go.id</h4>
        </td>
    </tr>
</table>
<hr>

<h2 class="title center"><u>SURAT PERINTAH TUGAS</u></h2>
<h2 class="title center">NOMOR : 700/ <?=$laporan['spt_no']?>/429.060/2017</h2><br>
<table width="100%">
    <tr>
        <td>Dasar</td>
        <td>:</td>
        <td></td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td>1.  Peraturan Menteri Dalam Negeri Nomor 76 Tahun 2016 tentang Kebijakan Pengawasan di Lingkungan Kementerian Dalam Negeri dan Penyelenggaraan Pemerintahan  Daerah Tahun 2017;</td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td>2.	Peraturan Daerah Kabupaten Banyuwangi Nomor 8 Tahun 2016 tentang Pembentukan dan Susunan Perangkat Daerah Kabupaten Banyuwangi;</td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td>3.	Peraturan Bupati Banyuwangi Nomor 71 Tahun 2016 tentang Kedudukan, Susunan Organisasi, Tugas dan Fungsi serta Tata Kerja Inspektorat Kabupaten Banyuwangi;</td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td>4.	Dokumen Pelaksanaan Anggaran (DPA-SKPD) Tahun 2017 Nomor : 1.20.07.20.01.5.2.</td>
    </tr>
</table>
<br>
<h4 class="title center">MEMERINTAHKAN</h4>
<table width="100%">
    <tr>
        <td>Kepada</td>
        <td>:</td>
        <td></td>
    </tr>
    <tr>
<td></td>
<td></td>
<td width="85%"> <table width="100px">
        <tr class="center bdr">
        <td class="bdr" width="100px" style="text-align: center">NO</td>
        <td class="bdr" width="200px">NAMA DAN NIP</td>
        <td class="bdr" width="100px">KETERANGAN</td>
        </tr>
        <tbody>
        <?php
        $no=1;
        for($i=0;$i<count($nama);$i++){
            ?>
            <tr class="center bdr">
                <td class="bdr" style="text-align: center"><?=$no?></td>
                <td class="bdr"><?=$nama[$i]?><br><?=$nip[$i]?></td>
                <td class="bdr">-</td>
            </tr>
            <?
            $no++;
        }
        ?>

        </tbody>
    </table>
</td>
       </tr>
    <br>
    <br>
    <tr>
        <td>Untuk</td>
        <td>:</td>
        <td><?php
            $no=1;
            foreach($tujuan as $t){
                echo $no." ".$t['ket'];
                $no++;}
            ?></td>
    </tr> <tr>
        <td></td>
        <td></td>
        <td><?=$no?> Membuat laporan kegiatan dimaksud</td>
    </tr>
    <br>

</table>
<table width="100%">


    <tr>
        <td></td>
        <td></td>
        <td  width=50%>Dikeluarkan di : Banyuwangi</td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td width=50%>Pada tanggal     :  <?=format_waktu($laporan['spt_date'])?></td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td width=50%>INSPEKTUR<br>
            KABUPATEN BANYUWANGI
        </td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td></td>
    </tr><br><br><br>
    <tr>
        <td></td>
        <td></td>
        <td width=50%>ISKANDAR AZIS, S.H., M.M.<br>
            Pembina Utama Muda<br>
            NIP. <?=$nip['nip']?>
        </td>
    </tr>
</table>

<htmlpagefooter name="footer">
    <table width="100%">
        <tr>
            <td style="text-align: left"><?= 'Export on: '.date('d/m/Y H:i:s') ?></td>
            <td style="text-align: right">{PAGENO}</td>
        </tr>
    </table>
</htmlpagefooter>
<style>
    @page {
        margin-top: 30px;
        margin-bottom: 70px;
        margin-left: 30px;
        margin-right: 30px;
        footer: html_footer;
        header: html_header;
    }
    .list {
        width: 100%;
        margin-top: 20px;
        margin-bottom: 10px;
        border-bottom: 1px solid black;
    }
    th{
        margin: 10px;
        text-transform: uppercase;
    }
    .right{
        text-align: right;
    }
    .center{
        text-align: center;
    }
    th.line{
        border-bottom: 1px solid black;
    }
    .title{
        margin: 0px;
    }
    .bdr{
        border-bottom: 1px solid black;
    }
</style>