<div class="row">
    <div class="col-sm-12">
        <div class="card-box row">
            <form method="post" class="col-sm-8 col-xs-11">
                <div class="form-group row">
                    <div class="col-sm-3">
                        <select name="bulan" class="select2 form-control" required>
                            <?php
                            $list_bulan=array('-','Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember');
                            foreach ($list_bulan as $i=>$b){
                                echo "<option value='$i' ".($i==$bulan?'selected=""':"").">$b</option>";
                            }
                            ?>
                        </select>
                    </div>
                    <div class="col-sm-3">
                        <select name="tahun" class="select2 form-control" required>
                            <?php
                            for($i=date('Y');$i>(date('Y')-5);$i--){
                                echo "<option value='$i' ".($i==$tahun?'selected=""':"").">$i</option>";
                            }
                            ?>
                        </select>
                    </div>
                    <div class="col-sm-6">
                        <input type="hidden" name="type">
                        <button type="submit" class="hidden">SUBMIT</button>
                        <button type="button" data-type="post" class="btn btn-submit-data btn-primary waves-effect waves-light">VIEW</button>
                        <button type="button" data-mode="pdf" data-type="get" class="btn btn-submit-data btn-danger waves-effect waves-light">PDF</button>
                        <button type="button" data-mode="excel" data-type="get" class="btn btn-submit-data btn-success waves-effect waves-light">EXCEL</button>
                    </div>
                </div>
            </form>
            <div class="col-xs-12">
                <hr>
                <?php
                if(isset($data)){
                ?>
                <div class="table-responsive">
                    <table class="table table-bordered table-striped table-hover">
                        <thead>
                        <tr>
                            <th class="no-sort center" rowspan="2">No</th>
                            <th class="no-sort center" rowspan="2">Kode Item</th>
                            <th class="no-sort center" rowspan="2">Nama Item</th>
                            <th class="no-sort center" colspan="2">Saldo Awal</th>
                            <th class="no-sort center" colspan="2">Masuk</th>
                            <th class="no-sort center" colspan="2">Keluar</th>
                            <th class="no-sort center" colspan="2">Adj</th>
                            <th class="no-sort center" colspan="2">Saldo Akhir</th>
                        </tr>
                        <tr>
                            <th class="no-sort center">Qty</th>
                            <th class="no-sort center">Harga</th>
                            <th class="no-sort center">Qty</th>
                            <th class="no-sort center">Harga</th>
                            <th class="no-sort center">Qty</th>
                            <th class="no-sort center">Harga</th>
                            <th class="no-sort center">Qty</th>
                            <th class="no-sort center">Harga</th>
                            <th class="no-sort center">Qty</th>
                            <th class="no-sort center">Harga</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        foreach ($data as $d) {
                            ?>
                            <tr>
                                <td class="center"><?= isset($d['id'])?@++$no:'' ?></td>
                                <td class="center"><?=$d['kode']?></td>
                                <td><?= $d['nama'] ?></td>
                                <td class="center"><?=$d['awal_qty']?></td>
                                <td class="right"><?=format_uang($d['awal_harga']) ?></td>
                                <td class="center"><?=$d['masuk_qty']?></td>
                                <td class="right"><?=format_uang($d['masuk_harga']) ?></td>
                                <td class="center"><?=$d['keluar_qty']?></td>
                                <td class="right"><?=format_uang($d['keluar_harga']) ?></td>
                                <td class="center"><?=$d['adj_qty']?></td>
                                <td class="right"><?=format_uang($d['adj_harga']) ?></td>
                                <td class="center"><?=$d['akhir_qty']?></td>
                                <td class="right"><?=format_uang($d['akhir_harga']) ?></td>
                            </tr>
                            <?php
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <?php
            }
            ?>
        </div>
    </div>
</div>
<link href="<?=base_url()?>assets/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css"/>
<link href="<?=base_url()?>assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="<?=base_url()?>assets/plugins/select2/js/select2.min.js"></script>
<script src="<?=base_url()?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script>
    $('select').select2();
    $('#date-range').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true,
        todayHighlight: true
    });
    function rincian(_i,_nota) {
        $.post('?',{d:_i},function (data,status) {
            $('h4.modal-title','#detailpenerimaan').html(_nota);
            $('tbody','#detailpenerimaan').html(data.data);
            $('#detailpenerimaan').modal('show');
        },'json');
    }
    $('.btn-submit-data').on('click',function (e) {
        var _typ=$(this).data('type');
        if(_typ=='post'){
            $($(this).closest('form')).attr('target','_self');
            $($(this).closest('form')).attr('action','');
        }else{
            var _act=$(this).data('mode');
            $($(this).closest('form')).attr('target','_blank');
            $('[name=type]',$(this).closest('form')).val(_act);
            $($(this).closest('form')).attr('action',"<?=base_url('export/stok')?>");
        }
        $($(this).closest('form')).attr('method',_typ);
        $('[type=submit]',$(this).closest('form')).click();
    });
</script>