
<div class="row" ng-app="emjesSA" ng-controller="emjesController" ng-init="init()">
    <div class="col-sm-12">
        <div class="card-box row">
            <?php
            show_alert();
            ?>
            <div class="col-md-12">
                <form role="form" method="post">
                    <div class="form-group row">
                        <label for="tim" class="col-sm-2 form-control-label">No. LHP<span
                                class="text-danger">*</span></label>
                        <div class="col-sm-6">
                            <input type="number" name="no" required class="form-control" id="no" placeholder="Nomor LHP" value="<?= @$edit['no'] ?>">
                            <input type="hidden" name="id_h" required value="<?= @$id_edit ?>">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="inputName" class="col-sm-2 form-control-label">Data Umum Obrik<span
                                class="text-danger">*</span></label>
                        <div class="col-sm-10">
                            <textarea class="form-control" class="mytextarea" name="obrik" placeholder="Data Umum Obrik" required><?=$edit['obrik']?></textarea>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="inputName" class="col-sm-2 form-control-label">Tujuan<span
                                class="text-danger">*</span></label>
                        <div class="col-sm-10">
                            <textarea class="form-control" class="mytextarea" name="tujuan" placeholder="Tujuan" required><?=$edit['tujuan']?></textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputName" class="col-sm-2 form-control-label">Sasaran<span
                                class="text-danger">*</span></label>
                        <div class="col-sm-10">
                            <textarea class="form-control" class="mytextarea" name="sasaran" placeholder="Sasaran" required><?=$edit['sasaran']?></textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputName" class="col-sm-2 form-control-label">Ruang Lingkup<span
                                class="text-danger">*</span></label>
                        <div class="col-sm-10">
                            <textarea class="form-control" class="mytextarea" name="ruang" placeholder="Ruang Lingkup" required><?=$edit['ruang']?></textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputName" class="col-sm-2 form-control-label">Evaluasi SPI<span
                                class="text-danger">*</span></label>
                        <div class="col-sm-10">
                            <textarea class="form-control" class="mytextarea" name="evaluasi" placeholder="Evaluasi SPI" required><?=$edit['evaluasi']?></textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputName" class="col-sm-2 form-control-label">Judul Temuan<span
                                class="text-danger">*</span></label>
                        <div class="col-sm-10">
                            <textarea class="form-control" class="mytextarea" name="judul" placeholder="Judul Temuan" required><?=$edit['judul']?></textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputName" class="col-sm-2 form-control-label">Kondisi<span
                                class="text-danger">*</span></label>
                        <div class="col-sm-10">
                            <textarea class="form-control" class="mytextarea" name="kondisi" placeholder="Kondisi" required><?=$edit['kondisi']?></textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputName" class="col-sm-2 form-control-label">Kriteria<span
                                class="text-danger">*</span></label>
                        <div class="col-sm-10">
                            <textarea class="form-control" class="mytextarea" name="kriteria" placeholder="Kriteria" required><?=$edit['kriteria']?></textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputName" class="col-sm-2 form-control-label">Penyebab<span
                                class="text-danger">*</span></label>
                        <div class="col-sm-10">
                            <textarea class="form-control" class="mytextarea" name="penyebab" placeholder="Penyebab" required><?=$edit['penyebab']?></textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputName" class="col-sm-2 form-control-label">Akibat<span
                                class="text-danger">*</span></label>
                        <div class="col-sm-10">
                            <textarea class="form-control" class="mytextarea" name="akibat" placeholder="Kriteria" required><?=$edit['kriteria']?></textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputName" class="col-sm-2 form-control-label">Rekomendasi<span
                                class="text-danger">*</span></label>
                        <div class="col-sm-10">
                            <textarea class="form-control" class="mytextarea" name="rekomendasi" placeholder="Rekomendasi" required><?=$edit['rekomendasi']?></textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputName" class="col-sm-2 form-control-label">Tanggapan<span
                                class="text-danger">*</span></label>
                        <div class="col-sm-10">
                            <textarea class="form-control" class="mytextarea" name="tanggapan" placeholder="Tanggapan" required><?=$edit['tanggapan']?></textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputName" class="col-sm-2 form-control-label">Tindak Lanjut<span
                                class="text-danger">*</span></label>
                        <div class="col-sm-10">
                            <textarea class="form-control" class="mytextarea" name="tindak_lanjut" placeholder="Tindak Lanjut" required><?=$edit['tindak_lanjut']?></textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputName" class="col-sm-2 form-control-label">Tindak Lanjut Sebelumnya<span
                                class="text-danger">*</span></label>
                        <div class="col-sm-10">
                            <textarea class="form-control" class="mytextarea" name="tindak_lanjut_s" placeholder="Tindak Lanjut Sebelumnya" required><?=$edit['tindak_lanjut_s']?></textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2 control-label">Temuan & Tindak Lanjut</label>
                        <button type="button" ng-click="tambah()" class="btn btn-inverse"><i class="fa fa-plus"></i> TAMBAH DATA</button>
                        <!--                        <label class="col-md-9 control-label">Temuan & Tindak Lanjut</label>-->
                        <br>
                        <br>
                        <br>
                        <div class="col-md-12">
                            <table class="table table-striped ada-border">
                                <tbody>
                                <tr>
                                    <th class="center v-mid ada-border" rowspan="2">Kode Temuan</th>
                                    <th class="center v-mid ada-border" rowspan="2">Temuan</th>
                                    <th class="center v-mid ada-border" rowspan="2">Kode Rekom</th>
                                    <th class="center v-mid ada-border" rowspan="2">Uraian Rekom</th>
                                    <th class="center v-mid ada-border" colspan="2">Nilai Kerugian</th>
                                    <th class="center v-mid ada-border" rowspan="2">Tindak Lanjut </th>
                                    <th class="center v-mid ada-border" colspan="2">Hasil Tidak Lanjut</th>
                                    <th class="center v-mid ada-border" rowspan="2">Keterangan</th>
                                    <th class="center v-mid ada-border" rowspan="2">Action</th>
                                </tr>
                                <tr>
                                    <th class="center ada-border">Negara</th>
                                    <th class="center ada-border">Daerah</th>
                                    <th class="center ada-border">Hasil Rekom</th>
                                    <th class="center ada-border">Nilai Rekom</th>
                                </tr>

                                <tr ng-repeat="(key, item) in temuan" ng-click="pilihaturan(key)" ng-class="item.checked?'selected':'unselect'"
                                    class="selection">
                                    <td class="center ada-border">{{item.kode_t}}</td>
                                    <td class=" ada-border">{{item.temuan}}</td>
                                    <td class="center ada-border">{{item.kode_r}}</td>
                                    <td class=" ada-border">{{item.rekom}}</td>
                                    <td class="right ada-border">{{item.nk1}}</td>
                                    <td class="right ada-border">{{item.nk2}}</td>
                                    <td class=" ada-border">{{item.tl}}</td>
                                    <td class="center ada-border">{{item.hasil_r}}</td>
                                    <td class="right ada-border">{{item.nilai_r}}</td>
                                    <td class=" ada-border">{{item.keterangan}}</td>
                                    <td class="center ada-border">
                                        <button class="btn btn-xs btn-danger" ng-click="hapustemuan(key)">
                                            <i class="fa fa-remove"></i></button>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-offset-3 col-md-4">
                            <input type="submit" class="hidden">
                            <button type="button" ng-disabled="temuan.length<1" ng-click="simpan(0)" class="btn btn-inverse"><i class="fa fa-check"></i> SUBMIT</button>
                            <!--                            <button type="button" ng-click="simpan(1)" class="btn btn-success"><i class="fa fa-file-text"></i> DRAFT</button>-->
                            <a href="<?=base_url('internalproses/tindak_lanjut')?>" class="btn btn-default"><i class="fa fa-backward"></i> KEMBALI</a>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-md-12">
                <hr>
            </div>
        </div>
    </div><div class="modal fade none-border" id="tambahModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Tambah Kode Temuan</h4>
                </div>
                <div class="modal-body p-20">
                    <form method="post" id="tambah_hari">
                        <div class="form-group row">
                            <label for="inputName" class="col-sm-4 form-control-label">Kode Temuan<span class="text-danger">*</span></label>
                            <div class="col-sm-7">
                                <select name="kt" ng-model="kt" id="kt" required class="form-control select2">
                                    <option value="">-</option>
                                    <?php
                                    foreach ($parent as $p){
                                        echo "<option value='$p[id]' ".($p['id']==@$form['parent']?'selected=""':"")." data-posisi='$p[dk]'>$p[kode] - $p[nama]</option>";
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="inputName" class="col-sm-4 form-control-label">Uraian Temuan<span class="text-danger">*</span></label>
                            <div class="col-sm-7">
                                <input type="text" name="ut" ng-model="ut" autocomplete="off" placeholder="Temuan" required value="<?=(@$rencana['rekom'])?>" class="form-control">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputName" class="col-sm-4 form-control-label">Kode Rekom<span class="text-danger">*</span></label>
                            <div class="col-sm-7">
                                <select name="kr" ng-model="kr" id="kr" required class="form-control select2">
                                    <option value="">-</option>
                                    <?php
                                    foreach ($parent2 as $p){
                                        echo "<option value='$p[id]' ".($p['id']==@$form['parent']?'selected=""':"")." data-posisi='$p[dk]'>$p[kode] - $p[nama]</option>";
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="inputName" class="col-sm-4 form-control-label">Uraian Rekom<span class="text-danger">*</span></label>
                            <div class="col-sm-7">
                                <input type="text" name="uk" ng-model="uk" autocomplete="off" placeholder="Rekomendasi" required value="<?=(@$rencana['rekom'])?>" class="form-control">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputName" class="col-sm-4 form-control-label">Negara/Daerah<span class="text-danger">*</span></label>
                            <div class="col-sm-7">
                                <select name="nd" ng-model="nd" id="nd" required class="form-control select2">
                                    <option value="">-</option>
                                    <option value="1">Negara</option>
                                    <option value="2">Daerah</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputName" class="col-sm-4 form-control-label">Nilai Kerugian<span class="text-danger">*</span></label>
                            <div class="col-sm-7">
                                <input type="number" name="nk" id="nk" ng-model="nk" autocomplete="off" placeholder="Nilai Kerugian" required value="<?=(@$temuan['kerugian'])?>" class="form-control">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputName" class="col-sm-4 form-control-label">Tindak Lanjut Entitas<span class="text-danger">*</span></label>
                            <div class="col-sm-7">
                                <input type="text" name="tl" id="tl" ng-model="tl" autocomplete="off" placeholder="Tindak Lanjut Entitas" required value="<?=(@$temuan['tl'])?>" class="form-control">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputName" class="col-sm-4 form-control-label">Hasil Tindak Lanjut<span class="text-danger">*</span></label>
                            <div class="col-sm-7">
                                <select name="htl" ng-model="htl" id="htl" required class="form-control select2">
                                    <option value="">-</option>
                                    <option value="1">Sesuai</option>
                                    <option value="2">Belum Sesuai</option>
                                    <option value="3">Belum Ditindaklanjuti</option>
                                    <option value="4">Tidak Dapat Ditindaklanjuti</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputName" class="col-sm-4 form-control-label">Nilai Tindak Lanjut<span class="text-danger">*</span></label>
                            <div class="col-sm-7">
                                <input type="number" name="ntl" id="ntl" ng-model="ntl" autocomplete="off" placeholder="Nilai Kerugian" required value="<?=(@$temuan['ntl']?$temuan['ntl']:0)?>" class="form-control">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputName" class="col-sm-4 form-control-label">Keterangan</label>
                            <div class="col-sm-7">
                                <input type="text" name="keterangan" id="keterangan" ng-model="keterangan" autocomplete="off" placeholder="Keterangan" required value="<?=(@$temuan['keterangan'])?>" class="form-control">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputName" class="col-sm-12 center form-control-label text-danger" id="alert-kurang"></label>
                        </div>
                        <div class="col-sm-offset-4">
                            <button type="button" ng-click="tambah_temuan()" id="tambah" class="btn btn-success save-event waves-effect waves-light">Simpan</button>
                            <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Batal</button>
                            <input type="submit" id="submit_tambah" class="hidden">
                        </div>
                </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="tambahModal2" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-large" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Tambah Kode Temuan</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label class="col-md-3 control-label">Kode Temuan</label>
                    <div class="col-md-4">
                        <input type="text" name="header[kode_temuan]" autocomplete="off" placeholder="Kode Temuan" required value="" class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Temuan</label>
                    <div class="col-md-4">
                        <input type="text" name="header[temuan]" autocomplete="off" placeholder="Temuan" required value="" class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Kode Rekomendasi</label>
                    <div class="col-md-4">
                        <input type="text" name="header[kode_rekom]" autocomplete="off" placeholder="Kode Rekomendasi" required value="" class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Uraian Rekomendasi</label>
                    <div class="col-md-4">
                        <input type="text" name="header[rekom]" autocomplete="off" placeholder="Rekomendasi" required value="" class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">NK Negara</label>
                    <div class="col-md-4">
                        <input type="text" name="header[nk_n]" autocomplete="off" placeholder="NK Negara" required value="" class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">NK Daerah</label>
                    <div class="col-md-4">
                        <input type="text" name="header[nk_d]" autocomplete="off" placeholder="NK Daerah" required value="" class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Tindak Lanjut Entitas</label>
                    <div class="col-md-4">
                        <input type="text" name="header[tindak_lanjut]" autocomplete="off" placeholder="Tindak Lanjut" required value="" class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Hasil </label>
                    <div class="col-md-4">
                        <select class="form-control select2" name="header[hasil]" required >
                            <option value="">pilih hasil</option>
                            <?php
                            $pkpt=array('Sesuai', 'Belum Sesuai','Belum Ditindaklanjuti', 'Tidak Dapat ditindaklanjuti');
                            foreach ($pkpt as $v):
                                ?>
                                <option value="<?= $v ?>" ><?= "$v" ?></option>
                                <?php
                            endforeach;
                            ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Nilai Tindak Lanjut</label>
                    <div class="col-md-4">
                        <input type="text" name="header[nilai]" autocomplete="off" placeholder="Nilai" required value="" class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Keterangan</label>
                    <div class="col-md-4">
                        <input type="text" name="header[keterangan]" autocomplete="off" placeholder="Keterangan" required value="" class="form-control">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<link href="<?= base_url() ?>assets/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="<?= base_url() ?>assets/plugins/select2/js/select2.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/plugins/parsleyjs/parsley.min.js"></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/tinymce/4.8.3/tinymce.min.js'></script><script src="<?=base_url()?>assets/js/angular.min.js"></script>
<script src="<?=base_url()?>assets/js/ng-messages.min.js"></script>
<script type="text/javascript">
    tinymce.init({
        selector: 'textarea',
        menubar:false,
        statusbar: false,

    });
    $(function () {
        $('.select2').select2({});
        $('form').parsley();
        $("form").submit(function (event) {
            $(this).parsley().validate();
            if (!$(this).parsley().isValid()) {
                event.preventDefault();
            } else if ($('#hori-pass1').val() != $('#hori-pass2').val()) {
                event.preventDefault();
                $('#hori-pass2').focus();
            }
        });

    });
    var app = angular.module("emjesSA", ['ngMessages']);
    app.filter("arrayFilter",function() {
        return function (items, idx, filterarray) {
            var filtered = [];
            angular.forEach(items, function (value, key) {
                angular.forEach(filterarray, function (v, k) {
                    if (value[idx] == v){
                        filtered.push(value);
                    }
                });
            });
            return filtered;
        }
    });
    app.filter("matchFilter",function() {
        return function (items, idx, filterarray) {
            var filtered = [];
            angular.forEach(items, function (value, key) {
                if (value[idx] == filterarray){
                    filtered.push(value);
                }
            });
            return filtered;
        }
    });
    app.directive('taoRepeatDirective', function() {
        return function(scope, element, attrs) {
            if (scope.$last){
                $('.select2').select2();
                $('.datepicker').datepicker({
                    format: 'dd/mm/yyyy',
                    autoclose: true,
                    todayHighlight: true
                });
            }
        };
    });
    app.directive('validNumber', function() {
        return {
            require: '?ngModel',
            link: function(scope, element, attrs, ngModelCtrl) {
                if(!ngModelCtrl) {
                    return;
                }
                ngModelCtrl.$parsers.push(function(val) {
                    if (angular.isUndefined(val)) {
                        var val = '';
                    }
                    var clean = val.replace(/[^-0-9\.]/g, '');
                    var decimalCheck = clean.split('.');
                    if(!angular.isUndefined(decimalCheck[1])) {
                        decimalCheck[1] = decimalCheck[1].slice(0,2);
                        clean =decimalCheck[0] + '.' + decimalCheck[1];
                    }
                    if (val !== clean) {
                        ngModelCtrl.$setViewValue(clean);
                        ngModelCtrl.$render();
                    }
                    return clean;
                });
                element.bind('keypress', function(event) {
                    if(event.keyCode === 32) {
                        event.preventDefault();
                    }
                });
            }
        };
    });
    app.controller('emjesController', function($scope,$http) {
        $scope.filtertim = function(item) {
            if ($scope.timpelaksana.indexOf(item.id) > -1) {
                return item;
            }
        };
        $scope.aturan=[];
        $scope.pedoman=[];
        $scope.pilihpelaksana='';
        $scope.listtim=[];
        $scope.init = function(){
        };
        $scope.pilihaturan = function(_i) {
            $scope.aturan[_i].checked=!$scope.aturan[_i].checked;
        };
        $scope.tambahpelaksana = function() {
            if ($scope.timpelaksana.indexOf($scope.pilihpelaksana) < 0 && $scope.pilihpelaksana!="") {
                $scope.timpelaksana.push($scope.pilihpelaksana);
            }
            $scope.pilihpelaksana="";
        };
        $scope.hapuspelaksana = function(_id) {
            var _index=$scope.timpelaksana.indexOf(_id);
            $scope.timpelaksana.splice(_index);
        };
        $scope.hapustemuan = function(_id) {
            var _index=$scope.temuan.indexOf(_id);
            $scope.temuan.splice(_index);
        };
        $scope.pilihpedoman = function(_i) {
            $scope.pedoman[_i].checked=!$scope.pedoman[_i].checked;
        };
        $scope.detailaturan = function(_tao){
            $('.modal-title','#taoModal').html(_tao.kode_kk+' '+_tao.langkah);
            $.post('?tao='+_tao.id,function(data,status){
                $('.modal-body','#taoModal').html(data);
                $('#taoModal').modal('show');
            });
        };
        $scope.tambah = function(){
            $('#tambahModal').modal('show');
//            $.post('?tao='+_tao.id,function(data,status){
//                $('.modal-body','#taoModal').html(data);
//                $('#taoModal').modal('show');
//            });
        };
        $scope.tambah_temuan = function(){
            var _isExist=false;
            console.log($scope.kt);
            console.log($scope.kr);
            console.log($scope.nd);
            console.log($scope.tl);
            if($scope.ut == '' ||  $scope.uk == '' || typeof $scope.kt ==='undefined' || typeof $scope.kr === 'undefined' || typeof $scope.nd === 'undefined' || typeof $scope.htl === 'undefined' || typeof $scope.nk === 'undefined'|| typeof $scope.tl === 'undefined'|| typeof $scope.ntl === 'undefined'){

                $('#alert-kurang').empty();
                $('#alert-kurang').html('Ada data yang belum terisi');
            }else{

//            for (var i in $scope.temuan) {
//                if ($scope.temuan[i].id_t == $scope.kt) {
//                    alert('Kode Temuan Sudah Ada');
//                    $('#tambahModal').modal('hide');
//                    _isExist=true;
//                    break;
//                }
//            }
                if(_isExist==false){
                    var kode_t=$( "#kt option:selected" ).text();
                    var kode_temuan=kode_t.split(' - ');
                    var kode_r=$( "#kr option:selected" ).text();
                    var kode_rekom=kode_r.split(' - ');
                    var nd=$( "#nd option:selected" ).text();
                    var htl=$( "#htl option:selected" ).text();

                    $scope.addtemuan={};
                    $scope.addtemuan.id_t=$scope.kt;
                    $scope.addtemuan.kode_t=kode_temuan[0];
//                $scope.addtemuan.temuan=kode_temuan[1];
                    $scope.addtemuan.temuan=$scope.ut;
                    $scope.addtemuan.id_r=$scope.kr;
                    $scope.addtemuan.kode_r=kode_rekom[0];
                    $scope.addtemuan.rekom=$scope.uk;
                    $scope.addtemuan.tl=$scope.tl;
                    $scope.addtemuan.kode_hr=$scope.htl;
                    $scope.addtemuan.hasil_r=htl;
                    $scope.addtemuan.nilai_r=$scope.ntl;
                    $scope.addtemuan.keterangan=$scope.keterangan;
                    if($scope.nd==1){
                        $scope.addtemuan.nd=1;
                        $scope.addtemuan.nk1=$scope.nk;
                        $scope.addtemuan.nk2=0;
                    }
                    else{
                        $scope.addtemuan.nd=2;
                        $scope.addtemuan.nk2=$scope.nk;
                        $scope.addtemuan.nk1=0;
                    }
                    $scope.temuan.unshift($scope.addtemuan);
                    console.log($scope.temuan);
                    $scope.addtemuan=null;
                    $('#tambahModal').modal('hide');
                }

                $scope.kt='';
                $scope.ut='';
                $scope.uk='';
                $scope.kr='';
                $scope.nd='';
                $scope.nk='';
                $scope.tl='';
                $scope.htl='';
                $scope.ntl='';
                $scope.keterangan='';
            }
        };
        $scope.simpan = function(_draft) {
            $('#preloader').show();
            $('#status','#preloader').show();
            $http({
                method: 'POST',
                url: "?id_test=<?=$rencana['id']?>",
                responseType: 'json',
                params: {
                    temuans:JSON.stringify($scope.temuan),
                }
            }).success(function(data){
                window.location.assign('?');
                setTimeout(function () {
                    $('#preloader').hide();
                    $('#status','#preloader').hide();
                },2000);
                console.log("OK", data)
            }).error(function(err){"ERR", console.log(err)});
        };
    });
</script>