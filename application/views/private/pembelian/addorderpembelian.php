<div ng-app="gradinSA" ng-controller="orderController" ng-init="init()">
<div class="row">
    <div class="col-sm-12">
        <div class="card-box row">
            <?php
            include VIEWPATH.'alert.php';
            ?>
            <form id="form-order" role="form" method="post" ng-submit="simpanorder()">
                <div class="col-md-6 col-md-offset-3">
                    <div class="form-group row">
                        <label for="inputEmail" class="col-sm-4 form-control-label">Tanggal<span class="text-danger">*</span></label>
                        <div class="col-sm-7">
                            <div class="input-group">
                                <input type="text" name="tanggal" required class="form-control datepicker" placeholder="tanggal" value="<?=date('d/m/Y')?>" readonly>
                                <span class="input-group-addon bg-custom b-0"><i class="mdi mdi-calendar text-white"></i></span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="hori-pass2" class="col-sm-4 form-control-label">Supplier<span class="text-danger">*</span></label>
                        <div class="col-sm-7">
                            <select name="supplier" class="select2 form-control" required>
                                <option value=''>Pilih Supplier</option>
                                <?php
                                foreach ($supplier as $r){
                                    echo "<option value='$r[id]'>$r[kode] $r[nama]</option>";
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row ini-haram">
                        <label for="inputName" class="col-sm-4 form-control-label">DPP</label>
                        <div class="col-sm-7" title="DPP">
                            <h5 class="nomargin">{{dpp|currency:'':2}}</h5>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputName" class="col-sm-4 form-control-label">Total</label>
                        <div class="col-sm-7" title="Subtotal">
                            <h5 class="nomargin">{{total|currency:'':2}}</h5>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputName" class="col-sm-4 form-control-label">Uang Muka</label>
                        <div class="col-sm-7">
                            <input name="uangmuka" ng-model="uangmuka" class="form-control input-override" style="height: 20px">
                            <h5 class="nomargin">{{uangmuka|currency:'':2}}</h5>
                        </div>
                    </div>
                    <div class="form-group row" ng-show="uangmuka+1 == 0">
                        <label class="col-sm-4 form-control-label">Metode Pembayaran<span class="text-danger">*</span></label>
                        <div class="col-sm-7">
                            <select name="metodepembayaran" ng-model="coa" class="select2 form-control">
                                <option value=''>-</option>
                                <?php
                                foreach ($metode as $index=>$m){
                                    $last=array();
                                    $last_id=array(
                                        1=>@$m[0]['k1'],
                                        2=>@$m[0]['k2'],
                                        3=>@$m[0]['k3']
                                    );
                                    $group=0;
                                    foreach ($m[1] as $r){
                                        if($r['level']==1){
                                            $last_id[1]=$r['k1'];
                                            $last[1]=$r['nama'];
                                            continue;
                                        }elseif($r['level']==2 && "$r[k1]"=="$last_id[1]"){
                                            $last_id[2]=$r['k2'];
                                            $last[2]=$r['nama'];
                                            continue;
                                        }elseif($r['level']==3 && "$r[k1]$r[k2]"=="$last_id[1]$last_id[2]"){
                                            $last_id[3]=$r['k3'];
                                            $last[3]=$r['nama'];
                                            if($group>0){
                                                echo "</optgroup>";
                                            }
                                            $group++;
                                            echo "<optgroup label=\"".implode(" - ",$last)."\">";
                                            continue;
                                        }elseif($r['level']==4 && "$r[k1]$r[k2]$r[k3]"==implode("",$last_id)){
                                            echo "<option value='$r[id];$index'>" . format_coa($r['kode']) . " $r[nama]</option>";
                                        }
                                    }
                                    if($group>0){
                                        echo "</optgroup>";
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputName" class="col-sm-4 form-control-label">Sisa Tagihan</label>
                        <div class="col-sm-7" title="Sisa Tagihan">
                            <h5 class="nomargin">{{total-uangmuka|currency:'':2}}</h5>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputName" class="col-sm-4 form-control-label">Mengetahui</label>
                        <div class="col-sm-7">
                            <input type="hidden" name="request_by">
                            <input type="text" name="karyawan" class="form-control" placeholder="Masukkan NIK / Nama" style="height: 20px">
                            <textarea name="items[]" class="hidden" ng-repeat="(key, item) in items">{{item}}</textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-8 col-sm-offset-4">
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <button type="button" ng-click="formtambah()" class="btn btn-primary waves-effect waves-light ini-haram"><i class="fa fa-plus"></i> Tambah Barang/Jasa</button>
                    <hr>
                    <div id="detailppb">
                        <table class="table table-bordered table-striped table-hover">
                            <thead>
                                <tr>
                                    <th class="center col-xs-3">Barang/Jasa</th>
                                    <th class="center col-xs-2">Harga</th>
                                    <th class="center col-xs-1">Diskon</th>
                                    <th class="center col-xs-1">DPP</th>
                                    <th class="center col-xs-2">Subtotal</th>
                                    <th class="center col-xs-2">No. PBB</th>
                                    <th class="center">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr ng-class="item.harga?'hand-cursor':'hand-cursor btn-warning'" ng-repeat="(key, item) in items" ng-click="edit(key)">
                                    <td>{{item.barangjasa}}</td>
                                    <td class="right">{{item.qty}} x {{item.harga|currency:'':2}}</td>
                                    <td class="right">{{item.diskon|currency:'':2}}</td>
                                    <td class="right">{{item.dpp|currency:'':2}}</td>
                                    <td class="right">{{item.subtotal|currency:'':2}}</td>
                                    <td class="center">{{item.nota}}</td>
                                    <td class="center"><button type="button" class="btn btn-xs btn-danger" ng-click="hapus(key)"><i class="fa fa-remove"></i></button></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <hr>
                    <button type="submit" class="btn btn-inverse waves-effect waves-light">Simpan OPB</button>
                    <a href="?permintaan=true" class="btn btn-default waves-effect m-l-5">Kembali ke pilih PBB</a>
                </div>
            </form>
        </div>
    </div>
</div>
    <div id="formedit" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <form ng-submit="simpanedit()">
                        <div class="form-group row">
                            <label for="inputName" class="col-sm-4 form-control-label">Nomor PPB</label>
                            <div class="col-sm-7">
                                <h5>{{editnota}}</h5>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="hori-pass2" class="col-sm-4 form-control-label">Barang / Jasa</label>
                            <div class="col-sm-7">
                                <input type="text" ng-model="editbarangjasa" readonly class="form-control" placeholder="Masukkan nama barang / jasa" style="height: 20px">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputName" class="col-sm-4 form-control-label">Jumlah<span class="text-danger">*</span></label>
                            <div class="col-sm-7">
                                <input type="text" ng-model="editqty" ng-change="updateppnpph()" required class="form-control" placeholder="" onclick="$(this).select()" style="height: 20px">
                                <span ng-if="editjumlah" class="help-block"><small style="color: {{jumlahcolor}};">jumlah permintaan: {{editjumlah}}</small></span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputName" class="col-sm-4 form-control-label">Harga Satuan<span class="text-danger">*</span></label>
                            <div class="col-sm-7">
                                <input ng-model="editharga" ng-change="updateppnpph()" class="form-control input-override" style="height: 20px">
                                <h5 class="nomargin">{{editharga|currency:'':2}}</h5>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputName" class="col-sm-4 form-control-label">Total</label>
                            <div class="col-sm-7">
                                <h5 class="nomargin">{{hargasatuan|currency:'':2}}</h5>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputName" class="col-sm-4 form-control-label">Diskon</label>
                            <div class="col-sm-7">
                                <input ng-model="editdiskon" ng-change="updateppnpph()" class="form-control input-override" style="height: 20px">
                                <h5 class="nomargin">{{editdiskon|currency:'':2}}</h5>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputName" class="col-sm-4 form-control-label">DPP</label>
                            <div class="col-sm-7">
                                <h5 class="nomargin">{{editdpp|currency:'':2}}</h5>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputName" class="col-sm-4 form-control-label">
                                <div class="col-xs-8 nopadding">
                                    PPn
                                </div>
                                <div class="col-xs-4">
                                    <input type="text" maxlength="5" valid-number ng-model="editppnpersen" ng-change="updateppnpph()" required class="form-control center" placeholder="%" onclick="$(this).select()" style="height: 20px">
                                </div>
                            </label>
                            <div class="col-sm-7">
                                <h5 class="nomargin">{{editppn|currency:'':2}}</h5>
                            </div>
                        </div>
                        <div class="form-group row" ng-show="edittype=='f'">
                            <label for="inputName" class="col-sm-4 form-control-label">
                                <div class="col-xs-8 nopadding">
                                    PPh 21
                                </div>
                                <div class="col-xs-4">
                                    <input type="text" maxlength="5" valid-number ng-model="editpph21persen" ng-change="updateppnpph()" required class="form-control center" placeholder="%" onclick="$(this).select()" style="height: 20px">
                                </div>
                            </label>
                            <div class="col-sm-7">
                                <h5 class="nomargin">{{editpph21|currency:'':2}}</h5>
                            </div>
                        </div>
                        <div class="form-group row" ng-show="edittype=='t'">
                            <label for="inputName" class="col-sm-4 form-control-label">
                                <div class="col-xs-8 nopadding">
                                    PPh 22
                                </div>
                                <div class="col-xs-4">
                                    <input type="text" maxlength="5" valid-number ng-model="editpph22persen" ng-change="updateppnpph()" required class="form-control center" placeholder="%" onclick="$(this).select()" style="height: 20px">
                                </div>
                            </label>
                            <div class="col-sm-7">
                                <h5 class="nomargin">{{editpph22|currency:'':2}}</h5>
                            </div>
                        </div>
                        <div class="form-group row" ng-show="edittype=='f'">
                            <label for="inputName" class="col-sm-4 form-control-label">
                                <div class="col-xs-8 nopadding">
                                    PPh 23
                                </div>
                                <div class="col-xs-4">
                                    <input type="text" maxlength="5" valid-number ng-model="editpph23persen" ng-change="updateppnpph()" required class="form-control center" placeholder="%" onclick="$(this).select()" style="height: 20px">
                                </div>
                            </label>
                            <div class="col-sm-7">
                                <h5 class="nomargin">{{editpph23|currency:'':2}}</h5>
                            </div>
                        </div>
                        <div class="form-group row" ng-show="edittype=='f'">
                            <label for="inputName" class="col-sm-4 form-control-label">
                                <div class="col-xs-8 nopadding">
                                    PPh 4 (2)
                                </div>
                                <div class="col-xs-4">
                                    <input type="text" maxlength="5" valid-number ng-model="editpph24persen" ng-change="updateppnpph()" required class="form-control center" placeholder="%" onclick="$(this).select()" style="height: 20px">
                                </div>
                            </label>
                            <div class="col-sm-7">
                                <h5 class="nomargin">{{editpph24|currency:'':2}}</h5>
                            </div>
                        </div>
                        <div class="form-group row" ng-show="edittype=='f'">
                            <label for="inputName" class="col-sm-4 form-control-label">
                                <div class="col-xs-8 nopadding">
                                    PPh 26
                                </div>
                                <div class="col-xs-4">
                                    <input type="text" maxlength="5" valid-number ng-model="editpph26persen" ng-change="updateppnpph()" required class="form-control center" placeholder="%" onclick="$(this).select()" style="height: 20px">
                                </div>
                            </label>
                            <div class="col-sm-7">
                                <h5 class="nomargin">{{editpph26|currency:'':2}}</h5>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputName" class="col-sm-4 form-control-label"></label>
                            <div class="col-sm-7">
                                <h3 class="nomargin">{{((edittotal))|currency:'':2}}</h3>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-8 col-sm-offset-4">
                                <button type="submit" class="btn btn-primary waves-effect waves-light">SIMPAN</button>
                                <button type="button" data-dismiss="modal" class="btn btn-default waves-effect waves-light">BATAL</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>
</div>
<link href="<?=base_url()?>assets/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css"/>
<link href="<?=base_url()?>assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css"/>
<link href="<?=base_url()?>assets/css/bootcomplete.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="<?=base_url()?>assets/plugins/select2/js/select2.min.js"></script>
<script src="<?=base_url()?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script src="<?=base_url()?>assets/js/jquery.bootcomplete.js"></script>
<script src="<?=base_url()?>assets/js/angular.min.js"></script>
<script type="text/javascript">
//    function goodbye(e) {
//        if(!e) e = window.event;
//        e.cancelBubble = true;
//        e.returnValue = 'You sure you want to leave?'; //This is displayed on the dialog
//        if (e.stopPropagation) {
//            e.stopPropagation();
//            e.preventDefault();
//        }
//    }
//    window.onbeforeunload=goodbye;
    $('#formedit').on('shown.bs.modal', function () {
        $('[ng-model=editharga]',this).focus();
    });
    $('#formtambah').on('shown.bs.modal', function () {
        $('[name=barangjasa]',this).focus();
    });
    var app = angular.module("gradinSA", []);
    app.directive('validNumber', function() {
        return {
            require: '?ngModel',
            link: function(scope, element, attrs, ngModelCtrl) {
                if(!ngModelCtrl) {
                    return;
                }
                ngModelCtrl.$parsers.push(function(val) {
                    if (angular.isUndefined(val)) {
                        var val = '';
                    }
                    var clean = val.replace(/[^-0-9\.]/g, '');
                    var decimalCheck = clean.split('.');
                    if(!angular.isUndefined(decimalCheck[1])) {
                        decimalCheck[1] = decimalCheck[1].slice(0,2);
                        clean =decimalCheck[0] + '.' + decimalCheck[1];
                    }
                    if (val !== clean) {
                        ngModelCtrl.$setViewValue(clean);
                        ngModelCtrl.$render();
                    }
                    return clean;
                });
                element.bind('keypress', function(event) {
                    if(event.keyCode === 32) {
                        event.preventDefault();
                    }
                });
            }
        };
    });
    app.controller('orderController', function($scope,$http) {
        $scope.items=[];
        $scope.total=0;
        $scope.uangmuka=0;
        $scope.diskonperitem=0;
        $scope.hargasatuan=0;
        $scope.dpp=0;
        $scope.ppn=0;
        $scope.pph21=0;
        $scope.pph22=0;
        $scope.pph23=0;
        $scope.pph24=0;
        $scope.pph25=0;
        $scope.pph26=0;
        $scope.init = function(){
            <?php
            foreach ($data as $d){
                $d['barangjasa']=htmlspecialchars($d['barangjasa']);
                $d['jumlah']=htmlspecialchars($d['jumlah']);
                $d['qty']=is_numeric($d['jumlah'])?$d['jumlah']:1;
                $d['nota']=htmlspecialchars($d['nota']);
                $d['note']='';
                echo '$scope'.".items.unshift({mstitem:'$d[header]_$d[mstitem]',barangjasa:'$d[barangjasa]',type:'$d[type]',jumlah:'$d[jumlah]',nota:'$d[nota]',note:'$d[note]',harga:0,diskon:0,qty:$d[qty],subtotal:0,dpp:0,ppn:0,ppnpersen:0,pph21:0,pph21persen:0,pph22:0,pph22persen:0,pph23:0,pph23persen:0,pph24:0,pph24persen:0,pph25:0,pph25persen:0,pph26:0,pph26persen:0});";
            }
            ?>
        };
        $scope.simpanorder = function() {
            $('#preloader').show();
            $('#status','#preloader').show();
            $scope.form=$('#form-order').serializeArray();
            $http({
                url: "?save=true",
                data: $scope.form,
                method: 'POST',
                headers : {'Content-Type':'application/x-www-form-urlencoded; charset=UTF-8'}

            }).success(function(data){
                window.location.assign('<?=base_url('pembelian/order?permintaan=true')?>');
                setTimeout(function () {
                    $('#preloader').hide();
                    $('#status','#preloader').hide();
                },2000);
                console.log("OK", data)
            }).error(function(err){"ERR", console.log(err)});
        };
        $scope.simpanedit = function() {
            if($scope.editjumlah>=$scope.editqty){
                $scope.items[$scope.editindex].qty=$scope.editqty;
                $scope.items[$scope.editindex].harga=$scope.editharga;

                $scope.items[$scope.editindex].diskon=$scope.editdiskon;
                $scope.items[$scope.editindex].dpp=$scope.editdpp;

                $scope.items[$scope.editindex].ppn=$scope.editppn;
                $scope.items[$scope.editindex].ppnpersen=$scope.editppnpersen;

                $scope.items[$scope.editindex].pph21=$scope.editpph21;
                $scope.items[$scope.editindex].pph22=$scope.editpph22;
                $scope.items[$scope.editindex].pph23=$scope.editpph23;
                $scope.items[$scope.editindex].pph24=$scope.editpph24;
                $scope.items[$scope.editindex].pph25=$scope.editpph25;
                $scope.items[$scope.editindex].pph26=$scope.editpph26;
                $scope.items[$scope.editindex].pph21persen=$scope.editpph21persen;
                $scope.items[$scope.editindex].pph22persen=$scope.editpph22persen;
                $scope.items[$scope.editindex].pph23persen=$scope.editpph23persen;
                $scope.items[$scope.editindex].pph24persen=$scope.editpph24persen;
                $scope.items[$scope.editindex].pph25persen=$scope.editpph25persen;
                $scope.items[$scope.editindex].pph26persen=$scope.editpph26persen;
                $scope.items[$scope.editindex].subtotal=$scope.edittotal;
                $scope.update();
                $('#formedit').modal('hide');
                $scope.jumlahcolor='#333';
            }else{
                $scope.jumlahcolor='red';
            }
        };
        $scope.edit = function(_id) {
            $scope.editindex=_id;
            $scope.jumlahcolor='#333';
            $scope.editnota=$scope.items[_id].nota;
            $scope.editmstitem=$scope.items[_id].mstitem;
            $scope.editbarangjasa=$scope.items[_id].barangjasa;
            $scope.editqty=$scope.items[_id].qty;
            $scope.editjumlah=$scope.items[_id].jumlah;
            $scope.editharga=$scope.items[_id].harga;
            $scope.editdiskon=$scope.items[_id].diskon;
            $scope.editdpp=$scope.items[_id].dpp;
            $scope.editppn=$scope.items[_id].ppn;
            $scope.edittype=$scope.items[_id].type;

            $scope.editpph21=$scope.items[_id].pph21;
            $scope.editpph22=$scope.items[_id].pph22;
            $scope.editpph23=$scope.items[_id].pph23;
            $scope.editpph24=$scope.items[_id].pph24;
            $scope.editpph25=$scope.items[_id].pph25;
            $scope.editpph26=$scope.items[_id].pph26;
            $scope.editpph21persen=$scope.items[_id].pph21persen;
            $scope.editpph22persen=$scope.items[_id].pph22persen;
            $scope.editpph23persen=$scope.items[_id].pph23persen;
            $scope.editpph24persen=$scope.items[_id].pph24persen;
            $scope.editpph25persen=$scope.items[_id].pph25persen;
            $scope.editpph26persen=$scope.items[_id].pph26persen;

            $scope.editppnpersen=$scope.items[_id].ppnpersen;
            $scope.updateppnpph();
            $('#formedit').modal('show');
        };
        $scope.hapus = function(_id) {
            $scope.items.splice(_id, 1);
            $scope.update();
        };
        $scope.updateppnpph=function () {
            $scope.hargasatuan=$scope.editharga*$scope.editqty;
            $scope.editdpp=$scope.hargasatuan-$scope.editdiskon;
            if($scope.editppnpersen>100){
                $scope.editppnpersen=100;
            }else if($scope.editppnpersen<0 || isNaN($scope.editppnpersen)){
                $scope.editppnpersen=0;
            }
            if($scope.editpph21persen>100){
                $scope.editpph21persen=100;
            }else if($scope.editpph21persen<0 || isNaN($scope.editpph21persen)){
                $scope.editpph21persen=0;
            }
            if($scope.editpph22persen>100){
                $scope.editpph22persen=100;
            }else if($scope.editpph22persen<0 || isNaN($scope.editpph22persen)){
                $scope.editpph22persen=0;
            }
            if($scope.editpph23persen>100){
                $scope.editpph23persen=100;
            }else if($scope.editpph23persen<0 || isNaN($scope.editpph23persen)){
                $scope.editpph23persen=0;
            }
            if($scope.editpph24persen>100){
                $scope.editpph24persen=100;
            }else if($scope.editpph24persen<0 || isNaN($scope.editpph24persen)){
                $scope.editpph24persen=0;
            }
            if($scope.editpph25persen>100){
                $scope.editpph25persen=100;
            }else if($scope.editpph25persen<0 || isNaN($scope.editpph25persen)){
                $scope.editpph25persen=0;
            }
            if($scope.editpph26persen>100){
                $scope.editpph26persen=100;
            }else if($scope.editpph26persen<0 || isNaN($scope.editpph26persen)){
                $scope.editpph26persen=0;
            }
            $scope.edittotal=$scope.editdpp;
            $scope.editppn=($scope.editdpp/100)*$scope.editppnpersen;
            $scope.edittotal=($scope.edittotal*1)+($scope.editppn*1);
            if($scope.edittype=='t'){
                $scope.editpph22=($scope.editdpp/100)*$scope.editpph22persen;
                $scope.edittotal=($scope.edittotal*1)-($scope.editpph22*1);
            }else{
                $scope.editpph21=($scope.editdpp/100)*$scope.editpph21persen;
                $scope.edittotal=($scope.edittotal*1)+($scope.editpph21*1);
                $scope.editpph23=($scope.editdpp/100)*$scope.editpph23persen;
                $scope.edittotal=($scope.edittotal*1)+($scope.editpph23*1);
                $scope.editpph24=($scope.editdpp/100)*$scope.editpph24persen;
                $scope.edittotal=($scope.edittotal*1)+($scope.editpph24*1);
                $scope.editpph25=($scope.editdpp/100)*$scope.editpph25persen;
                $scope.edittotal=($scope.edittotal*1)+($scope.editpph25*1);
                $scope.editpph26=($scope.editdpp/100)*$scope.editpph26persen;
                $scope.edittotal=($scope.edittotal*1)+($scope.editpph26*1);
            }
        };
        $scope.update=function () {
            $scope.total=0;
            $scope.dpp=0;
            $scope.ppn=0;
            $scope.diskonperitem=0;
            for (var i in $scope.items) {
                $scope.dpp=parseFloat($scope.dpp)+parseFloat($scope.items[i].dpp);
                $scope.total=parseFloat($scope.total)+parseFloat(($scope.items[i].subtotal));
                $scope.diskonperitem=parseFloat($scope.diskonperitem)+parseFloat($scope.items[i].diskon);
            }
        };
    });
    $(function () {
        $('.select2').select2();
        $('.datepicker').datepicker({
            format: 'dd/mm/yyyy',
            autoclose: true,
            todayHighlight: true
        });
        $('input[name=karyawan]').bootcomplete({
            url:'?k=true',
            minLength : 1,
            method: 'post',
            idFieldName: 'request_by',
            formParams: {
                'unit' : $('[name=request_unit]')
            }
        });
        $('input[name=barangjasa]').bootcomplete({
            url:'?bj=true',
            minLength : 1,
            method: 'post',
            idFieldName: 'mstitem'
        });
        $('[name=supplier]').select2('open');
    });
</script>