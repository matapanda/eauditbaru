<div ng-app="gradinSA" ng-controller="orderController" ng-init="init()">
    <div class="row">
        <div class="col-sm-12">
            <div class="card-box row">
                <?php
                include VIEWPATH.'alert.php';
                ?>
                <form id="form-order" role="form" method="post" ng-submit="simpanorder()">
                    <div class="col-md-6 col-md-offset-3">
                        <div class="form-group row">
                            <label for="hori-pass2" class="col-sm-4 form-control-label">ID Supplier</label>
                            <div class="col-sm-7">
                                <h5 class="nomargin"><?=$header['kode']?></h5>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="hori-pass2" class="col-sm-4 form-control-label">Nama Supplier</label>
                            <div class="col-sm-7">
                                <h5 class="nomargin"><?=$header['nama']?></h5>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="hori-pass2" class="col-sm-4 form-control-label">Alamat Supplier</label>
                            <div class="col-sm-7">
                                <p class="nomargin" style="white-space: pre-wrap;"><?=$header['alamat']?></p>
                            </div>
                        </div>
                        <div class="form-group row hidden">
                            <label for="hori-pass2" class="col-sm-4 form-control-label">Utang (Piutang)</label>
                            <div class="col-sm-7">
                                <h5 class="nomargin">{{hutangpiutang-jumlahbayar|currency:'':2}}</h5>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputEmail" class="col-sm-4 form-control-label">Tanggal<span class="text-danger">*</span></label>
                            <div class="col-sm-7">
                                <div class="input-group">
                                    <input type="hidden" name="id" value="<?=$header['id']?>">
                                    <input type="text" name="tanggal" required class="form-control datepicker" placeholder="tanggal" value="<?=date('d/m/Y')?>" readonly>
                                    <span class="input-group-addon bg-custom b-0"><i class="mdi mdi-calendar text-white"></i></span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputName" class="col-sm-4 form-control-label">Total Tagihan</label>
                            <div class="col-sm-7" title="Subtotal">
                                <h5 class="nomargin">{{total|currency:'':2}}</h5>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputName" class="col-sm-4 form-control-label">Jumlah Bayar</label>
                            <div class="col-sm-7">
                                <input name="jumlahbayar" ng-model="jumlahbayar" class="form-control input-override" style="height: 20px">
                                <h5 class="nomargin">{{jumlahbayar|currency:'':2}}</h5>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 form-control-label">Metode Pembayaran<span class="text-danger">*</span></label>
                            <div class="col-sm-7">
                                <select name="metodepembayaran" required class="select2 form-control">
                                    <option value=''>-</option>
                                    <?php
                                    foreach ($metode as $index=>$m){
                                        $last=array();
                                        $last_id=array(
                                            1=>@$m[0]['k1'],
                                            2=>@$m[0]['k2'],
                                            3=>@$m[0]['k3']
                                        );
                                        $group=0;
                                        foreach ($m[1] as $r){
                                            if($r['level']==1){
                                                $last_id[1]=$r['k1'];
                                                $last[1]=$r['nama'];
                                                continue;
                                            }elseif($r['level']==2 && "$r[k1]"=="$last_id[1]"){
                                                $last_id[2]=$r['k2'];
                                                $last[2]=$r['nama'];
                                                continue;
                                            }elseif($r['level']==3 && "$r[k1]$r[k2]"=="$last_id[1]$last_id[2]"){
                                                $last_id[3]=$r['k3'];
                                                $last[3]=$r['nama'];
                                                if($group>0){
                                                    echo "</optgroup>";
                                                }
                                                $group++;
                                                echo "<optgroup label=\"".implode(" - ",$last)."\">";
                                                continue;
                                            }elseif($r['level']==4 && "$r[k1]$r[k2]$r[k3]"==implode("",$last_id)){
                                                echo "<option value='$r[id];$index'>" . format_coa($r['kode']) . " $r[nama]</option>";
                                            }
                                        }
                                        if($group>0){
                                            echo "</optgroup>";
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row hidden">
                            <label for="inputName" class="col-sm-4 form-control-label">Catatan</label>
                            <div class="col-sm-7">
                                <textarea name="note" class="form-control" placeholder="Catatan"></textarea>
                            </div>
                        </div>
                        <div class="form-group row hidden">
                            <label for="inputName" class="col-sm-4 form-control-label">Mengetahui</label>
                            <div class="col-sm-7">
                                <input type="hidden" name="request_by">
                                <input type="text" name="karyawan" class="form-control" placeholder="Masukkan NIK / Nama" style="height: 20px">
                                <textarea name="items[]" class="hidden" ng-repeat="(key, item) in items">{{item}}</textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-8 col-sm-offset-4">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <hr>
                        <div id="detailppb">
                            <table class="table table-bordered table-striped table-hover">
                                <thead>
                                <tr>
                                    <th class="center col-xs-2">Tanggal</th>
                                    <th class="center col-xs-2">Nomor UMK</th>
                                    <th class="center col-xs-2">Tagihan</th>
                                    <th class="center col-xs-6">COA</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr ng-class="item.status==1?'hand-cursor':'hand-cursor btn-warning'" ng-repeat="(key, item) in items">
                                    <td class="center">{{item.tanggal}}</td>
                                    <td class="center">{{item.nota}}</td>
                                    <td class="right">{{item.harga|currency:'':2}}</td>
                                    <td class="">
                                        <select ng-model="item.coa" class="select2" ng-change="pilih(key)">
                                            <option value=''><?= str_pad(' PILIH COA ', 20, '-', STR_PAD_BOTH) ?></option>
                                            <?php
                                            foreach ($coa as $index=>$m){
                                                $last=array();
                                                $last_id=array(
                                                    1=>@$m[0]['k1'],
                                                    2=>@$m[0]['k2'],
                                                    3=>@$m[0]['k3']
                                                );
                                                $group=0;
                                                foreach ($m[1] as $r){
                                                    if($r['level']==1){
                                                        $last_id[1]=$r['k1'];
                                                        $last[1]=$r['nama'];
                                                        continue;
                                                    }elseif($r['level']==2 && "$r[k1]"=="$last_id[1]"){
                                                        $last_id[2]=$r['k2'];
                                                        $last[2]=$r['nama'];
                                                        continue;
                                                    }elseif($r['level']==3 && "$r[k1]$r[k2]"=="$last_id[1]$last_id[2]"){
                                                        $last_id[3]=$r['k3'];
                                                        $last[3]=$r['nama'];
                                                        if($group>0){
                                                            echo "</optgroup>";
                                                        }
                                                        $group++;
                                                        echo "<optgroup label=\"".implode(" - ",$last)."\">";
                                                        continue;
                                                    }elseif($r['level']==4 && "$r[k1]$r[k2]$r[k3]"==implode("",$last_id)){
                                                        echo "<option value='$r[id]'>" . format_coa($r['kode']) . " $r[nama]</option>";
                                                    }
                                                }
                                                if($group>0){
                                                    echo "</optgroup>";
                                                }
                                            }
                                            ?>
                                        </select>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <hr>
                        <button type="submit" class="btn btn-inverse waves-effect waves-light">Simpan Pembayaran</button>
                        <a href="?permintaan=true" class="btn btn-default waves-effect m-l-5">Kembali</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<link href="<?=base_url()?>assets/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css"/>
<link href="<?=base_url()?>assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css"/>
<link href="<?=base_url()?>assets/css/bootcomplete.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="<?=base_url()?>assets/plugins/select2/js/select2.min.js"></script>
<script src="<?=base_url()?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script src="<?=base_url()?>assets/js/jquery.bootcomplete.js"></script>
<script src="<?=base_url()?>assets/js/angular.min.js"></script>
<script type="text/javascript">
    var app = angular.module("gradinSA", []);
    app.controller('orderController', function($scope,$http) {
        $scope.items=[];
        $scope.total=0;
        $scope.jumlahbayar=0;
        $scope.hutangpiutang=<?=null_is_nol(pure_money($header['hutangpiutang']))?>;
        $scope.init = function(){
            <?php
            foreach ($data as $d){
                $d['tanggal']=format_waktu($d['tanggal']);
                $d['harga']=null_is_nol(($d['uangmuka']));
                echo '$scope'.".items.unshift({status:0,uangmuka:'$d[id]',nota:'$d[nota]',tanggal:'$d[tanggal]',harga:$d[harga],coa:''});";
            }
            ?>
            $scope.update();
        };
        $scope.simpanorder = function() {
            if($scope.jumlah>0){
                $('#preloader').show();
                $('#status','#preloader').show();
                $scope.form=$('#form-order').serializeArray();
                $http({
                    url: "?save=true",
                    data: $scope.form,
                    method: 'POST',
                    headers : {'Content-Type':'application/x-www-form-urlencoded; charset=UTF-8'}

                }).success(function(data){
                    window.location.assign('<?=base_url('pembelian/umk')?>');
                    setTimeout(function () {
                        $('#preloader').hide();
                        $('#status','#preloader').hide();
                    },2000);
                    console.log("OK", data)
                }).error(function(err){"ERR", console.log(err)})
            }else{
                swal({
                    title: "Anda belum memilih data uang muka!",
                    type: "error",
                    confirmButtonColor: "#DD5555",
                    confirmButtonText: "OK",
                    closeOnConfirm: true,
                });
            }
        };
        $scope.pilih=function (_i) {
            if($scope.items[_i].coa){
                $scope.items[_i].status=1;
            }else{
                $scope.items[_i].status=0;
            }
            $scope.update();
        };
        $scope.update=function () {
            $scope.total=0;
            $scope.jumlah=0;
            $scope.jumlahbayar=0;
            for (var i in $scope.items) {
                console.log($scope.items);
                if($scope.items[i].status==1){
                    $scope.total=parseFloat($scope.total)+parseFloat($scope.items[i].harga*1);
                    $scope.jumlahbayar=parseFloat($scope.jumlahbayar)+parseFloat($scope.items[i].harga*1);
                    $scope.jumlah++;
                }
            }
            $scope.jumlahbayar=$scope.jumlahbayar>=$scope.total?$scope.total:$scope.jumlahbayar;
        };
    });
    $(function () {
        $('.select2').select2();
        $('.datepicker').datepicker({
            format: 'dd/mm/yyyy',
            autoclose: true,
            todayHighlight: true
        });
        $('input[name=karyawan]').bootcomplete({
            url:'?k=true',
            minLength : 2,
            method: 'post',
            idFieldName: 'request_by',
            formParams: {
                'unit' : $('[name=request_unit]')
            }
        });
    });
</script>