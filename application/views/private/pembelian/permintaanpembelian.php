<div class="row">
    <div class="col-sm-12">
        <div class="card-box">
            <a href="?new=barang" class="<?=is_authority(@$access['c'])?> btn btn-inverse"><i class="fa fa-plus"></i> BARANG</a>
            <a href="?new=jasa" class="<?=is_authority(@$access['c'])?> btn btn-inverse"><i class="fa fa-plus"></i> JASA</a>
            <a href="?" class="btn btn-primary"><i class="fa fa-refresh"></i> RELOAD</a>
            <hr>
            <form method="get" class="row" action="<?=base_url('pembelian/permintaan')?>">
                <div class="col-md-6">
                    <div class="dataTables_wrapper form-inline">
                        <label>Filter: </label>
                        <div class="input-daterange input-group" id="date-range">
                            <input type="text" readonly class="form-control input-sm" name="start" onchange="this.form.submit()" value="<?=$start?>">
                            <span class="input-group-addon input-sm">~</span>
                            <input type="text" readonly class="form-control input-sm" name="end" onchange="this.form.submit()" value="<?=$end?>">
                        </div>
                        <select name="closed" onchange="this.form.submit()" class="form-control input-sm">
                            <option value="">Semua status</option>
                            <option value="f" <?=$closed=='f'?'selected':''?>>Status aktif</option>
                            <option value="t" <?=$closed=='t'?'selected':''?>>Status closed</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="dataTables_wrapper form-inline right">
                        <label class="hidden-xs">No. PBB: &nbsp;</label><input type="search" name="nota" class="form-control input-sm" autocomplete="off" value="<?=$nota?>">
                    </div>
                </div>
            </form>
            <div class="table-responsive">
                <table class="table table-bordered table-striped table-hover">
                    <thead>
                    <tr>
                        <th class="center col-xs-1">#</th>
                        <th class="center col-xs-2">Tanggal</th>
                        <th class="center col-xs-2">No. PPB</th>
                        <th class="hidden col-xs-2">Unit Kerja</th>
                        <th class="col-xs-3">Keterangan</th>
                        <th class="center col-xs-2">Diajukan Oleh</th>
                        <th class="center col-xs-1">Status</th>
                        <th class="center col-xs-1">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $no=$page;
                    foreach($data as $g) {
                        ?>
                        <tr class="data<?=$g['id']?>">
                            <td scope="row" class="center"><?=++$no?></td>
                            <td class="center"><?=format_waktu($g['tanggal'])?></td>
                            <td class="center"><?=$g['nota']?></td>
                            <td class="hidden"><?=$g['request_unit_name']?></td>
                            <td class=""><?=$g['note']?></td>
                            <td><?=$g['request_by_name']?></td>
                            <td class="center"><?=getLabelCLosed($g['closed'])?></td>
                            <td class="center">
                                <button onclick="rincian('<?=$g['id']?>','<?=$g['nota']?>')" class="btn btn-xs btn-block btn-inverse"><i class="fa fa-search"></i></button>
                                <?php
                                if($g['editable']=='t' && isset($access['u'])){
                                ?>
                                <a href="?e=<?=$g['id']?>" class="btn btn-xs btn-block btn-primary"><i class="fa fa-pencil"></i></a>
                                <?php
                                }
                                ?>
                            </td>
                        </tr>
                        <?php
                    }
                    ?>
                    </tbody>
                </table>
            </div>
            <?=$pagination?>
        </div>
    </div>
</div>
<div id="detailpembelian" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <table class="table table-bordered table-striped table-hover">
                    <thead>
                        <tr>
                            <th class="center col-xs-1">#</th>
                            <th class="center col-xs-3">Barang/Jasa</th>
                            <th class="center col-xs-2">Jumlah</th>
                            <th class="center col-xs-3">Keterangan</th>
                            <th class="center col-xs-2">Tanggal Diperlukan</th>
                            <th class="center col-xs-1">Status</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<link href="<?=base_url()?>assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css"/>
<script src="<?=base_url()?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script>
    $('#date-range').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true,
        todayHighlight: true
    });
    function rincian(_i,_nota) {
        $('.hitung'+_i).html('<img src="<?=base_url('assets/loading.gif')?>">');
        $.post('?',{d:_i},function (data,status) {
            $('h4.modal-title','#detailpembelian').html(_nota);
            $('tbody','#detailpembelian').html(data);
            $('#detailpembelian').modal('show');
        });
    }
</script>