<div class="row">
    <div class="col-sm-12">
        <div class="card-box row">
            <?php
            include VIEWPATH.'alert.php';
            ?>
            <form role="form" method="post">
                <div class="col-md-6 col-md-offset-3">
                    <div class="form-group row">
                        <label for="inputEmail" class="col-sm-4 form-control-label">NO. PPB</label>
                        <div class="col-sm-7">
                            <input type="hidden" name="updateppb" value="<?=$header['id']?>" readonly>
                            <input type="text" readonly="" class="form-control" value="<?=$header['nota']?>">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputEmail" class="col-sm-4 form-control-label">Tanggal</label>
                        <div class="col-sm-7">
                            <div class="input-group">
                                <input type="text" name="tanggal" required class="form-control" placeholder="tanggal" value="<?=format_waktu($header['tanggal'])?>" readonly>
                                <span class="input-group-addon bg-custom b-0"><i class="mdi mdi-calendar text-white"></i></span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row hidden">
                        <label for="hori-pass2" class="col-sm-4 form-control-label">Unit Kerja</label>
                        <div class="col-sm-7">
                            <select name="request_unit" class="form-control">
                                <option value=''>-</option>
                                <?php
                                foreach ($unit as $r){
                                    echo "<option value='$r[id]' ".($header['request_unit']==$r['id']?'selected=""':'').">$r[kode] $r[nama]</option>";
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputName" class="col-sm-4 form-control-label">Diajukan Oleh<span class="text-danger">*</span></label>
                        <div class="col-sm-7">
                            <input type="hidden" name="request_by" value="<?=$header['request_by']?>">
                            <input type="text" name="karyawan" required class="form-control" placeholder="Masukkan NIK / Nama" value="<?=$header['request_by_name']?>">
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <button type="button" onclick="formtambah()" class="btn btn-primary waves-effect waves-light"><i class="fa fa-plus"></i> Tambah Barang/Jasa</button>
                    <hr>
                    <div id="detailppb">
                        <table class="table table-bordered table-striped table-hover">
                            <thead>
                            <tr>
                                <th class="center col-xs-3">Barang/Jasa</th>
                                <th class="center col-xs-2">Jumlah</th>
                                <th class="center col-xs-3">Keterangan</th>
                                <th class="center col-xs-2">Tanggal Diperlukan</th>
                                <th class="center col-xs-1">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $array="";
                            foreach ($detail as $d){
                                $list=array();
                                foreach ($d as $n=>$v){
                                    $v=$n=='diperlukan'?format_waktu($v):$v;
                                    $list[]=array("name"=>$n,"value"=>$v);
                                }
                                $array.="items['$d[mstitem]']=".json_encode($list).";";
                                echo "<tr id='$d[mstitem]'><td><textarea class=\"hidden\" name='items[]'>".json_encode($list)."</textarea>$d[barangjasa]</td><td class=\"center\">$d[jumlah]</td><td style=\"white-space: pre-wrap\">$d[note]</td><td class=\"center\">".format_waktu($d['diperlukan'])."</td>
                                <td class=\"center\"><button type=\"button\" class=\"btn btn-xs btn-primary\" onclick=\"perbarui('$d[mstitem]')\"><i class=\"fa fa-pencil\"></i></button><button type=\"button\" onclick=\"hapus('$d[mstitem]')\" class=\"btn btn-xs btn-danger\"><i class=\"fa fa-trash\"></i></button></td></tr>";
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                    <hr>
                    <button type="submit" class="btn btn-inverse waves-effect waves-light">Update Data <?=$header['nota']?></button>
                    <a href="?" class="btn btn-default waves-effect m-l-5">Back</a>
                </div>
            </form>
        </div>
    </div>
</div>
<div id="formtambah" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <form>
                    <div class="form-group row">
                        <label for="inputEmail" class="col-sm-4 form-control-label">Tanggal diperlukan<span class="text-danger">*</span></label>
                        <div class="col-sm-7">
                            <div class="input-group">
                                <input type="text" name="diperlukan" required class="form-control datepicker" placeholder="tanggal" readonly>
                                <span class="input-group-addon bg-custom b-0"><i class="mdi mdi-calendar text-white"></i></span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="hori-pass2" class="col-sm-4 form-control-label">Barang / Jasa<span class="text-danger">*</span></label>
                        <div class="col-sm-7">
                            <input type="hidden" name="mstitem">
                            <input type="text" name="barangjasa" required class="form-control" placeholder="Masukkan nama barang / jasa">
                            <ul class="parsley-errors-list" id="parsley-id-barangjasa" style="display: inherit!important;"></ul>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputName" class="col-sm-4 form-control-label">Jumlah<span class="text-danger">*</span></label>
                        <div class="col-sm-7">
                            <input type="text" name="jumlah" required class="form-control" placeholder="Jumlah yang diperlukan">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputName" class="col-sm-4 form-control-label">Catatan<span class="text-danger"></span></label>
                        <div class="col-sm-7">
                            <textarea name="note" class="form-control" placeholder="Catatan tambahan"></textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-8 col-sm-offset-4">
                            <button type="submit" class="btn btn-primary waves-effect waves-light">TAMBAHKAN</button>
                            <button type="button" data-dismiss="modal" class="btn btn-default waves-effect waves-light">BATAL</button>
                        </div>
                    </div>
                </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<link href="<?=base_url()?>assets/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css"/>
<link href="<?=base_url()?>assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css"/>
<link href="<?=base_url()?>assets/css/bootcomplete.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="<?=base_url()?>assets/plugins/select2/js/select2.min.js"></script>
<script src="<?=base_url()?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script src="<?=base_url()?>assets/js/jquery.bootcomplete.js"></script>
<script type="text/javascript">
    function formtambah() {
        $('form','#formtambah').get(0).reset();
        $('#parsley-id-barangjasa').html('');
        $('[name=diperlukan]','#formtambah').val($('[name=tanggal]').val());
        $('#formtambah').modal('show');
    }
    function perbarui(_id) {
        $('form','#formtambah').get(0).reset();
        $('#parsley-id-barangjasa').html('');
        items[_id].forEach(function(element) {
            $('[name='+element['name']+']').val(element['value']);
        });
        $('#formtambah').modal('show');
    }
    function hapus(_id) {
        items[_id]=undefined;
        $('tr#'+_id).remove();
    }
    $(function () {
        $('.select2').select2();
        $('.datepicker').datepicker({
            format: 'dd/mm/yyyy',
            autoclose: true,
            todayHighlight: true
        });
        $('input[name=karyawan]').bootcomplete({
            url:'?k=true',
            minLength : 2,
            method: 'post',
            idFieldName: 'request_by',
            formParams: {
                'unit' : $('[name=request_unit]')
            }
        });
        $('input[name=barangjasa]').bootcomplete({
            url:'?bj=true',
            minLength : 2,
            method: 'post',
            idFieldName: 'mstitem'
        });
    });
    var items=[];
    <?=$array?>
    $('form','#formtambah').on('submit',function (event) {
        event.preventDefault();
        if($('[name=diperlukan]').val()==''){
            $('[name=diperlukan]').val($('[name=tanggal]').val());
        }
        if($('[name=mstitem]').val()==''){
            $('#parsley-id-barangjasa').html('<li class="parsley-required">barang / jasa tidak ada</li>');
        }else{
            $('input[name=barangjasa]').select();
            $('input[name=barangjasa]').focus();
            $('#parsley-id-barangjasa').html('');
            var _html='<td>'+'<textarea name=\'items[]\' class="hidden">'+JSON.stringify($(this).serializeArray())+'</textarea>'+$('[name=barangjasa]').val()+'</td><td class="center">'+$('[name=jumlah]').val()+'</td><td style="white-space: pre-wrap">'+$('[name=note]').val()+'</td><td class="center">'+$('[name=diperlukan]').val()+'</td>' +
                '<td class="center"><button class="btn btn-xs btn-primary" type="button" onclick="perbarui(\''+$('[name=mstitem]').val()+'\')"><i class="fa fa-pencil"></i></button><button type="button" onclick="hapus(\''+$('[name=mstitem]').val()+'\')" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></button></td>';
            if($('tr#'+$('[name=mstitem]').val()).length>0){
                $('tr#'+$('[name=mstitem]').val()).html(_html);
            }else{
                $('tbody','#detailppb').append('<tr id="'+$('[name=mstitem]').val()+'">'+_html+'</tr>')
            }
            items[$('[name=mstitem]').val()]=$(this).serializeArray();
            $('#formtambah').modal('hide');
        }
        console.log(items);
    });
</script>