<div class="row">
    <div class="col-sm-12">
        <div class="card-box">
            <?php
            include VIEWPATH.'alert.php';
            ?>
            <form id="form-permintaan" method="post" action="?new=pembelian">
                <button type="submit" class="btn btn-inverse"><i class="fa fa-shopping-bag"></i> ORDER PEMBELIAN</button>
                <a href="?permintaan=true" class="btn btn-primary"><i class="fa fa-refresh"></i> RELOAD</a>
                <hr>
                <div class="col-md-12">
                    <table id="datatable" class="table table-striped table-colored table-inverse">
                        <thead>
                        <tr>
                            <th class="center col-xs-1">#</th>
                            <th class="center col-xs-1">Diperlukan</th>
                            <th class="center col-xs-1">Diajukan</th>
                            <th class="center col-xs-2">No. PPB</th>
                            <th class="center col-xs-4">Barang/Jasa</th>
                            <th class="center col-xs-1">Jumlah</th>
                            <th class="center col-xs-2">Catatan</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        foreach($data as $g) {
                            ?>
                            <tr class="hand-cursor permintaan">
                                <td scope="row" class="center">
                                    <input data-plugin="switchery" data-color="#64b0f2" data-size="small" data-switchery="true" name="permintaan[]" type="checkbox" value="<?="$g[header]_$g[mstitem]"?>">
                                </td>
                                <td class="center"><?=format_waktu($g['diperlukan'])?></td>
                                <td class="center"><?=format_waktu($g['tanggal'])?></td>
                                <td class="center"><?=$g['nota']?></td>
                                <td><?=$g['barangjasa']?></td>
                                <td class="center"><?=$g['jumlah']?></td>
                                <td style="white-space: pre-wrap"><?=$g['note']?></td>
                            </tr>
                            <?php
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
                <hr style="clear: both">
            </form>
        </div>
    </div>
</div>
<link href="<?=base_url()?>assets/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css"/>
<link href="<?=base_url()?>assets/plugins/datatables/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css"/>
<script src="<?=base_url('assets/plugins/datatables/jquery.dataTables.min.js')?>"></script>
<script src="<?=base_url('assets/plugins/datatables/dataTables.bootstrap.js')?>"></script>
<script>
    $(function () {
        $('#datatable').DataTable();
    });
    $('#form-permintaan').on('submit',function (event) {
        if($("input[name='permintaan[]']:checked").length==0){
            event.preventDefault();
            swal({
                title: "Anda belum memilih data PBB!",
                type: "error",
                showCancelButton: false,
                confirmButtonColor: "#DD5555",
                //confirmButtonText: "Lanjutkan",
                cancelButtonText: "Batalkan",
                closeOnConfirm: true,
                closeOnCancel: true
            }, function (isConfirm) {
                if (isConfirm) {
                    //window.location.assign('<?=base_url('pembelian/order?new=true')?>');
                }
            });
        }
    });
    $('.permintaan').on('click',function (e) {
//        $('input[name="permintaan[]"]',this).click();
    });
</script>