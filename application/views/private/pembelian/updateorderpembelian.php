<div ng-app="gradinSA" ng-controller="orderController" ng-init="init()">
<div class="row">
    <div class="col-sm-12">
        <div class="card-box row">
            <?php
            include VIEWPATH.'alert.php';
            ?>
            <form id="form-order" role="form" method="post" ng-submit="simpanorder()">
                <div class="col-md-6 col-md-offset-3">
                    <div class="form-group row">
                        <label for="inputEmail" class="col-sm-4 form-control-label">NO. OPB</label>
                        <div class="col-sm-7">
                            <input type="hidden" name="id" value="<?=$header['id']?>" readonly>
                            <input type="text" readonly="" class="form-control" value="<?=$header['nota']?>">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputEmail" class="col-sm-4 form-control-label">Tanggal</label>
                        <div class="col-sm-7">
                            <div class="input-group">
                                <input type="text" name="tanggal" required class="form-control" placeholder="tanggal" value="<?=format_waktu($header['tanggal'])?>" readonly>
                                <span class="input-group-addon bg-custom b-0"><i class="mdi mdi-calendar text-white"></i></span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="hori-pass2" class="col-sm-4 form-control-label">Supplier<span class="text-danger">*</span></label>
                        <div class="col-sm-7">
                            <select name="supplier" class="select2 form-control" required>
                                <option value=''>Pilih Supplier</option>
                                <?php
                                foreach ($supplier as $r){
                                    echo "<option value='$r[id]' ".($header['supplier_id']==$r['id']?'selected=""':'').">$r[kode] $r[nama]</option>";
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputName" class="col-sm-4 form-control-label">Subtotal</label>
                        <div class="col-sm-7" title="Subtotal">
                            <h5 class="nomargin">{{total|currency:'':2}}</h5>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputName" class="col-sm-4 form-control-label">DPP</label>
                        <div class="col-sm-7" title="DPP">
                            <h5 class="nomargin">{{dpp|currency:'':2}}</h5>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputName" class="col-sm-4 form-control-label">PPn</label>
                        <div class="col-sm-7" title="PPn">
                            <h5 class="nomargin">{{ppn|currency:'':2}}</h5>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputName" class="col-sm-4 form-control-label">Diskon</label>
                        <div class="col-sm-7" title="Diskon">
                            <h5 class="nomargin">{{diskonperitem|currency:'':2}}</h5>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputName" class="col-sm-4 form-control-label">Grand Total</label>
                        <div class="col-sm-7" title="Grand Total">
                            <h5 class="nomargin">{{(total*1)+(dpp*1)+(ppn*1)-diskonperitem|currency:'':2}}</h5>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputName" class="col-sm-4 form-control-label">Uang Muka</label>
                        <div class="col-sm-7">
                            <input name="uangmuka" ng-model="uangmuka" class="form-control input-override" style="height: 20px">
                            <h5 class="nomargin">{{uangmuka|currency:'':2}}</h5>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputName" class="col-sm-4 form-control-label">Sisa Tagihan</label>
                        <div class="col-sm-7" title="Sisa Tagihan">
                            <h5 class="nomargin">{{(total*1)+(dpp*1)+(ppn*1)-diskonperitem-uangmuka|currency:'':2}}</h5>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputName" class="col-sm-4 form-control-label">Mengetahui<span class="text-danger">*</span></label>
                        <div class="col-sm-7">
                            <input type="hidden" name="request_by" value="<?=$header['request_by']?>">
                            <input type="text" name="karyawan" required value="<?=$header['request_by_name']?>" class="form-control" placeholder="Masukkan NIK / Nama" style="height: 20px">
                            <textarea name="items[]" class="hidden" ng-repeat="(key, item) in items">{{item}}</textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-8 col-sm-offset-4">
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <button type="button" ng-click="formtambah()" class="btn btn-primary waves-effect waves-light hidden"><i class="fa fa-plus"></i> Tambah Barang/Jasa</button>
                    <hr>
                    <div id="detailppb">
                        <table class="table table-bordered table-striped table-hover">
                            <thead>
                                <tr>
                                    <th class="center col-xs-2">Barang/Jasa</th>
                                    <th class="center col-xs-2">Harga</th>
                                    <th class="center col-xs-1">DPP</th>
                                    <th class="center col-xs-1">PPn</th>
                                    <th class="center col-xs-1">Diskon</th>
                                    <th class="center col-xs-2">Subtotal</th>
                                    <th class="center col-xs-2">No. PBB</th>
                                    <th class="center col-xs-1">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr ng-class="item.harga?'hand-cursor':'hand-cursor btn-warning'" ng-repeat="(key, item) in items" ng-click="edit(key)">
                                    <td>{{item.barangjasa}}</td>
                                    <td class="right">{{item.qty}} x {{item.harga|currency:'':2}}</td>
                                    <td class="right">{{item.dpp|currency:'':2}}</td>
                                    <td class="right">{{item.ppn|currency:'':2}}</td>
                                    <td class="right">{{item.diskon|currency:'':2}}</td>
                                    <td class="right">{{item.subtotal|currency:'':2}}</td>
                                    <td class="center">{{item.nota}}</td>
                                    <td class="center"><button type="button" class="btn btn-xs btn-danger" ng-click="hapus(key)"><i class="fa fa-remove"></i></button></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <hr>
                    <button type="submit" class="btn btn-inverse waves-effect waves-light">Update Data <?=$header['nota']?></button>
                    <a href="?" class="btn btn-default waves-effect m-l-5">Cancel</a>
                </div>
            </form>
        </div>
    </div>
</div>
<div id="formtambah" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <form ng-submit="simpanbaru()">
                    <div class="form-group row">
                        <label for="hori-pass2" class="col-sm-4 form-control-label">Barang / Jasa</label>
                        <div class="col-sm-7">
                            <input type="hidden" ng-model="addmstitem" name="mstitem">
                            <input type="text" ng-model="addbarangjasa" name="barangjasa" class="form-control" placeholder="Masukkan nama barang / jasa" style="height: 20px">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputName" class="col-sm-4 form-control-label">Jumlah<span class="text-danger">*</span></label>
                        <div class="col-sm-7">
                            <input type="text" ng-model="addqty" required class="form-control" placeholder="" onclick="$(this).select()" style="height: 20px">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputName" class="col-sm-4 form-control-label">Harga Satuan<span class="text-danger">*</span></label>
                        <div class="col-sm-7">
                            <input ng-model="addharga" class="form-control input-override" style="height: 20px">
                            <h5 class="nomargin">{{addharga|currency:'':2}}</h5>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputName" class="col-sm-4 form-control-label">DPP</label>
                        <div class="col-sm-7">
                            <input ng-model="adddpp" class="form-control input-override" style="height: 20px">
                            <h5 class="nomargin">{{adddpp|currency:'':2}}</h5>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputName" class="col-sm-4 form-control-label">PPn</label>
                        <div class="col-sm-7">
                            <input ng-model="addppn" class="form-control input-override" style="height: 20px">
                            <h5 class="nomargin">{{addppn|currency:'':2}}</h5>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputName" class="col-sm-4 form-control-label">Diskon</label>
                        <div class="col-sm-7">
                            <input ng-model="adddiskon" class="form-control input-override" style="height: 20px">
                            <h5 class="nomargin">{{adddiskon|currency:'':2}}</h5>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputName" class="col-sm-4 form-control-label"></label>
                        <div class="col-sm-7">
                            <h3 class="nomargin">{{((addharga*addqty)+(adddpp*1)+(addppn*1)-(adddiskon*1))|currency:'':2}}</h3>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-8 col-sm-offset-4">
                            <button type="submit" class="btn btn-primary waves-effect waves-light">SIMPAN</button>
                            <button type="button" data-dismiss="modal" class="btn btn-default waves-effect waves-light">BATAL</button>
                        </div>
                    </div>
                </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<div id="formedit" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <form ng-submit="simpanedit()">
                    <div class="form-group row">
                        <label for="inputName" class="col-sm-4 form-control-label">Nomor PPB</label>
                        <div class="col-sm-7">
                            <h5>{{editnota}}</h5>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="hori-pass2" class="col-sm-4 form-control-label">Barang / Jasa</label>
                        <div class="col-sm-7">
                            <input type="text" ng-model="editbarangjasa" readonly class="form-control" placeholder="Masukkan nama barang / jasa" style="height: 20px">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputName" class="col-sm-4 form-control-label">Jumlah<span class="text-danger">*</span></label>
                        <div class="col-sm-7">
                            <input type="text" ng-model="editqty" required class="form-control" placeholder="" onclick="$(this).select()" style="height: 20px">
                            <span ng-if="editjumlah" class="help-block"><small>jumlah permintaan: {{editjumlah}}</small></span>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputName" class="col-sm-4 form-control-label">Harga Satuan<span class="text-danger">*</span></label>
                        <div class="col-sm-7">
                            <input ng-model="editharga" class="form-control input-override" style="height: 20px">
                            <h5 class="nomargin">{{editharga|currency:'':2}}</h5>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputName" class="col-sm-4 form-control-label">DPP</label>
                        <div class="col-sm-7">
                            <h5 class="nomargin">{{editdpp|currency:'':2}}</h5>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputName" class="col-sm-4 form-control-label">PPn
                            <input type="text" ng-model="editppnpersen" ng-change="updateppn()" required class="form-control" placeholder="" onclick="$(this).select()" style="width: 100px">
                        </label>
                        <div class="col-sm-7">
                            <h5 class="nomargin">{{editppn|currency:'':2}}</h5>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputName" class="col-sm-4 form-control-label">PPh 21
                            <input type="text" ng-model="editpph21persen" ng-change="updatepph21()" required class="form-control" placeholder="" onclick="$(this).select()" style="width: 100px">
                        </label>
                        <div class="col-sm-7">
                            <h5 class="nomargin">{{editpph21|currency:'':2}}</h5>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputName" class="col-sm-4 form-control-label">PPh 22
                            <input type="text" ng-model="editpph22persen" ng-change="updatepph22()" required class="form-control" placeholder="" onclick="$(this).select()" style="width: 100px">
                        </label>
                        <div class="col-sm-7">
                            <h5 class="nomargin">{{editpph22|currency:'':2}}</h5>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputName" class="col-sm-4 form-control-label">PPh 23
                            <input type="text" ng-model="editpph23persen" ng-change="updatepph23()" required class="form-control" placeholder="" onclick="$(this).select()" style="width: 100px">
                        </label>
                        <div class="col-sm-7">
                            <h5 class="nomargin">{{editpph23|currency:'':2}}</h5>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputName" class="col-sm-4 form-control-label">PPh 24
                            <input type="text" ng-model="editpph24persen" ng-change="updatepph24()" required class="form-control" placeholder="" onclick="$(this).select()" style="width: 100px">
                        </label>
                        <div class="col-sm-7">
                            <h5 class="nomargin">{{editpph24|currency:'':2}}</h5>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputName" class="col-sm-4 form-control-label">PPh 25
                            <input type="text" ng-model="editpph25persen" ng-change="updatepph25()" required class="form-control" placeholder="" onclick="$(this).select()" style="width: 100px">
                        </label>
                        <div class="col-sm-7">
                            <h5 class="nomargin">{{editpph25|currency:'':2}}</h5>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputName" class="col-sm-4 form-control-label">PPh 26
                            <input type="text" ng-model="editpph26persen" ng-change="updatepph26()" required class="form-control" placeholder="" onclick="$(this).select()" style="width: 100px">
                        </label>
                        <div class="col-sm-7">
                            <h5 class="nomargin">{{editpph26|currency:'':2}}</h5>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputName" class="col-sm-4 form-control-label">Diskon</label>
                        <div class="col-sm-7">
                            <input ng-model="editdiskon" class="form-control input-override" style="height: 20px">
                            <h5 class="nomargin">{{editdiskon|currency:'':2}}</h5>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputName" class="col-sm-4 form-control-label"></label>
                        <div class="col-sm-7">
                            <h3 class="nomargin">{{((editharga*editqty)-(editdiskon*1))|currency:'':2}}</h3>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-8 col-sm-offset-4">
                            <button type="submit" class="btn btn-primary waves-effect waves-light">SIMPAN</button>
                            <button type="button" data-dismiss="modal" class="btn btn-default waves-effect waves-light">BATAL</button>
                        </div>
                    </div>
                </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
</div>
<link href="<?=base_url()?>assets/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css"/>
<link href="<?=base_url()?>assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css"/>
<link href="<?=base_url()?>assets/css/bootcomplete.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="<?=base_url()?>assets/plugins/select2/js/select2.min.js"></script>
<script src="<?=base_url()?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script src="<?=base_url()?>assets/js/jquery.bootcomplete.js"></script>
<script src="<?=base_url()?>assets/js/angular.min.js"></script>
<script type="text/javascript">
//    function goodbye(e) {
//        if(!e) e = window.event;
//        e.cancelBubble = true;
//        e.returnValue = 'You sure you want to leave?'; //This is displayed on the dialog
//        if (e.stopPropagation) {
//            e.stopPropagation();
//            e.preventDefault();
//        }
//    }
//    window.onbeforeunload=goodbye;
    $('#formedit').on('shown.bs.modal', function () {
        $('[ng-model=editharga]',this).focus();
    });
    $('#formtambah').on('shown.bs.modal', function () {
        $('[name=barangjasa]',this).focus();
    });
    var app = angular.module("gradinSA", []);
    app.controller('orderController', function($scope,$http) {
        $scope.items=[];
        $scope.total=0;
        $scope.diskonperitem=0;
        $scope.dpp=0;
        $scope.ppn=0;
        $scope.uangmuka=<?=pure_money($header['uangmuka'])?>;
        $scope.init = function(){
            <?php
            foreach ($data as $d){
                $d['nama']=htmlveryspecialchars($d['nama']);
                $d['jumlah']=is_numeric($d['jumlah'])?$d['jumlah']:0;
                $d['harga']=pure_money($d['harga']);
                $d['dpp']=pure_money($d['dpp']);
                $d['ppn']=pure_money($d['ppn']);
                $d['diskon']=pure_money($d['diskon']);
                $d['nota']=htmlveryspecialchars($d['nota']);
                echo '$scope'.".items.unshift({mstitem:'".(strlen($d['permintaan'])>30?"$d[permintaan]_":"")."$d[mstitem]',barangjasa:'$d[nama]',jumlah:'$d[jumlah]',nota:'$d[nota]',note:'',harga:$d[harga],diskon:$d[diskon],qty:$d[jumlah],subtotal:0,dpp:$d[dpp],ppn:$d[ppn]});";
            }
            ?>
            $scope.update();
        };
        $scope.simpanorder = function() {
            $('#preloader').show();
            $('#status','#preloader').show();
            $scope.form=$('#form-order').serializeArray();
            $http({
                url: "?update=true",
                data: $scope.form,
                method: 'POST',
                headers : {'Content-Type':'application/x-www-form-urlencoded; charset=UTF-8'}

            }).success(function(data){
                window.location.assign('<?=base_url('pembelian/order')?>');
                setTimeout(function () {
                    $('#preloader').hide();
                    $('#status','#preloader').hide();
                },2000);
                console.log("OK", data)
            }).error(function(err){"ERR", console.log(err)})
        };
        $scope.simpanedit = function() {
            $scope.items[$scope.editindex].qty=$scope.editqty;
            $scope.items[$scope.editindex].harga=$scope.editharga;
            $scope.items[$scope.editindex].diskon=$scope.editdiskon;
            $scope.items[$scope.editindex].dpp=$scope.editdpp;
            $scope.items[$scope.editindex].ppn=$scope.editppn;
            $scope.update();
            $('#formedit').modal('hide');
        };
        $scope.simpanbaru = function () {
//            $scope.addmstitem=$('[ng-model=addmstitem]').val();
//            $scope.addbarangjasa=$('[ng-model=addbarangjasa]').val();
//            var _isExist=false;
//            for (var i in $scope.items) {
//                if ($scope.items[i].mstitem == $scope.addmstitem) {
//                    $scope.items[i].qty=$scope.addqty;
//                    $scope.items[i].harga=$scope.addharga;
//                    $scope.items[i].diskon=$scope.adddiskon;
//                    $scope.items[i].dpp=$scope.adddpp;
//                    $scope.items[i].ppn=$scope.addppn;
//                    _isExist=true;
//                    break;
//                }
//            }
//            if(_isExist==false){
//                $scope.items.unshift({mstitem:$scope.addmstitem,barangjasa:$scope.addbarangjasa,jumlah:'',nota:'-',note:'',harga:$scope.addharga,diskon:$scope.adddiskon,qty:$scope.addqty,dpp:$scope.adddpp,ppn:$scope.addppn,subtotal:0});
//            }
//            $scope.update();
//            $('#formtambah').modal('hide');
        };
        $scope.updateppn = function() {
            $scope.editdpp=$scope.items[_id].dpp;
            $scope.editppn=$scope.items[_id].ppn;
        };
        $scope.edit = function(_id) {
            $scope.editindex=_id;
            $scope.editnota=$scope.items[_id].nota;
            $scope.editmstitem=$scope.items[_id].mstitem;
            $scope.editbarangjasa=$scope.items[_id].barangjasa;
            $scope.editqty=$scope.items[_id].qty;
            $scope.editjumlah=$scope.items[_id].jumlah;
            $scope.editharga=$scope.items[_id].harga;
            $scope.editdiskon=$scope.items[_id].diskon;
            $scope.editdpp=$scope.items[_id].dpp;
            $scope.editppn=$scope.items[_id].ppn;
            $scope.editpph21=$scope.items[_id].pph21;
            $scope.editpph22=$scope.items[_id].pph22;
            $scope.editpph23=$scope.items[_id].pph23;
            $scope.editpph24=$scope.items[_id].pph24;
            $scope.editpph25=$scope.items[_id].pph25;
            $scope.editpph26=$scope.items[_id].pph26;
            $scope.editpph21persen=$scope.items[_id].pph21persen;
            $scope.editpph22persen=$scope.items[_id].pph22persen;
            $scope.editpph23persen=$scope.items[_id].pph23persen;
            $scope.editpph24persen=$scope.items[_id].pph24persen;
            $scope.editpph25persen=$scope.items[_id].pph25persen;
            $scope.editpph26persen=$scope.items[_id].pph26persen;
            $scope.editppnpersen=$scope.items[_id].editppnpersen;
            $('#formedit').modal('show');
        };
        $scope.formtambah = function () {
            $scope.addnota="-";
            $scope.addmstitem="";
            $scope.addbarangjasa="";
            $scope.addqty=1;
            $scope.adddpp=1;
            $scope.addppn=1;
            $scope.addharga="";
            $scope.adddiskon="";
            $('form','#formtambah').get(0).reset();
            $('#parsley-id-barangjasa').html('');
            $('[name=diperlukan]','#formtambah').val($('[name=tanggal]').val());
            $('#formtambah').modal('show');
        };
        $scope.hapus = function(_id) {
            $scope.items.splice(_id, 1);
            $scope.update();
        };
        $scope.update=function () {
            $scope.total=0;
            $scope.dpp=0;
            $scope.ppn=0;
            $scope.diskonperitem=0;
            for (var i in $scope.items) {
                $scope.dpp=parseFloat($scope.dpp)+parseFloat($scope.items[i].dpp);
                $scope.ppn=parseFloat($scope.ppn)+parseFloat($scope.items[i].ppn);
                $scope.total=parseFloat($scope.total)+parseFloat(($scope.items[i].qty*$scope.items[i].harga));
                $scope.diskonperitem=parseFloat($scope.diskonperitem)+parseFloat($scope.items[i].diskon);
                $scope.items[i].subtotal=parseFloat(parseFloat($scope.items[i].qty*$scope.items[i].harga)-parseFloat($scope.items[i].diskon));
            }
        };
    });
    $(function () {
        $('.select2').select2();
        $('.datepicker').datepicker({
            format: 'dd/mm/yyyy',
            autoclose: true,
            todayHighlight: true
        });
        $('input[name=karyawan]').bootcomplete({
            url:'?k=true',
            minLength : 1,
            method: 'post',
            idFieldName: 'request_by',
            formParams: {
                'unit' : $('[name=request_unit]')
            }
        });
        $('input[name=barangjasa]').bootcomplete({
            url:'?bj=true',
            minLength : 1,
            method: 'post',
            idFieldName: 'mstitem'
        });
//        $('[name=supplier]').select2('open');
    });
</script>