<div ng-app="gradinSA" ng-controller="orderController" ng-init="init()">
    <div class="row">
        <div class="col-sm-12">
            <div class="card-box row">
                <?php
                include VIEWPATH.'alert.php';
                ?>
                <form id="form-order" role="form" method="post" ng-submit="simpanorder()">
                    <div class="col-md-6 col-md-offset-3">
                        <div class="form-group row">
                            <label for="hori-pass2" class="col-sm-4 form-control-label">No. FPB</label>
                            <div class="col-sm-7">
                                <h5 class="nomargin"><?=$header['nota']?></h5>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="hori-pass2" class="col-sm-4 form-control-label">Tanggal Diterima</label>
                            <div class="col-sm-7">
                                <h5 class="nomargin"><?=format_waktu($header['tanggal'])?></h5>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="hori-pass2" class="col-sm-4 form-control-label">Supplier</label>
                            <div class="col-sm-7">
                                <h5 class="nomargin"><?=$header['supplier_name']?></h5>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputEmail" class="col-sm-4 form-control-label">Tanggal Retur<span class="text-danger">*</span></label>
                            <div class="col-sm-7">
                                <div class="input-group">
                                    <input type="hidden" name="id" value="<?=$header['id']?>">
                                    <input type="text" name="tanggal" required class="form-control datepicker" placeholder="tanggal" value="<?=date('d/m/Y')?>" readonly>
                                    <span class="input-group-addon bg-custom b-0"><i class="mdi mdi-calendar text-white"></i></span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputName" class="col-sm-4 form-control-label">Mengetahui<span class="text-danger">*</span></label>
                            <div class="col-sm-7">
                                <input type="hidden" name="request_by">
                                <input type="text" name="karyawan" required class="form-control" placeholder="Masukkan NIK / Nama" style="height: 20px">
                                <textarea name="items[]" class="hidden" ng-repeat="(key, item) in items">{{item}}</textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-8 col-sm-offset-4">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <hr>
                        <div id="detailppb">
                            <table class="table table-bordered table-striped table-hover">
                                <thead>
                                <tr>
                                    <th class="center col-xs-1" style="vertical-align: inherit;">PPB</th>
                                    <th class="center col-xs-2" style="vertical-align: inherit;">Nama Barang</th>
                                    <th class="center col-xs-1">Jumlah</br>Diterima</th>
                                    <th class="center col-xs-1">Retur</br>Sebelumnya</th>
                                    <th class="center col-xs-1">Retur</br>Sekarang</th>
                                    <th class="center col-xs-1">Terjual</th>
                                    <th class="center col-xs-1">Sisa</th>
                                    <th class="center col-xs-3" style="vertical-align: inherit;">Catatan</th>
                                    <th class="center col-xs-1" style="vertical-align: inherit;">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr ng-class="item.sekarang?'hand-cursor':'hand-cursor btn-warning'" ng-repeat="(key, item) in items" ng-click="edit(key)">
                                    <td class="center">{{item.nota}}</td>
                                    <td>{{item.barangjasa}}</td>
                                    <td class="center">{{item.jumlah}}</td>
                                    <td class="center">{{item.retur}}</td>
                                    <td class="center">{{item.sekarang}}</td>
                                    <td class="center">{{item.terjual}}</td>
                                    <td class="center">{{(item.jumlah*1)-(item.retur*1)-(item.terjual*1)-(item.sekarang*1)}}</td>
                                    <td style="white-space: pre-wrap">{{item.note}}</td>
                                    <td class="center"><button type="button" class="btn btn-xs btn-danger" ng-click="hapus(key)"><i class="fa fa-remove"></i></button></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <hr>
                        <button type="submit" class="btn btn-inverse waves-effect waves-light">Simpan RPB</button>
                        <a href="?permintaan=true" class="btn btn-default waves-effect m-l-5">Kembali</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div id="formedit" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <form ng-submit="simpanedit()">
                        <div class="form-group row">
                            <label for="hori-pass2" class="col-sm-4 form-control-label">Nama Barang</label>
                            <div class="col-sm-7">
                                <input type="text" ng-model="editbarangjasa" class="form-control" readonly="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputName" class="col-sm-4 form-control-label">Jumlah Diterima</label>
                            <div class="col-sm-7">
                                <input type="text" ng-model="editjumlah" class="form-control" readonly="" style="height: 20px">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputName" class="col-sm-4 form-control-label">Retur Sebelumnya</label>
                            <div class="col-sm-7">
                                <input type="text" ng-model="editretur" class="form-control" readonly="" style="height: 20px">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputName" class="col-sm-4 form-control-label">Retur Sekarang<span class="text-danger">*</span></label>
                            <div class="col-sm-7">
                                <input type="text" ng-model="editsekarang" required class="form-control" placeholder="" onclick="$(this).select()" style="height: 20px">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputName" class="col-sm-4 form-control-label">Sisa Diterima</label>
                            <div class="col-sm-7">
                                <h5>{{(editjumlah*1)-(editretur*1)-(editsekarang*1)}}</h5>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputName" class="col-sm-4 form-control-label">Catatan</label>
                            <div class="col-sm-7">
                                <textarea ng-model="editnote" class="form-control"></textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-8 col-sm-offset-4">
                                <button type="submit" class="btn btn-primary waves-effect waves-light">SIMPAN</button>
                                <button type="button" data-dismiss="modal" class="btn btn-default waves-effect waves-light">BATAL</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>
</div>
<link href="<?=base_url()?>assets/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css"/>
<link href="<?=base_url()?>assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css"/>
<link href="<?=base_url()?>assets/css/bootcomplete.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="<?=base_url()?>assets/plugins/select2/js/select2.min.js"></script>
<script src="<?=base_url()?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script src="<?=base_url()?>assets/js/jquery.bootcomplete.js"></script>
<script src="<?=base_url()?>assets/js/angular.min.js"></script>
<script type="text/javascript">
    $('#formedit').on('shown.bs.modal', function () {
        $('[ng-model=editharga]',this).focus();
    });
    $('#formtambah').on('shown.bs.modal', function () {
        $('[name=barangjasa]',this).focus();
    });
    var app = angular.module("gradinSA", []);
    app.controller('orderController', function($scope,$http) {
        $scope.items=[];
        $scope.total=0;
        $scope.init = function(){
            <?php
            foreach ($data as $d){
                $d['nama']=htmlveryspecialchars($d['nama']);
                $d['jumlah']=is_numeric($d['jumlah'])?$d['jumlah']:0;
                $d['retur']=is_numeric($d['retur'])?$d['retur']:0;
                echo '$scope'.".items.unshift({permintaan:'$d[permintaan]',nota:'$d[nota]',mstitem:'$d[mstitem]',barangjasa:'$d[nama]',jumlah:'$d[jumlah]',note:'',retur:$d[retur],terjual:$d[terjual],sekarang:0});";
            }
            ?>
        };
        $scope.simpanorder = function() {
            $('#preloader').show();
            $('#status','#preloader').show();
            $scope.form=$('#form-order').serializeArray();
            $http({
                url: "?save=true",
                data: $scope.form,
                method: 'POST',
                headers : {'Content-Type':'application/x-www-form-urlencoded; charset=UTF-8'}

            }).success(function(data){
                window.location.assign('<?=base_url('pembelian/retur')?>');
                setTimeout(function () {
                    $('#preloader').hide();
                    $('#status','#preloader').hide();
                },2000);
                console.log("OK", data)
            }).error(function(err){"ERR", console.log(err)})
        };
        $scope.simpanedit = function() {
            $scope.items[$scope.editindex].sekarang=$scope.editsekarang;
            $scope.items[$scope.editindex].note=$scope.editnote;
            $scope.update();
            $('#formedit').modal('hide');
        };
        $scope.edit = function(_id) {
            $scope.editindex=_id;
            $scope.editmstitem=$scope.items[_id].mstitem;
            $scope.editbarangjasa=$scope.items[_id].barangjasa;
            $scope.editjumlah=$scope.items[_id].jumlah;
            $scope.editretur=$scope.items[_id].retur;
            $scope.editsekarang=$scope.items[_id].sekarang;
            $scope.editnote=$scope.items[_id].note;
            $('#formedit').modal('show');
        };
        $scope.hapus = function(_id) {
            $scope.items.splice(_id, 1);
            $scope.update();
        };
        $scope.update=function () {
            $scope.total=0;
            for (var i in $scope.items) {
                $scope.total=parseFloat($scope.total)+parseFloat($scope.items[i].sekarang);
            }
        };
    });
    $(function () {
        $('.select2').select2();
        $('.datepicker').datepicker({
            format: 'dd/mm/yyyy',
            autoclose: true,
            todayHighlight: true
        });
        $('input[name=karyawan]').bootcomplete({
            url:'?k=true',
            minLength : 1,
            method: 'post',
            idFieldName: 'request_by',
            formParams: {
                'unit' : $('[name=request_unit]')
            }
        });
        $('input[name=barangjasa]').bootcomplete({
            url:'?bj=true',
            minLength : 1,
            method: 'post',
            idFieldName: 'mstitem'
        });
        $('[name=supplier]').select2('open');
    });
</script>