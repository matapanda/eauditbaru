<htmlpageheader name="header">
</htmlpageheader>
<h2>NERACA LAJUR</h2>
<?php
$list_bulan=array('-','Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember');
if(isset($data)){
if ($bulan > 0) {
    if($bulan==1){
        $sebelum = $list_bulan[12] . " " . ($tahun - 1);
        $sekarang = $list_bulan[1] . " " . $tahun;
        $tahun=array(($tahun-1),($tahun));
        $bulan=array((12),(1));
    }else{
        $sebelum = $list_bulan[$bulan - 1] . " " . $tahun;
        $sekarang = $list_bulan[$bulan - 0] . " " . $tahun;
        $tahun=array(($tahun),($tahun));
        $bulan=array(($bulan - 1),($bulan));
    }
} else {
    $bulan = 12;
    $sebelum = $list_bulan[$bulan] . " " . ($tahun - 1);
    $sekarang = $list_bulan[$bulan] . " " . $tahun;
    $tahun=array(($tahun - 1),($tahun));
    $bulan=array(($bulan),($bulan));
}
$sebelum = date("t", strtotime("$tahun[0]-$bulan[0]-1")) . " " . $sebelum;
$sekarang = date("t", strtotime("$tahun[1]-$bulan[1]-1")) . " " . $sekarang;
?>
    <table class="list">
        <thead>
        <tr>
            <th class="center line" rowspan="2" style="vertical-align: middle;" width="50px">Kode Rek</th>
            <th class="center line" rowspan="2" style="vertical-align: middle;" width="180px">Nama Perkiraan</th>
            <th class="center line" colspan="2" width="100px"><?php echo $sebelum; ?></th>
            <th class="center line" colspan="2" width="100px">Mutasi</th>
            <th class="center line" colspan="2" width="100px"><?php echo $sekarang; ?></th>
            <th class="center line" colspan="2" width="100px">Laba Rugi</th>
            <th class="center line" colspan="2" width="100px">Neraca</th>
        </tr>
        <tr>
            <th class="center line" width="80px">Debet</th>
            <th class="center line" width="80px">Kredit</th>
            <th class="center line" width="80px">Debet</th>
            <th class="center line" width="80px">Kredit</th>
            <th class="center line" width="80px">Debet</th>
            <th class="center line" width="80px">Kredit</th>
            <th class="center line" width="80px">Debet</th>
            <th class="center line" width="80px">Kredit</th>
            <th class="center line" width="80px">Debet</th>
            <th class="center line" width="80px">Kredit</th>
        </tr>
        </thead>
        <tbody>
        <?php
        $temp = null;
        foreach ($data as $d) {
            if ($d['status'] == 'f') {
                if (@$temp <> $d['k1']) {
                    if (isset($temp)) {
                        ?>
                        <tr style="font-size: larger;font-weight: bolder">
                            <td colspan="12">&nbsp;</td>
                        </tr>
                        <?php
                    }
                    $temp = $d['k1'];
                }
                ?>
                <tr style="font-size: larger;font-weight: bolder">
                    <td class="center"></td>
                    <td colspan="11"><b><?= $d['nama'] ?></b></td>
                </tr>
                <?php
            } else {
                ?>
                <tr>
                    <td class="center"><?= format_coa("$d[k1].$d[k2].$d[k3].$d[k4]") ?></td>
                    <td class="left"><?= $d['nama'] ?></td>
                    <td class="right"><?= format_uang($d['sebelum_debet']) ?></td>
                    <td class="right"><?= format_uang($d['sebelum_kredit']) ?></td>
                    <td class="right"><?= format_uang(pure_money($d['sekarang_debet']) - pure_money($d['sebelum_debet'])) ?></td>
                    <td class="right"><?= format_uang(pure_money($d['sekarang_kredit']) - pure_money($d['sekarang_kredit'])) ?></td>
                    <td class="right"><?= format_uang($d['sekarang_debet']) ?></td>
                    <td class="right"><?= format_uang($d['sekarang_kredit']) ?></td>
                    <td class="right"><?= $d['nl'] == 'L' ? format_uang($d['sekarang_debet']) : '' ?></td>
                    <td class="right"><?= $d['nl'] == 'L' ? format_uang($d['sekarang_kredit']) : '' ?></td>
                    <td class="right"><?= $d['nl'] == 'N' ? format_uang($d['sekarang_debet']) : '' ?></td>
                    <td class="right"><?= $d['nl'] == 'N' ? format_uang($d['sekarang_kredit']) : '' ?></td>
                </tr>
                <?php
            }
        }
        ?>
        </tbody>
    </table>
<?php
}
?>
<htmlpagefooter name="footer">
    <table width="100%">
        <tr>
            <td style="text-align: left"><?= 'Export on: '.date('d/m/Y H:i:s') ?></td>
            <td style="text-align: right">{PAGENO}</td>
        </tr>
    </table>
</htmlpagefooter>
<style>
    @page {
        margin-top: 30px;
        margin-bottom: 70px;
        margin-left: 30px;
        margin-right: 30px;
        footer: html_footer;
        header: html_header;
    }
    .list {
        width: 100%;
        margin-top: 10px;
        margin-bottom: 10px;
        border-bottom: 1px solid black;
    }
    th{
        margin: 10px;
        text-transform: uppercase;
    }
    .right{
        text-align: right;
    }
    .center{
        text-align: center;
    }
    .left{
        text-align: left;
    }
    .line{
        border-bottom: 1px solid black;
    }
</style>