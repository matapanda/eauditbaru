<htmlpageheader name="header">
</htmlpageheader>
<?php $list_bulan=array(' ','Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember');?>
<h2>LAPORAN PERINCIAN SEDIAAN</h2>
<h4 class="title">Periode : <?= $list_bulan[$bulan] . ' '. $tahun?></h4>
<table class="list">
    <thead>
    <tr>
        <th rowspan="2" class="center line" width="50px">No</th>
        <th rowspan="2" class="center line" width="100px">Kode Item</th>
        <th rowspan="2" class="center line" width="200px">Nama Item</th>
        <th colspan="2" class="center line" width="100px">Saldo Awal</th>
        <th colspan="2" class="center line" width="100px">Masuk</th>
        <th colspan="2" class="center line" width="100px">Keluar</th>
        <th colspan="2" class="center line" width="100px">Adj</th>
        <th colspan="2" class="center line" width="100px">Saldo Akhir</th>
    </tr>
    <tr>
        <th class="center line" width="70px">Qty</th>
        <th class="center line" width="80px">Harga</th>
        <th class="center line" width="70px">Qty</th>
        <th class="center line" width="80px">Harga</th>
        <th class="center line" width="70px">Qty</th>
        <th class="center line" width="80px">Harga</th>
        <th class="center line" width="70px">Qty</th>
        <th class="center line" width="80px">Harga</th>
        <th class="center line" width="70px">Qty</th>
        <th class="center line" width="80px">Harga</th>
    </tr>
    </thead>
    <tbody>
    <?php
    foreach ($data as $d) {
        ?>
        <tr>
            <td class="center"><?= isset($d['id'])?@++$no:'' ?></td>
            <td class="center"><?=$d['kode']?></td>
            <td class="left"><?= $d['nama'] ?></td>
            <td class="center"><?=$d['awal_qty']?></td>
            <td class="right"><?=format_uang($d['awal_harga']) ?></td>
            <td class="center"><?=$d['masuk_qty']?></td>
            <td class="right"><?=format_uang($d['masuk_harga']) ?></td>
            <td class="center"><?=$d['keluar_qty']?></td>
            <td class="right"><?=format_uang($d['keluar_harga']) ?></td>
            <td class="center"><?=$d['adj_qty']?></td>
            <td class="right"><?=format_uang($d['adj_harga']) ?></td>
            <td class="center"><?=$d['akhir_qty']?></td>
            <td class="right"><?=format_uang($d['akhir_harga']) ?></td>
        </tr>
        <?php
    }
    ?>
    </tbody>
</table>
<htmlpagefooter name="footer">
    <table width="100%">
        <tr>
            <td style="text-align: left"><?= 'Export on: '.date('d/m/Y H:i:s') ?></td>
            <td style="text-align: right">{PAGENO}</td>
        </tr>
    </table>
</htmlpagefooter>
<style>
    @page {
        margin-top: 30px;
        margin-bottom: 70px;
        margin-left: 30px;
        margin-right: 30px;
        footer: html_footer;
        header: html_header;
    }
    .list {
        width: 100%;
        margin-top: 10px;
        margin-bottom: 10px;
        border-bottom: 1px solid black;
    }
    th{
        margin: 10px;
        text-transform: uppercase;
    }
    .right{
        text-align: right;
    }
    .center{
        text-align: center;
    }
    .left{
        text-align: left;
    }
    .line{
        border-bottom: 1px solid black;
    }
</style>