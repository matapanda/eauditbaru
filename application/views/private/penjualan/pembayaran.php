<div class="row">
    <div class="col-sm-12">
        <div class="card-box">
            <?php
            include VIEWPATH.'alert.php';
            ?>
            <button onclick="$('#list-pending').toggle()" class="<?=is_authority(@$access['c'])?> btn btn-inverse"><i class="fa fa-plus"></i> ADD NEW DATA</button>
            <a href="?" class="btn btn-primary"><i class="fa fa-refresh"></i> RELOAD</a>
            <hr>
            <div id="list-pending" class="table-responsive" style="display: none">
                <table class="table table-bordered table-striped table-hover">
                    <thead>
                    <tr>
                        <th class="center col-xs-1">#</th>
                        <th class="center col-xs-2">ID</th>
                        <th class="center">Customer</th>
                        <th class="center col-xs-2">Hutang/Piutang</th>
                        <th class="center col-xs-2">Tagihan</th>
                        <th class="center col-xs-1">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $no=$page;
                    foreach($pending as $g) {
                        ?>
                        <tr class="data<?=$g['customer']?>">
                            <td scope="row" class="center"><?=++$no?></td>
                            <td class="center"><?=$g['customer_id']?></td>
                            <td><?=$g['customer_name']?></td>
                            <td class="right"><?= format_uang($g['hutangpiutang'])?></td>
                            <td class="right"><?= format_uang($g['tagihan'])?></td>
                            <td class="center">
                                <a href="?entry=<?=$g['customer']?>" class="btn btn-xs btn-inverse"><i class="fa fa-pencil"></i> BAYAR</a>
                            </td>
                        </tr>
                        <?php
                    }
                    ?>
                    </tbody>
                </table>
                <hr>
            </div>
            <form method="get" class="row" action="<?=base_url('pembelian/pembayaran')?>">
                <div class="col-md-6">
                    <div class="dataTables_wrapper form-inline">
                        <label>Filter: </label>
                        <div class="input-daterange input-group" id="date-range">
                            <input type="text" readonly class="form-control input-sm" name="start" onchange="this.form.submit()" value="<?=$start?>">
                            <span class="input-group-addon input-sm">~</span>
                            <input type="text" readonly class="form-control input-sm" name="end" onchange="this.form.submit()" value="<?=$end?>">
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="dataTables_wrapper form-inline right">
                        <label class="hidden-xs">No. PPU: &nbsp;</label><input type="search" name="nota" class="form-control input-sm" autocomplete="off" value="<?=$nota?>">
                    </div>
                </div>
            </form>
            <div class="table-responsive">
                <table class="table table-bordered table-striped table-hover">
                    <thead>
                    <tr>
                        <th class="center">#</th>
                        <th class="center col-xs-1">Tanggal</th>
                        <th class="center col-xs-1">No. PPU</th>
                        <th class="center col-xs-2">Customer</th>
                        <th class="center col-xs-2">Jumlah Bayar</th>
                        <th class="center col-xs-2">Metode Pembayaran</th>
                        <th class="center col-xs-3">Catatan</th>
                        <th class="center">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $no=$page;
                    foreach($data as $g) {
                        ?>
                        <tr class="data<?=$g['id']?>">
                            <td scope="row" class="center"><?=++$no?></td>
                            <td class="center"><?=format_waktu($g['tanggal'])?></td>
                            <td class="center"><?=$g['nota']?></td>
                            <td><?=$g['customer_name']?></td>
                            <td class="right"><?=format_uang($g['jumlahbayar'])?></td>
                            <td class="center"><?=$g['keterangan']?></td>
                            <td><?=$g['note']?></td>
                            <td class="center">
                                <button onclick="rincian('<?=$g['id']?>','<?=$g['nota']?>')" class="btn btn-xs btn-inverse"><i class="fa fa-search"></i></button>
                            </td>
                        </tr>
                        <?php
                    }
                    ?>
                    </tbody>
                </table>
            </div>
            <?=$pagination?>
        </div>
    </div>
</div>
<div id="detailpenerimaan" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <table class="table table-bordered table-striped table-hover">
                    <thead>
                    <tr>
                        <th class="center col-xs-2">Invoice</th>
                        <th class="center col-xs-2">PPn</th>
                        <th class="center col-xs-2">PPh</th>
                        <th class="center col-xs-2">Tagihan</th>
                        <th class="center col-xs-2">Diskon</th>
                        <th class="center col-xs-2">Sisa Tagihan</th>
                    </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<link href="<?=base_url()?>assets/plugins/custombox/css/custombox.min.css" rel="stylesheet">
<script src="<?=base_url()?>assets/plugins/custombox/js/custombox.min.js"></script>
<script src="<?=base_url()?>assets/plugins/custombox/js/legacy.min.js"></script>
<link href="<?=base_url()?>assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css"/>
<link href="<?=base_url()?>assets/css/bootcomplete.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="<?=base_url()?>assets/plugins/select2/js/select2.min.js"></script>
<script src="<?=base_url()?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script src="<?=base_url()?>assets/js/jquery.bootcomplete.js"></script>
<script src="<?=base_url()?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script>
    $('input[name=nota_id]','#new-data').bootcomplete({
        url:'?sj=true',
        minLength : 2,
        method: 'post'
    });
    $('#date-range').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true,
        todayHighlight: true
    });
    function hapus(_i) {
        swal({
            title: "Batalkan Retur?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD5555",
            confirmButtonText: "Ya",
            cancelButtonText: "Tidak",
            closeOnConfirm: true,
            closeOnCancel: true
        }, function (isConfirm) {
            if (isConfirm) {
                $.post('?',{delete:_i},function () {
                    $('.data'+_i).remove();
                });
            }
        });
    }
    function rincian(_i,_nota) {
        $.post('?',{d:_i},function (data,status) {
            $('h4.modal-title','#detailpenerimaan').html(_nota);
            $('tbody','#detailpenerimaan').html(data.data);
            $('#detailpenerimaan').modal('show');
        },'json');
    }
</script>