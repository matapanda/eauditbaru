<div class="row">
    <div class="col-sm-12">
        <div class="card-box">
            <?php
            include VIEWPATH.'alert.php';
            ?>
            <a href="?new=barang" class="<?=is_authority(@$access['c'])?> btn btn-inverse"><i class="fa fa-plus"></i> BARANG</a>
            <a href="?new=jasa" class="<?=is_authority(@$access['c'])?> btn btn-inverse"><i class="fa fa-plus"></i> JASA</a>
            <a href="?" class="btn btn-primary"><i class="fa fa-refresh"></i> RELOAD</a>
            <hr>
            <form method="get" class="row" action="<?=base_url('penjualan/order')?>">
                <div class="col-md-6">
                    <div class="dataTables_wrapper form-inline">
                        <label>Filter: </label>
                        <div class="input-daterange input-group" id="date-range">
                            <input type="text" readonly class="form-control input-sm" name="start" onchange="this.form.submit()" value="<?=$start?>">
                            <span class="input-group-addon input-sm">~</span>
                            <input type="text" readonly class="form-control input-sm" name="end" onchange="this.form.submit()" value="<?=$end?>">
                        </div>
                        <select name="closed" onchange="this.form.submit()" class="form-control input-sm">
                            <option value="">Semua status</option>
                            <option value="f" <?=$closed=='f'?'selected':''?>>Status aktif</option>
                            <option value="t" <?=$closed=='t'?'selected':''?>>Status closed</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="dataTables_wrapper form-inline right">
                        <label class="hidden-xs">No. OPJ: &nbsp;</label><input type="search" name="nota" class="form-control input-sm" autocomplete="off" value="<?=$nota?>">
                    </div>
                </div>
            </form>
            <div class="table-responsive">
                <table class="table table-bordered table-striped table-hover">
                    <thead>
                    <tr>
                        <th class="center">#</th>
                        <th class="center col-xs-1">Tanggal</th>
                        <th class="center col-xs-1">No. OPJ</th>
                        <th class="center col-xs-2">Customer</th>
                        <th class="center col-xs-1">Sub</th>
                        <th class="center col-xs-1">DPP</th>
                        <th class="center col-xs-1">PPn</th>
                        <th class="center col-xs-1">PPh</th>
                        <th class="center col-xs-1">Diskon</th>
                        <th class="center col-xs-1">Total</th>
                        <th class="center">Status</th>
                        <th class="center">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $no=$page;
                    foreach($data as $g) {
                        ?>
                        <tr class="data<?=$g['id']?>">
                            <td scope="row" class="center"><?=++$no?></td>
                            <td class="center"><?=format_waktu($g['tanggal'])?></td>
                            <td class="center"><?=$g['nota']?></td>
                            <td><?=$g['customer']?></td>
                            <td class="right"><?=format_uang($g['subtotal'])?></td>
                            <td class="right"><?=format_uang($g['dpp'])?></td>
                            <td class="right"><?=format_uang($g['ppn'])?></td>
                            <td class="right"><?=format_uang($g['pph'])?></td>
                            <td class="right"><?=format_uang($g['diskon'])?></td>
                            <td class="right"><?=format_uang($g['grandtotal'])?></td>
                            <td class="center"><?= getLabelCLosed($g['closed'])?></td>
                            <td class="center">
                                <button onclick="rincian('<?=$g['id']?>','<?=$g['nota']?>')" class="btn btn-xs btn-block btn-inverse"><i class="fa fa-search"></i></button>
                                <?php
                                if($g['editable']=='t' && isset($access['u']) && false){
                                    ?>
                                    <a href="?e=<?=$g['id']?>" class="btn btn-xs btn-block btn-primary"><i class="fa fa-pencil"></i></a>
                                    <?php
                                }
                                ?>
                            </td>
                        </tr>
                        <?php
                    }
                    ?>
                    </tbody>
                </table>
            </div>
            <?=$pagination?>
        </div>
    </div>
</div>
<div id="detailpembelian" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-full">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <table class="table table-bordered table-striped table-hover">
                    <thead>
                    <tr>
                        <th class="center col-xs-3">Barang</th>
                        <th class="center col-xs-1">Qty</th>
                        <th class="center col-xs-1">Harga</th>
                        <th class="center col-xs-1">DPP</th>
                        <th class="center col-xs-1">PPn</th>
                        <th class="center col-xs-1">PPh</th>
                        <th class="center col-xs-1">Diskon</th>
                        <th class="center col-xs-1">Subtotal</th>
                        <th class="center col-xs-1">Barang/Jasa</th>
                        <th class="center col-xs-1">Status</th>
                    </tr>
                    </thead>
                    <tbody></tbody>
                </table>
                <p>Uang Muka:</p>
                <h4 class="uangmuka"></h4>
                <p>Mengetahui:</p>
                <h4 class="mengetahui"></h4>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<link href="<?=base_url()?>assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css"/>
<script src="<?=base_url()?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script>
    $('#date-range').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true,
        todayHighlight: true
    });
    function rincian(_i,_nota) {
        $('.hitung'+_i).html('<img src="<?=base_url('assets/loading.gif')?>">');
        $.post('?',{d:_i},function (data,status) {
            $('h4.modal-title','#detailpembelian').html(_nota);
            $('tbody','#detailpembelian').html(data.data);
            $('.mengetahui','#detailpembelian').html(data.mengetahui);
            $('.uangmuka','#detailpembelian').html(data.uangmuka);
            $('#detailpembelian').modal('show');
        },'json');
    }
</script>