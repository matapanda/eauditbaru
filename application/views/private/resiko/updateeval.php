<div class="row">
    <div class="col-sm-12">
        <div class="card-box row">
            <?php
            show_alert();
            ?>
            <div class="col-sm-12">
                <form role="form" method="post" action="?upval=true">
                    <input type="hidden" name="jml_program" required class="form-control" id="jml_program" value="<?=@$jml_kr?$jml_kr:1?>">

                    <?php
                    $no=1;
                    if(@$resiko_kr){
                    foreach ($resiko_kr as $r) {
                        ?>
                    <div class="col-md-12 tambahan">
                        <div class="form-group <?= $no ?>">
                            <h3>Resiko</h3> <br>
                            <div class="form-group resiko<?= $no ?>">
                                <div class="form-group row">
                                    <label for="inputName" class="col-sm-2 right form-control-label">Resiko<span
                                                class="text-danger">*</span></label>
                                    <div class="col-sm-6">
                                        <input type="hidden" name="urutan" value="<?=$no?>">
                                        <select disabled class="select2 form-control select2-multiple" name="resiko<?=$no?>" data-placeholder="Pilih Tipe Resiko" required>
                                            <option value="">Pilih Tipe Resiko</option>
                                            <?php
                                            foreach ($tipe_resiko as $v):
                                                ?>
                                                <option value="<?= $v['id'] ?>" <?= @$r['resiko']==$v['id']?'selected':'' ?>><?= "$v[nama]" ?></option>
                                            <?php
                                            endforeach;
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputName" class="col-sm-2 right form-control-label">Penyebab Resiko<span
                                                class="text-danger">*</span></label>
                                    <div class="col-sm-6">
                                        <select disabled class="select2 form-control select2-multiple" name="p_resiko<?=$no?>" data-placeholder="Pilih Penyebab Resiko" required>
                                            <option value="">Pilih Penyebab Resiko</option>
                                            <?php
                                            foreach ($pe_resiko as $v):
                                                ?>
                                                <option value="<?= $v['id'] ?>" <?= @$r['p_resiko']==$v['id']?'selected':'' ?>><?= "$v[nama]" ?></option>
                                            <?php
                                            endforeach;
                                            ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group col-sm-offset-1">
                                <div class="form-group row">
                                    <h5>Kendali </h5> <br>
                                </div>
                                    <div class="form-group kendali<?= $no ?>">
                                        <div class="form-group row">
                                   <label for="inputName" class="col-sm-2 right form-control-label">Kendali Seharusnya <span class="text-danger">*</span></label>
                                    <div class="col-sm-6">
                                        <select disabled class="select2 form-control select2-multiple" name="kendali_s<?=$no?>[]" multiple="multiple" multiple data-placeholder="Pilih Kendali Seharusnya" required>
                                            <option value="">Pilih Kendali Sementara</option>
                                            <?php
                                            foreach ($kendali as $v):
                                                ?>
                                                <option value="<?= $v['id'] ?>" <?= in_array($v['id'], json_decode($r['kendali_s'], true)) ? 'selected' : '' ?> ><?="$v[nama]"?></option>
                                            <?php
                                            endforeach;
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputName" class="col-sm-2 right form-control-label">Kendali Yang Ada<span
                                                class="text-danger">*</span></label>
                                    <div class="col-sm-6">
                                        <select disabled class="select2 form-control select2-multiple" name="kendali_a<?=$no?>[]" multiple="multiple" multiple data-placeholder="Pilih Kendali Yang Ada" required>
                                            <option value="">Pilih Kendali Yang Ada</option>
                                            <?php
                                            foreach ($kendali as $v):
                                                ?>
                                                <option value="<?= $v['id'] ?>" <?= in_array($v['id'], json_decode($r['kendali_a'], true)) ? 'selected' : '' ?> ><?= "$v[nama]" ?></option>
                                            <?php
                                            endforeach;
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                        <div class="form-group row">
                                    <label for="inputName" class="col-sm-2 right form-control-label">Kendali Yang Belum Ada<span
                                                class="text-danger">*</span></label>
                                    <div class="col-sm-6">
                                        <select disabled class="select2 form-control select2-multiple" name="kendali_ba<?=$no?>[]" multiple="multiple" multiple data-placeholder="Pilih Kendali Yang Belum Ada" required>
                                            <option value="">Pilih Kendali Yang Belum Ada</option>
                                            <?php
                                            foreach ($kendali as $v):
                                                ?>
                                                <option value="<?= $v['id'] ?>" <?= in_array($v['id'], json_decode($r['kendali_ba'], true)) ? 'selected' : '' ?> ><?= "$v[nama]" ?></option>
                                            <?php
                                            endforeach;
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                        <div class="form-group row">
                                            <label for="inputName" class="col-sm-2 right form-control-label">Penilaian atas Kendali<span class="text-danger">*</span></label>
                                            <div class="col-sm-6">
                                                <select class="select2 form-control select2-multiple" name="nilai<?= $no ?>" data-placeholder="Pilih Penilaian atas Kendali Yang sudah ada" required>
                                                    <option value="">Pilih Penilaian atas Kendali</option>
                                                    <option value="1" <?=@$r['nilai']==1?'selected':''?>>1. EFEKTIF</option>
                                                    <option value="2" <?=@$r['nilai']==2?'selected':''?>>2. KURANG EFEKTIF</option>
                                                    <option value="3" <?=@$r['nilai']==3?'selected':''?>>3. TIDAK EFEKTIF</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="inputName" class="col-sm-2 right form-control-label">Penanggung Jawab<span class="text-danger">*</span></label>
                                            <div class="col-sm-6">
                                                <input type="hidden" name="id<?= $no ?>" value="<?=$r['id']?>" required>
                                                <input type="hidden" name="ken<?= $no ?>[]" value="<?=$r['kendali_a']?>" required>
                                                <input type="hidden" name="res<?= $no ?>" value="<?=$r['resiko']?>" required>
                                                <input type="text" class="form-control " name="pj<?= $no ?>" value="<?=$r['penanggung']?>" placeholder="Penanggung Jawab" required>
                                            </div>
                                        </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    </div>
                        <?php
                    $no++;
                    }
                    }
                    ?>
                    <div class="form-group row">
                        <div class="col-sm-8 col-sm-offset-4">
                            <button type="submit" class="btn btn-inverse waves-effect waves-light">Simpan</button>
                            <a href="?" class="btn btn-default waves-effect m-l-5">Batal</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<link href="<?= base_url() ?>assets/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="<?= base_url() ?>assets/plugins/select2/js/select2.min.js"></script>
<script type="text/javascript">
    $('.select2').select2();
    function tambah_kendali(i){
        var no=i;
        $.post('?',{kendali:i},function (data,status) {
            $('.kendali'+i).append(data);
            $('.select2').select2();
        });
    }function tambah_resiko(i){
        var no=$('#jml_program').val();
        var akhir=parseFloat(no)+1;
        $.post('?',{resiko:akhir},function (data,status) {
            $('.tambahan').append(data);
            $('.select2').select2();
            $('#jml_program').val(akhir);
            $("input[name='resiko"+akhir+"']").focus();
        });
    }
</script>