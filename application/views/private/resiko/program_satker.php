<div class="row">
    <div class="col-sm-12  col-print-12">
        <h1 class="center visible-print" style="margin:0">PKPT <?=@$tahun?></h1>
        <div class="card-box">
            <?php
            show_alert();
            ?>
            <a href="?i=true" class="<?=is_authority(@$access['c'])?> btn btn-inverse hidden-print"><i class="fa fa-plus"></i> Tambah Data Program Satker</a>
            <hr>
            <form method="get" class="row hidden-print" action="<?=base_url('resiko/program')?>">
                <div class="col-md-6">
                    <div class="dataTables_wrapper form-inline">
                        <select name="periode" onchange="this.form.submit()" class="form-control input-sm">
                            <?php
                            foreach($periode as $p){
                                ?>
                                <option value="<?=$p['id']?>" <?=@$periode_s==$p['id']?'selected':''?>><?=$p['nama']?></option>
                                <?php
                            }
                            ?>
                        </select>
                        <select name="closed" onchange="this.form.submit()" class="form-control input-sm">
                            <option value="" <?=$closed==''?'selected':''?>>Semua Status</option>
                            <option value="t" <?=$closed=='t'?'selected':''?>>Status Aktif</option>
                            <option value="f" <?=$closed=='f'?'selected':''?>>Status Non Aktif</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="dataTables_wrapper form-inline right">
                        <label class="hidden-xs">Search: &nbsp;</label><input type="search" name="search" class="form-control input-sm" autocomplete="off" value="<?=@$search?>">
                    </div>
                </div>

            </form>
            <div class="table-responsive">
               <br> <table class="table table-bordered table-striped table-hover col-print-12">
                    <thead>
                    <tr>
                        <th class="center col-xs-1 col-print-1">#</th>
                        <th class="center col-xs-6 col-print-6">Nama</th>
                        <th class="center col-xs-1 col-print-1">Jumlah</th>
                        <th class="center col-xs-2 col-print-2">Prosedur Audit</th>
                        <th class="center col-xs-1 hidden-print">Status</th>
                        <th class="center col-xs-1 hidden-print <?=is_authority(@$access['u'])?>"></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $no=1;
                    foreach($pkpt as $g) {
                        ?>
                        <tr>
                            <td class="center"><?=$no?></td>
                            <td class="">
                            <?=@$g['nama']?>
                            </td>
                            <td class="right">
                                <?=format_uang(@$g['jumlah'])?>
                            </td>
                            <td class="center">
                                <?=@$g['nama_prosedur']?>
                            </td>
                            <td class="center hidden-print <?= $g['id'] ?> hand-cursor" <?= isset($access['u']) ? "onclick=\"setStatusActive('$g[id]')\"" : '' ?>><?= getLabelStatus($g['status']) ?></td>
                            <td class=" hidden-print <?=is_authority(@$access['u'])?> center">
                                <a href="?e=<?=$g['id']?>" class="btn btn-sm btn-inverse"><i class="fa fa-pencil"></i></a>
                            </td>
                        </tr>
                        <?php
                    $no++;}
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<link href="<?=base_url()?>assets/plugins/custombox/css/custombox.min.css" rel="stylesheet">
<link href="<?=base_url()?>assets/plugins/bootstrap3-editable/css/bootstrap-editable.css" rel="stylesheet">
<script src="<?=base_url()?>assets/plugins/bootstrap3-editable/js/bootstrap-editable.min.js"></script>
<script src="<?=base_url()?>assets/plugins/custombox/js/custombox.min.js"></script>
<script src="<?=base_url()?>assets/plugins/custombox/js/legacy.min.js"></script>
<div id="new-group" class="modal-gradin">
    <button type="button" class="close" onclick="Custombox.close();">
        <span>&times;</span><span class="sr-only">Close</span>
    </button>
    <h4 class="custom-modal-title">Tambah PKPT</h4>
    <div class="custom-modal-text">
        <form action="?" method="post">
            <div class="form-group row">
                <div class="col-sm-12">
                    <input type="text" name="no" required class="form-control" placeholder="No PKPT"><br>
                </div>
                <div class="col-sm-12">
                    <input type="text" name="tema" required class="form-control" placeholder="Tema PKPT"><br>
                </div>
                <div class="col-sm-12">
                    <input type="text" name="nama" required class="form-control" placeholder="Nama PKPT"><br>
                </div>
                <div class="col-sm-12">
                    <input type="text" name="kegiatan" required class="form-control" placeholder="Kegiatan PKPT"><br>
                </div>
                <div class="col-sm-12">
                    <input type="text" name="hp" required class="form-control" placeholder="HP"><br>
                </div>
                <div class="col-sm-12">
                    <input type="text" name="rmp" required class="form-control" placeholder="RMP"><br>
                </div>
                <div class="col-sm-12">
                    <input type="text" name="rpl" required class="form-control" placeholder="RPL"><br>
                </div>
                <div class="col-sm-12">
                    <input type="text" name="dana" required class="form-control" placeholder="Dana PKPT"><br>
                </div>
                <div class="col-sm-12">
                    <input type="text" name="risiko" required class="form-control" placeholder="Risiko PKPT"><br>
                </div>
                <div class="col-sm-12">
                    <input type="text" name="keterangan" required class="form-control" placeholder="Keterangan PKPT">
                </div>
                <div class="col-sm-12 right" style="padding-top: 1em">
                    <button type="button" onclick="Custombox.close();" class="btn btn-default">BATAL</button>
                    <button type="submit" class="btn btn-inverse">SIMPAN</button>
                </div>
            </div>
        </form>
    </div>
</div>
<script>
    $.fn.editable.defaults.mode = 'inline';
    function setStatusActive(_i) {
        $('.'+_i).html('<img src="<?=base_url('assets/loading.gif')?>">');
        $.post('?',{status:_i},function (data,status) {
            $('.'+_i).html(data);
        });
    }
    $(function(){
        <?php
        if(!isset($access['u'])){
        ?>
        $(".emjes-editable").editable('option', 'disabled', true);
        <?php
        }
        ?>
    });
</script>