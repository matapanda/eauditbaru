<div class="row">
    <div class="col-sm-12">
        <div class="card-box row">
            <?php
            show_alert();
            ?>
            <div class="col-sm-12">
                <form role="form" method="post" action="?ev=true">
                    <div class="form-group row">
                        <label for="inputName" class="col-sm-2 form-control-label">Periode<span
                                    class="text-danger">*</span></label>
                        <div class="col-sm-5">
                            <select class="select2 form-control select2-multiple" name="periode" disabled data-placeholder="Pilih Periode" required>
                                <option value="">Pilih Periode</option>
                                <?php
                                foreach ($periode as $v):
                                    ?>
                                    <option value="<?= $v['id'] ?>" <?= @$user['periode']==$v['id']?'selected':'' ?>><?= "$v[nama]" ?></option>
                                <?php
                                endforeach;
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputName" class="col-sm-2 form-control-label">Nama Kegiatan<span
                                    class="text-danger">*</span></label>
                        <div class="col-sm-5    ">
                            <select class="select2 form-control select2-multiple" onchange="kegiat()" id="keg" name="nama" data-placeholder="Pilih Nama Kegiatan" disabled required>
                                <option value="">Pilih Nama Kegiatan</option>
                                <?php
                                foreach ($keg as $v):
                                    ?>
                                    <option value="<?= $v['id'] ?>" <?= @$user['kegiatan']==$v['id']?'selected':'' ?>><?= "$v[nama]" ?></option>
                                <?php
                                endforeach;
                                ?>
                            </select>

                        </div>
                        <input type="hidden" name="jml_program" required class="form-control" id="jml_program" value="<?=@$jml_kr?$jml_kr:1?>">

                    </div>
                    <div class="form-group row">
                        <label for="inputName" class="col-sm-2 form-control-label">Satker<span
                                    class="text-danger">*</span></label>
                        <div class="col-sm-5">
                            <select class="select2 form-control select2-multiple" name="satker" disabled data-placeholder="Pilih Satker" required>
                                <option value="">Pilih Satker</option>
                                <?php
                                foreach ($satker as $v):
                                    ?>
                                    <option value="<?= $v['id'] ?>" <?= @$user['satker']==$v['id']?'selected':'' ?>><?= "$v[kode] - $v[ket]" ?></option>
                                <?php
                                endforeach;
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputName" class="col-sm-2 form-control-label">Nama Program<span
                                    class="text-danger">*</span></label>
                        <div class="col-sm-5">
                            <select class="select2 form-control select2-multiple" name="program" disabled data-placeholder="Pilih Program Satker" required>
                                <option value="">Pilih Program Satker</option>
                                <?php
                                foreach ($program as $v):
                                    ?>
                                    <option value="<?= $v['id'] ?>" <?= @$user['program_satker']==$v['id']?'selected':'' ?>><?= "$v[nama]" ?></option>
                                <?php
                                endforeach;
                                ?>
                            </select>
                        </div>
                    </div>

                    <hr>
                    <?php
                        for($no=1;$no<=$jml_kr;$no++){
                        foreach($resiko_kr as $r){
                            ?>
                            <div class="col-md-12 tambahan">
                                <div class="form-group <?=$no?>">
                                    <h3>Analisa Resiko </h3> <br>

                                    <div class="form-group resiko<?=$no?>">

                                        <div class="form-group row">
                                            <label for="inputName" class="col-sm-2 right form-control-label">Resiko<span
                                                        class="text-danger">*</span></label>
                                            <div class="col-sm-6">
                                                <select class="select2 form-control select2-multiple" disabled name="resiko<?=$no?>" data-placeholder="Pilih Tipe Resiko" required>
                                                    <option value="">Pilih Tipe Resiko</option>
                                                    <?php
                                                    foreach ($tipe_resiko as $v):
                                                        ?>
                                                        <option value="<?= $v['id'] ?>" <?= @$r['resiko']==$v['id']?'selected':'' ?>><?= "$v[nama]" ?></option>
                                                    <?php
                                                    endforeach;
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="inputName" class="col-sm-2 right form-control-label">Penyebab Resiko<span
                                                        class="text-danger">*</span></label>
                                            <div class="col-sm-6">
                                                <select class="select2 form-control select2-multiple" disabled name="p_resiko<?=$no?>" data-placeholder="Pilih Penyebab Resiko" required>
                                                    <option value="">Pilih Penyebab Resiko</option>
                                                    <?php
                                                    foreach ($pe_resiko as $v):
                                                        ?>
                                                        <option value="<?= $v['id'] ?>" <?= @$r['p_resiko']==$v['id']?'selected':'' ?>><?= "$v[nama]" ?></option>
                                                    <?php
                                                    endforeach;
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="inputName" class="col-sm-2 right form-control-label">Dampak Resiko<span
                                                        class="text-danger">*</span></label>
                                            <div class="col-sm-6">
                                                <select class="select2 form-control select2-multiple" name="da_resiko<?=$no?>" disabled data-placeholder="Pilih Dampak Resiko" required>
                                                    <option value="">Pilih Dampak Resiko</option>
                                                    <?php
                                                    foreach ($da_resiko as $v):
                                                        ?>
                                                        <option value="<?= $v['id'] ?>" <?= @$r['da_resiko']==$v['id']?'selected':'' ?>><?= "$v[nama]" ?></option>
                                                    <?php
                                                    endforeach;
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="inputName" class="col-sm-2 right form-control-label">Skala Kemungkinan<span
                                                        class="text-danger">*</span></label>
                                            <div class="col-sm-6">
                                                <input type="hidden" name="idrs<?=$no?>" value="<?=$r['id']?>">
                                                <input type="hidden" name="rs<?=$no?>" value="<?=$r['resiko']?>">
                                                <select class="select2 form-control" name="sk<?=$no?>" data-placeholder="Skala Kemungkinan" required>
                                                    <option value="">Pilih Skala Kemungkinan</option>
                                                    <option value="1" <?=$r['sk']==1?'selected':''?>>1. HAMPIR TIDAK</option>
                                                    <option value="2" <?=$r['sk']==2?'selected':''?>>2. JARANG</option>
                                                    <option value="3" <?=$r['sk']==3?'selected':''?>>3. KADANG</option>
                                                    <option value="4" <?=$r['sk']==4?'selected':''?>>4. SERING</option>
                                                    <option value="5" <?=$r['sk']==5?'selected':''?>>5. HAMPIR PASTI</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="inputName" class="col-sm-2 right form-control-label">Skala Dampak<span
                                                        class="text-danger">*</span></label>
                                            <div class="col-sm-6">
                                                <select class="select2 form-control" name="sd<?=$no?>" data-placeholder="Skala Dampak" required>
                                                    <option value="">Pilih Skala Kemungkinan</option>
                                                    <option value="1" <?=$r['sd']==1?'selected':''?>>1. SANGAT SIGNIFIKAN</option>
                                                    <option value="2" <?=$r['sd']==2?'selected':''?>>2. SIGNIFIKAN</option>
                                                    <option value="3" <?=$r['sd']==3?'selected':''?>>3. MODERAT</option>
                                                    <option value="4" <?=$r['sd']==4?'selected':''?>>4. MINOR</option>
                                                    <option value="5" <?=$r['sd']==5?'selected':''?>>5. TIDAK SIGNIFIKAN</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php
                        $no++;
                        }
                    }
                    ?>
                    <div class="form-group row">
                        <div class="col-sm-8 col-sm-offset-4">
                            <button type="submit" class="btn btn-inverse waves-effect waves-light">Simpan</button>
                            <a href="?" class="btn btn-default waves-effect m-l-5">Batal</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<link href="<?= base_url() ?>assets/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="<?= base_url() ?>assets/plugins/select2/js/select2.min.js"></script>
<script type="text/javascript">
    $('.select2').select2();
    function tambah_kendali(i){
        var no=i;
        $.post('?',{kendali:i},function (data,status) {
            $('.kendali'+i).append(data);
            $('.select2').select2();
        });
    }function tambah_resiko(i){
        var no=$('#jml_program').val();
        var akhir=parseFloat(no)+1;
        $.post('?',{resiko:akhir},function (data,status) {
            $('.tambahan').append(data);
            $('.select2').select2();
            $('#jml_program').val(akhir);
            $("input[name='resiko"+akhir+"']").focus();
        });
    }
</script>