<div ng-app="gradinSA" ng-controller="orderController" ng-init="init()">
    <div class="row">
        <div class="col-sm-12">
            <div class="card-box row">
                <?php
                include VIEWPATH.'alert.php';
                ?>
                <form id="form-order" role="form" method="post" ng-submit="simpanorder()">
                    <div class="col-md-6 col-md-offset-3">
                        <div class="form-group row">
                            <label for="inputEmail" class="col-sm-4 form-control-label">Tanggal<span class="text-danger">*</span></label>
                            <div class="col-sm-7">
                                <div class="input-group">
                                    <input type="text" name="tanggal" required class="form-control datepicker" placeholder="tanggal" value="<?=date('d/m/Y')?>" readonly>
                                    <span class="input-group-addon bg-custom b-0"><i class="mdi mdi-calendar text-white"></i></span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputEmail" class="col-sm-4 form-control-label"><?=@$kbg_title?><span class="text-danger">*</span></label>
                            <div class="col-sm-7">
                                <select name="kbg" class="form-control" required onchange="kbg_change()">
                                    <option value=''>-</option>
                                    <?php
                                    $last=array();
                                    $last_id=array(1=>@$kbg[0]['k1'],2=>@$kbg[0]['k2'],3=>@$kbg[0]['k3']);
                                    $group=0;
                                    foreach ($kbg[1] as $r){
                                        if($r['level']==1){
                                            $last_id[1]=$r['k1'];
                                            $last[1]=$r['nama'];
                                            continue;
                                        }elseif($r['level']==2 && "$r[k1]"=="$last_id[1]"){
                                            $last_id[2]=$r['k2'];
                                            $last[2]=$r['nama'];
                                            continue;
                                        }elseif($r['level']==3 && "$r[k1]$r[k2]"=="$last_id[1]$last_id[2]"){
                                            $last_id[3]=$r['k3'];
                                            $last[3]=$r['nama'];
                                            if($group>0){
                                                echo "</optgroup>";
                                            }
                                            $group++;
                                            echo "<optgroup label=\"".implode(" - ",$last)."\">";
                                            continue;
                                        }elseif($r['level']==4 && "$r[k1]$r[k2]$r[k3]"==implode("",$last_id)){
                                            echo "<option value='$r[id]'>" . format_coa($r['kode']) . " $r[nama]</option>";
                                        }
                                    }
                                    if($group>0){
                                        echo "</optgroup>";
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="barangjasa" class="col-sm-4 form-control-label">Posisi<span class="text-danger">*</span></label>
                            <div class="col-sm-7">
                                <div class="btn-switch btn-switch-inverse">
                                    <label class="radio-inline"><input type="radio" ng-model="dk" value="D"/> Masuk</label>
                                    <label class="radio-inline"><input type="radio" ng-model="dk" value="K"/> Keluar</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputEmail" class="col-sm-4 form-control-label">Jumlah</label>
                            <div class="col-sm-7">{{total|currency:'':2}}</div>
                        </div>
                        <div class="form-group row">
                            <label for="inputEmail" class="col-sm-4 form-control-label">Catatan</label>
                            <div class="col-sm-7">
                                <textarea name="note" class="form-control" placeholder="Catatan"></textarea>
                                <textarea name="items[]" class="hidden" ng-repeat="(key, item) in items">{{item}}</textarea>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <button type="button" ng-click="add(false)" class="btn btn-primary waves-effect waves-light"><i class="fa fa-plus"></i> Tambah Data [F1]</button>
                        <hr>
                        <div ng-hide="!dk">
                            <table class="table table-bordered table-striped table-hover">
                                <thead>
                                <tr>
                                    <th class="center col-xs-3">COA</th>
                                    <th class="center col-xs-2">Debet</th>
                                    <th class="center col-xs-2">Kredit</th>
                                    <th class="center col-xs-2">Nomor Bukti</th>
                                    <th class="col-xs-3">Catatan</th>
                                    <th class="center">&nbsp;</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td id="kbg_coa"></td>
                                    <td class="right" ng-if="dk=='K'">{{0|currency:'':2}}</td>
                                    <td class="right">{{total|currency:'':2}}</td>
                                    <td class="right" ng-if="dk=='D'">{{0|currency:'':2}}</td>
                                    <td colspan="3"></td>
                                </tr>
                                <tr ng-repeat="(key, item) in items" ng-click="edit(key)">
                                    <td>{{item.coa_value}}</td>
                                    <td class="right" ng-if="dk=='D'">{{0|currency:'':2}}</td>
                                    <td class="right">{{item.jumlah|currency:'':2}}</td>
                                    <td class="right" ng-if="dk=='K'">{{0|currency:'':2}}</td>
                                    <td class="center">{{item.nobukti}}</td>
                                    <td>{{item.note}}</td>
                                    <td class="center"><button type="button" class="btn btn-xs btn-danger" ng-click="hapus(key)"><i class="fa fa-remove"></i></button></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <hr>
                        <button ng-show="total>0 && dk" type="submit" class="btn btn-inverse waves-effect waves-light">Submit Data</button>
                        <a href="?" class="btn btn-default waves-effect m-l-5">Kembali</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div id="formedit" class="modal fade" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <form ng-submit="simpanedit()">
                        <div class="form-group row">
                            <label for="hori-pass2" class="col-sm-4 form-control-label">COA<span class="text-danger">*</span></label>
                            <div class="col-sm-7">
                                <select name="coa" ng-model="coa" class="select2 form-control" required>
                                    <option value=''>-</option>
                                    <?php
                                    $last=array();
                                    $last_id=array();
                                    $group=0;
                                    foreach ($coa as $r){
                                        if($r['level']==1){
                                            $last_id[1]=$r['k1'];
                                            $last[1]=$r['nama'];
                                            continue;
                                        }elseif($r['level']==2 && "$r[k1]"=="$last_id[1]"){
                                            $last_id[2]=$r['k2'];
                                            $last[2]=$r['nama'];
                                            continue;
                                        }elseif($r['level']==3 && "$r[k1]$r[k2]"=="$last_id[1]$last_id[2]"){
                                            $last_id[3]=$r['k3'];
                                            $last[3]=$r['nama'];
                                            if($group>0){
                                                echo "</optgroup>";
                                            }
                                            $group++;
                                            echo "<optgroup label=\"".implode(" - ",$last)."\">";
                                            continue;
                                        }elseif($r['level']==4 && "$r[k1]$r[k2]$r[k3]"==implode("",$last_id)){
                                            echo "<option value='$r[id]'>" . format_coa($r['kode']) . " $r[nama]</option>";
                                        }
                                    }
                                    echo "</optgroup>";
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputName" class="col-sm-4 form-control-label">Jumlah<span class="text-danger">*</span></label>
                            <div class="col-sm-7">
                                <input name="jumlah" ng-model="editjumlah" autocomplete="off" onclick="$(this).select()" class="form-control input-override" style="height: 20px;width: 250px">
                                <h5 class="nomargin">{{editjumlah|currency:'':2}}</h5>
                                <ul class="parsley-errors-list filled"><li class="parsley-required">{{error_edit_jumlah}}</li></ul>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputName" class="col-sm-4 form-control-label">Nomor Bukti</label>
                            <div class="col-sm-7">
                                <input name="nobukti" ng-model="editnobukti" class="form-control">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputName" class="col-sm-4 form-control-label">Note</label>
                            <div class="col-sm-7">
                                <textarea name="note" ng-model="editnote" class="form-control"></textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-8 col-sm-offset-4">
                                <button type="submit" class="btn btn-primary waves-effect waves-light">SIMPAN</button>
                                <button type="button" data-dismiss="modal" class="btn btn-default waves-effect waves-light">BATAL</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>
</div>
<link href="<?=base_url()?>assets/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css"/>
<link href="<?=base_url()?>assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css"/>
<link href="<?=base_url()?>assets/css/bootcomplete.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="<?=base_url()?>assets/plugins/select2/js/select2.min.js"></script>
<script src="<?=base_url()?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script src="<?=base_url()?>assets/js/jquery.bootcomplete.js"></script>
<script src="<?=base_url()?>assets/js/angular.min.js"></script>
<script type="text/javascript">
    function kbg_change() {
        var _kbg=$('[name=kbg] option:selected').text();
        $('#kbg_coa').html(_kbg);
    }
    $('#formedit').on('shown.bs.modal', function () {
        $('[name=coa]','#formedit').select2();
    });
    var app = angular.module("gradinSA", []);
    app.directive('validNumber', function() {
        return {
            require: '?ngModel',
            link: function(scope, element, attrs, ngModelCtrl) {
                if(!ngModelCtrl) {
                    return;
                }
                ngModelCtrl.$parsers.push(function(val) {
                    if (angular.isUndefined(val)) {
                        var val = '';
                    }
                    var clean = val.replace(/[^-0-9\.]/g, '');
                    var decimalCheck = clean.split('.');
                    if(!angular.isUndefined(decimalCheck[1])) {
                        decimalCheck[1] = decimalCheck[1].slice(0,2);
                        clean =decimalCheck[0] + '.' + decimalCheck[1];
                    }
                    if (val !== clean) {
                        ngModelCtrl.$setViewValue(clean);
                        ngModelCtrl.$render();
                    }
                    return clean;
                });
                element.bind('keypress', function(event) {
                    if(event.keyCode === 32) {
                        event.preventDefault();
                    }
                });
            }
        };
    });
    app.controller('orderController', function($scope,$http) {
        $scope.items=[];
        $scope.total=0;
        $scope.init = function(){
            $(document).on('keydown',function(e) {
                switch(e.keyCode){
                    case 112:
                        e.preventDefault();
                        $scope.add(false);
                        break;
                }
            });
        };
        $scope.simpanorder = function() {
            if($scope.item_exist){
                $('#preloader').show();
                $('#status','#preloader').show();
                $scope.form=$('#form-order').serializeArray();
                $http({
                    url: "?save=<?=$kbg_value?>",
                    data: $scope.form,
                    method: 'POST',
                    headers : {'Content-Type':'application/x-www-form-urlencoded; charset=UTF-8'}
                }).success(function(data){
                    window.location.assign('<?=base_url('accounting/'.$kbg_value.'?i=true')?>');
                    setTimeout(function () {
                        $('#preloader').hide();
                        $('#status','#preloader').hide();
                    },2000);
                    console.log("OK", data)
                }).error(function(err){"ERR", console.log(err)})
            }else{
                toastr.options = {
                    "closeButton": false,
                    "debug": false,
                    "newestOnTop": false,
                    "progressBar": false,
                    "positionClass": "toast-bottom-right",
                    "preventDuplicates": false,
                    "onclick": null,
                    "showDuration": "300",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                }
                toastr["error"]("Something Wrong!");
            }
        };
        $scope.simpanedit = function() {
            var _coa=$('[name=coa]','#formedit').val();
            var _coa_value=$('[name=coa] option:selected','#formedit').text();
            $scope.error_edit_jumlah='';
            if($scope.editjumlah<=0) {
                $scope.error_edit_jumlah='This value invalid.';
                $('[ng-model=editjumlah]').focus();
                return;
            }else if($scope.editindex==null){
                $scope.items.unshift({
                    coa:_coa,
                    coa_value:_coa_value,
                    jumlah:$scope.editjumlah,
                    note:$scope.editnote,
                    nobukti:$scope.editnobukti,
                });
            }else{
                $scope.items[$scope.editindex].coa=_coa;
                $scope.items[$scope.editindex].coa_value=_coa_value;
                $scope.items[$scope.editindex].note=$scope.editnote;
                $scope.items[$scope.editindex].jumlah=$scope.editjumlah;
                $scope.items[$scope.editindex].nobukti=$scope.editnobukti;
            }
            $scope.update();
            $('#formedit').modal('hide');
        };
        $scope.edit = function(_id) {
            $scope.editindex=_id;
            $scope.coa=$scope.items[_id].coa;
            $scope.editjumlah=$scope.items[_id].jumlah;
            $scope.editnobukti=$scope.items[_id].nobukti;
            $scope.editnote=$scope.items[_id].note;
            $('#formedit').modal('show');
        };
        $scope.add = function(_status) {
            $scope.editindex=null;
            $scope.coa="";
            if(_status){
                $scope.editjumlah=Math.abs($scope.debet-$scope.kredit);
                $scope.editdk=($scope.debet<$scope.kredit)?"D":"K";
            }else{
                $scope.editjumlah="";
                $scope.editdk="";
            }
            $scope.editnobukti="";
            $scope.editnote="";
            $('#formedit').modal('show');
        };
        $scope.hapus = function(_id) {
            $scope.items.splice(_id, 1);
            $scope.update();
        };
        $scope.update=function () {
            $scope.total=0;
            $scope.item_exist=false;
            for (var i in $scope.items) {
                $scope.item_exist=true;
                $scope.total=parseFloat($scope.total)+parseFloat($scope.items[i].jumlah);
            }
        };
    });
    $(function () {
        $('[name=kbg]').select2();
        $('.datepicker').datepicker({
            format: 'dd/mm/yyyy',
            autoclose: true,
            todayHighlight: true
        });
    });
</script>