<div class="row">
    <div class="col-sm-12">
        <div class="card-box">
            <?php
            include VIEWPATH.'alert.php';
                ?>
            <a href="?i=true" class="<?= is_authority(@$access['c']) ?> btn btn-inverse fa fa-plus"></a>
            <a href="?" class="btn btn-primary fa fa-refresh"></a>
            <hr>
            <form method="get" class="row" action="<?=base_url("accounting/$jurnal")?>">
                <div class="col-md-6">
                    <div class="dataTables_wrapper form-inline">
                        <label>Filter: </label>
                        <div class="input-daterange input-group" id="date-range">
                            <input type="text" readonly class="form-control input-sm" name="start" onchange="this.form.submit()" value="<?=$start?>">
                            <span class="input-group-addon input-sm">~</span>
                            <input type="text" readonly class="form-control input-sm" name="end" onchange="this.form.submit()" value="<?=$end?>">
                        </div>
                        <select name="closed" onchange="this.form.submit()" class="form-control input-sm">
                            <option value="">Semua status</option>
                            <option value="f" <?=$closed=='t'?'selected':''?>>Status aktif</option>
                            <option value="t" <?=$closed=='f'?'selected':''?>>Status closed</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="dataTables_wrapper form-inline right">
                        <label class="hidden-xs">No. Bukti: &nbsp;</label><input type="search" name="nota" class="form-control input-sm" autocomplete="off" value="<?=$nota?>">
                    </div>
                </div>
            </form>
            <div class="table-responsive">
                <table class="table table-bordered table-striped table-hover">
                    <thead>
                    <tr>
                        <th class="center col-xs-1">#</th>
                        <th class="center col-xs-2 hand-cursor" onclick="_sort_by()">Tanggal</th>
                        <th class="center col-xs-2 hand-cursor" onclick="_sort_by('type')">No. Bukti</th>
                        <th class="center col-xs-2">Jumlah</th>
                        <th class="center col-xs-3">Note</th>
                        <th class="center col-xs-1">Status</th>
                        <th class="center col-xs-1">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $no=$page;
                    foreach($data as $g) {
                        ?>
                        <tr class="data<?=$g['id']?>">
                            <td scope="row" class="center"><?=++$no?></td>
                            <td class="center"><?=format_waktu($g['tanggal'])?></td>
                            <td class="center"><?=$g['nota']?></td>
                            <td class="right"><?=format_uang($g['jumlah'])?></td>
                            <td><?=$g['note']?></td>
                            <td class="center"><?= getLabelStatus($g['status'])?></td>
                            <td class="center">
                                <button onclick="rincian('<?=$g['id']?>','<?=$g['nota']?>')" class="btn btn-xs btn-block btn-inverse"><i class="fa fa-search"></i></button>
                                <?php
                                if(isset($access['u'])){
                                    ?>
                                    <a href="?e=<?=$g['id']?>" class="btn btn-xs btn-block btn-primary"><i class="fa fa-pencil"></i></a>
                                    <?php
                                }
                                ?>
                            </td>
                        </tr>
                        <?php
                    }
                    ?>
                    </tbody>
                </table>
            </div>
            <?=$pagination?>
        </div>
    </div>
</div>
<div id="detailjurnal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-full">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <table class="table table-bordered table-striped table-hover">
                    <thead>
                    <tr>
                        <th class="center">No</th>
                        <th class="center col-xs-3">COA</th>
                        <th class="center col-xs-2">Debet</th>
                        <th class="center col-xs-2">Kredit</th>
                        <th class="center col-xs-3">Note</th>
                        <th class="center col-xs-1">No. Bukti</th>
                        <th class="center col-xs-1">Nota Ref.</th>
                    </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<link href="<?=base_url()?>assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css"/>
<script src="<?=base_url()?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script>
    $('#date-range').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true,
        todayHighlight: true
    });
    function _sort_by(_id) {
        window.location="?<?=generate_uri('sort_by',null)?>&sort_by="+_id;
    }
    function rincian(_i,_nota) {
        $('h4.modal-title','#detailjurnal').html(_nota);
        $.post('<?=base_url('accounting/detailjurnal')?>',{d:_i},function (data,status) {
            $('tbody','#detailjurnal').html(data.data);
            $('#detailjurnal').modal('show');
        },'json');
    }
</script>