<div class="row">
    <div class="col-sm-12">
        <div class="card-box">
            <form method="post" class="row" action="?" enctype="multipart/form-data">
                <div class="col-md-6">
                    <div class="form-inline">
                        <input name="file" class="form-control input-sm" type="file">
                        <button class="btn btn-danger" type="submit"><i class="fa fa-upload"></i> SUBMIT</button>
                    </div>
                </div>
            </form>

        </div>
    </div>
</div>