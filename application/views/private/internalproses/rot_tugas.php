<style>
    .select2-selection.select2-selection--single {
        width: 300px;
    }

</style><div class="row">
    <div class="col-sm-12">
        <div class="card-box">
            <a href="?" class="btn btn-default"><i class="fa fa-refresh"></i></a>
            <hr>
            <form method="get" class="row" action="<?=base_url('internalproses/persetujuan_r')?>">
                <div class="col-md-6">
                    <div class="dataTables_wrapper form-inline right">
                        <label class="hidden-xs">Search: &nbsp;</label><input type="search" name="search" class="form-control input-sm" autocomplete="off" value="<?=@$search?>">
                    </div>
                </div>
            </form>
            <div class="table-responsive">
                <table class="table table-bordered table-striped table-hover">
                    <thead>
                    <tr>
                        <th class="center">#</th>
                        <th class="center col-xs-2">No SPT</th>
                        <th class="center col-xs-2">Tanggal SPT</th>
                        <th class="center col-xs-3">SPT Disusun Oleh</th>
                        <th class="center col-xs-3">SPT Disetujui Dalnis</th>
                        <th class="center col-xs-2">SPT Disetujui Irban</th>
                        </tr>
                    </thead>
                    <tbody>

                    <?php
                    $no=1;
                    foreach($tao as $r) {
                        ?>
                        <tr class="data">
                            <td class="center"><?=$no;?></td>
                            <td class="center"><?=$r['spt_no']?></td>
                            <td class=""><?=$r['spt_date']?></td>
                            <td class=""><?=$r['created_by']?> - <?=$r['created_at']?></td>
                            <td class=""><?=$r['dalnis']?> - <?=$r['app_sv1_tstamp']?></td>
                            <td class=""><?=$r['irban']?> - <?=$r['app_sv2_tstamp']?></td>
                        </tr>
                        <?php
                        $no++; }
                    ?>
                    </tbody>

                </table>
            </div>
        </div>
    </div>
</div>
<link href="<?=base_url()?>assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css"/>
<script src="<?=base_url()?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script>
</script>