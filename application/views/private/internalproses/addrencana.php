<div class="row" ng-app="emjesSA" ng-controller="emjesController" ng-init="init()">
    <div class="col-sm-12">
        <div class="card-box row">
            <?php
            show_alert();
            ?>
            <div class="col-xs-12">
                <form id="form-data" name="datauserForm" novalidate="" method="post" class="form-horizontal"
                      ng-submit="simpan(1)">
                    <div class="form-group">
                        <label class="col-md-3 control-label">Periode</label>
                        <div class="col-md-5">
                            <select class="form-control select2" name="header[periode]" id="periode" onchange="cek_jumlah_hari()" ng-model="periode" required>
                                <option value="">pilih periode</option>
                                <?php
                                foreach ($periode as $v):
                                    ?>
                                    <option value="<?= $v['id'] ?>"><?= "$v[nama]" ?></option>
                                <?php
                                endforeach;
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Nomor PKPT</label>
                        <div class="col-md-9" id="pkpts">
                            <select class="form-control select2" onchange="pkpts()" id="no_pkpt" ng-model="pkpts" name="header[pkpt_no]" required>
                                <option value="">pilih nomor PKPT</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Jenis audit</label>
                        <div class="col-md-5">
                            <select class="form-control select2" name="header[jenis]" id="jenis_a" ng-model="jenis" required>
                                <option value="">pilih jenis audit</option>
                                <?php
                                foreach ($jenis as $v):
                                    ?>
                                    <option value="<?= $v['id'] ?>"><?= "$v[kode] - $v[ket]" ?></option>
                                <?php
                                endforeach;
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Satuan kerja yang diaudit</label>
                        <div class="col-md-7">
                            <select class="form-control select2" name="header[satker]" ng-model="satker" required>
                                <option value="">pilih satuan kerja</option>
                                <?php
                                foreach ($satker as $v):
                                    ?>
                                    <option value="<?= $v['id'] ?>"><?= "$v[kode] - $v[ket]" ?></option>
                                <?php
                                endforeach;
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Judul SPT</label>
                        <div class="col-md-6">
                            <input type="text" name="header[judul]" autocomplete="off" ng-model="judul"
                                   placeholder="judul surat perintah tugas" required="" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Sasaran audit</label>
                        <div class="col-md-9">
                            <select class="select2 form-control select2-multiple" id="sasaran_a" name="sasaran[]" ng-model="sasaran"
                                    multiple="multiple" multiple data-placeholder="pilih sasaran audit" required>
                                <option value="">pilih sasaran audit</option>
                                <?php
                                foreach ($sasaran as $v):
                                    ?>
                                    <option value="<?= $v['id'] ?>"><?= "$v[kode] - $v[ket]" ?></option>
                                <?php
                                endforeach;
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Tujuan audit</label>
                        <div class="col-md-9">
                            <select class="select2 form-control select2-multiple" id="tujuan_a" name="tujuan[]" ng-model="tujuan"
                                    multiple="multiple" multiple data-placeholder="pilih tujuan audit" required>
                                <option value="">pilih tujuan audit</option>
                                <?php
                                foreach ($tujuan as $v):
                                    ?>
                                    <option value="<?= $v['id'] ?>"><?= "$v[kode] - $v[ket]" ?></option>
                                <?php
                                endforeach;
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Jangka waktu audit</label>
                        <div class="col-md-9">
                            <div class="col-md-4">
                                <div class="input-daterange input-group" id="jangkawaktu">
                                    <input type="text" readonly class="form-control input-sm" onchange="cek_jumlah_hari()" id="start_date" name="header[start_date]"
                                           value="<?= date('d/m/Y') ?>">
                                    <span class="input-group-addon input-sm">~</span>
                                    <input type="text" readonly class="form-control input-sm" onchange="cek_jumlah_hari()" id="end_date" name="header[end_date]"
                                           value="<?= date('d/m/Y') ?>">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <span class="col-md-4 text-muted font-17 pd-025" id="jangkawaktuday"></span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Tim Pelaksana</label>
                        <div class="col-md-7">
                            <select class="form-control select2" ng-model="pilihpelaksana"
                                    ng-change="tambahpelaksana()">
                                <option value="">pilih tim</option>
                                <?php
                                $temp_tim = "";
                                foreach ($tim as $i => $v):
                                    if ($v['nama'] == null)
                                        continue;
                                    if ($v['id_tim'] <> $temp_tim) {
                                        if ($i > 0) {
                                            echo "</optgroup>";
                                        }
                                        echo "<optgroup label='$v[wilayah] - $v[tim]'>";
                                        $temp_tim = $v['id_tim'];
                                    }
                                    ?>
                                    <option value="<?= $v['id'] ?>"><?= "$v[nama] - $v[jabatan]" ?></option>
                                <?php
                                endforeach;
                                echo "</optgroup>";
                                ?>
                            </select>
                        </div>
                        <div class="col-md-2 hidden">
                            <button type="button" ng-click="tambahpelaksana()" class="btn btn-sm btn-inverse"><i
                                        class="fa fa-plus"></i></button>
                        </div>
                        <div class="col-md-offset-3 col-md-9">
                            <ul class="list-group">
                                <li class="list-group-item" ng-repeat="(timkey, timitem) in listtim | filter:filtertim"
                                    ng-dblClick="hapuspelaksana(timitem.id)">
                                    <div class="row hand-cursor">
                                        <div class="col-md-1">
                                            <button class="btn btn-xs btn-danger" ng-click="hapuspelaksana(timitem.id)">
                                                <i class="fa fa-remove"></i></button>
                                        </div>
                                        <div class="col-md-10">
                                            {{timitem.nama}}
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label">Ketua Tim</label>
                        <div class="col-md-7">
                            <select class="form-control select2" name="header[ketuatim]" ng-model="ketuatim">
                                <option value="">pilih ketua tim</option>
                                <option  ng-repeat="(timkey, timitem) in listtim | filter:filtertim" value="{{timitem.id}}">{{timitem.nama}}</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Dalnis</label>
                        <div class="col-md-7">
                            <select class="form-control select2" name="header[dalnis]"  ng-model="dalnis">
                                <option value="">pilih dalnis</option>
                                <option  ng-repeat="(timkey, timitem) in listtim | filter:filtertim" value="{{timitem.id}}">{{timitem.nama}}</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Irban</label>
                        <div class="col-md-7">
                            <select class="form-control select2" name="header[irban]" ng-model="irban">
                                <option value="">pilih irban</option>
                                <option  ng-repeat="(timkey, timitem) in listtim | filter:filtertim" value="{{timitem.id}}">{{timitem.nama}}</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Peraturan yang terkait</label>
                        <div class="col-md-9">
                            <span ng-hide="jenis && sasaran" class="col-md-4 text-muted font-17 pd-025">Peraturan yang terkait</span>
                            <table class="table table-striped">
                                <tbody>
                                <tr ng-repeat="(key, item) in aturan | matchFilter:'jenis':jenis | arrayFilter:'sasaran':sasaran"
                                    ng-click="pilihaturan(key)" ng-class="item.checked?'selected':'unselect'"
                                    class="selection">
                                    <td class="">{{item.kode}}</td>
                                    <td class="right">
                                        <a href="<?= base_url("img") . "/" ?>{{item.file}}"
                                           class="btn btn-sm btn-success" target="_blank"><i
                                                    class="fa fa-file-text"></i></a>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Pedoman audit</label>
                        <div class="col-md-9">
                            <span ng-hide="jenis && sasaran" class="col-md-4 text-muted font-17 pd-025">Pedoman audit</span>
                            <table class="table table-striped">
                                <tbody>
                                <tr ng-repeat="(key, item) in pedoman | matchFilter:'jenis':jenis | arrayFilter:'sasaran':sasaran"
                                    ng-click="pilihpedoman(key)" ng-class="item.checked?'selected':'unselect'"
                                    class="selection">
                                    <td class="">{{item.kode}}</td>
                                    <td class="right">
                                        <a href="<?= base_url("img") . "/" ?>{{item.file}}"
                                           class="btn btn-sm btn-success" target="_blank"><i
                                                    class="fa fa-file-text"></i></a>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Audit program</label>
                        <div class="col-md-9">
                            <span ng-hide="jenis && sasaran" class="col-md-4 text-muted font-17 pd-025">Audit program</span>
                            <table class="table table-striped" ng-show="jenis && sasaran">
                                <?php
                                foreach ($tahapan as $t):
                                    ?>
                                    <thead>
                                    <th class="center" colspan="4"><?= $t['tahapan'] ?></th>
                                    </thead>
                                    <tbody>
                                    <tr ng-repeat="(key, item) in tao | filter:{tahapan:'<?= $t['kode'] ?>'} | matchFilter:'jenis':jenis | arrayFilter:'sasaran':sasaran | arrayFilter:'tujuan':tujuan"
                                        ng-class="item.pelaksana && item.pelaksanaan?'selected':'unselect'"
                                        class="selection" tao-repeat-directive>
                                        <td class="col-xs-4">{{item.kode_kk}} {{item.langkah}}</td>
                                        <td class="col-xs-4">
                                            <select class="select2 form-control" ng-model="item.pelaksana">
                                                <option value="">pilih pelaksana</option>
                                                <option ng-repeat="(timkey, timitem) in listtim | filter:filtertim"
                                                        value="{{timitem.id}}">{{timitem.nama}}
                                                </option>
                                            </select>
                                        </td>
                                        <td class="col-xs-2">
                                            <input type="text" ng-model="item.pelaksanaan" placeholder="tanggal"
                                                   value="" readonly="" class="form-control datepicker">
                                        </td>
                                        <td class="col-xs-1">
                                            <input type="number" ng-model="item.jumlah" ng-change="hitung_hari()" placeholder="jumlah"
                                                   value="" class="form-control jumlah_hari">
                                        </td>
                                        <td class="right">
                                            <button type="button" ng-click="detailaturan(item)"
                                                    class="btn btn-sm btn-success"><i class="fa fa-search"></i></button>
                                        </td>
                                    </tr>
                                    </tbody>
                                <?php
                                endforeach;
                                ?>
                            </table>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Total Hari</label>
                        <div class="col-md-4">
                            <input type="text" name="header[jml_hari]" ng-model="total_hari" autocomplete="off"
                                   placeholder="Total Hari" readonly required="" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Nomor SPT</label>
                        <div class="col-md-4">
                            <input type="text" name="header[spt_no]" ng-model="spt_no" autocomplete="off"
                                   placeholder="nomor surat perintah tugas" required="" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Tanggal SPT</label>
                        <div class="col-md-4">
                            <input type="text" name="header[spt_date]" readonly autocomplete="off"
                                   placeholder="tanggal surat perintah tugas" required="" value="<?= date('d/m/Y') ?>"
                                   class="form-control datepicker">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-offset-3 col-md-4">
                            <input type="submit" class="hidden">
                            <button type="button" ng-disabled="datauserForm.$invalid" ng-click="simpan(0)"
                                    class="btn btn-inverse"><i class="fa fa-check"></i> SUBMIT
                            </button>
                            <button type="button" ng-click="simpan(1)" class="btn btn-success"><i
                                        class="fa fa-file-text"></i> DRAFT
                            </button>
                            <a href="<?= base_url('internalproses/rencana') ?>" class="btn btn-default"><i
                                        class="fa fa-backward"></i> KEMBALI</a>
                        </div>
                    </div>
                </form>
            </div><!-- end col-->
        </div>
    </div>
</div>
<div class="modal fade" id="taoModal" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-large" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
            </div>
        </div>
    </div>
</div>
<link href="<?= base_url() ?>assets/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css"/>
<link href="<?= base_url() ?>assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet"
      type="text/css"/>
<script src="<?= base_url() ?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/plugins/select2/js/select2.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/plugins/parsleyjs/parsley.min.js"></script>
<script src="<?= base_url() ?>assets/js/angular.min.js"></script>
<script src="<?= base_url() ?>assets/js/ng-messages.min.js"></script>
<script type="text/javascript">
    const component = this;

    function pkpts(){
        var no= $('#no_pkpt').val();
        $.post('?no_pkpt='+no, function (data, status) {
            if(data.tujuan!=null){
                $("#jenis_a").val(data.jenis);
                $("#jenis_a").trigger("change");
                $("#tujuan_a").val(data.tujuan);
                $("#tujuan_a").trigger("change");
                $("#sasaran_a").val(data.sasaran); //set the value
                $("#sasaran_a").trigger("change");
            }
        },'json');

        angular.scope().cek_pkpts();
    }
    function jangkawaktuaudit() {
        var _sd = $('input[name="header[start_date]"]').datepicker("getDate");
        var _ed = $('input[name="header[end_date]"]').datepicker("getDate");
        var timeDiff = Math.abs(_ed.getTime() - _sd.getTime());
        var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
        $('span#jangkawaktuday').html(diffDays + ' Hari');
    }
    function cek_jumlah_hari(){
        var periode= $('#periode').val();
        var periode_nama= $('option:selected','#periode').text();
        var start= $('#start_date').val();
        var end= $('#end_date').val();

        $.post('?periode=' +periode+'&a='+start+'&e='+end, function (data, status) {
            $('#jangkawaktuday').empty();
            $('#jangkawaktuday').html(data);
        });
        var _periode=$('select[name="header[pkpt_no]"]').val();
        $.post('?periodes=' +periode_nama, function (data, status) {
            $('select[name="header[pkpt_no]"]').html(data);
            $('select[name="header[pkpt_no]"]').val(_periode);
            $('.select2').select2();
        });
    }
    $(function () {
        $('.select2').select2();
        cek_jumlah_hari();
        $('.datepicker').datepicker({
            format: 'dd/mm/yyyy',
            autoclose: true,
            todayHighlight: true
        });
        $('#jangkawaktu').datepicker({
            format: 'dd/mm/yyyy',
            autoclose: true,
            todayHighlight: true
        }).on("change", function (e) {
            jangkawaktuaudit();
        });
        jangkawaktuaudit();
    });
    var app = angular.module("emjesSA", ['ngMessages']);
    app.filter("arrayFilter", function () {
        return function (items, idx, filterarray) {
            var filtered = [];
            angular.forEach(items, function (value, key) {
                angular.forEach(filterarray, function (v, k) {
                    if (value[idx] == v) {
                        filtered.push(value);
                    }
                });
            });
            return filtered;
        }
    });
    app.filter("matchFilter", function () {
        return function (items, idx, filterarray) {
            var filtered = [];
            angular.forEach(items, function (value, key) {
                if (value[idx] == filterarray) {
                    filtered.push(value);
                }
            });
            return filtered;
        }
    });
    app.directive('taoRepeatDirective', function () {
        return function (scope, element, attrs) {
            if (scope.$last) {
                $('.select2').select2();
                $('.datepicker').datepicker({
                    format: 'dd/mm/yyyy',
                    autoclose: true,
                    todayHighlight: true
                });
            }
        };
    });
    app.directive('validNumber', function () {
        return {
            require: '?ngModel',
            link: function (scope, element, attrs, ngModelCtrl) {
                if (!ngModelCtrl) {
                    return;
                }
                ngModelCtrl.$parsers.push(function (val) {
                    if (angular.isUndefined(val)) {
                        var val = '';
                    }
                    var clean = val.replace(/[^-0-9\.]/g, '');
                    var decimalCheck = clean.split('.');
                    if (!angular.isUndefined(decimalCheck[1])) {
                        decimalCheck[1] = decimalCheck[1].slice(0, 2);
                        clean = decimalCheck[0] + '.' + decimalCheck[1];
                    }
                    if (val !== clean) {
                        ngModelCtrl.$setViewValue(clean);
                        ngModelCtrl.$render();
                    }
                    return clean;
                });
                element.bind('keypress', function (event) {
                    if (event.keyCode === 32) {
                        event.preventDefault();
                    }
                });
            }
        };
    });
    app.controller('emjesController', function ($scope, $http) {
        $scope.filtertim = function (item) {
            if ($scope.timpelaksana.indexOf(item.id) > -1) {
                return item;
            }
        };
        $scope.aturan = [];
        $scope.pedoman = [];
        $scope.pilihpelaksana = '';
        $scope.timpelaksana = [];
        $scope.listtim = [];
        $scope.init = function () {
            $scope.aturan =<?=json_encode($aturan)?>;
            $scope.pedoman =<?=json_encode($pedoman)?>;
            $scope.tao =<?=json_encode($tao)?>;
            $scope.listtim =<?=json_encode($listtim)?>;
        };
        $scope.pilihaturan = function (_i) {
            $scope.aturan[_i].checked = !$scope.aturan[_i].checked;
        };
        $scope.tambahpelaksana = function () {
            if ($scope.timpelaksana.indexOf($scope.pilihpelaksana) < 0 && $scope.pilihpelaksana != "") {
                $scope.timpelaksana.push($scope.pilihpelaksana);
            }
            $scope.pilihpelaksana = "";
        };
        $scope.hitung_hari = function () {
            $scope.total_hari=0;
            $('.jumlah_hari').each(function() {
                var n = parseFloat($(this).val());
                if (!isNaN(n))
                    $scope.total_hari += n;
                if (!$scope.$$phase) $scope.$apply();
            });
        }
        $scope.hapuspelaksana = function (_id) {
            var _index = $scope.timpelaksana.indexOf(_id);
            $scope.timpelaksana.splice(_index);
        };
        $scope.pilihpedoman = function (_i) {
            $scope.pedoman[_i].checked = !$scope.pedoman[_i].checked;
        };
        $scope.detailaturan = function (_tao) {
            $('.modal-title', '#taoModal').html(_tao.kode_kk + ' ' + _tao.langkah);
            $.post('?tao=' + _tao.id, function (data, status) {
                $('.modal-body', '#taoModal').html(data);
                $('#taoModal').modal('show');
            });
        };
        $scope.periode='<?=$per_no['id']?>';
        $scope.simpan = function (_draft) {
            if (_draft == 0) {
                if ($scope.timpelaksana.length == 0) {
                    return;
                }
            }
            // if ($scope.pkpts !='') {
            //         alert('PKPT BELUM TERISI');
            //     }
            $('#preloader').show();
            $('#status', '#preloader').show();
            $scope.form = $('#form-data').serializeArray();
            $http({
                url: "?save=true",
                data: {
                    draft: _draft,
                    form: $scope.form,
                    aturan: $scope.aturan,
                    pedoman: $scope.pedoman,
                    tim: $scope.timpelaksana,
                    tao: $scope.tao
                },
                method: 'POST',
                headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
            }).success(function (data) {
                if (data === '200') {
                    window.location.assign('?');
                }
                setTimeout(function () {
                    $('#preloader').hide();
                    $('#status', '#preloader').hide();
                }, 2000);
                console.log("OK", data)
            }).error(function (err) {
                "ERR", console.log(err)
            });
        };

        $scope.cek_pkpts = function () {

            var no= $('#no_pkpt').val();
            $http({
                url: "?no_pkpt="+no,
                method: 'POST',
                headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
            }).success(function (data) {
                $scope.listtim =JSON.parse(data.tim);
                console.log($scope.listtim);
            });
        };
    });
</script>