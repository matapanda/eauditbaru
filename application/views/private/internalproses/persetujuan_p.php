<div class="row">
    <div class="col-sm-12">
        <div class="card-box">
            <a href="?" class="btn btn-default"><i class="fa fa-refresh"></i></a>
            <hr>
            <div class="table-responsive">
                <table class="table table-bordered table-striped table-hover">
                    <thead>
                    <tr>
                        <th class="center">#</th>
                        <th class="center col-xs-2">Tanggal SPT</th>
                        <th class="center col-xs-2">Nomor SPT</th>
                        <th class="center col-xs-3">Judul SPT</th>
                        <th class="center col-xs-4">Progress</th>
                        <th class="center"></th>
                    </tr>
                    </thead>
                    <tbody>

                    <?php
                    $no=1;
                    foreach($rencana as $r) {
                        $progress_disetujui=0;
                        if($r['disetujui']>0){
                            $progress_disetujui=($r['disetujui']/$r['total']*100);
                        }
                        $r['realisasi']=$r['realisasi']-$r['disetujui'];
                        $progress_realisasi=0;
                        if($r['realisasi']>0){
                            $progress_realisasi=($r['realisasi']/$r['total']*100);
                        }
                        ?>
                        <tr class="data<?=$r['id']?>">
                            <td class="center"><?=$no;?></td>
                            <td class="center"><?=format_waktu($r['spt_date'])?></td>
                            <td class="center"><?=$r['spt_no']?></td>
                            <td class=""><?=$r['judul']?></td>
                            <td class="">
                                <div class="progress">
                                    <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="<?=$r['disetujui']?>" aria-valuemin="0" aria-valuemax="<?=$r['total']?>" style="width: <?=$progress_disetujui?>%">
                                        <span class="sr-only"><?=$progress_disetujui?>% disetujui</span>
                                    </div>
                                    <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="<?=$r['realisasi']?>" aria-valuemin="0" aria-valuemax="<?=$r['total']?>" style="width: <?=$progress_realisasi?>%">
                                        <span class="sr-only"><?=$progress_realisasi?>% realisasi</span>
                                    </div>
                                </div>
                            </td>
                            <td class="center">
                                    <a href="?v=<?=$r['id']?>" class="btn btn-sm btn-default"><i class="fa fa-search"></i></a>

                            </td>
                        </tr>
                        <?php
                        $no++; }
                    ?>
                    </tbody>

                </table>
            </div>
        </div>
    </div>
</div>
<link href="<?=base_url()?>assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css"/>
<script src="<?=base_url()?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script>
    function hapus(_i) {
        swal({
            title: "Apakah anda yakin?",
            text: "data yang terhapus tidak dapat dikembalikan!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD5555",
            confirmButtonText: "Lanjutkan!",
            cancelButtonText: "Batalkan!",
            closeOnConfirm: true,
            closeOnCancel: true
        }, function (isConfirm) {
            if (isConfirm) {
                $.post('?',{delete:_i},function () {
                    $('.data'+_i).remove();
                });
            }
        });
    }
    $('#date-range').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true,
        todayHighlight: true
    });
    function rincian(_i,_r) {
        $.post('?',{ri:_i,rn:_r},function (data,status) {
            $('h4.modal-title','#detailpenerimaan').html("Detail");
            $('.modal-body','#detailpenerimaan').html(data.data);
            $('#detailpenerimaan').modal('show');
        },'json');
    }
    function setStatusActive(_i) {
        $('.'+_i).html('<img src="<?=base_url('assets/loading.gif')?>">');
        $.post('?',{status:_i},function (data,status) {
            $('.'+_i).html(data);
        });
    }
</script>