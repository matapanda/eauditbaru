<div class="row" ng-app="emjesSA" ng-controller="emjesController" ng-init="init()">
    <div class="col-sm-12">
        <div class="card-box row">
            <?php
            show_alert();
            ?>
            <div class="col-xs-12">
                <div id="form-data" name="datauserForm" novalidate="" method="post" class="form-horizontal" ng-submit="simpan(1)">
                    <div class="form-group">
                        <label class="col-md-3 control-label">Nomor PKPT</label>
                        <div class="col-md-7" style="padding: .5em">
                            <?php
                            foreach ($pkpt as $v):
                                if($v['id']!=$rencana['pkpt_no'])
                                    continue;
                                echo "$v[no] - $v[tema]";
                            endforeach;
                            ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Jenis audit</label>
                        <div class="col-md-7" style="padding: .5em">
                                <?php
                                foreach ($jenis as $v):
                                    if($v['id']!=$rencana['jenis'])
                                        continue;
                                    echo "$v[kode] - $v[ket]";
                                endforeach;
                                ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Satuan kerja yang diaudit</label>
                        <div class="col-md-7" style="padding: .5em">
                                <?php
                                foreach ($satker as $v):
                                    if($v['id']!=$rencana['satker'])
                                        continue;
                                    echo "$v[kode] - $v[ket]";
                                endforeach;
                                ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Judul SPT</label>
                        <div class="col-md-7" style="padding: .5em">
                            {{judul}}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Sasaran audit</label>
                        <div class="col-md-9">
                            <div class="col-md-7" style="padding: .5em">
                                <?php
                                $raw=json_decode($rencana['sasaran'],TRUE);
                                foreach ($sasaran as $v):
                                    if(!in_array($v['id'],$raw))
                                        continue;
                                    echo "<li>$v[kode] - $v[ket]</li>";
                                endforeach;
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Tujuan audit</label>
                        <div class="col-md-9">
                            <div class="col-md-7" style="padding: .5em">
                                <?php
                                $raw=json_decode($rencana['tujuan'],TRUE);
                                foreach ($tujuan as $v):
                                    if(!in_array($v['id'],$raw))
                                        continue;
                                    echo "<li>$v[kode] - $v[ket]</li>";
                                endforeach;
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Jangka waktu audit</label>
                        <div class="col-md-7" style="padding: .5em">
                                <div class="input-daterange input-group" id="jangkawaktu">
                                    <?=format_waktu($rencana['start_date'])?> ~ <?=format_waktu($rencana['end_date'])?> ( <?=floor((strtotime($rencana['end_date'])-strtotime($rencana['start_date'])) / (60 * 60 * 24))?> hari )
                                </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Tim Pelaksana</label>
                        <div class="col-md-7" style="padding: .5em">
                                <li ng-repeat="(timkey, timitem) in listtim | filter:filtertim">
                                    {{timitem.nama}}
                                </li>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Peraturan yang terkait</label>
                        <div class="col-md-9">
                            <span ng-hide="jenis && sasaran" class="col-md-4 text-muted font-17 pd-025">pilih jenis dan sasaran</span>
                            <table class="table table-striped">
                                <tbody>
                                <tr ng-repeat="(key, item) in aturan | matchFilter:'jenis':jenis | arrayFilter:'sasaran':sasaran" ng-click="pilihaturan(key)" ng-class="item.checked?'selected':'unselect'" class="selection">
                                    <td class="">{{item.kode}}</td>
                                    <td class="right">
                                        <a href="<?=base_url("img")."/"?>{{item.file}}" class="btn btn-sm btn-success" target="_blank"><i class="fa fa-file-text"></i></a>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Pedoman audit</label>
                        <div class="col-md-9">
                            <span ng-hide="jenis && sasaran" class="col-md-4 text-muted font-17 pd-025">pilih jenis dan sasaran</span>
                            <table class="table table-striped">
                                <tbody>
                                <tr ng-repeat="(key, item) in pedoman | matchFilter:'jenis':jenis | arrayFilter:'sasaran':sasaran" ng-click="pilihpedoman(key)" ng-class="item.checked?'selected':'unselect'" class="selection">
                                    <td class="">{{item.kode}}</td>
                                    <td class="right">
                                        <a href="<?=base_url("img")."/"?>{{item.file}}" class="btn btn-sm btn-success" target="_blank"><i class="fa fa-file-text"></i></a>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Audit program</label>
                        <div class="col-md-12">
                            <span ng-hide="jenis && sasaran" class="col-md-4 text-muted font-17 pd-025">pilih jenis dan sasaran</span>
                            <table class="table table-striped" ng-show="jenis && sasaran">
                                <?php
                                foreach($tahapan as $t):
                                    ?>
                                    <thead><th class="center" colspan="4"><?=$t['tahapan']?></th></thead>
                                    <tbody>
                                    <tr ng-repeat="(key, item) in tao | filter:{tahapan:'<?=$t['kode']?>'} | matchFilter:'jenis':jenis | arrayFilter:'sasaran':sasaran | arrayFilter:'tujuan':tujuan" ng-class="item.pelaksana && item.pelaksanaan?'selected':'unselect'" class="selection" tao-repeat-directive>
                                        <td class="col-xs-4">{{item.kode_kk}} {{item.langkah}}</td>
                                        <td class="col-xs-4">
                                            <select class="form-control" ng-model="item.pelaksana" disabled>
                                                <option value="">-</option>
                                                <option  ng-repeat="(timkey, timitem) in listtim | filter:filtertim" value="{{timitem.id}}">{{timitem.nama}}</option>
                                            </select>
                                        </td>
                                        <td class="col-xs-2">
                                            <input type="text" ng-model="item.pelaksanaan" placeholder="tanggal" ng-disabled="" value="" readonly="" class="form-control center">
                                        </td>
                                        <td class="col-xs-2">
                                            <input type="text" ng-model="item.jumlah" placeholder="jumlah" ng-disabled="" value="" readonly="" class="form-control center">
                                        </td>
                                        <td class="right">
                                            <button type="button" ng-click="detailaturan(item)" class="btn btn-sm btn-success"><i class="fa fa-search"></i></button>
                                        </td>
                                    </tr>
                                    </tbody>
                                    <?php
                                endforeach;
                                ?>
                            </table>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Nomor SPT</label>
                        <div class="col-md-7" style="padding: .5em">
                            <?=($rencana['spt_no'])?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Tanggal SPT</label>
                        <div class="col-md-7" style="padding: .5em">
                            <?=format_waktu($rencana['spt_date'])?>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-offset-3 col-md-4">
                            <a href="<?=base_url('internalproses/rencana')?>" class="btn btn-default"><i class="fa fa-backward"></i> KEMBALI</a>
                        </div>
                    </div>
                </div>
            </div><!-- end col-->
        </div>
    </div>
</div>
<div class="modal fade" id="taoModal" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-large" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
            </div>
        </div>
    </div>
</div>
<link href="<?=base_url()?>assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css"/>
<script src="<?=base_url()?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/plugins/parsleyjs/parsley.min.js"></script>
<script src="<?=base_url()?>assets/js/angular.min.js"></script>
<script src="<?=base_url()?>assets/js/ng-messages.min.js"></script>
<script type="text/javascript">
    function jangkawaktuaudit(){
        var _sd=$('input[name="header[start_date]"]').datepicker( "getDate" );
        var _ed=$('input[name="header[end_date]"]').datepicker( "getDate" );
        var timeDiff = Math.abs(_ed.getTime() - _sd.getTime());
        var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
        $('span#jangkawaktuday').html(diffDays+' Hari');
    }
    $(function () {
        jangkawaktuaudit();
    });
    var app = angular.module("emjesSA", ['ngMessages']);
    app.filter("arrayFilter",function() {
        return function (items, idx, filterarray) {
            var filtered = [];
            angular.forEach(items, function (value, key) {
                angular.forEach(filterarray, function (v, k) {
                    if (value[idx] == v){
                        filtered.push(value);
                    }
                });
            });
            return filtered;
        }
    });
    app.filter("matchFilter",function() {
        return function (items, idx, filterarray) {
            var filtered = [];
            angular.forEach(items, function (value, key) {
                if (value[idx] == filterarray){
                    filtered.push(value);
                }
            });
            return filtered;
        }
    });
    app.directive('validNumber', function() {
        return {
            require: '?ngModel',
            link: function(scope, element, attrs, ngModelCtrl) {
                if(!ngModelCtrl) {
                    return;
                }
                ngModelCtrl.$parsers.push(function(val) {
                    if (angular.isUndefined(val)) {
                        var val = '';
                    }
                    var clean = val.replace(/[^-0-9\.]/g, '');
                    var decimalCheck = clean.split('.');
                    if(!angular.isUndefined(decimalCheck[1])) {
                        decimalCheck[1] = decimalCheck[1].slice(0,2);
                        clean =decimalCheck[0] + '.' + decimalCheck[1];
                    }
                    if (val !== clean) {
                        ngModelCtrl.$setViewValue(clean);
                        ngModelCtrl.$render();
                    }
                    return clean;
                });
                element.bind('keypress', function(event) {
                    if(event.keyCode === 32) {
                        event.preventDefault();
                    }
                });
            }
        };
    });
    app.controller('emjesController', function($scope,$http) {
        $scope.filtertim = function(item) {
            if ($scope.timpelaksana.indexOf(item.id) > -1) {
                return item;
            }
        };
        $scope.aturan=[];
        $scope.pedoman=[];
        $scope.pilihpelaksana='';
        $scope.timpelaksana=<?=$rencana['tim']?>;
        $scope.listtim=[];
        $scope.init = function(){
            $scope.sasaran=<?=strlen($rencana['sasaran'])>1?$rencana['sasaran']:"''"?>;
            $scope.tujuan=<?=strlen($rencana['tujuan'])>1?$rencana['tujuan']:"''"?>;
            $scope.aturan=<?=json_encode($aturan)?>;
            $scope.pedoman=<?=json_encode($pedoman)?>;
            $scope.tao=<?=json_encode($tao)?>;
            $scope.listtim=<?=json_encode($listtim)?>;
            $scope.jenis='<?=$rencana['jenis']?>';
            $scope.satker='<?=$rencana['satker']?>';
            $scope.judul='<?=$rencana['judul']?>';
            $scope.spt_no='<?=$rencana['spt_no']?>';
            $scope.start_date='<?=format_waktu($rencana['start_date'])?>';
            $scope.end_date='<?=format_waktu($rencana['end_date'])?>';
            $scope.spt_date='<?=format_waktu($rencana['spt_date'])?>';
        };
        $scope.detailaturan = function(_tao){
            $('.modal-title','#taoModal').html(_tao.kode_kk+' '+_tao.langkah);
            $.post('?tao='+_tao.id,function(data,status){
                $('.modal-body','#taoModal').html(data);
                $('#taoModal').modal('show');
            });
        };
    });
</script>