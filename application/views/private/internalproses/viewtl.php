<style>
    .v-mid{
        vertical-align: middle!important;
    }
    .ada-border{
        border: 1px solid;
    }
</style>
<div class="row" ng-app="emjesSA" ng-controller="emjesController" ng-init="init()">
    <div class="col-sm-12">
        <div class="card-box row">
            <?php
            show_alert();
            ?>
            <form method="post" enctype="multipart/form-data" class="form-horizontal">
                <div class="col-xs-12">
                    <?php
                    if(@$rencana['pkpt_no']!=''){
                        ?>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Nomor PKPT</label>
                            <div class="col-md-4">
                                <select class="form-control select2" disabled >
                                    <option value="">pilih nomor PKPT</option>
                                    <?php
                                    foreach ($pkpt as $v):
                                        $v=purify_token($v);
                                        ?>
                                        <option value="<?= $v['id'] ?>" <?=($rencana['pkpt_no']==$v['id'])?'selected=""':""?>><?= "$v[no] - $v[tema]" ?></option>
                                    <?php
                                    endforeach;
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Judul SPT</label>
                            <div class="col-md-6">
                                <input type="text" autocomplete="off" ng-model="judul" placeholder="judul surat perintah tugas" disabled class="form-control">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label">Nomor SPT</label>
                            <div class="col-md-4">
                                <input type="text" ng-model="spt_no" autocomplete="off" placeholder="nomor surat perintah tugas"  disabled="" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Tanggal SPT</label>
                            <div class="col-md-4">
                                <input type="text" readonly autocomplete="off" placeholder="tanggal surat perintah tugas" disabled="" value="<?=format_waktu($rencana['spt_date'])?>" class="form-control datepicker">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Tanggal LHP</label>
                            <div class="col-md-5">
                                <input type="text" disabled autocomplete="off" value="<?=@$rencana['tgl_lhp']?>" placeholder="Tanggal LHP" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Periode LHP</label>
                            <div class="col-md-5">
                                <input type="text" name="spt_date" autocomplete="off" disabled value="<?=@$rencana['thn_lhp']?$rencana['thn_lhp']:$rencana['thn_laporan']?>" placeholder="Periode LHP" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Judul SKPD</label>
                            <div class="col-md-5">
                                <input type="text" name="judul_spt" autocomplete="off" disabled value="<?=@$rencana['skpd']?>" placeholder="Judul SKPD" class="form-control">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label">Nomor LHP</label>
                            <div class="col-md-5">
                                <input type="text" name="spt_no" autocomplete="off" disabled value="<?=@$rencana['no_lhp']?$rencana['no_lhp']:$rencana['no_laporan']?>" placeholder="Nomor LHP"  class="form-control">
                              </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Tim Pelaksana</label>
                            <label class="col-md-7 control-label" ng-show="listtim.length<1" style="text-align: left!important;"> <?=@$tl['pelaksana']?></label>

                            <div class="col-md-6" ng-show="listtim.length>1">
                                <ul class="list-group" >
                                    <li class="list-group-item" ng-repeat="(timkey, timitem) in listtim | filter:filtertim">
                                        <div class="row hand-cursor">
                                            <!--                                        <div class="col-md-1">-->
                                            <!--                                            <button class="btn btn-xs btn-danger" ng-click="hapuspelaksana(timitem.id)"><i class="fa fa-remove"></i></button>-->
                                            <!--                                        </div>-->
                                            <div class="col-md-5">
                                                {{timitem.nama}}
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="form-group" ng-show="listtim.length>1">
                            <label class="col-md-3 control-label">Ketua Tim</label>
                            <label class="col-md-7 control-label" style="text-align: left!important;"> <?=@$ketuatim_nama?></label>
                        </div>
                        <div class="form-group" ng-show="listtim.length>1">
                            <label class="col-md-3 control-label">Dalnis</label>
                            <label class="col-md-7 control-label" style="text-align: left!important;"> <?=@$dalnis_nama?></label>
                            <!--                        <label class="col-md-7 control-label text-left"> --><?//=@$dalnis_nama?><!--</label>-->
                        </div>
                        <div class="form-group" ng-show="listtim.length>1">
                            <label class="col-md-3 control-label">Irban</label>
                            <label class="col-md-7 control-label" style="text-align: left!important;"> <?=@$irban_nama?></label>

                        </div>

                        <?php
                    }else{
                    ?><div class="form-group">
                        <label class="col-md-3 control-label">Tanggal LHP</label>
                        <div class="col-md-5">
                            <input type="text" disabled autocomplete="off" value="<?=@$rencana['tgl_lhp']?>" placeholder="Tanggal LHP" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Periode LHP</label>
                        <div class="col-md-5">
                            <input type="text" name="spt_date" autocomplete="off" disabled value="<?=@$rencana['thn_lhp']?$rencana['thn_lhp']:$rencana['thn_laporan']?>" placeholder="Periode LHP" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Judul SKPD</label>
                        <div class="col-md-5">
                            <input type="text" name="judul_spt" autocomplete="off" disabled value="<?=@$rencana['skpd']?>" placeholder="Judul SKPD" class="form-control">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label">Nomor LHP</label>
                        <div class="col-md-5">
                            <input type="text" name="spt_no" autocomplete="off" disabled value="<?=@$rencana['no_lhp']?$rencana['no_lhp']:$rencana['no_laporan']?>" placeholder="Nomor LHP"  class="form-control">
                            <input type="hidden" name="no_temuan" id="no_temuan" class="form-control" value="<?=@$jml_temuan?$jml_temuan:1?>">
                            <input type="hidden" name="id_rencana" id="id_rencana" value="<?=@$id_rencana?>">
                            <input type="hidden" name="id_tl" id="id_tl" value="<?=@$id_tindak?>">
                        </div>
                    </div>
                    <?php
                    if(@$rencana['pelaksana']){
                        ?>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Tim Pelaksana</label>
                            <div class="col-md-5">
                                <input type="text" name="pelaksana"  autocomplete="off" disabled value="<?=@$rencana['pelaksana']?>" placeholder="Tim Pelaksana"  class="form-control">
                            </div>
                        </div>
                        <?php
                    }else{
                        ?>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Tim Pelaksana</label>
                            <div class="col-md-6">
                                <ul class="list-group">
                                    <li class="list-group-item" ng-repeat="(timkey, timitem) in listtim | filter:filtertim">
                                        <div class="row hand-cursor">
                                            <div class="col-md-5">
                                                {{timitem.nama}}
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Ketua Tim</label>
                            <label class="col-md-7 control-label" style="text-align: left!important;"> <?=@$ketuatim_nama?></label>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Dalnis</label>
                            <label class="col-md-7 control-label" style="text-align: left!important;"> <?=@$dalnis_nama?></label>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Irban</label>
                            <label class="col-md-7 control-label" style="text-align: left!important;"> <?=@$irban_nama?></label>
                            <input type="hidden" name="no_temuan" id="no_temuan" class="form-control" value="1">
                        </div>
                        <?php
                    }
                    }
                    ?>

                    <div class="form-group">
                        <label class="col-md-3 control-label">Temuan & Tindak Lanjut</label>
                    </div>
                    <hr>
                    <div class="col-md-12 tambahan">

                        <?php
                        $no=1;
                        if(@$temuan_list) {
                            foreach($temuan_list as $t){
                                ?>
                                <div class="form-group <?=$no?>">
                                    <h3>TEMUAN</h3> <br>
                                    <div class="form-group">
                                        <label for="inputName" class="col-sm-3 form-control-label right">Judul Temuan<span
                                                    class="text-danger">*</span></label>
                                        <div class="col-sm-5">
                                            <input  type="text" disabled class="form-control" name="judul_tl<?=$no?>" placeholder="Judul Temuan" required value="<?=@$t['judul_temuan']?>">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputName" class="col-sm-3 form-control-label right">Kode Temuan<span class="text-danger">*</span></label>
                                        <div class="col-sm-5">
                                            <select name="kt<?=$no?>"  disabled class="form-control select2">
                                                <option value="">-</option>
                                                <?php
                                                foreach ($parent as $p){
                                                    echo "<option value='$p[id]' ".(@$t['kode_temuan']==$p['id']?'selected':'').">$p[kode] - $p[nama]</option>";
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="inputName" class="col-sm-3 form-control-label right">Uraian Temuan<span class="text-danger">*</span></label>
                                        <div class="col-sm-5">
                                            <input type="text"  disabled name="ut<?=@$no?>" autocomplete="off" placeholder="Temuan" value="<?=@$t['uraian_temuan']?>" required class="form-control">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputName" class="col-sm-3 form-control-label right">Nilai Temuan<span
                                                    class="text-danger">*</span></label>
                                        <div class="col-sm-5">
                                            <input  type="text" class="form-control" disabled name="nilai_tl<?=@$no?>" value="<?=@$t['nilai_temuan']?>" placeholder="Nilai Temuan" required>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="inputName" class="col-sm-3 form-control-label right">Kondisi<span
                                                    class="text-danger">*</span></label>
                                        <div class="col-sm-5">
                                            <textarea class="form-control"  disabled class="mytextarea" name="kondisi<?=@$no?>" placeholder="Kondisi" required><?=@$t['kondisi']?></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputName" class="col-sm-3 form-control-label right">Kriteria<span
                                                    class="text-danger">*</span></label>
                                        <div class="col-sm-5">
                                            <textarea class="form-control" disabled class="mytextarea" name="kriteria<?=@$no?>" placeholder="Kriteria" required><?=@$t['kriteria']?></textarea>
                                        </div>
                                    </div>
                                    <!--                                //rekom-->
                                    <h4>Rekomendasi Temuan</h4><br>

                                    <?php
                                    if(@$rekom_list) {
                                        foreach ($rekom_list as $t) {
                                            if ($no == $t['urutan']) {
                                                ?>
                                                <div class="form-group rekom<?= $no ?>">
                                                    <div class="form-group">
                                                        <label for="inputName"
                                                               class="col-sm-3 form-control-label right">Kode
                                                            Rekom<span class="text-danger">*</span></label>
                                                        <div class="col-sm-5">
                                                            <select name="kr<?= $no ?>[]" disabled
                                                                    class="form-control select2"
                                                                    data-placeholder="pilih kode rekom">
                                                                <option value="">-</option>
                                                                <?php
                                                                foreach ($parent2 as $p) {
                                                                    ?><option value='<?=$p['id']?>' <?=@$t['kode_rekom'] == $p['id'] ? 'selected' : '' ?>><?=$p['kode']?>-<?=$p['nama']?></option>";
                                                                    <?php
                                                                }
                                                                ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="inputName"
                                                               class="col-sm-3 form-control-label right">Uraian
                                                            Rekom<span class="text-danger">*</span></label>
                                                        <div class="col-sm-5">
                                                            <input type="text" name="uk<?= $no ?>[]" autocomplete="off"
                                                                   placeholder="Rekomendasi" disabled
                                                                   value="<?= @$t['uraian_rekom'] ?>"
                                                                   class="form-control">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="inputName"
                                                               class="col-sm-3 form-control-label right">No
                                                            Tindak Lanjut<span class="text-danger">*</span></label>
                                                        <div class="col-sm-5">
                                                            <select name="ntl<?= $no ?>[]" disabled
                                                                    class="form-control select2"
                                                                    data-placeholder="No Tindak Lanjut">
                                                                <option value="">-</option>
                                                                <?php
                                                                foreach ($tlb as $q) {
                                                                    ?>
                                                                    <option value="<?=$q['id']?>" <?=@$t['no_tl'] == $q['id'] ? 'selected' : ''?>><?=$q['kode']?> - <?=$q['nama']?></option>
                                                                    <?php
                                                                }
                                                                ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="inputName"
                                                               class="col-sm-3 form-control-label right">Uraian
                                                            Tindak Lanjut<span class="text-danger">*</span></label>
                                                        <div class="col-sm-5">

                                                            <input type="text" name="utl<?= $no ?>[]" autocomplete="off"
                                                                   placeholder="Uraian Tindak Lanjut" disabled
                                                                   value="<?= @$t['uraian_tl'] ?>"
                                                                   class="form-control">
                                                        </div>
                                                    </div>
                                                    <hr>
                                                </div>
                                                <?php
                                            }
                                        }
                                    }
                                    ?>

                                    <h4>Penyebab </h4><br>

                                    <?php
                                    if(@$penyebab_list) {
                                        foreach($penyebab_list as $t) {
                                            if($no==$t['urutan']){
                                                ?>
                                                <div class="form-group penyebab<?=$no?>">
                                                    <div class="form-group">
                                                        <label for="inputName" class="col-sm-3 form-control-label right">Kode Penyebab<span class="text-danger">*</span></label>
                                                        <div class="col-sm-5">
                                                            <select name="penyebab<?=$no?>[]" disabled class="form-control select2" data-placeholder="pilih kode penyebab" >
                                                                <option value="">-</option>
                                                                <?php
                                                                foreach ($penyebab as $p){
                                                                    ?>
                                                                    <option value='<?=$p['id']?>' <?=@$t['kode_penyebab']==$p['id']?'selected':''?>> <?=$p['kode']?> - <?=$p['nama']?></option>";
                                                                    <?php
                                                                }
                                                                ?>
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="form-group kn">
                                                        <label for="inputName" class="col-sm-3 form-control-label right">Uraian penyebab<span class="text-danger">*</span></label>
                                                        <div class="col-sm-5">
                                                            <textarea class="form-control" disabled class="mytextarea" name="up<?=$no?>[]" placeholder="Uraian penyebab" required><?=$t['uraian_penyebab']?></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <?php
                                            }
                                        }
                                    }
                                    ?>
                                    <hr> <h4>Akibat </h4><br>

                                    <?php
                                    if(@$akibat_list) {
                                        foreach($akibat_list as $t) {
                                            if($no==$t['urutan']){
                                                ?>
                                                <div class="form-group akibat<?=$no?>">
                                                    <div class="form-group">
                                                        <label for="inputName" class="col-sm-3 form-control-label right">Kode Akibat<span class="text-danger">*</span></label>
                                                        <div class="col-sm-5">
                                                            <select name="a<?=$no?>[]" disabled class="form-control select2" data-placeholder="pilih kode penyebab" >
                                                                <option value="">-</option>
                                                                <?php
                                                                foreach ($akibat as $p){
                                                                    ?>
                                                                    <option value='<?=$p['id']?>' <?=@$t['kode_akibat']==$p['id']?'selected':''?>> <?=$p['kode']?> - <?=$p['nama']?></option>";
                                                                    <?php
                                                                }
                                                                ?>
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="form-group kn">
                                                        <label for="inputName" class="col-sm-3 form-control-label right">Uraian akibat<span class="text-danger">*</span></label>
                                                        <div class="col-sm-5">
                                                            <textarea class="form-control" disabled class="mytextarea" name="ap<?=$no?>[]" placeholder="Uraian akibat" required><?=$t['uraian_akibat']?></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <?php
                                            }
                                        }
                                    }
                                    ?>
                                    <hr>
                                </div>
                                <?php

                                $no++;
                            }
                        }
                        ?>
                    </div>

            </form></div>

        <form method="post" action="?" enctype="multipart/form-data" class="form-horizontal">
        <div class="col-xs-12">
            <?php
            if(@$tl['dalnis_n']!=''){
                ?>
                <div class="form-group">
                    <label class="col-md-3 control-label">Direviu oleh </label>
                    <div class="col-md-7">

                            <?=@$tl['dalnis_n']?$tl['dalnis_n']:'-'?>

                    </div>
                </div>
                <?php
            }
            ?>

                    <div class="form-group">
                        <label class="col-md-3 control-label">Reviu Dalnis </label>
                        <div class="col-md-7">
                            <?php

//                            echo $tl['status'];
                            ?>
                            <?php
                            if(@$dalnis>0 && @$tl['proses']==0 ){
                                ?>
                                <input type="hidden" name="id_rencana" value="<?=$rencana['id']?>">

                                <div class="radio radio-success radio-inline">
                                    <input type="radio" id="inlineRadioDalnis1" value="t" name="persetujuanD" checked="">
                                    <label for="inlineRadioDalnis1"> Setuju </label>
                                </div>
                                <div class="radio radio-success radio-inline">
                                    <input type="radio" id="inlineRadioDalnis2" value="f" name="persetujuanD">
                                    <label for="inlineRadioDalnis2"> Tidak Setuju </label>
                                </div>
                                <input type="text" name="reviu1" required autocomplete="off" placeholder="Reviu Oleh Dalnis" class="form-control">
                                <?php
                            }elseif( $tl['app_sv1']!=''||@$dalnis<1){
                                ?>
                                <?=@$tl['app_sv1_note']?$tl['app_sv1_note']:'-'?>
                               <?php
                            }
                            ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">File Pendukung Dalnis</label>
                        <div class="col-md-7">
                            <?php
                            if(@$tl['app_sv1_file']!=null){
                                $kk = json_decode($tl['app_sv1_file'], TRUE);
                                foreach ($kk as $d):
                                ?>
                                    <a href="<?=base_url('img/'.$d['file'])?>" class="btn btn-xs btn-primary" target="_blank"><i class="fa fa-file-text"></i> <?=$d['nama']?></a>
                                    <?php
                                endforeach;
                            }elseif(@$dalnis>0&& @$tl['proses']==0){
                                ?>
                                <input type="file" name="file">
                                <?php
                            }else{
                                echo '-';
                            }
                            ?>

                        </div>
                    </div>

            <?php
            if(@$tl['irban_n']!='') {
                ?>
                <div class="form-group">
                    <label class="col-md-3 control-label">Direviu oleh </label>
                    <div class="col-md-7">
                        <?= @$tl['irban_n'] ? $tl['irban_n'] : '-' ?>
                    </div>
                </div>
                <?php
            }
            ?>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Reviu Irban </label>
                        <div class="col-md-7">
                            <?php
                            if(@$irban>0&& @$tl['proses']==1){
                                ?>

                                <input type="hidden" name="id_rencana" value="<?=$rencana['id']?>">
                                <div class="radio radio-success radio-inline">
                                    <input type="radio" id="inlineRadioIrban1" value="t" name="persetujuanI" checked="">
                                    <label for="inlineRadioIrban1"> Setuju </label>
                                </div>
                                <div class="radio radio-success radio-inline">
                                    <input type="radio" id="inlineRadioIrban2" value="f" name="persetujuanI">
                                    <label for="inlineRadioIrban2"> Tidak Setuju </label>
                                </div>
                                <input type="text" required name="reviu2" autocomplete="off" placeholder="Reviu Oleh Irban" class="form-control">
                                <?php
                            }
                            elseif($tl['app_sv2']!=''||@$irban<1){
                                    ?>
                                    <?=@$tl['app_sv2_note']?$tl['app_sv2_note']:'-'?>
                                    <?php
                                }else{
                                echo '-';
                            }
                            ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">File Pendukung Irban</label>
                        <div class="col-md-7">
                            <?php
                            if(@$tl['app_sv2_file']){
                                $kk2 = json_decode($tl['app_sv2_file'], TRUE);
                                foreach ($kk2 as $d):
                                    ?>
                                    <a href="<?=base_url('img/'.$d['file'])?>" class="btn btn-xs btn-primary" target="_blank"><i class="fa fa-file-text"></i> <?=$d['nama']?></a>
                                    <?php
                                endforeach;
                            }elseif(@$irban>0 && @$tl['proses']==1){
                                ?>
                                <input type="file" name="file">
                                <?php
                            }else{
                                echo '-';
                            }
                            ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-offset-3 col-md-4">
                            <?php
                            if(((@$irban>0 && @$tl['app_sv2']=='' && @$tl['app_sv1']) || ((@$dalnis>0 && @$tl['app_sv1']=='')) || @$tl['app_sv1_status']=='f'|| @$tl['app_sv2_status']=='f')){
                                ?>
<!--                            <button type="button"  ng-click="simpan()" class="btn btn-inverse"><i class="fa fa-check"></i> SUBMIT</button>-->
                                <input type="submit" class="btn btn-inverse" value="SUBMIT">
                                <?php
                            }
                            ?>

                            <!--                            <button type="button" ng-click="simpan(1)" class="btn btn-success"><i class="fa fa-file-text"></i> DRAFT</button>-->
                            <a href="<?=base_url('internalproses/tindak_lanjut')?>" class="btn btn-default"><i class="fa fa-backward"></i> KEMBALI</a>
                        </div>
                    </div>
            </div><!-- end col-->
                </form>
        </div>
    </div>

    <div class="modal fade none-border" id="tambahModal">
        <div class="modal-dialog modal-lg" style="overflow-y: scroll">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Tambah Kode Temuan</h4>
                </div>
                <div class="modal-body p-20" style="height:100%;" >
                    <form method="post" id="tambah_hari">
                        <div class="form-group row">
                            <div class="form-group row">
                                <label for="inputName" class="col-sm-3 form-control-label">Kode Temuan<span class="text-danger">*</span></label>
                                <div class="col-sm-9">
                                    <select name="kt" ng-model="kt" id="kt" required class="form-control select2">
                                        <option value="">-</option>
                                        <?php
                                        foreach ($parent as $p){
                                            echo "<option value='$p[id]' ".($p['id']==@$form['parent']?'selected=""':"")." data-posisi='$p[dk]'>$p[kode] - $p[nama]</option>";
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="inputName" class="col-sm-3 form-control-label">Uraian Temuan<span class="text-danger">*</span></label>
                                <div class="col-sm-9">
                                    <input type="text" name="ut" ng-model="ut" autocomplete="off" placeholder="Temuan" required value="<?=(@$rencana['rekom'])?>" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="form-group row">
                                <label for="inputName" class="col-sm-3 form-control-label">Kode Rekom<span class="text-danger">*</span></label>
                                <div class="col-sm-9">
                                    <select name="kr" ng-model="kr" id="kr" required class="form-control select2">
                                        <option value="">-</option>
                                        <?php
                                        foreach ($parent2 as $p){
                                            echo "<option value='$p[id]' ".($p['id']==@$form['parent']?'selected=""':"")." data-posisi='$p[dk]'>$p[kode] - $p[nama]</option>";
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="inputName" class="col-sm-3 form-control-label">Uraian Rekom<span
                                        class="text-danger">*</span></label>
                                <div class="col-sm-9">
                                    <textarea class="form-control" class="mytextarea" name="uk" ng-model="uk" placeholder="Rekomendasi" required><?=(@$rencana['rekom'])?></textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputName" class="col-sm-3 form-control-label">Negara/Daerah<span class="text-danger">*</span></label>
                                <div class="col-sm-9">
                                    <select name="nd" ng-model="nd" id="nd" required class="form-control select2">
                                        <option value="">-</option>
                                        <option value="1">Negara</option>
                                        <option value="2">Daerah</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputName" class="col-sm-3 form-control-label">Nilai Kerugian<span class="text-danger">*</span></label>
                                <div class="col-sm-9">
                                    <input type="number" name="nk" id="nk" ng-model="nk" autocomplete="off" placeholder="Nilai Kerugian" required value="<?=(@$temuan['kerugian'])?>" class="form-control">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputName" class="col-sm-3 form-control-label">Hasil Tindak Lanjut<span class="text-danger">*</span></label>
                                <div class="col-sm-9">
                                    <select name="htl" ng-model="htl" id="htl" required class="form-control select2">
                                        <option value="">-</option>
                                        <option value="1">Sesuai</option>
                                        <option value="2">Belum Sesuai</option>
                                        <option value="3">Belum Ditindaklanjuti</option>
                                        <option value="4">Tidak Dapat Ditindaklanjuti</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputName" class="col-sm-3 form-control-label">Nilai Tindak Lanjut<span class="text-danger">*</span></label>
                                <div class="col-sm-9">
                                    <input type="number" name="ntl" id="ntl" ng-model="ntl" autocomplete="off" placeholder="Nilai Kerugian" required value="<?=(@$temuan['ntl']?$temuan['ntl']:0)?>" class="form-control">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputName" class="col-sm-3 form-control-label">Keterangan</label>
                                <div class="col-sm-9">
                                    <input type="text" name="keterangan" id="keterangan" ng-model="keterangan" autocomplete="off" placeholder="Keterangan" required value="<?=(@$temuan['keterangan'])?>" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="form-group row">
                                <label for="inputName" class="col-sm-3 form-control-label">Data Umum Obrik<span class="text-danger">*</span></label>
                                <div class="col-sm-9">
                                    <textarea class="form-control" class="mytextarea" ng-model="obrik" name="obrik" placeholder="Data Umum Obrik" required><?=$rencana['obrik']?></textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputName" class="col-sm-3 form-control-label">Tujuan<span class="text-danger">*</span></label>
                                <div class="col-sm-9">
                                    <textarea class="form-control" class="mytextarea" ng-model="tujuan_tl" name="tujuan_tl" placeholder="Tujuan" required><?=$rencana['tujuan_tl']?></textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputName" class="col-sm-3 form-control-label">Sasaran<span class="text-danger">*</span></label>
                                <div class="col-sm-9">
                                    <textarea class="form-control" class="mytextarea" ng-model="sasaran_tl" name="sasaran_tl" placeholder="Sasaran" required><?=$rencana['tujuan_tl']?></textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputName" class="col-sm-3 form-control-label">Ruang Lingkup<span
                                        class="text-danger">*</span></label>
                                <div class="col-sm-9">
                                    <textarea class="form-control" class="mytextarea" name="ruang" ng-model="ruang" placeholder="Ruang Lingkup" required><?=$rencana['ruang']?></textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputName" class="col-sm-3 form-control-label">Evaluasi SPI<span
                                        class="text-danger">*</span></label>
                                <div class="col-sm-9">
                                    <textarea class="form-control" class="mytextarea" name="evaluasi" ng-model="evaluasi" placeholder="Evaluasi SPI" required><?=$rencana['evaluasi']?></textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputName" class="col-sm-3 form-control-label">Judul Temuan<span
                                        class="text-danger">*</span></label>
                                <div class="col-sm-9">
                                    <textarea class="form-control" class="mytextarea" name="judul_tl" ng-model="judul_tl" placeholder="Judul Temuan" required><?=$rencana['judul_tl']?></textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputName" class="col-sm-3 form-control-label">Kondisi<span
                                        class="text-danger">*</span></label>
                                <div class="col-sm-9">
                                    <textarea class="form-control" class="mytextarea" name="kondisi" ng-model="kondisi" placeholder="Kondisi" required><?=$rencana['kondisi']?></textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputName" class="col-sm-3 form-control-label">Kriteria<span
                                        class="text-danger">*</span></label>
                                <div class="col-sm-9">
                                    <textarea class="form-control" class="mytextarea" name="kriteria" ng-model="kriteria" placeholder="Kriteria" required><?=$rencana['kriteria']?></textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputName" class="col-sm-3 form-control-label">Penyebab<span
                                        class="text-danger">*</span></label>
                                <div class="col-sm-9">
                                    <textarea class="form-control" class="mytextarea" name="penyebab" ng-model="penyebab" placeholder="Penyebab" required><?=$rencana['penyebab']?></textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputName" class="col-sm-3 form-control-label">Akibat<span
                                        class="text-danger">*</span></label>
                                <div class="col-sm-9">
                                    <textarea class="form-control" class="mytextarea" name="akibat" ng-model="akibat" placeholder="Kriteria" required><?=$rencana['kriteria']?></textarea>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="inputName" class="col-sm-3 form-control-label">Tanggapan<span
                                        class="text-danger">*</span></label>
                                <div class="col-sm-9">
                                    <textarea class="form-control" class="mytextarea" name="tanggapan" ng-model="tanggapan" placeholder="Tanggapan" required><?=$rencana['tanggapan']?></textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputName" class="col-sm-3 form-control-label">Tindak Lanjut<span
                                        class="text-danger">*</span></label>
                                <div class="col-sm-9">
                                    <textarea class="form-control" class="mytextarea" name="tindak_lanjut" ng-model="tindak_lanjut" placeholder="Tindak Lanjut" required><?=$rencana['tindak_lanjut']?></textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputName" class="col-sm-3 form-control-label">Tindak Lanjut Sebelumnya<span
                                        class="text-danger">*</span></label>
                                <div class="col-sm-9">
                                    <textarea class="form-control" class="mytextarea" name="tindak_lanjut_s" ng-model="tindak_lanjut_s" placeholder="Tindak Lanjut Sebelumnya" required><?=$rencana['tindak_lanjut_s']?></textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputName" class="col-sm-12 center form-control-label text-danger" id="alert-kurang"></label>
                            </div>
                            <div class="col-sm-offset-3">
                                <button type="button" ng-click="tambah_temuan()" id="tambah" class="btn btn-success save-event waves-effect waves-light">Simpan</button>
                                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Batal</button>
                                <input type="submit" id="submit_tambah" class="hidden">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade none-border" id="detailModal">
        <div class="modal-dialog modal-lg" style="overflow-y: scroll">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Detail Temuan</h4>
                </div>
                <div class="modal-body p-20" style="height:100%;overflow-y: scroll" >
                    <div class="form-group row">

                        <div class="form-group row">
                            <div class="form-group row">
                                <label for="inputName" class="col-sm-3 form-control-label">Data Umum Obrik<span class="text-danger">*</span></label>
                                <div class="col-sm-9">
                                    <textarea class="form-control" class="mytextarea" readonly style="background-color: #d7d6d6;" ng-model="obrik_d" name="obrik" placeholder="Data Umum Obrik" required><?=$rencana['obrik']?></textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputName" class="col-sm-3 form-control-label">Tujuan<span class="text-danger">*</span></label>
                                <div class="col-sm-9">
                                    <textarea class="form-control" class="mytextarea" ng-model="tujuan_tl_d" style="background-color: #d7d6d6;" disabled name="tujuan_tl_d" placeholder="Tujuan" required><?=$rencana['tujuan_tl']?></textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputName" class="col-sm-3 form-control-label">Sasaran<span class="text-danger">*</span></label>
                                <div class="col-sm-9">
                                    <textarea class="form-control" class="mytextarea" ng-model="sasaran_tl_d" style="background-color: #d7d6d6;" disabled name="sasaran_tl_d" placeholder="Sasaran" required><?=$rencana['tujuan_tl']?></textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputName" class="col-sm-3 form-control-label">Ruang Lingkup<span
                                        class="text-danger">*</span></label>
                                <div class="col-sm-9">
                                    <textarea class="form-control" class="mytextarea" name="ruang" disabled style="background-color: #d7d6d6;" ng-model="ruang_d" placeholder="Ruang Lingkup" required><?=$rencana['ruang']?></textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputName" class="col-sm-3 form-control-label">Evaluasi SPI<span
                                        class="text-danger">*</span></label>
                                <div class="col-sm-9">
                                    <textarea class="form-control" class="mytextarea" name="evaluasi" readonly style="background-color: #d7d6d6;" ng-model="evaluasi_d" placeholder="Evaluasi SPI" required><?=$rencana['evaluasi']?></textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputName" class="col-sm-3 form-control-label">Judul Temuan<span
                                        class="text-danger">*</span></label>
                                <div class="col-sm-9">
                                    <textarea class="form-control" class="mytextarea" name="judul_tl" readonly style="background-color: #d7d6d6;" ng-model="judul_tl_d" placeholder="Judul Temuan" required><?=$rencana['judul_tl']?></textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputName" class="col-sm-3 form-control-label">Kondisi<span
                                        class="text-danger">*</span></label>
                                <div class="col-sm-9">
                                    <textarea class="form-control" class="mytextarea" name="kondisi" readonly style="background-color: #d7d6d6;" ng-model="kondisi_d" placeholder="Kondisi" required><?=$rencana['kondisi']?></textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputName" class="col-sm-3 form-control-label">Kriteria<span
                                        class="text-danger">*</span></label>
                                <div class="col-sm-9">
                                    <textarea class="form-control" class="mytextarea" name="kriteria" readonly style="background-color: #d7d6d6;" ng-model="kriteria_d" placeholder="Kriteria" required><?=$rencana['kriteria']?></textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputName" class="col-sm-3 form-control-label">Penyebab<span
                                        class="text-danger">*</span></label>
                                <div class="col-sm-9">
                                    <textarea class="form-control" class="mytextarea" name="penyebab" readonly style="background-color: #d7d6d6;" ng-model="penyebab_d" placeholder="Penyebab" required><?=$rencana['penyebab']?></textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputName" class="col-sm-3 form-control-label">Akibat<span
                                        class="text-danger">*</span></label>
                                <div class="col-sm-9">
                                    <textarea class="form-control" class="mytextarea" readonly name="akibat" style="background-color: #d7d6d6;" ng-model="akibat_d" placeholder="Akibat" required><?=$rencana['kriteria']?></textarea>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="inputName" class="col-sm-3 form-control-label">Tanggapan<span
                                        class="text-danger">*</span></label>
                                <div class="col-sm-9">
                                    <textarea class="form-control" readonly class="mytextarea" name="tanggapan" style="background-color: #d7d6d6;" ng-model="tanggapan_d" placeholder="Tanggapan" required><?=$rencana['tanggapan']?></textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputName" class="col-sm-3 form-control-label">Tindak Lanjut<span
                                        class="text-danger">*</span></label>
                                <div class="col-sm-9">
                                    <textarea class="form-control" readonly class="mytextarea" name="tindak_lanjut" style="background-color: #d7d6d6;" ng-model="tindak_lanjut_d" placeholder="Tindak Lanjut" required><?=$rencana['tindak_lanjut']?></textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputName" class="col-sm-3 form-control-label">Tindak Lanjut Sebelumnya<span
                                        class="text-danger">*</span></label>
                                <div class="col-sm-9">
                                    <textarea class="form-control" readonly class="mytextarea" name="tindak_lanjut_s " style="background-color: #d7d6d6;" ng-model="tindak_lanjut_s_d" placeholder="Tindak Lanjut Sebelumnya" required><?=$rencana['tindak_lanjut_s']?></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="taoModal" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-large" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body">

                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="tambahModal2" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-large" style="overflow-y: scroll" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Tambah Kode Temuan</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label class="col-md-3 control-label">Kode Temuan</label>
                        <div class="col-md-4">
                            <input type="text" name="header[kode_temuan]" autocomplete="off" placeholder="Kode Temuan" required value="<?=(@$rencana['kode_temuan'])?>" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Temuan</label>
                        <div class="col-md-4">
                            <input type="text" name="header[temuan]" autocomplete="off" placeholder="Temuan" required value="<?=(@$rencana['temuan'])?>" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Kode Rekomendasi</label>
                        <div class="col-md-4">
                            <input type="text" name="header[kode_rekom]" autocomplete="off" placeholder="Kode Rekomendasi" required value="<?=(@$rencana['kode_rekom'])?>" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Data Umum Obrik</label>
                        <div class="col-md-4">
                            <textarea class="form-control" class="mytextarea" name="header[obrik]" placeholder="Data Umum Obrik" required><?=$rencana['obrik']?></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Uraian Rekomendasi</label>
                        <div class="col-md-4">
                            <input type="text" name="header[rekom]" autocomplete="off" placeholder="Rekomendasi" required value="<?=(@$rencana['rekom'])?>" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">NK Negara</label>
                        <div class="col-md-4">
                            <input type="text" name="header[nk_n]" autocomplete="off" placeholder="NK Negara" required value="<?=(@$rencana['nk_n'])?>" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">NK Daerah</label>
                        <div class="col-md-4">
                            <input type="text" name="header[nk_d]" autocomplete="off" placeholder="NK Daerah" required value="<?=(@$rencana['nk_d'])?>" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Tindak Lanjut Entitas</label>
                        <div class="col-md-4">
                            <input type="text" name="header[tindak_lanjut]" autocomplete="off" placeholder="Tindak Lanjut" required value="<?=(@$rencana['tindak_lanjut'])?>" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Hasil </label>
                        <div class="col-md-4">
                            <select class="form-control select2" name="header[hasil]" required >
                                <option value="">pilih hasil</option>
                                <?php
                                $pkpt=array('Sesuai', 'Belum Sesuai','Belum Ditindaklanjuti', 'Tidak Dapat ditindaklanjuti');
                                foreach ($pkpt as $v):
                                    ?>
                                    <option value="<?= $v ?>" <?=(@$rencana['pkpt_no']==$v)?'selected=""':""?>><?= "$v" ?></option>
                                    <?php
                                endforeach;
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Nilai Tindak Lanjut</label>
                        <div class="col-md-4">
                            <input type="text" name="header[nilai]" autocomplete="off" placeholder="Nilai" required value="<?=(@$rencana['nilai'])?>" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Keterangan</label>
                        <div class="col-md-4">
                            <input type="text" name="header[keterangan]" autocomplete="off" placeholder="Keterangan" required value="<?=(@$rencana['keterangan'])?>" class="form-control">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <link href="<?= base_url() ?>assets/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css"/>
    <link href="<?=base_url()?>assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css"/>
    <script src="<?=base_url()?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
    <script type="text/javascript" src="<?= base_url() ?>assets/plugins/select2/js/select2.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/plugins/parsleyjs/parsley.min.js"></script>
    <script src="<?=base_url()?>assets/js/angular.min.js"></script>
    <script src="<?=base_url()?>assets/js/ng-messages.min.js"></script>
    <script type="text/javascript">

        var app = angular.module("emjesSA", ['ngMessages']);
        app.filter("arrayFilter",function() {
            return function (items, idx, filterarray) {
                var filtered = [];
                angular.forEach(items, function (value, key) {
                    angular.forEach(filterarray, function (v, k) {
                        if (value[idx] == v){
                            filtered.push(value);
                        }
                    });
                });
                return filtered;
            }
        });
        app.filter("matchFilter",function() {
            return function (items, idx, filterarray) {
                var filtered = [];
                angular.forEach(items, function (value, key) {
                    if (value[idx] == filterarray){
                        filtered.push(value);
                    }
                });
                return filtered;
            }
        });
        app.directive('taoRepeatDirective', function() {
            return function(scope, element, attrs) {
                if (scope.$last){
                    $('.select2').select2();
                    $('.datepicker').datepicker({
                        format: 'dd/mm/yyyy',
                        autoclose: true,
                        todayHighlight: true
                    });
                }
            };
        });
        app.directive('validNumber', function() {
            return {
                require: '?ngModel',
                link: function(scope, element, attrs, ngModelCtrl) {
                    if(!ngModelCtrl) {
                        return;
                    }
                    ngModelCtrl.$parsers.push(function(val) {
                        if (angular.isUndefined(val)) {
                            var val = '';
                        }
                        var clean = val.replace(/[^-0-9\.]/g, '');
                        var decimalCheck = clean.split('.');
                        if(!angular.isUndefined(decimalCheck[1])) {
                            decimalCheck[1] = decimalCheck[1].slice(0,2);
                            clean =decimalCheck[0] + '.' + decimalCheck[1];
                        }
                        if (val !== clean) {
                            ngModelCtrl.$setViewValue(clean);
                            ngModelCtrl.$render();
                        }
                        return clean;
                    });
                    element.bind('keypress', function(event) {
                        if(event.keyCode === 32) {
                            event.preventDefault();
                        }
                    });
                }
            };
        });
        app.controller('emjesController', function($scope,$http) {
            $scope.filtertim = function(item) {
                if ($scope.timpelaksana.indexOf(item.id) > -1) {
                    return item;
                }
            };
            $scope.aturan=[];
            $scope.pedoman=[];
            $scope.pilihpelaksana='';
            $scope.timpelaksana=<?=@$rencana['tim']?$rencana['tim']:'[]'?>;
            $scope.listtim=[];
            $scope.init = function(){
                $scope.sasaran=<?=strlen(@$rencana['sasaran'])>1?@$rencana['sasaran']:"''"?>;
                $scope.tujuan=<?=strlen(@$rencana['tujuan'])>1?$rencana['tujuan']:"''"?>;
                $scope.aturan=<?=json_encode($aturan)?>;
                $scope.temuan=<?=@$temuan?json_encode($temuan):'[]'?>;
                $scope.pedoman=<?=json_encode($pedoman)?>;
                $scope.tao=<?=json_encode(@$tao)?>;
                $scope.listtim=<?=json_encode($listtim)?>;
                $scope.jenis='<?=@$rencana['jenis']?>';
                $scope.satker='<?=@$rencana['satker']?>';
                $scope.judul='<?=@$rencana['judul']?>';
                $scope.sasaran_tl=<?=strlen(@$rencana['sasaran_tl'])>1?@$rencana['sasaran_tl']:"''"?>;
                $scope.tujuan_tl=<?=strlen(@$rencana['tujuan_tl'])>1?@$rencana['tujuan_tl']:"''"?>;
                $scope.judul_tl='<?=@$rencana['judul_tl']?>';
                $scope.spt_no='<?=@$rencana['spt_no']?>';
                $scope.start_date='<?=format_waktu(@$rencana['start_date'])?>';
                $scope.end_date='<?=format_waktu(@$rencana['end_date'])?>';
                $scope.spt_date='<?=format_waktu(@$rencana['spt_date'])?>';
            };
            $scope.detail_temuan = function(_i){
                console.log($scope.temuan[_i]);
                $scope.ut_d=$scope.temuan[_i].temuan;
                $scope.akibat_d=$scope.temuan[_i].akibat;
                $scope.penyebab_d=$scope.temuan[_i].penyebab;
                $scope.kriteria_d=$scope.temuan[_i].kriteria;
                $scope.kondisi_d=$scope.temuan[_i].kondisi;
                $scope.tujuan_tl_d=$scope.temuan[_i].tujuan_tl;
                $scope.sasaran_tl_d=$scope.temuan[_i].sasaran_tl;
                $scope.judul_tl_d=$scope.temuan[_i].judul_tl;
                $scope.evaluasi_d=$scope.temuan[_i].evaluasi;
                $scope.ruang_d=$scope.temuan[_i].ruang;
                $scope.obrik_d=$scope.temuan[_i].obrik;
                $scope.tanggapan_d=$scope.temuan[_i].tanggapan;
                $scope.tindak_lanjut_d=$scope.temuan[_i].tindak_lanjut;
                $scope.tindak_lanjut_s_d=$scope.temuan[_i]  .tindak_lanjut_s;

                $('#detailModal').modal('show');
            };
            $scope.pilihaturan = function(_i) {
                $scope.aturan[_i].checked=!$scope.aturan[_i].checked;
            };
            $scope.tambahpelaksana = function() {
                if ($scope.timpelaksana.indexOf($scope.pilihpelaksana) < 0 && $scope.pilihpelaksana!="") {
                    $scope.timpelaksana.push($scope.pilihpelaksana);
                }
                $scope.pilihpelaksana="";
            };
            $scope.hapuspelaksana = function(_id) {
                var _index=$scope.timpelaksana.indexOf(_id);
                $scope.timpelaksana.splice(_index);
            };
            $scope.hapustemuan = function(_id) {
                var _index=$scope.temuan.indexOf(_id);
                $scope.temuan.splice(_index);
            };
            $scope.pilihpedoman = function(_i) {
                $scope.pedoman[_i].checked=!$scope.pedoman[_i].checked;
            };
            $scope.detailaturan = function(_tao){
                $('.modal-title','#taoModal').html(_tao.kode_kk+' '+_tao.langkah);
                $.post('?tao='+_tao.id,function(data,status){
                    $('.modal-body','#taoModal').html(data);
                    $('#taoModal').modal('show');
                });
            };
            $scope.tambah = function(){
                $('#tambahModal').modal('show');
//            $.post('?tao='+_tao.id,function(data,status){
//                $('.modal-body','#taoModal').html(data);
//                $('#taoModal').modal('show');
//            });
            };
            $scope.tambah_temuan = function(){
                var _isExist=false;
                console.log($scope.kt);
                console.log($scope.kr);
                console.log($scope.nd);
                console.log($scope.tl);
                if($scope.ut == '' ||  $scope.uk == '' || typeof $scope.kt ==='undefined' || typeof $scope.kr === 'undefined' || typeof $scope.nd === 'undefined' || typeof $scope.htl === 'undefined' || typeof $scope.nk === 'undefined'|| typeof $scope.tl === 'undefined'|| typeof $scope.ntl === 'undefined'){

                    $('#alert-kurang').empty();
                    $('#alert-kurang').html('Ada data yang belum terisi');
                }else{

//            for (var i in $scope.temuan) {
//                if ($scope.temuan[i].id_t == $scope.kt) {
//                    alert('Kode Temuan Sudah Ada');
//                    $('#tambahModal').modal('hide');
//                    _isExist=true;
//                    break;
//                }
//            }
                    if(_isExist==false){
                        var kode_t=$( "#kt option:selected" ).text();
                        var kode_temuan=kode_t.split(' - ');
                        var kode_r=$( "#kr option:selected" ).text();
                        var kode_rekom=kode_r.split(' - ');
                        var nd=$( "#nd option:selected" ).text();
                        var htl=$( "#htl option:selected" ).text();

                        $scope.addtemuan={};
                        $scope.addtemuan.id_t=$scope.kt;
                        $scope.addtemuan.kode_t=kode_temuan[0];
//                $scope.addtemuan.temuan=kode_temuan[1];
                        $scope.addtemuan.temuan=$scope.ut;
                        $scope.addtemuan.akibat=$scope.akibat;
                        $scope.addtemuan.penyebab=$scope.penyebab;
                        $scope.addtemuan.kriteria=$scope.kriteria;
                        $scope.addtemuan.kondisi=$scope.kondisi;
                        $scope.addtemuan.tujuan_tl=$scope.tujuan_tl;
                        $scope.addtemuan.sasaran_tl=$scope.sasaran_tl;
                        $scope.addtemuan.judul_tl=$scope.judul_tl;
                        $scope.addtemuan.evaluasi=$scope.evaluasi;
                        $scope.addtemuan.ruang=$scope.ruang;
                        $scope.addtemuan.obrik=$scope.obrik;
                        $scope.addtemuan.tanggapan=$scope.tanggapan;
                        $scope.addtemuan.tindak_lanjut=$scope.tindak_lanjut;
                        $scope.addtemuan.tindak_lanjut_s=$scope.tindak_lanjut_s;
                        $scope.addtemuan.id_r=$scope.kr;
                        $scope.addtemuan.kode_r=kode_rekom[0];
                        $scope.addtemuan.rekom=$scope.uk;
                        $scope.addtemuan.tl=$scope.tl;
                        $scope.addtemuan.kode_hr=$scope.htl;
                        $scope.addtemuan.hasil_r=htl;
                        $scope.addtemuan.nilai_r=$scope.ntl;
                        $scope.addtemuan.keterangan=$scope.keterangan;
                        if($scope.nd==1){
                            $scope.addtemuan.nd=1;
                            $scope.addtemuan.nk1=$scope.nk;
                            $scope.addtemuan.nk2=0;
                        }
                        else{
                            $scope.addtemuan.nd=2;
                            $scope.addtemuan.nk2=$scope.nk;
                            $scope.addtemuan.nk1=0;
                        }
                        $scope.temuan.unshift($scope.addtemuan);
                        console.log($scope.temuan);
                        $scope.addtemuan=null;
                        $('#tambahModal').modal('hide');
                    }

                    $scope.kt='';
                    $scope.ut='';
                    $scope.uk='';
                    $scope.kr='';
                    $scope.nd='';
                    $scope.nk='';
                    $scope.tl='';
                    $scope.htl='';
                    $scope.ntl='';
                    $scope.akibat='';
                    $scope.penyebab='';
                    $scope.kriteria='';
                    $scope.kondisi='';
                    $scope.judul_tl='';
                    $scope.sasaran_tl='';
                    $scope.tujuan_tl='';
                    $scope.ruang='';
                    $scope.evaluasi='';
                    $scope.obrik='';
                    $scope.keterangan='';
                    $scope.tanggapan='';
                    $scope.tindak_lanjut='';
                    $scope.tindak_lanjut_s='';
                }
            };
            $scope.simpan = function(_draft) {
                $('#preloader').show();
                $('#status','#preloader').show();

                $scope.form = $('#form-gas').serializeArray();
                console.log($scope.form);
                $http({
                    method: 'POST',
                    url: "?reviu=<?=$rencana['id']?>",
                    responseType: 'json',
                    data: {
                        form:$scope.form,
                    }
                }).success(function(data){
//                    window.location.assign('?');
                    setTimeout(function () {
                        $('#preloader').hide();
                        $('#status','#preloader').hide();
                    },2000);
                    console.log("OK", data)
                }).error(function(err){"ERR", console.log(err)});
            };
        });
    </script>