<style>
    .select2-selection.select2-selection--single {
        width: 300px;
    }

</style><div class="row">
    <div class="col-sm-12">
        <div class="card-box">
            <?php
            show_alert();
            ?>
            <a href="?" class="btn btn-default"><i class="fa fa-refresh"></i></a>
            <hr>
            <div class="table-responsive">
                <table class="table table-bordered table-striped table-hover">
                    <thead>
                    <tr>
                        <th class="center">#</th>
                        <th class="center col-xs-2">Tanggal SPT</th>
                        <th class="center col-xs-3">Judul SPT</th>
                        <th class="center col-xs-3">Satuan Kerja</th>
                        <th class="center col-xs-2">Jenis Audit</th>
                        <th class="center col-xs-1">Detail</th>
                    </tr>
                    </thead>
                    <tbody>

                    <?php
                    $no=1;
                    foreach($rencana as $r) {
                        ?>
                        <tr class="data<?=$r['id']?>">
                            <td class="center"><?=$no;?></td>
                            <td class="center"><?=format_waktu($r['spt_date'])?></td>
                            <td class=""><?=$r['judul']?></td>
                            <td class=""><?=$r['nama_satker']?></td>
                            <td class=""><?=$r['nama_jenis']?></td>
                                <td class="center"> <a href="?v=<?=$r['id']?>" class="btn btn-sm btn-inverse" target="_blank"><i class="fa fa-search"></i></a></td>
                        </tr>
                        <?php
                        $no++; }
                    ?>
                    </tbody>

                </table>
            </div>
        </div>
    </div>
</div>
<link href="<?=base_url()?>assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css"/>
<script src="<?=base_url()?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script>
    function terima(_i) {
        swal({
            title: "Apakah anda yakin?",
            text: "data yang anda terima tidak akan bisa di tolak kembali!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD5555",
            confirmButtonText: "Lanjutkan!",
            cancelButtonText: "Batalkan!",
            closeOnConfirm: true,
            closeOnCancel: true
        }, function (isConfirm) {
            if (isConfirm) {
                $.post('?',{terima:_i},function () {
                    $('.data'+_i).remove();
                });
            }
        });
    }function tolak(_i) {
        swal({
            title: "Apakah anda yakin?",
            text: "data yang anda tolak tidak dapat di setujui kembali!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD5555",
            confirmButtonText: "Lanjutkan!",
            cancelButtonText: "Batalkan!",
            closeOnConfirm: true,
            closeOnCancel: true
        }, function (isConfirm) {
            if (isConfirm) {
                $.post('?',{tolak:_i},function () {
                    $('.data'+_i).remove();
                });
            }
        });
    }function terimaI(_i) {
        swal({
            title: "Apakah anda yakin?",
            text: "data yang anda terima tidak akan bisa di tolak kembali!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD5555",
            confirmButtonText: "Lanjutkan!",
            cancelButtonText: "Batalkan!",
            closeOnConfirm: true,
            closeOnCancel: true
        }, function (isConfirm) {
            if (isConfirm) {
                $.post('?',{terimaI:_i},function () {
                    location.reload();
                });
            }
        });
    }function tolakI(_i) {
        swal({
            title: "Apakah anda yakin?",
            text: "data yang anda tolak tidak dapat di setujui kembali!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD5555",
            confirmButtonText: "Lanjutkan!",
            cancelButtonText: "Batalkan!",
            closeOnConfirm: true,
            closeOnCancel: true
        }, function (isConfirm) {
            if (isConfirm) {
                $.post('?',{tolakI:_i},function () {
                    $('.data'+_i).remove();
                });
            }
        });
    }
    $('#date-range').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true,
        todayHighlight: true
    });
    function rincian(_i,_r) {
        $.post('?',{ri:_i,rn:_r},function (data,status) {
            $('h4.modal-title','#detailpenerimaan').html("Detail");
            $('.modal-body','#detailpenerimaan').html(data.data);
            $('#detailpenerimaan').modal('show');
        },'json');
    }
    function setStatusActive(_i) {
        $('.'+_i).html('<img src="<?=base_url('assets/loading.gif')?>">');
        $.post('?',{status:_i},function (data,status) {
            $('.'+_i).html(data);
        });
    }
</script>