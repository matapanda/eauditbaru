<div class="row">
    <div class="col-sm-12">
        <div class="card-box">
            <a href="?i=true" class="<?=is_authority(@$access['c'])?> btn btn-inverse"><i class="fa fa-plus"></i> FORM ISIAN</a>
            <a href="?" class="btn btn-default"><i class="fa fa-refresh"></i></a>
            <hr>
            <?php
            show_alert();
            ?>
            <form method="get" class="row" action="<?=base_url('internalproses/rencana')?>">
                <div class="col-md-6">
                    <div class="dataTables_wrapper form-inline">
                        <label>Filter: </label>
                        <div class="input-daterange input-group" id="date-range">
                            <input type="text" readonly class="form-control input-sm" name="start" onchange="this.form.submit()" value="<?=$start?>">
                            <span class="input-group-addon input-sm">~</span>
                            <input type="text" readonly class="form-control input-sm" name="end" onchange="this.form.submit()" value="<?=$end?>">
                        </div>
                        <select name="closed" onchange="this.form.submit()" class="form-control input-sm">
                            <option value="">Semua status</option>
                            <option value="f" <?=$closed=='f'?'selected':''?>>Status aktif</option>
                            <option value="t" <?=$closed=='t'?'selected':''?>>Status closed</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="dataTables_wrapper form-inline right">
                        <label class="hidden-xs">Search: &nbsp;</label><input type="search" name="search" class="form-control input-sm" autocomplete="off" value="<?=@$search?>">
                    </div>
                </div>
            </form>
            <div class="table-responsive">
                <table class="table table-bordered table-striped table-hover">
                    <thead>
                    <tr>
                        <th class="center">#</th>
                        <th class="center col-xs-2">Tanggal SPT</th>
                        <th class="center col-xs-3">Judul SPT</th>
                        <th class="center col-xs-2">Satuan Kerja</th>
                        <th class="center col-xs-2">Jenis Audit</th>
                        <th class="center col-xs-1">Status</th>
                        <th class="center col-xs-1"></th>
                    </tr>
                    </thead>
                    <tbody>

                    <?php
                    $no=1;
                    foreach($rencana as $r) {
                        ?>
                        <tr class="data<?=$r['id']?>">
                            <td class="center"><?=$no;?></td>
                            <td class="center"><?=format_waktu($r['spt_date'])?></td>
                            <td class=""><?=$r['judul']?></td>
                            <td class=""><?=$r['nama_satker']?></td>
                            <td class=""><?=$r['nama_jenis']?></td>
                            <td class="center"><?=getLabelDraftCLosedProcess($r['draft'],$r['closed'],$r['proses'],$r['app_sv1_status'])?></td>
                            <td class="center">
                                <a href="?e=<?=$r['id']?>" class="btn btn-sm btn-inverse <?= is_authority(@$access['u']) ?>"><i class="fa fa-pencil"></i></a>
                                <?php
                                if(($r['draft']=='t' || $r['draft']==1)) {
                                    ?>
                                    <button type="button" onclick="hapus('<?=$r['id']?>')"
                                       class="btn btn-sm btn-danger <?= is_authority(@$access['d']) ?>"><i
                                            class="fa fa-remove"></i></button>
                                    <?php
                                }else{
                                    ?>
                                    <a href="?v=<?=$r['id']?>" class="btn btn-sm btn-success" target="_blank"><i class="fa fa-file-text"></i></a>
                                    <?php
                                }
                                ?>
                            </td>
                        </tr>
                        <?php
                        $no++; }
                    ?>
                    </tbody>

                </table>
            </div>
        </div>
    </div>
</div>
<link href="<?=base_url()?>assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css"/>
<script src="<?=base_url()?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script>
    function hapus(_i) {
        swal({
            title: "Apakah anda yakin?",
            text: "data yang terhapus tidak dapat dikembalikan!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD5555",
            confirmButtonText: "Lanjutkan!",
            cancelButtonText: "Batalkan!",
            closeOnConfirm: true,
            closeOnCancel: true
        }, function (isConfirm) {
            if (isConfirm) {
                $.post('?',{delete:_i},function () {
                    $('.data'+_i).remove();
                });
            }
        });
    }
    $('#date-range').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true,
        todayHighlight: true
    });
    function rincian(_i,_r) {
        $.post('?',{ri:_i,rn:_r},function (data,status) {
            $('h4.modal-title','#detailpenerimaan').html("Detail");
            $('.modal-body','#detailpenerimaan').html(data.data);
            $('#detailpenerimaan').modal('show');
        },'json');
    }
    function setStatusActive(_i) {
        $('.'+_i).html('<img src="<?=base_url('assets/loading.gif')?>">');
        $.post('?',{status:_i},function (data,status) {
            $('.'+_i).html(data);
        });
    }
</script>