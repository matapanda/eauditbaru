<div class="row">

    <div class="col-lg-6 col-xs-12">
        <div class="card-box">
            <div id="container1" style="width:100%; height:400px;"></div>
        </div>
    </div>

    <div class="col-lg-6 col-xs-12">
        <div class="card-box">
            <div id="container2" style="width:100%; height:400px;"></div>
        </div>
    </div>

    <div class="col-lg-6 col-xs-12">
        <div class="card-box">
            <div id="container3" style="width:100%; height:400px;"></div>
        </div>
    </div>
    <div class="col-lg-6 col-xs-12">
        <div class="card-box">
            <div id="container4" style="width:100%; height:400px;"></div>
        </div>
    </div>

</div>

<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="<?=base_url()?>assets/plugins/moment/moment.js"></script>
<script>
    $(document).ready(function () {
        // Build the chart
        Highcharts.chart('container1', {
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            title: {
                text: 'Monitoring SPT'
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false
                    },
                    showInLegend: true
                }
            },credits: {
                enabled: false
            },
            series: [{
                name: 'Brands',
                colorByPoint: true,
                data: [{
                    name: 'SPT Terbit',
                    y: <?=$sesuai['total'];?>
                }, {
                    name: 'SPT Belum Terbit',
                    y: <?=$tidak['total'];?>
                },]
            }]
        });

        // Build the chart
        Highcharts.theme = {
            colors: ['#f45b5b', '#8085e9', '#8d4654', '#7798BF', '#aaeeee', '#ff0066', '#eeaaee',
                '#55BF3B', '#DF5353', '#7798BF', '#aaeeee'],}

        Highcharts.setOptions(Highcharts.theme);

        Highcharts.chart('container2', {
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            title: {
                text: 'Monitoring LHP'
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false
                    },
                    showInLegend: true
                }
            },credits: {
                enabled: false
            },
            series: [{
                name: 'Brands',
                colorByPoint: true,
                data: [{
                    name: 'LHP Terbit',
                    y: <?=$lhp_sesuai['total'];?>
                }, {
                    name: 'LHP Belum Terbit',
                    y: <?=$lhp_tidak['total'];?>
                },]
            }]
        });

        Highcharts.chart('container3', {
            chart: {
                type: 'column'
            },
            title: {
                text: 'Proses Audit'
            },xAxis: {
                type: 'category'
            },
            tooltip: {
                headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
            },
            legend: {
                enabled: false
            },credits: {
                enabled: false
            },
            plotOptions: {
                series: {
                    borderWidth: 0,
                    dataLabels: {
                        enabled: true,
                        format: '{point.y:.1f}%'
                    }
                }
            },
            series: [{
                name: 'Proses',
                colorByPoint: true,
                data: [
                    {name: 'Perencanaan',
                        y: <?=$perencanaan['total']/$perencanaan_all['total']*100?>,
                    },
                    {name: 'Pelaksanaan',
                        y: <?=$pelaksanaan['total']/$perencanaan_all['total']*100?>,
                    },{name: 'Pelaporan',
                        y: <?=$pelaporan['total']/$perencanaan_all['total']*100?>,
                    },
                ]
            }]
        });

        Highcharts.chart('container4', {
            chart: {
                type: 'column'
            },
            title: {
                text: 'Monitoring Penugasan'
            },xAxis: {
                type: 'category'
            },
            tooltip: {
                headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
            },
            legend: {
                enabled: false
            },credits: {
                enabled: false
            },
            plotOptions: {
                series: {
                    borderWidth: 0,
                    dataLabels: {
                        enabled: true,
                        format: '{point.y:.1f}%'
                    }
                }
            },
            series: [{
                name: 'Kinerja Telah Selesai',
                colorByPoint: true,
                data: [
                    <?php
                    foreach ($kinerja as $u) {
                            $realisasi=$u['realisasi'];
                            $realisasi2=$u['selesai'];
                            $disetujui = 0;
                            $direalisasi = 0;
                            if ($u['jumlah'] > 0) {
                                if ($u['selesai'] > 0) {
                                    $disetujui = ($u['selesai'] / $u['jumlah'] * 100);
                                }
                                $u['realisasi'] = $u['realisasi'] - $u['selesai'];
                                if ($u['realisasi'] > 0) {
                                    $direalisasi = ($u['realisasi'] / $u['jumlah'] * 100);
                                }
                            }

                    ?>
                    {
                        name: '<?=$u['nama']?>',
                        y: <?= $realisasi2?>,
                    },
                    <?php
                    }
                    ?>
                ]
            }]
        });
    });
</script>