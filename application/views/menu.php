<ul>
    <li><a href="<?=base_url('welcome')?>" class="waves-effect"><i class="mdi mdi-view-dashboard"></i><span> Dashboard </span></a></li>
    <?php
    if(isset($role['f10293a6-79c7-41e2-840e-a597e5ace426']['r'])){
    ?>
        <li class="has_sub">
            <a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-archive"></i> <span> Master</span> <span class="menu-arrow"></span></a>
            <ul class="list-unstyled">
                <?php
                if(isset($role['b0014449-4706-48e1-b81d-d758057ef0d0']['r'])) {
                    ?>
                    <li><a href="<?= base_url('master/periode') ?>"><span>Data Periode </span></a></li>
                    <?php
                }
                if(isset($role['2a5a511b-7bd4-424f-8b38-252f6115640f']['r'])){
                    ?>
                    <li><a href="<?=base_url('master/pkpt')?>"><span>Data PKPT</span></a></li>
                    <?php
                }
                if(isset($role['72e9e5f5-9b6c-4213-9c23-85588627d997']['r'])){
                    ?>
                    <li><a href="<?=base_url('master/prosedur_audit')?>"><span>Data Prosedur Audit </span></a></li>
                    <?php
                }
                if(isset($role['c43ac70d-a037-4148-827d-d5c0a8b00b97']['r'])){
                    ?>
                    <li><a href="<?=base_url('master/penyebab')?>"><span>Data Penyebab</span></a></li>
                    <?php
                }
                if(isset($role['2a5a511b-7bd4-424f-8b38-252f6115640d']['r'])){
                    ?>
                    <li><a href="<?=base_url('master/akibat')?>"><span>Data Akibat</span></a></li>
                    <?php
                }
                if(isset($role['bfa6447a-7dc4-4495-9397-db2e5865ade1']['r'])){
                    ?>
                    <li><a href="<?=base_url('master/kendali')?>"><span>Data Kendali</span></a></li>
                    <?php
                }if(isset($role['acda06a9-1a27-45f0-bceb-1b77c81b8749']['r'])){
                    ?>
                    <li><a href="<?=base_url('master/jabatan')?>"><span>Data Jabatan</span></a></li>
                    <?php
                }
                if(isset($role['50492ec8-26b3-4178-9d66-501ccef6ef8b']['r'])){
                    ?>
                    <li><a href="<?=base_url('master/jenis')?>"><span>Data Jenis Audit</span></a></li>
                    <?php
                }
                if(isset($role['8a1dfd71-6c85-412f-91de-970ca913dc60']['r'])){
                    ?>
                    <li><a href="<?=base_url('master/satker')?>"><span>Data Satker</span></a></li>
                    <?php
                }
                if(isset($role['6ea5af6e-aeb0-4bde-9e9b-8dd58f322c75']['r'])){
                    ?>
                    <li><a href="<?=base_url('master/sasaran')?>"><span>Data Sasaran Audit</span></a></li>
                    <?php
                }
                $menuid='b4be4621-3e9e-4e3d-a47b-df549ae100f4';
                if(isset($role[$menuid]['r'])){
                    ?>
                    <li><a href="<?=base_url('master/wilayah')?>">Data Wilayah </a></li>
                    <?php
                }
                $menuid='292e5ff3-1095-49b3-aafd-565794ee545e';
                if(isset($role[$menuid]['r'])){
                    ?>
                    <li><a href="<?=base_url('master/conf_spt')?>">Konfigurasi Dasar SPT </a></li>
                    <?php
                }$menuid='92559c74-927e-4edf-85c8-203447141840';
                if(isset($role[$menuid]['u'])){
                    ?>
                    <li><a href="<?=base_url('master/group')?>">Data Role</a></li>
                    <?php
                }
                if(isset($role['9312a5c1-7991-4eb1-be6d-05fef767935b']['r'])){
                    ?>
                    <li><a href="<?=base_url('master/tujuan')?>"><span>Data Tujuan Audit</span></a></li>
                    <?php
                }
                if(isset($role['e89cdedb-32f8-47fd-bbfc-960c894dae01']['r'])){
                    ?>
                    <li><a href="<?=base_url('master/golongan')?>"><span>Data Golongan</span></a></li>
                    <?php
                }
                $menuid='2f4e5764-01c0-49f5-b178-555f814a015a';
                if(isset($role[$menuid]['r'])){
                    ?>
                    <li><a href="<?=base_url('master/tao')?>">Data TAO</a></li>
                    <?php
                }
                $menuid='1541696d-a165-4a06-9181-88472bd61293';
                if(isset($role[$menuid]['r'])){
                    ?>
                    <li><a href="<?=base_url('master/aturan')?>">Data Aturan</a></li>
                    <?php
                }
                $menuid='e36e779a-e2aa-468e-a11c-972f75c1c052';
                if(isset($role[$menuid]['r'])){
                    ?>
                    <li><a href="<?=base_url('master/tindak_lanjut')?>">Data Kode Tindak Lanjut</a></li>
                    <?php
                }
                $menuid='7a2eee70-8883-4c74-9a9b-e820f2249b4b';
                if(isset($role[$menuid]['r'])){
                    ?>
                    <li><a href="<?=base_url('master/pedoman')?>">Data Pedoman</a></li>
                    <?php
                }
                $menuid='1541696d-a165-4a06-9181-88472bd61294';
                if(isset($role[$menuid]['r'])){
                    ?>
                    <li><a href="<?=base_url('master/dampak')?>">Data Dampak</a></li>
                    <?php
                }
                $menuid='d66ab356-6792-451d-8f42-84f07e48b9e7';
                if(isset($role[$menuid]['r'])){
                    ?>
                    <li><a href="<?=base_url('master/user')?>">Data User</a></li>
                    <?php
                }
                $menuid='36fbf7b0-d4ba-49d1-af67-1e0af4a05766';
                if(isset($role[$menuid]['r'])){
                    ?>
                    <li><a href="<?=base_url('master/tim')?>">Data Tim</a></li>
                    <?php
                }?>

            </ul>
        </li>
    <?php
    }
    if(isset($role['fb7ca00c-ef46-4dc1-bf9f-5d00358213a0']['r'])){
        ?>
        <li class="has_sub">
            <a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-scale-balance"></i> <span> Internal Proses</span> <span class="menu-arrow"></span></a>
            <ul class="list-unstyled"><?php
                $menuid='74fa394a-0fee-4b9d-827c-91a09f5c68f5';
                if(isset($role[$menuid]['r'])){
                    ?>
                    <li><a href="<?=base_url('internalproses/rencana')?>">Rencana Audit</a></li>
                    <?php
                }
                $menuid='e3033e60-42d8-47fd-8c8a-6e51cebaba33';
                if(isset($role[$menuid]['r'])){
                    ?>
                    <li><a href="<?=base_url('internalproses/persetujuan_r')?>">Persetujuan Rencana Audit</a></li>
                    <?php
                }
                $menuid='ca530176-384e-4c7d-8377-1171ec3a8d05';
                if(isset($role[$menuid]['r'])){
                    ?>
                    <li><a href="<?=base_url('internalproses/pelaksanaan')?>">Pelaksanaan Audit</a></li>
                    <?php
                }
                $menuid='b7d62610-7f21-43e4-a95b-43f1856411b4';
                if(isset($role[$menuid]['r'])){
                    ?>
                    <li><a href="<?=base_url('internalproses/persetujuan_p')?>">Persetujuan Pelaksanaan Audit</a></li>
                    <?php
                }
                $menuid='fe012b79-adf5-42f4-bdbd-5ffd5417289c';
                if(isset($role[$menuid]['r'])){
                    ?>
                    <li><a href="<?=base_url('internalproses/proses')?>">Pelaporan Hasil Audit</a></li>
                    <?php
                }
                $menuid='fe012b79-adf5-42f4-bdbd-5ffd5417289c';
                if(isset($role[$menuid]['r'])){
                    ?>
                    <li><a href="<?=base_url('internalproses/tindak_lanjut')?>">Temuan Tindak Lanjut</a></li>
                    <?php
                }
                ?>
            </ul>
        </li>
        <?php
    }
   if(isset($role['e964618e-29bd-4e17-91f2-1f42c0fa0488']['r'])){
       ?>
       <li class="has_sub">
           <a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-blinds"></i> <span> Laporan</span> <span class="menu-arrow"></span></a>
           <ul class="list-unstyled"><?php
               $menuid='82f810bf-86ce-4c88-927e-5e9a4ea82587';
               if(isset($role[$menuid]['r'])){
                   ?>
                   <li><a href="<?=base_url('laporan/spt')?>">Monitoring SPT</a></li>
                   <?php
               }?>
                   <li><a href="<?=base_url('laporan/cetak_spt')?>">Pencarian No SPT</a></li>
                   <?php
               $menuid='97c15763-d7b1-4846-9fc9-f5a36dfa5468';
               if(isset($role[$menuid]['r'])){
                   ?>
                   <li><a href="<?=base_url('laporan/lhp')?>">Monitoring LHP</a></li>
                   <?php
               }
               $menuid='e964618e-29bd-4e17-91f2-3212c0fa0488';
               if(isset($role[$menuid]['r'])){
                   ?>
                   <li><a href="<?=base_url('laporan/lhp_o')?>">Laporan LHP Online</a></li>
                   <?php
               }
               $menuid='59b034e9-a1ed-4267-afba-606ead1b9500';
               if(isset($role[$menuid]['r'])){
                   ?>
                   <li><a href="<?=base_url('laporan/file_manager')?>">File Manager</a></li>
                   <?php
               }$menuid='e741e54c-9c5f-454e-a2f9-a38b8806091b';
               if(isset($role[$menuid]['r'])){
                   ?>
                   <li><a href="<?=base_url('laporan/per_user')?>">Monitoring Penugasan</a></li>
                   <?php
               }$menuid='08f6f5e6-0933-48e2-88ce-ce52f2709040';
               if(isset($role[$menuid]['r'])){
                   ?>
                   <li><a href="<?=base_url('laporan/lap_tl')?>">Laporan Tindak Lanjut</a></li>
                   <?php
               }
               ?>
           </ul>
       </li>
       <?php
   }
   ?>
</ul>
