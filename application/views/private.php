<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="<?=$config['d']?>">
    <meta name="author" content="<?=$config['a']?>">
    <title><?=(isset($page['title'])?$page['title']." - ":"").$config['company']?></title>
    <link rel="stylesheet" href="<?=base_url()?>assets/plugins/morris/morris.css">
    <link href="<?=base_url()?>assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="<?=base_url()?>assets/css/core.css" rel="stylesheet" type="text/css" />
    <link href="<?=base_url()?>assets/css/components.css" rel="stylesheet" type="text/css" />
    <link href="<?=base_url()?>assets/css/icons.css" rel="stylesheet" type="text/css" />
    <link href="<?=base_url()?>assets/css/pages.css" rel="stylesheet" type="text/css" />
    <link href="<?=base_url()?>assets/css/menu.css" rel="stylesheet" type="text/css" />
    <link href="<?=base_url()?>assets/css/responsive.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="<?=base_url()?>assets/plugins/switchery/switchery.min.css">
    <link rel="stylesheet" href="<?= base_url() ?>assets/plugins/bootstrap-daterangepicker/daterangepicker.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/plugins/bootstrap-sweetalert/sweet-alert.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/plugins/toastr/toastr.min.css">
    <link href="<?=base_url()?>assets/css/bootcomplete.css" rel="stylesheet" type="text/css"/>
    <style>
        .table>thead>tr>th{
            vertical-align: middle;
        }
        .modal-full{
            width: 100%!important;
            margin: 0!important;
        }
        .table-striped>tbody>tr.selection.selected:nth-of-type(even){
            background: palegoldenrod;
        }
        .table-striped>tbody>tr.selection.selected:nth-of-type(odd){
            background: lightgoldenrodyellow;
        }
        tr.selection{
            cursor: pointer!important;
        }
        .pd-025{
            padding: .25em;
        }
        .app-search .bc-menu a {
            position: absolute;
            top: 0px;
            left: 0px;
            color: #555;
            width: 100%;
        }
        .app-search .bc-menu {
            margin: 0 12px 0 5px;
        }
        .alert-readed{
            background: #FFF;
            color: #000!important;
        }
        ::-webkit-scrollbar {
            -webkit-appearance: none;
            width: 3px;
        }
        ::-webkit-scrollbar-thumb{
            border-radius: 4px;
            background-color: #1c1610;
            -webkit-box-shadow: 0 0 1px #1c1610;
        }
        @media print {
            .content-page {
                margin-left: 0!important;
                width: 100%!important;
                float: left!important;
            }
            .editable-click, a.editable-click, a.editable-click:hover{
                border: none!important;
            }
            .table-bordered>tbody>tr>td, .table-bordered>tbody>tr>th, .table-bordered>tfoot>tr>td, .table-bordered>tfoot>tr>th, .table-bordered>thead>tr>td, .table-bordered>thead>tr>th,hr{
                border: solid 1px #000!important;
            }
        }
    </style>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <script src="<?=base_url()?>assets/js/modernizr.min.js"></script>
    <script>
        var resizefunc = [];
    </script>
    <script src="<?=base_url()?>assets/js/jquery.min.js"></script>
    <script src="<?=base_url()?>assets/js/bootstrap.min.js"></script>
    <script src="<?=base_url()?>assets/js/detect.js"></script>
    <script src="<?=base_url()?>assets/js/fastclick.js"></script>
    <script src="<?=base_url()?>assets/js/jquery.blockUI.js"></script>
    <script src="<?=base_url()?>assets/js/waves.js"></script>
    <script src="<?=base_url()?>assets/js/jquery.slimscroll.js"></script>
    <script src="<?=base_url()?>assets/js/jquery.scrollTo.min.js"></script>
    <script src="<?=base_url()?>assets/plugins/toastr/toastr.min.js"></script>
    <script src="<?=base_url()?>assets/plugins/switchery/switchery.min.js"></script>
    <script src="<?=base_url()?>assets/plugins/bootstrap-sweetalert/sweet-alert.min.js"></script>
</head>
<body class="fixed-left">
<div id="preloader">
    <div id="status">
        <div class="spinner">
            <div class="spinner-wrapper">
                <div class="rotator">
                    <div class="inner-spin"></div>
                    <div class="inner-spin"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="wrapper">
    <div class="topbar">
        <div class="topbar-left">
            <a href="<?=base_url()?>" class="logo"><span><?=$config['logotext']?></span><i class="mdi logo-text"><?=$config['logotext'][0]?></i></a>
        </div>
        <div class="navbar navbar-default hidden-print" role="navigation">
            <div class="container">
                <ul class="nav navbar-nav navbar-left">
                    <li>
                        <button class="button-menu-mobile open-left waves-effect waves-light">
                            <i class="mdi mdi-menu"></i>
                        </button>
                    </li>
                    <?php
                    if(isset($role['e964618e-29bd-4e17-91f2-1f42c0fa0488']['r'])) {
                        ?>
                        <li class="hidden-xs">
                            <form role="search" class="app-search" method="post" action="<?= base_url('laporan') ?>">
                                <input type="text" name="nomor_spt" autocomplete="off" placeholder="Nomor SPT" class="form-control" required>
                                <a class="hand-cursor" onclick="$('form.app-search').submit();" type="submit"><i class="fa fa-search"></i></a>
                                <button type="submit" class="hidden"></button>
                            </form>
                        </li>
                        <?php
                    }
                    ?>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a href="#" class="right-menu-item dropdown-toggle" data-toggle="dropdown">
                            <i class="mdi mdi-bell"></i>
                            <span class="badge up bg-primary <?=$user['notif']->num_rows()>0?'':'hidden'?>"><?=$user['notif']->num_rows()?></span>
                        </a>

                        <ul class="dropdown-menu dropdown-menu-right arrow-dropdown-menu arrow-menu-right dropdown-lg user-list notify-list">
                            <li>
                                <h5>Notifications</h5>
                            </li>
                            <?php
                            foreach($user['notif']->result_array() as $i=>$n):
                                if($i>5){
                                    break;
                                }
                            ?>
                            <li>
                                <a href="<?=base_url("notifications?id=$n[id]")?>" class="user-list-item">
                                    <div class="icon bg-info">
                                        <i class="mdi mdi-account"></i>
                                    </div>
                                    <div class="user-desc">
                                        <span class="name"><?=$n['message']?></span>
                                        <span class="time"><?=format_waktu($n['created_at'])?></span>
                                    </div>
                                </a>
                            </li>
                            <?php
                            endforeach;
                            ?>
                            <li class="all-msgs text-center">
                                <p class="m-0"><a href="<?=base_url('notifications')?>">See all Notification</a></p>
                            </li>
                        </ul>
                    </li>
                    <li class="dropdown user-box">
                        <a href="" class="dropdown-toggle waves-effect waves-light user-link" data-toggle="dropdown" aria-expanded="true">
                            <img src="<?=imageExist($user['avatar'])?>" alt="user-img" class="img-circle user-img">
                        </a>
                        <ul class="dropdown-menu dropdown-menu-right arrow-dropdown-menu arrow-menu-right user-list notify-list">
                            <li>
                                <h5>Hi, <?=$user['name']?></h5>
                            </li>
                            <li><a href="<?=base_url('profile')?>"><i class="ti-user m-r-5"></i> Profile</a></li>
                            <li><a href="<?=base_url('profile/settings')?>"><i class="ti-settings m-r-5"></i> Settings</a></li>
                            <li><a href="<?=base_url('gateway/lock')?>"><i class="ti-lock m-r-5"></i> Lock screen</a></li>
                            <li><a href="<?=base_url('gateway/logout')?>"><i class="ti-power-off m-r-5"></i> Logout</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="left side-menu hidden-print">
        <div class="sidebar-inner slimscrollleft">
            <!--- Sidemenu -->
            <div id="sidebar-menu">
                <div class="user-details">
                    <div class="overlay"></div>
                    <div class="text-center">
                        <img src="<?=imageExist($user['avatar'])?>" alt="<?=$user['name']?>" class="thumb-md img-circle">
                    </div>
                    <div class="user-info">
                        <div>
                            <a href="#setting-dropdown" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><?=$user['name']?> <span class="mdi mdi-menu-down"></span></a>
                        </div>
                    </div>
                </div>
                <div class="dropdown" id="setting-dropdown">
                    <ul class="dropdown-menu">
                        <li><a href="<?=base_url('profile')?>"><i class="ti-user m-r-5"></i> Profile</a></li>
                        <li><a href="<?=base_url('profile/settings')?>"><i class="ti-settings m-r-5"></i> Settings</a></li>
                        <li><a href="<?=base_url('gateway/lock')?>"><i class="ti-lock m-r-5"></i> Lock screen</a></li>
                        <li><a href="<?=base_url('gateway/logout')?>"><i class="ti-power-off m-r-5"></i> Logout</a></li>
                    </ul>
                </div>
                <?php
                $this->load->view('menu',@$sidebar);
                ?>
            </div>
            <!-- Sidebar -->
            <div class="clearfix"></div>

            <div class="help-box hidden">
                <h5 class="text-muted m-t-0">For Help ?</h5>
                <p class=""><span class="text-dark"><b>Email:</b></span> <br/> info@emjes.id</p>
            </div>
        </div>
    </div>
    <div class="content-page col-print-12">
        <div class="content">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="page-title-box">
                            <h1 class="page-title"><?=@$page['title']?></h1>
                            <ol class="breadcrumb p-0 m-0">
                                <li><a href="<?=base_url()?>">Home</a></li>
                                <?php
                                if(isset($page['breadcrumbs'])){
                                    foreach ($page['breadcrumbs'] as $b){
                                        echo "<li>$b</li>";
                                    }
                                }
                                ?>
                                <li class="active">
                                    <?=@$page['title']?>
                                </li>
                            </ol>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                <?php $this->load->view("private/$content[view]",@$content['data'])?>
            </div>
        </div>
        <footer class="footer text-right">
            <?=date('Y')." &copy; $config[company]"?>.
        </footer>
    </div>
</div>
<script src="<?=base_url()?>assets/js/jquery.core.js"></script>
<script src="<?=base_url()?>assets/js/jquery.app.js"></script>
<script src="<?=base_url()?>assets/js/jquery.bootcomplete.js"></script>
<script type="text/javascript">
    $('.ini-haram').remove();
    $('input[name=nomor_spt]','.app-search').bootcomplete({
        url:'<?=base_url('laporan/search')?>',
        minLength : 2,
        method: 'post'
    });
</script>
</body>
</html>