<section>
    <div class="container-alt">
        <div class="row">
            <div class="col-sm-12 text-center">
                <div class="home-wrapper">
                    <img src="<?=base_url()?>assets/images/animat-rocket-color.gif" alt="" height="180">
                    <h2 class="home-text text-uppercase text-danger"><?=$config['logotext']?> is Under Maintenance</h2>
                    <p class="text-muted">We're making the system more awesome.we'll be back shortly.</p>
                    <a href="<?=base_url('init/refresh')?>" class="btn w-md btn-bordered btn-inverse waves-effect waves-light">RELOAD</a>
                </div>
            </div>
        </div>
    </div>
</section>