<div class="container-alt">
    <div class="row">
        <div class="col-sm-12">
            <div class="wrapper-page">
                <div class="m-t-40 account-pages">
                    <div class="text-center account-logo-box">
                        <h2 class="text-uppercase logo"><?=$config['logotext']?></h2>
                    </div>
                    <div class="account-content">
                        <div class="text-center m-b-20">
                            <div class="m-b-20">
                                <img src="<?=imageExist($user['avatar'])?>" class="img-circle img-thumbnail thumb-lg" alt="<?=$user['name']?>">
                            </div>
                            <p class="text-muted m-b-0 font-13">Enter your password to access the <?=$user['name']?>.</p>
                        </div>
                        <form class="form-horizontal">
                            <div class="form-group">
                                <div class="col-xs-12">
                                    <input class="form-control" name="password" type="password" required="" placeholder="Password">
                                </div>
                            </div>
                            <p class="text-center" id="response-login"></p>
                            <div class="form-group account-btn text-center m-t-10">
                                <div class="col-xs-12">
                                    <button class="btn w-md btn-bordered btn-inverse waves-effect waves-light" type="submit">Log In</button>
                                </div>
                            </div>
                        </form>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="row m-t-50">
                    <div class="col-sm-12 text-center">
                        <p class="text-muted">Not you? return<a href="<?=base_url('gateway/logout')?>" class="text-primary m-l-5"><b>Sign In</b></a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(function () {
        $('input[type=password]').focus();
        $('form').on('submit', function (e) {
            e.preventDefault();
            var _form=this;
            $('button',_form).hide();
            $("#response-login").html('<img src="<?=base_url('assets/loading.gif')?>">');
            $.post("<?= base_url() ?>gateway/lock", $(this).serializeArray(), function (data, status) {
                if (data.status == 1) {
                    window.location.assign('<?= base_url('gateway') ?>');
                    $("#response-login").html('');
                } else {
                    $("#response-login").html("<code>"+data.message+"</code>");
                }
                $('button',_form).show();
            },'json');
        });
    });
</script>