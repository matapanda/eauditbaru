<section>
    <div class="container-alt">
        <div class="row">
            <div class="col-sm-12 text-center">
                <div class="home-wrapper">
                    <img src="<?=base_url()?>assets/images/animat-search-color.gif" alt="" height="180">
                    <h2 class="home-text text-uppercase text-danger">PAGE NOT FOUND</h2>
                    <p class="text-muted">It's looking like you may have taken a wrong turn.</p>
                    <a href="<?=base_url()?>" class="btn w-md btn-bordered btn-inverse waves-effect waves-light">HOME PAGE</a>
                </div>
            </div>
        </div>
    </div>
</section>