<div class="container-alt">
    <div class="row">
        <div class="col-sm-12">
            <div class="wrapper-page">
                <div class="m-t-40 account-pages">
                    <div class="text-center account-logo-box">
                        <img src="<?=base_url('assets/images/logo.png')?>">
                        <!-- <h2 class="text-uppercase logo" style="color: #000!important;color: #000!important;padding: 0;margin: 0;"><?=$config['logotext']?></h2> -->
                    </div>
                    <div class="account-content">
                        <form class="form-horizontal">
                            <div class="form-group ">
                                <div class="col-xs-12">
                                    <input class="form-control" type="text" name="username" required="" placeholder="Username">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-xs-12">
                                    <input class="form-control" type="password" name="password" required="" placeholder="Password">
                                </div>
                            </div>
                            <input type="checkbox" class="form-checkbox"> Show password
                            <br/>
                            <script type="text/javascript">
                                $(document).ready(function(){       
                                    $('.form-checkbox').click(function(){
                                        if($(this).is(':checked')){
                                            $('.form-control').attr('type','text');
                                        }else{
                                            $('.form-control').attr('type','password');
                                        }
                                    });
                                });
                            </script>
                            <div class="form-group text-center m-t-30">
                                <div class="col-sm-12">
                                    <a href="<?=base_url('gateway/forgot')?>" class="text-muted hidden"><i class="fa fa-lock m-r-5"></i> Forgot your password?</a>
                                </div>
                            </div>
                            <p class="text-center" id="response-login"></p>
                            <div class="form-group account-btn text-center m-t-10">
                                <div class="col-xs-12">
                                    <button class="btn w-md btn-bordered btn-inverse waves-effect waves-light" type="submit">Log In</button>
                                </div>
                            </div>
                        </form>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<style>
    body{/* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#549bfd+0,1531bd+100 */
        background: #549bfd; /* Old browsers */
        background: -moz-radial-gradient(center, ellipse cover, #549bfd 0%, #1531bd 100%); /* FF3.6-15 */
        background: -webkit-radial-gradient(center, ellipse cover, #549bfd 0%,#1531bd 100%); /* Chrome10-25,Safari5.1-6 */
        background: radial-gradient(ellipse at center, #549bfd 0%,#1531bd 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
        filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#549bfd', endColorstr='#1531bd',GradientType=1 ); /* IE6-9 fallback on horizontal gradient */
    }
    .account-logo-box {
        background-color: #FFF;
    }
</style>
<script type="text/javascript">
    $(function () {
        $('input[name=username]').focus();
        $('form').on('submit', function (e) {
            e.preventDefault();
            var _form=this;
            $('button',_form).hide();
            $("#response-login").html('<img src="<?=base_url('assets/loading.gif')?>">');
            $.post("<?= base_url() ?>gateway/login", $(this).serializeArray(), function (data, status) {
                if (data.status == 1) {
                    $('#preloader').show();
                    $("#response-login").html('');
                    window.location.assign('<?= base_url('gateway') ?>');
                } else {
                    $("#response-login").html("<code>"+data.message+"</code>");
                }
                $('button',_form).show();
            },'json');
        });
    });
</script>