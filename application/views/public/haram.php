<section>
    <div class="container-alt">
        <div class="row">
            <div class="col-sm-12 text-center">
                <div class="home-wrapper">
                    <img src="<?=base_url()?>assets/images/animat-customize-color.gif" alt="" height="180">
                    <h2 class="home-text text-uppercase text-danger">403 Forbidden Access</h2>
                    <p class="text-muted">Access to this resource on the server is denied!</p>
                    <a href="<?=base_url()?>" class="btn w-md btn-bordered btn-inverse waves-effect waves-light">HOME PAGE</a>
                </div>
            </div>
        </div>
    </div>
</section>