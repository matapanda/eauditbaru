<?php
/**
 * Created by PhpStorm.
 * User: GRADINdesign5
 * Date: 12/18/2015
 * Time: 9:31 AM
 */
function format_coa($kode){
    $kode=explode(".",$kode);
    if($kode[3]!=''){
        $kode[3]=".$kode[3]";
    }
    return implode("",$kode);
}
function check_csrf($new=true,$token=null){
    $CI = & get_instance();
    $csrf=$CI->session->userdata('csrf');
    if($new){
        if(is_array($csrf)){
            $c=count($csrf);
            $no=0;
            foreach ($csrf as $i=>$j){
                $no++;
                if (($c-10)>$no){
                    unset($csrf[$i]);
                }
            }
        }else{
            $csrf=array();
        }
        $token=md5(time());
        $csrf[]=$token;
        $CI->session->set_userdata('csrf',$csrf);
        return $token;
    }else{
        foreach ($csrf as $i=>$j){
            if($token==$j){
                unset($csrf[$i]);
                $CI->session->set_userdata('csrf',$csrf);
                return true;
            }
        }
        return false;
    }
}
function check_access($menu,$action='r'){
    $CI = & get_instance();
    $role=$CI->session->userdata('role');
    if(!isset($role[$menu][$action])){
        if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
            exit("<h1>403 Haram :)</h1><script>window.location='".base_url('welcome/haram')."';</script>");
        }else{
            redirect(base_url('welcome/haram'));
            exit("403 Haram :)");
        }
    }
}
function show_alert(){
    $CI = & get_instance();
    if($CI->session->flashdata('status_update')){
        $status_update = $CI->session->flashdata('status_update');
    }
    if(isset($status_update)){
        if(is_array($status_update)){
            echo "<div class=\"alert alert-$status_update[0] alert-dismissible fade in\" role=\"alert\">
                <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">
                    <span aria-hidden=\"true\">×</span>
                </button>
                <strong>$status_update[1]</strong>
            </div>";
        }elseif($status_update){
            echo "<div class=\"alert alert-success alert-dismissible fade in\" role=\"alert\">
                <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">
                    <span aria-hidden=\"true\">×</span>
                </button>
                <strong>Well done!</strong>
            </div>";
        }else{
            echo "<div class=\"alert alert-danger\" role=\"alert\">
                <strong>Sorry :(</strong> Change a few things up and try submitting again.
            </div>";
        }
    }
}
function is_authority($auth=null){
    if($auth==null){
        return "hidden ini-haram";
    }
}
function list_access($menu){
    $CI = & get_instance();
    $role=$CI->session->userdata('role');
    if(!isset($role[$menu]['r'])){
        exit("<h1>403 Haram :)</h1>");
    }else{
        return $role[$menu];
    }
}
function romanic_number($integer){
    $table = array('M'=>1000, 'CM'=>900, 'D'=>500, 'CD'=>400, 'C'=>100, 'XC'=>90, 'L'=>50, 'XL'=>40, 'X'=>10, 'IX'=>9, 'V'=>5, 'IV'=>4, 'I'=>1);
    $return = '';
    while($integer > 0){
        foreach($table as $rom=>$arb){
            if($integer >= $arb){
                $integer -= $arb;
                $return .= $rom;
                break;
            }
        }
    }
    return $return;
}
function htmlveryspecialchars($param,$tag="'"){
    $param=str_replace(PHP_EOL,"\\n",$param);
    $param=str_replace($tag,"\\$tag",$param);
    return htmlspecialchars($param);
}
function pure_money($param){
//    return str_replace(array('Rp','.',','),array('','','.'),$param);
    return $param;
}
function format_uang($param){
    if(!is_numeric($param)){
        $param=pure_money($param);
    }
    if($param<0){
        return "( ".number_format(abs($param),2,",",".")." )";
    }
    return number_format($param,2,",",".");
}
function getLabelStatus($status){
    if($status=='t' || $status==1){
        echo "<label class=\"hand-cursor btn-block label label-success\">Active</label>";
    }else{
        echo "<label class=\"hand-cursor btn-block label label-danger\">Non-Active</label>";
    }
}
function getLabelProcess($status,$cetak=true){
    if($status=='t' || $status==1){
        $r="<label class=\"hand-cursor btn-block label label-primary\">Sudah diproses</label>";
    }else{
        $r="<label class=\"hand-cursor btn-block label label-warning\">Belum diproses</label>";
    }
    if($cetak){
        echo $r;
    }else{
        return $r;
    }
}
function getLabelCLosed($status){
    if($status=='t' || $status==1){
        echo "<label class=\"hand-cursor btn-block label label-white\">Closed</label>";
    }else{
        echo "<label class=\"hand-cursor btn-block label label-primary\">Active</label>";
    }
}
function imageExist($img){
    if(strlen($img)>10){
        return $img;
    }else{
        return "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQEASABIAAD/2wBDAAMCAgMCAgMDAwMEAwMEBQgFBQQEBQoHBwYIDAoMDAsKCwsNDhIQDQ4RDgsLEBYQERMUFRUVDA8XGBYUGBIUFRT/wgALCAC6AM8BAREA/8QAGgABAAMBAQEAAAAAAAAAAAAAAAMEBQECCP/aAAgBAQAAAAH6pAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAIZXXOkMcvI7QBgVdTMs1eeprmZJWtwGlsAAKsd4AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAH//xAAeEAACAgMAAwEAAAAAAAAAAAACAwEEERITITCAMv/aAAgBAQABBQL4LE5I/Q05AAfs8z0WlnVaH9vUShZbX5GhnFn9SgItwQgsZxWFew1IHreIYlQzNbxCc4rmUC1cxBr0mJ25Uojf3WEdxVW0b8Wf/8QAKxAAAgECBAQEBwAAAAAAAAAAAQIRAAMSEyExBCIwQTIzYYBRUoGCkaHB/9oACAEBAAY/AvYW4KQBsfj0cSrj9BT248NM28CaVtp7U+kYTHS4nEJhf5XDm7JswfzVzfLxck1xf21bWOVl19aZSsrmQJ2FXwDADT9Ku3LWFTh0VTJpCjCY1UKf3VsMsz3OwpivjtPIrNuLJuvOuwriF9QRXEkjEMK6VdC7NaOijSrGV5081XPlzeanwPKnsBp1wJiDNZhaWiNBHst//8QAIhABAAICAAYDAQAAAAAAAAAAAQARITFBUWGBkaEQMIDB/9oACAEBAAE/IfwQtbi0X8PUq2sWc9SyrvEG/gRi1ubmotW+ZNE0jd7uHozUOkEkw3lA5y77g3r6dDATwiKSC41k79QCqniJ7kDfYm3ymdtbD3qjJuDAg/xKuURTnP1KmWApeOZghAnb4DcVptYnXKPVmBo4XUQC4qAQ31iMcgGuBKjCkRlXWAItTheDjcKo4/ZCsBrtge/3hnMA1Z3JfCbguvxb/9oACAEBAAAAEP8A/wD/AP8A/wD/AP8A/wD/AP8A/wD/AP8A/wD/AP8A/wD/AP8A/wD/AP8A/wD/AP8A/wD/AP8A/wD/AP8A/wD/AA2CP/h93/8A7/8A/wD/AP8A/wD/AP8A/wD/AP8A/wD/AP8A/wD/AP8A/wD/AP8A/wD/AP8A/wD/AP8A/wD/AP8A/wD/AP8A/wD/AP8A/wD/AP8A/8QAIxABAAICAgICAgMAAAAAAAAAAQARITFBUWFxMIEQgKHw8f/aAAgBAQABPxD9CAKoA5YFhA7WDZGicBEuZr1LlKXsXqcQ5XeIAERHk/FpSNNNOoBVAHLBAI2OkigVaDawWDQKii0t+IN1Z3bANV9wIg0Jul1ctYMtZCCVdtu2HMAihHk+EuqjFQHZ7lpZBbEGYeKH+xyKEi9+t5rX8z+q7Ir0Ts0t5ZyD9QgBmVGjIyfXuAnEbmqoNhxhb77l0sZoVL+Lse5n5Teg2QEee4X/AELbYy5F0SsQrSqpeWjC14zuIITFQyzlIWtc46mBxrCXYNLxtuCzxLWOVMh3LBKyK6NnA57uXuaIjZk1Wvq5SNSxBT3BFO89QkEFr+wpy8nzoNUbEThMJnUSu0gxtehbfP6W/wD/2Q==";
    }
}
function getLabelAccessRights($status){
    if($status=='t' || $status==1){
        echo "<label class=\"hand-cursor btn-block label label-primary\"><i class='glyphicon glyphicon-ok'></i></label>";
    }else{
        echo "<label class=\"hand-cursor btn-block label label-white\"><i class='glyphicon glyphicon-remove'></i></label>";
    }
}
function null_is_nol($param=''){
    return is_numeric($param)?$param:0;
}
function format_waktu($param,$reverse=false){
    if(strlen($param)<=0){
        return '-';
    }elseif($reverse){
		$waktu=explode(" ",$param);
		$tgl=explode("/",$waktu[0]);
		return "$tgl[2]-$tgl[1]-$tgl[0]".(isset($waktu[1])?" $waktu[1]":'');
	}else{
		$waktu=explode(" ",$param);
		$tgl=explode("-",$waktu[0]);
		return "$tgl[2]/$tgl[1]/$tgl[0]".(isset($waktu[1])?" $waktu[1]":'');		
	}
}
function encrypt_url($id){
    return rawurlencode(base64_encode($id));
}
function decrypt_url($id){
    return base64_decode(rawurldecode($id));
}
function generate_form($pre='',$form=null){
	if(isset($form)){
		$CI = & get_instance();
		return ($CI->session->userdata($pre.'form')==$form);
	}else{
		$form = md5(time());
		$CI = & get_instance();
		$CI->session->set_userdata($pre.'form', $form);
		return $form;
	}
}
function generate_url($id,$nama){
    $nama=str_replace(array(' ','/','&'),array('-','or','and'),strtolower($nama));
    return rawurlencode($id . "-" . $nama . ".html");
}
function reverse_url($url){
    return explode("-",rawurldecode(str_replace(".html",'',$url)));
}
function generate_uri($key=null,$value=null){
    $data=array();
    foreach($_GET as $d=>$k){
        $data[$d]=$k;
    }
    if(isset($key)){
        if(is_array($key)){
            foreach ($key as $i=>$k){
                if(isset($value[$i]) && $value[$i]!=null){
                    $data[$k]=rawurlencode($value[$i]);
                }else{
                    unset($data[$k]);
                }
            }
        }else{
            $data[$key]=rawurlencode($value);
            if($value==null){
                unset($data[$key]);
            }
        }
    }
    return implode('&', array_map(function ($v, $k) { return $k . '=' . $v; }, $data, array_keys($data)));
}
function summary_html($data,$max=250){
    $data=substr(strip_tags($data),0,max(strip_tags($data),$max));
    return $data."...";
}
function explode_waktu($status,$param){
    $waktu=explode(" ",$param);
    $tgl=explode("-",$waktu[0]);
    switch(strtolower($status)){
        case 'day':
            $return=$tgl[2];
            break;
        case 'month':
            $bulan=array('Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec');
            $return=$bulan[$tgl[1]-1];
            break;
        case 'year':
            $return=$tgl[0];
            break;
        case 'time':
            $return=$waktu[1];
            break;
        default:
            $return="";
            break;
    }
    return $return;
}
function gradinEncrypt($param)
{
    return rtrim(
        base64_encode(
            mcrypt_encrypt(
                MCRYPT_RIJNDAEL_256,
                md5("GRADIN-DESIGN-STUDIO-031-7451133"),
                $param,
                MCRYPT_MODE_ECB,
                mcrypt_create_iv(
                    mcrypt_get_iv_size(
                        MCRYPT_RIJNDAEL_256,
                        MCRYPT_MODE_ECB
                    ),
                    MCRYPT_RAND)
            )
        ), "\0"
    );
}

function gradinDecrypt($param)
{
    return rtrim(
        mcrypt_decrypt(
            MCRYPT_RIJNDAEL_256,
            md5("GRADIN-DESIGN-STUDIO-031-7451133"),
            base64_decode($param),
            MCRYPT_MODE_ECB,
            mcrypt_create_iv(
                mcrypt_get_iv_size(
                    MCRYPT_RIJNDAEL_256,
                    MCRYPT_MODE_ECB
                ),
                MCRYPT_RAND
            )
        ), "\0"
    );
}