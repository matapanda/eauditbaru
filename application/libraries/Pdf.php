<?php

/**
 * Created by PhpStorm.
 * User: GRADINdesign5
 * Date: 1/5/2016
 * Time: 3:29 PM
 */
class pdf{
    function __construct()
    {
        error_reporting(0);
    }

    function pdf()
    {
        $CI = & get_instance();
        log_message('Debug', 'mPDF class is loaded.');
    }
    function load($format='utf-8',$size='A4')
    {
        include_once APPPATH.'/third_party/mpdf/mpdf.php';
        return new mPDF($format,$size);
    }
}