<?php

/**
 * Created by PhpStorm.
 * User: gradindesign4
 * Date: 07/11/2016
 * Time: 10.23
 */
defined('BASEPATH') OR exit('No direct script access allowed');
class template extends CI_Model {
    private $view="";
    public function set_view($param){
        $this->view=$param."/";
    }
    public function guest($view,$data=array()){
        $data['config']=$this->session->userdata('config');
        $data['content']['view']=$view;
        $data['content']['data']=$data;
        $this->load->view('public',$data);
    }
    public function user($view,$input=array(),$options=array()){
        $data['config']=$this->session->userdata('config');
        $data['user']=$this->session->userdata('user');
        $data['role']=$this->session->userdata('role');
        if(!isset($data['user'])){
            redirect(base_url());
        }
        //
        $this->db->where('id_user',$data['user']['id']);
        $this->db->where('readed_at is null',null,false);
        $data['user']['notif']=$this->db->get('support.notif');
        $data['content']['view']=$this->view.$view;
        $data['content']['data']=$input;
        $data['page']=$options;
        $this->load->view('private',$data);
    }
    public function pagination($uri,$total,$perpage=50){
        $this->load->library('pagination');
        $config['base_url'] = base_url($uri);
        $config['total_rows'] = $total;
        $config['per_page'] = $perpage;
        $config['reuse_query_string'] = TRUE;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a class="btn-inverse">';
        $config['cur_tag_close'] = '</a></li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $this->pagination->initialize($config);
        return $this->pagination->create_links();
    }
}