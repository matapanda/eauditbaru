<?php

/**
 * Created by PhpStorm.
 * User: gradindesign4
 * Date: 10/11/2016
 * Time: 14.13
 */
defined('BASEPATH') OR exit('No direct script access allowed');
class Barang extends CI_Model
{
    private $user;
    public function __construct()
    {
        parent::__construct();
        $this->user=$this->session->userdata('user');
    }

    public function hapus($id){
        return $this->db->query("SELECT main.delete_mstitem(?,?)",array($id,$this->user['id']));
    }
    public function insert($data){
        return $this->db->query("SELECT main.insert_mstitem(?,?,?,?,?)",array(@$data['kode'],@$data['nama'],@$data['type']?'t':'f',@$data['type']?(@$data['hitung']?'t':'f'):'f',$this->user['id']));
    }
    public function update($id,$kode,$nama){
        return $this->db->query("SELECT main.update_mstitem_data(?,?,?,?)",array($id,$kode,$nama,$this->user['id']));
    }
    public function status($id){
        $data=$this->db->query("SELECT return as status FROM main.update_mstitem_status(?,?)",array($id,$this->user['id']))->row_array();
        getLabelStatus($data['status']);
    }
    public function hitung($id){
        $data=$this->db->query("SELECT return as status FROM main.update_mstitem_hitung(?,?)",array($id,$this->user['id']))->row_array();
        getLabelAccessRights($data['status']);
    }
    public function get($id){
        $this->db->where('id',$id);
        $this->db->where('deleted',false);
        return $this->db->get("main.mstitem")->row_array();
    }

    public function pembayaran_pembelian(){
        $postdata = file_get_contents("php://input");
        $data = json_decode($postdata, true);
        $form=array();
        foreach ($data as $d) {
            if($d['name']=='items[]'){
                $form['items'][]=json_decode($d['value'],TRUE);
            }else{
                $form[$d['name']]=$d['value'];
            }
        }
        $date=explode("/",$form['tanggal']);
        $this->db->where('extract(month from tanggal)=',$date[1]);
        $this->db->where('extract(year from tanggal)=',$date[2]);
        $increment=$this->db->get('pembelian.trpembayaransupplier')->num_rows();
        $nota=str_pad($increment+1,3,"0",STR_PAD_LEFT)."/PUU/".romanic_number($date[1])."/$date[2]";
        $this->load->library('uuid');
        $id = $this->uuid->v4();
        $this->db->trans_begin();
        $this->db->insert('pembelian.trpembayaransupplier',
            array(
                'id'=>$id,
                'nota'=>$nota,
                'tanggal'=>format_waktu($form['tanggal'],true),
                'metodepembayaran'=>@$form['metodepembayaran'],
                'jumlahbayar'=>null_is_nol($form['jumlahbayar']),
                'supplier'=>$form['id'],
                'note'=>$form['note'],
                'status'=>true,
                'request_by'=>strlen($form['request_by'])>30?$form['request_by']:null,
                'created_by'=>$this->user['id'],
                'created_at'=>date('Y-m-d H:i:s+07'),
            )
        );
        $jumlah=0;
        foreach ($form['items'] as $item){
            if($item['status']==1){
                $jumlah+=($item['harga']-$item['diskon']);
                $this->db->insert('pembelian.trpembayaransupplierd',
                    array(
                        'id'=>$id,
                        'tagihan'=>$item['tagihan'],
                        'nota'=>$item['nota'],
                        'harga'=>$item['harga'],
                        'ppn'=>$item['ppn'],
                        'pph'=>$item['pph'],
                        'diskon'=>$item['diskon'],
                        'created_by'=>$this->user['id'],
                        'created_at'=>date('Y-m-d H:i:s+07'),
                    )
                );
            }
        }

        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return array('success',"$nota");
        }
    }
    public function pembayaran_umk(){
        $postdata = file_get_contents("php://input");
        $data = json_decode($postdata, true);
        $form=array();
        foreach ($data as $d) {
            if($d['name']=='items[]'){
                $form['items'][]=json_decode($d['value'],TRUE);
            }else{
                $form[$d['name']]=$d['value'];
            }
        }
        $this->load->library('uuid');
        $kredit=explode(';',$form['metodepembayaran']);
        $JURNALID = $this->uuid->v4();

        $date=explode("/",$form['tanggal']);
        $this->db->where('extract(month from tanggal)=',$date[1]);
        $this->db->where('extract(year from tanggal)=',$date[2]);
        $this->db->where('type','0'.$kredit[1]);
        $increment=$this->db->get('akuntansi.jurnal')->num_rows();
        switch(strtolower($kredit[1])){
            case 'b':
                $kode="BKK";
                break;
            case 'c':
                $kode="BBK";
                break;
            case 'd':
                $kode="BCMK";
                break;
            default:
                exit('invalid data');
                break;
        }
        $nota=str_pad($increment+1,3,"0",STR_PAD_LEFT)."/$kode/".romanic_number($date[1])."/$date[2]";

        $this->db->trans_begin();
        $this->db->insert('akuntansi.jurnal',
            array(
                'id'=>$JURNALID,
                'nota'=>$nota,
                'type'=>'0'.$kredit[1],
                'mk'=>'2',
                'created_by'=>$this->user['id'],
                'created_at'=>date('Y-m-d H:i:s+07'),
                'deleted'=>FALSE,
                'tanggal'=>format_waktu($form['tanggal'],true),
                'note'=>$form['note'],
                'status'=>true,
            )
        );
        $jumlah=0;
        foreach ($form['items'] as $item){
            if($item['status']==1){
                $this->db->where('closed','f');
                $this->db->where('id',$item['uangmuka']);
                $this->db->update('main.truangmuka',array(
                    'debet'=>$item['coa'],
                    'metode_d'=>'',
                    'kredit'=>$kredit[0],
                    'metode_k'=>$kredit[1],
                    'jurnal_id'=>$JURNALID,
                    'jurnal_no'=>$jumlah,
                    'jurnal_nota'=>$nota,
                ));
                $this->db->insert('akuntansi.jurnald',
                    array(
                        'id'=>$JURNALID,
                        'no'=>++$jumlah,
                        'coa'=>$item['coa'],
                        'jumlah'=>$item['harga'],
                        'posisi'=>'D',
                        'note'=>"Pembayaran uang muka pembelian $item[nota]",
                        'nobukti'=>'',
                        'reftrans'=>'main.truangmuka',
                        'refid'=>$item['uangmuka'],
                        'refnota'=>$item['nota'],
                        'refid2'=>$item['uangmuka'],
                    )
                );
                $this->db->insert('akuntansi.jurnald',
                    array(
                        'id'=>$JURNALID,
                        'no'=>++$jumlah,
                        'coa'=>$kredit[0],
                        'jumlah'=>$item['harga'],
                        'posisi'=>'K',
                        'note'=>"Pembayaran uang muka pembelian $item[nota]",
                        'nobukti'=>'',
                        'reftrans'=>'main.truangmuka',
                        'refid'=>$item['uangmuka'],
                        'refnota'=>$item['nota'],
                        'refid2'=>$item['uangmuka'],
                    )
                );
            }
        }


        if ($this->db->trans_status() === FALSE || $jumlah==0){
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return array('success',"$nota");
        }
    }
    public function pembayaran_penjualan(){
        $postdata = file_get_contents("php://input");
        $data = json_decode($postdata, true);
        $form=array();
        foreach ($data as $d) {
            if($d['name']=='items[]'){
                $form['items'][]=json_decode($d['value'],TRUE);
            }else{
                $form[$d['name']]=$d['value'];
            }
        }
        $date=explode("/",$form['tanggal']);
        $this->db->where('extract(month from tanggal)=',$date[1]);
        $this->db->where('extract(year from tanggal)=',$date[2]);
        $increment=$this->db->get('penjualan.trpembayaran')->num_rows();
        $nota=str_pad($increment+1,3,"0",STR_PAD_LEFT)."/PPU/".romanic_number($date[1])."/$date[2]";
        $this->load->library('uuid');
        $id = $this->uuid->v4();
        $this->db->trans_begin();
        $this->db->insert('penjualan.trpembayaran',
            array(
                'id'=>$id,
                'nota'=>$nota,
                'tanggal'=>format_waktu($form['tanggal'],true),
                'metodepembayaran'=>@$form['metodepembayaran'],
                'jumlahbayar'=>null_is_nol($form['jumlahbayar']),
                'customer'=>$form['id'],
                'note'=>$form['note'],
                'status'=>true,
                'request_by'=>strlen($form['request_by'])>30?$form['request_by']:null,
                'created_by'=>$this->user['id'],
                'created_at'=>date('Y-m-d H:i:s+07'),
            )
        );
        foreach ($form['items'] as $item){
            if($item['status']==1){
                $this->db->insert('penjualan.trpembayarand',
                    array(
                        'id'=>$id,
                        'tagihan'=>$item['tagihan'],
                        'nota'=>$item['nota'],
                        'harga'=>$item['harga'],
                        'ppn'=>$item['ppn'],
                        'pph'=>$item['pph'],
                        'diskon'=>$item['diskon'],
                        'created_by'=>$this->user['id'],
                        'created_at'=>date('Y-m-d H:i:s+07'),
                    )
                );
            }
        }
        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return array('success',"$nota");
        }
    }
    public function tagihan_pembelian(){
        $postdata = file_get_contents("php://input");
        $data = json_decode($postdata, true);
        $form=array();
        foreach ($data as $d) {
            if($d['name']=='items[]'){
                $form['items'][]=json_decode($d['value'],TRUE);
            }else{
                $form[$d['name']]=$d['value'];
            }
        }
        $date=explode("/",$form['tanggal']);
        $this->db->where('extract(month from tanggal)=',$date[1]);
        $this->db->where('extract(year from tanggal)=',$date[2]);
        $increment=$this->db->get('pembelian.trtagihansupplier')->num_rows();
        $nota=str_pad($increment+1,3,"0",STR_PAD_LEFT)."/INVSUP/".romanic_number($date[1])."/$date[2]";
        $this->load->library('uuid');
        $id = $this->uuid->v4();
        $this->db->trans_begin();
        $this->db->insert('pembelian.trtagihansupplier',
            array(
                'id'=>$id,
                'nota'=>$nota,
                'tanggal'=>format_waktu($form['tanggal'],true),
                'request_by'=>strlen($form['request_by'])>30?$form['request_by']:null,
                'diskon'=>null_is_nol($form['tambahan']),
                'supplier'=>$form['id'],
                'status'=>true,
                'closed'=>false,
                'editable'=>true,
                'created_by'=>$this->user['id'],
                'created_at'=>date('Y-m-d H:i:s+07'),
            )
        );
        foreach ($form['items'] as $item){
            if($item['status']==1){
                $this->db->insert('pembelian.trtagihansupplierd',
                    array(
                        'id'=>$id,
                        'refname'=>$item['refname'],
                        'refid'=>$item['refid'],
                        'nota'=>$item['nota'],
                        'tanggal'=>format_waktu($item['tanggal'],true),
                        'harga'=>$item['harga'],
                        'dpp'=>$item['dpp'],
                        'ppn'=>$item['ppn'],
                        'pph'=>$item['pph'],
                        'diskon'=>$item['diskon'],
                        'orderid'=>@$item['orderid'],
                        'orderno'=>$item['orderno'],
                        'orderdate'=>format_waktu($item['orderdate'],true),
                        'created_by'=>$this->user['id'],
                        'created_at'=>date('Y-m-d H:i:s+07'),
                    )
                );
            }
        }
        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return array('success',"$nota");
        }
    }
    public function tagihan_penjualan(){
        $postdata = file_get_contents("php://input");
        $data = json_decode($postdata, true);
        $form=array();
        foreach ($data as $d) {
            if($d['name']=='items[]'){
                $form['items'][]=json_decode($d['value'],TRUE);
            }else{
                $form[$d['name']]=$d['value'];
            }
        }
        $date=explode("/",$form['tanggal']);
        $this->db->where('extract(month from tanggal)=',$date[1]);
        $this->db->where('extract(year from tanggal)=',$date[2]);
        $increment=$this->db->get('penjualan.trtagihan')->num_rows();
        $nota=str_pad($increment+1,3,"0",STR_PAD_LEFT)."/INV/".romanic_number($date[1])."/$date[2]";
        $this->load->library('uuid');
        $id = $this->uuid->v4();
        $this->db->trans_begin();
        $this->db->insert('penjualan.trtagihan',
            array(
                'id'=>$id,
                'nota'=>$nota,
                'tanggal'=>format_waktu($form['tanggal'],true),
                'request_by'=>strlen($form['request_by'])>30?$form['request_by']:null,
                'diskon'=>null_is_nol($form['tambahan']),
                'customer'=>$form['id'],
                'status'=>true,
                'closed'=>false,
                'editable'=>true,
                'created_by'=>$this->user['id'],
                'created_at'=>date('Y-m-d H:i:s+07'),
            )
        );
        foreach ($form['items'] as $item){
            if($item['status']==1){
                $this->db->insert('penjualan.trtagihand',
                    array(
                        'id'=>$id,
                        'refname'=>$item['refname'],
                        'refid'=>$item['refid'],
                        'nota'=>$item['nota'],
                        'tanggal'=>format_waktu($item['tanggal'],true),
                        'harga'=>$item['harga']-$item['pph']-$item['diskon'],
                        'dpp'=>$item['dpp'],
                        'ppn'=>$item['ppn'],
                        'pph'=>$item['pph'],
                        'diskon'=>$item['diskon'],
                        'orderid'=>$item['refid'],
                        'orderno'=>$item['orderno'],
                        'orderdate'=>format_waktu($item['orderdate'],true),
                        'created_by'=>$this->user['id'],
                        'created_at'=>date('Y-m-d H:i:s+07'),
                    )
                );
            }
        }
        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return array('success',"$nota");
        }
    }
    public function retur_pembelian(){
        $postdata = file_get_contents("php://input");
        $data = json_decode($postdata, true);
        $form=array();
        foreach ($data as $d) {
            if($d['name']=='items[]'){
                $form['items'][]=json_decode($d['value'],TRUE);
            }else{
                $form[$d['name']]=$d['value'];
            }
        }
        $date=explode("/",$form['tanggal']);
        $this->db->where('extract(month from tanggal)=',$date[1]);
        $this->db->where('extract(year from tanggal)=',$date[2]);
        $increment=$this->db->get('pembelian.trretur')->num_rows();
        $nota=str_pad($increment+1,3,"0",STR_PAD_LEFT)."/RPB/".romanic_number($date[1])."/$date[2]";
        $this->load->library('uuid');
        $id = $this->uuid->v4();
        $this->db->trans_begin();
        $this->db->insert('pembelian.trretur',
            array(
                'id'=>$id,
                'nota'=>$nota,
                'tanggal'=>format_waktu($form['tanggal'],true),
                'request_by'=>strlen($form['request_by'])>30?$form['request_by']:null,
                'penerimaan'=>$form['id'],
                'status'=>true,
                'closed'=>true,
                'editable'=>false,
                'cancel'=>false,
                'created_by'=>$this->user['id'],
                'created_at'=>date('Y-m-d H:i:s+07'),
            )
        );
        foreach ($form['items'] as $item){
            if($item['sekarang']>0){
                $this->db->insert('pembelian.trreturd',
                    array(
                        'id'=>$id,
                        'mstitem'=>$item['mstitem'],
                        'permintaan'=>strlen($item['permintaan'])>30?$item['permintaan']:NULL,
                        'nota'=>$item['nota'],
                        'nama'=>$item['barangjasa'],
                        'jumlah'=>$item['sekarang'],
                        'note'=>$item['note'],
                        'created_by'=>$this->user['id'],
                        'created_at'=>date('Y-m-d H:i:s+07'),
                    )
                );
                $this->db->order_by('created_at','asc');
                $this->db->where('jumlah>keluar',null,false);
                $this->db->select('id,harga,(jumlah-keluar) as sisa');
                $fifo=$this->db->get('main.mstfifo')->result_array();
                $sisaretur=$item['sekarang'];
                foreach ($fifo as $f){
                    if($sisaretur==0){
                        break;
                    }
                    if($sisaretur>$f['sisa']){
                        $sisaretur-=$f['sisa'];
                        $jumlah=$f['sisa'];
                    }else{
                        $sisaretur=0;
                        $jumlah=$sisaretur;
                    }
                    $this->db->where('id', $f['id']);
                    $this->db->set('keluar', "keluar+$jumlah", FALSE);
                    $this->db->update('main.mstfifo');
                    $this->db->insert('main.trstok',
                        array(
                            'id'=>$this->uuid->v4(),
                            'mstitem'=>$item['mstitem'],
                            'jumlah'=>(0-$item['sekarang']),
                            'reftrans'=>'pembelian.trreturd',
                            'refid'=>$id,
                            'refnota'=>$item['nota'],
                            'harga'=>pure_money($f['harga']*$jumlah),
                            'fifo'=>$f['id'],
                            'created_at'=>date('Y-m-d H:i:s+07'),
                            'type'=>'K',
                            'tanggal'=>format_waktu($form['tanggal'],true),
                        )
                    );
                }
            }
        }
        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return array('success',"$nota");
        }
    }
    public function retur_penjualan(){
        $postdata = file_get_contents("php://input");
        $data = json_decode($postdata, true);
        $form=array();
        foreach ($data as $d) {
            if($d['name']=='items[]'){
                $form['items'][]=json_decode($d['value'],TRUE);
            }else{
                $form[$d['name']]=$d['value'];
            }
        }
        $date=explode("/",$form['tanggal']);
        $this->db->where('extract(month from tanggal)=',$date[1]);
        $this->db->where('extract(year from tanggal)=',$date[2]);
        $increment=$this->db->get('penjualan.trretur')->num_rows();
        $nota=str_pad($increment+1,3,"0",STR_PAD_LEFT)."/RPJ/".romanic_number($date[1])."/$date[2]";
        $this->load->library('uuid');
        $id = $this->uuid->v4();
        $this->db->trans_begin();
        $this->db->insert('penjualan.trretur',
            array(
                'id'=>$id,
                'nota'=>$nota,
                'tanggal'=>format_waktu($form['tanggal'],true),
                'request_by'=>strlen($form['request_by'])>30?$form['request_by']:null,
                'pengiriman'=>$form['id'],
                'status'=>true,
                'closed'=>false,
                'editable'=>true,
                'cancel'=>false,
                'created_by'=>$this->user['id'],
                'created_at'=>date('Y-m-d H:i:s+07'),
            )
        );
        foreach ($form['items'] as $item){
            if($item['sekarang']>0){
                $this->db->insert('penjualan.trreturd',
                    array(
                        'id'=>$id,
                        'mstitem'=>$item['mstitem'],
                        'id2'=>strlen($item['permintaan'])>30?$item['permintaan']:NULL,
                        'nota'=>$item['nota'],
                        'nama'=>$item['barangjasa'],
                        'jumlah'=>$item['sekarang'],
                        'note'=>$item['note'],
                        'created_by'=>$this->user['id'],
                        'created_at'=>date('Y-m-d H:i:s+07'),
                    )
                );
            }
        }
        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return array('success',"$nota");
        }
    }
    public function penerimaan_pembelian(){
        $postdata = file_get_contents("php://input");
        $data = json_decode($postdata, true);
        $form=array();
        foreach ($data as $d) {
            if($d['name']=='items[]'){
                $form['items'][]=json_decode($d['value'],TRUE);
            }else{
                $form[$d['name']]=$d['value'];
            }
        }
        $date=explode("/",$form['tanggal']);
        $this->db->where('extract(month from tanggal)=',$date[1]);
        $this->db->where('extract(year from tanggal)=',$date[2]);
        $increment=$this->db->get('pembelian.vpenerimaanbarang')->num_rows();
        $nota=str_pad($increment+1,3,"0",STR_PAD_LEFT)."/FPB/".romanic_number($date[1])."/$date[2]";
        $this->load->library('uuid');
        $id = $this->uuid->v4();
        $this->db->trans_begin();
        $this->db->insert('pembelian.trpenerimaan',
            array(
                'id'=>$id,
                'nota'=>$nota,
                'tanggal'=>format_waktu($form['tanggal'],true),
                'pembelian'=>$form['id'],
                'penerima'=>strlen($form['request_by'])>30?$form['request_by']:null,
                'status'=>true,
                'closed'=>false,
                'editable'=>true,
                'created_by'=>$this->user['id'],
                'created_at'=>date('Y-m-d H:i:s+07'),
            )
        );
        $jurnal=array();
        $jurnal['jumlah']=array(1=>0,2=>0,3=>0,4=>0,5=>0);
        foreach ($form['items'] as $item){
            if($item['sekarang']>0){
                $this->db->where('id',$form['id']);
                $this->db->where('permintaan',$item['permintaan']);
                $this->db->where('mstitem',$item['mstitem']);
                $this->db->select("(dpp/jumlah*$item[sekarang]) as dpp,(dpp/jumlah) as satuan,(ppn/jumlah*$item[sekarang]) as ppn,(pph22/jumlah*$item[sekarang]) as pph22");
                $pembelian=$this->db->get('pembelian.trpembelianorderd')->row_array();
                $jurnal['jumlah'][1]+=pure_money($pembelian['dpp']);
                $jurnal['jumlah'][2]+=pure_money($pembelian['ppn']);
                $jurnal['jumlah'][3]+=pure_money($pembelian['pph22']);
                $this->db->insert('pembelian.trpenerimaand',
                    array(
                        'id'=>$id,
                        'nota'=>$item['nota'],
                        'permintaan'=>strlen($item['permintaan'])>30?$item['permintaan']:NULL,
                        'mstitem'=>$item['mstitem'],
                        'nama'=>$item['barangjasa'],
                        'note'=>$item['note'],
                        'jumlah'=>$item['sekarang'],
                        'created_by'=>$this->user['id'],
                        'created_at'=>date('Y-m-d H:i:s+07'),
                    )
                );
                $fifo = $this->uuid->v4();
                $this->db->insert('main.mstfifo',
                    array(
                        'id'=>$fifo,
                        'mstitem'=>$item['mstitem'],
                        'jumlah'=>$item['sekarang'],
                        'keluar'=>0,
                        'created_at'=>date('Y-m-d H:i:s+07'),
                        'harga'=>pure_money($pembelian['satuan']),
                    )
                );
                $this->db->insert('main.trstok',
                    array(
                        'id'=>$this->uuid->v4(),
                        'mstitem'=>$item['mstitem'],
                        'jumlah'=>$item['sekarang'],
                        'reftrans'=>'pembelian.trpenerimaan',
                        'refid'=>$id,
                        'refnota'=>$item['nota'],
                        'harga'=>pure_money($pembelian['dpp']),
                        'fifo'=>$fifo,
                        'created_at'=>date('Y-m-d H:i:s+07'),
                        'type'=>'M',
                        'tanggal'=>format_waktu($form['tanggal'],true),
                    )
                );
            }
        }
        //jurnal start
        $JURNALCONFIG='3c6246e7-899b-4c17-9e2d-9f417a7176fd';
        $JURNALID = $this->uuid->v4();
        $this->db->insert('akuntansi.jurnal',
            array(
                'id'=>$JURNALID,
                'nota'=>$nota,
                'type'=>'1a',
                'mk'=>'0',
                'created_by'=>$this->user['id'],
                'created_at'=>date('Y-m-d H:i:s+07'),
                'deleted'=>FALSE,
                'tanggal'=>format_waktu($form['tanggal'],true),
                'note'=>'',
                'status'=>true,
            )
        );
        $this->db->where('id',$form['id']);
        $opb=$this->db->get('pembelian.trpembelianorder')->row_array();
        $jurnal['jumlah'][1]=$jurnal['jumlah'][1]-$jurnal['jumlah'][2];
        $jurnal['jumlah'][4]=$jurnal['jumlah'][1]+$jurnal['jumlah'][2];
        $jurnal['jumlah'][5]=$jurnal['jumlah'][3];
        $jurnal['note']=array(
            1=>"Penerimaan barang $nota dari pembelian $opb[nota]",
            2=>"Penerimaan barang $nota dari pembelian $opb[nota]",
            3=>"Penerimaan barang $nota dari pembelian $opb[nota]",
            4=>"Penerimaan barang $nota dari pembelian $opb[nota]",
            5=>"Penerimaan barang $nota dari pembelian $opb[nota]",
        );
        for($i=1;$i<=5;$i++){
            $this->db->where('id',$JURNALCONFIG);
            $this->db->where('no',$i);
            $jurnald=$this->db->get('akuntansi.configd')->row_array();
            if($jurnal['jumlah'][$i]<>0){
                $this->db->insert('akuntansi.jurnald',
                    array(
                        'id'=>$JURNALID,
                        'no'=>$i,
                        'coa'=>$jurnald['coa'],
                        'jumlah'=>$jurnal['jumlah'][$i],
                        'posisi'=>$jurnald['dk'],
                        'note'=>$jurnal['note'][$i],
                        'nobukti'=>'',
                        'reftrans'=>'pembelian.trpenerimaand',
                        'refid'=>$id,
                        'refnota'=>$opb['nota'],
                        'refid2'=>$form['id'],
                    )
                );
            }
        }
        //jurnal end
        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return array('success',"$nota");
        }
    }
    public function penerimaan_penjualan(){
        $postdata = file_get_contents("php://input");
        $data = json_decode($postdata, true);
        $form=array();
        foreach ($data as $d) {
            if($d['name']=='items[]'){
                $form['items'][]=json_decode($d['value'],TRUE);
            }else{
                $form[$d['name']]=$d['value'];
            }
        }
        $date=explode("/",$form['tanggal']);
        $this->db->where('extract(month from tanggal)=',$date[1]);
        $this->db->where('extract(year from tanggal)=',$date[2]);
        $increment=$this->db->get('penjualan.trsuratjalan')->num_rows();
        $nota=str_pad($increment+1,3,"0",STR_PAD_LEFT)."/SJ/".romanic_number($date[1])."/$date[2]";
        $this->load->library('uuid');
        $id = $this->uuid->v4();
        $this->db->trans_begin();
        $this->db->insert('penjualan.trsuratjalan',
            array(
                'id'=>$id,
                'nota'=>$nota,
                'tanggal'=>format_waktu($form['tanggal'],true),
                'penjualan'=>$form['id'],
                'pengirim'=>strlen($form['request_by'])>30?$form['request_by']:null,
                'status'=>true,
                'closed'=>false,
                'editable'=>true,
                'created_by'=>$this->user['id'],
                'created_at'=>date('Y-m-d H:i:s+07'),
            )
        );
        foreach ($form['items'] as $item){
            if($item['sekarang']>0){
                $this->db->insert('penjualan.trsuratjaland',
                    array(
                        'id'=>$id,
                        'id2'=>strlen($item['permintaan'])>30?$item['permintaan']:NULL,
                        'nota'=>$nota,
                        'mstitem'=>$item['mstitem'],
                        'nama'=>$item['barangjasa'],
                        'note'=>$item['note'],
                        'jumlah'=>$item['sekarang'],
                        'created_by'=>$this->user['id'],
                        'created_at'=>date('Y-m-d H:i:s+07'),
                    )
                );
            }
        }
        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return array('success',"$nota");
        }
    }
    public function penyelesaian_pembelian(){
        $postdata = file_get_contents("php://input");
        $data = json_decode($postdata, true);
        $form=array();
        foreach ($data as $d) {
            if($d['name']=='items[]'){
                $form['items'][]=json_decode($d['value'],TRUE);
            }else{
                $form[$d['name']]=$d['value'];
            }
        }
        $date=explode("/",$form['tanggal']);
        $this->db->where('extract(month from tanggal)=',$date[1]);
        $this->db->where('extract(year from tanggal)=',$date[2]);
        $increment=$this->db->get('pembelian.vpenyelesaianjasa')->num_rows();
        $nota=str_pad($increment+1,3,"0",STR_PAD_LEFT)."/FPJ/".romanic_number($date[1])."/$date[2]";
        $this->load->library('uuid');
        $id = $this->uuid->v4();
        $this->db->trans_begin();
        $this->db->insert('pembelian.trpenerimaan',
            array(
                'id'=>$id,
                'nota'=>$nota,
                'tanggal'=>format_waktu($form['tanggal'],true),
                'pembelian'=>$form['id'],
                'penerima'=>strlen($form['request_by'])>30?$form['request_by']:null,
                'status'=>true,
                'closed'=>false,
                'editable'=>true,
                'created_by'=>$this->user['id'],
                'created_at'=>date('Y-m-d H:i:s+07'),
            )
        );
        $count=0;
        foreach ($form['items'] as $item){
            if($item['sekarang']>0){
                $count++;
                $this->db->insert('pembelian.trpenerimaand',
                    array(
                        'id'=>$id,
                        'nota'=>$item['nota'],
                        'permintaan'=>strlen($item['permintaan'])>30?$item['permintaan']:NULL,
                        'mstitem'=>$item['mstitem'],
                        'nama'=>$item['barangjasa'],
                        'note'=>$item['note'],
                        'jumlah'=>$item['sekarang'],
                        'beban'=>isset($item['beban'])?$item['beban']:null,
                        'created_by'=>$this->user['id'],
                        'created_at'=>date('Y-m-d H:i:s+07'),
                    )
                );
            }
        }
        if ($this->db->trans_status() === FALSE || $count==0){
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return array('success',"$nota");
        }
    }
    public function order_pembelian_update(){
        $postdata = file_get_contents("php://input");
        $data = json_decode($postdata, true);
        $form=array();
        foreach ($data as $d) {
            if($d['name']=='items[]'){
                $form['items'][]=json_decode($d['value'],TRUE);
            }else{
                $form[$d['name']]=$d['value'];
            }
        }
        $id = $form['id'];
        $this->db->where('id',$id);
        $raw=$this->db->get('pembelian.trpembelianorderd')->result_array();
        $olditem=array();
        foreach ($raw as $i) {
            $olditem[(strlen($i['permintaan'])>30?"$i[permintaan]_":"").$i['mstitem']]=$i;
        }
        $this->db->trans_begin();
        $this->db->where('id',$id);
        $this->db->update('pembelian.trpembelianorder',
            array(
                'uangmuka'=>null_is_nol(@$form['uangmuka']),
                'request_by'=>strlen($form['request_by'])>30?$form['request_by']:null,
                'modified_by'=>$this->user['id'],
                'modified_at'=>date('Y-m-d H:i:s+07'),
                'supplier'=>$form['supplier'],
            )
        );
        foreach ($form['items'] as $item){
            if(isset($olditem[$item['mstitem']])) {
                $this->db->where('status',false);
                $this->db->where('id',$id);
                $this->db->where('mstitem',$olditem[$item['mstitem']]['mstitem']);
                $this->db->update('pembelian.trpembelianorderd',
                    array(
                        'jumlah'=>$item['qty'],
                        'harga'=>$item['harga'],
                        'dpp'=>$item['dpp'],
                        'ppn'=>$item['ppn'],
                        'diskon'=>$item['diskon'],
                        'modified_by'=>$this->user['id'],
                        'modified_at'=>date('Y-m-d H:i:s+07')
                    )
                );
                unset($olditem[$item['mstitem']]);
            }else{
                $mstitem=explode('_',$item['mstitem']);
                $this->db->where('id',isset($mstitem[1])?$mstitem[1]:$mstitem[0]);
                $data=$this->db->get('main.mstitem')->row_array();
                $this->db->insert('pembelian.trpembelianorderd',
                    array(
                        'id'=>$id,
                        'nota'=>$item['nota'],
                        'mstitem'=>$data['id'],
                        'type'=>$data['type'],
                        'permintaan'=>isset($mstitem[1])?$mstitem[0]:NULL,
                        'nama'=>$data['nama'],
                        'jumlah'=>$item['qty'],
                        'harga'=>$item['harga'],
                        'dpp'=>$item['dpp'],
                        'ppn'=>$item['ppn'],
                        'ppnpersen'=>$item['ppnpersen'],
                        'pph21'=>$item['pph21'],
                        'pph22'=>$item['pph22'],
                        'pph23'=>$item['pph23'],
                        'pph24'=>$item['pph24'],
                        'pph25'=>$item['pph25'],
                        'pph26'=>$item['pph26'],
                        'pph21persen'=>$item['pph21persen'],
                        'pph22persen'=>$item['pph22persen'],
                        'pph23persen'=>$item['pph23persen'],
                        'pph24persen'=>$item['pph24persen'],
                        'pph25persen'=>$item['pph25persen'],
                        'pph26persen'=>$item['pph26persen'],
                        'diskon'=>$item['diskon'],
                        'created_by'=>$this->user['id'],
                        'created_at'=>date('Y-m-d H:i:s+07'),
                        'status'=>false,
                    )
                );
            }
        }
        foreach ($olditem as $i){
            $this->db->where('status',false);
            $this->db->where('id',$i['id']);
            $this->db->where('mstitem',$i['mstitem']);
            $this->db->where('permintaan',$i['permintaan']);
            $this->db->update('pembelian.trpembelianorderd',
                array(
                    'modified_by'=>$this->user['id'],
                    'modified_at'=>date('Y-m-d H:i:s+07')
                )
            );
            $this->db->where('status',false);
            $this->db->where('id',$i['id']);
            $this->db->where('mstitem',$i['mstitem']);
            $this->db->where('permintaan',$i['permintaan']);
            $this->db->delete('pembelian.trpembelianorderd');
        }
        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }
    public function order_pembelian(){
        $postdata = file_get_contents("php://input");
        $data = json_decode($postdata, true);
        $form=array();
        foreach ($data as $d) {
            if($d['name']=='items[]'){
                $form['items'][]=json_decode($d['value'],TRUE);
            }else{
                $form[$d['name']]=$d['value'];
            }
        }
        $date=explode("/",$form['tanggal']);
        $this->db->where('extract(month from tanggal)=',$date[1]);
        $this->db->where('extract(year from tanggal)=',$date[2]);
        $increment=$this->db->get('pembelian.trpembelianorder')->num_rows();
        $nota=str_pad($increment+1,3,"0",STR_PAD_LEFT)."/OPB/".romanic_number($date[1])."/$date[2]";
        $this->load->library('uuid');
        $id = $this->uuid->v4();
        $this->db->trans_begin();
        $mp=explode(';',@$form['metodepembayaran']);
        $this->db->insert('pembelian.trpembelianorder',
            array(
                'id'=>$id,
                'nota'=>$nota,
                'tanggal'=>format_waktu($form['tanggal'],true),
                'uangmuka'=>null_is_nol(@$form['uangmuka']),
                'metodepembayaran'=>NULL,
                'request_by'=>strlen($form['request_by'])>30?$form['request_by']:null,
                'status'=>true,
                'closed'=>false,
                'editable'=>true,
                'created_by'=>$this->user['id'],
                'created_at'=>date('Y-m-d H:i:s+07'),
                'supplier'=>$form['supplier'],
                'jurnalpembayaran'=>isset($mp[1])?$mp[1]:null,
            )
        );
        foreach ($form['items'] as $item){
            $mstitem=explode('_',$item['mstitem']);
            $this->db->where('id',isset($mstitem[1])?$mstitem[1]:$mstitem[0]);
            $data=$this->db->get('main.mstitem')->row_array();
            $this->db->insert('pembelian.trpembelianorderd',
                array(
                    'id'=>$id,
                    'nota'=>$item['nota'],
                    'mstitem'=>$data['id'],
                    'type'=>$data['type'],
                    'permintaan'=>isset($mstitem[1])?$mstitem[0]:NULL,
                    'nama'=>$data['nama'],
                    'jumlah'=>$item['qty'],
                    'harga'=>$item['subtotal'],
                    'hargaawal'=>$item['harga'],
                    'dpp'=>$item['dpp'],
                    'ppn'=>$item['ppn'],
                    'ppnpersen'=>null_is_nol(@$item['ppnpersen']),
                    'pph21'=>$item['pph21'],
                    'pph22'=>$item['pph22'],
                    'pph23'=>$item['pph23'],
                    'pph24'=>$item['pph24'],
                    'pph25'=>$item['pph25'],
                    'pph26'=>$item['pph26'],
                    'pph21persen'=>null_is_nol(@$item['pph21persen']),
                    'pph22persen'=>null_is_nol(@$item['pph22persen']),
                    'pph23persen'=>null_is_nol(@$item['pph23persen']),
                    'pph24persen'=>null_is_nol(@$item['pph24persen']),
                    'pph25persen'=>null_is_nol(@$item['pph25persen']),
                    'pph26persen'=>null_is_nol(@$item['pph26persen']),
                    'diskon'=>$item['diskon'],
                    'created_by'=>$this->user['id'],
                    'created_at'=>date('Y-m-d H:i:s+07'),
                    'status'=>false,
                )
            );
        }
        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return array('success',"$nota");
        }
    }
    public function order_penjualan(){
        $postdata = file_get_contents("php://input");
        $data = json_decode($postdata, true);
        $form=array();
        foreach ($data as $d) {
            if($d['name']=='items[]'){
                $form['items'][]=json_decode($d['value'],TRUE);
            }else{
                $form[$d['name']]=$d['value'];
            }
        }
        $date=explode("/",$form['tanggal']);
        $this->db->where('extract(month from tanggal)=',$date[1]);
        $this->db->where('extract(year from tanggal)=',$date[2]);
        $increment=$this->db->get('penjualan.trorder')->num_rows();
        $nota=str_pad($increment+1,3,"0",STR_PAD_LEFT)."/OPJ/".romanic_number($date[1])."/$date[2]";
        $this->load->library('uuid');
        $id = $this->uuid->v4();
        $this->db->trans_begin();
        $this->db->insert('penjualan.trorder',
            array(
                'id'=>$id,
                'nota'=>$nota,
                'tanggal'=>format_waktu($form['tanggal'],true),
                'uangmuka'=>null_is_nol(@$form['uangmuka']),
                'metodepembayaran'=>NULL,
                'request_by'=>strlen($form['request_by'])>30?$form['request_by']:null,
                'status'=>true,
                'closed'=>false,
                'editable'=>true,
                'barangjasa'=>$form['type']=='barang'?true:false,
                'created_by'=>$this->user['id'],
                'created_at'=>date('Y-m-d H:i:s+07'),
                'customer'=>$form['customer'],
            )
        );
        foreach ($form['items'] as $item){
            $this->db->where('id',$item['mstitem']);
            $data=$this->db->get('main.mstitem')->row_array();
            $this->db->insert('penjualan.trorderd',
                array(
                    'id'=>$id,
                    'id2'=>$this->uuid->v4(),
                    'mstitem'=>$data['id'],
                    'type'=>$data['type'],
                    'nama'=>$data['nama'],
                    'jumlah'=>$item['qty'],
                    'harga'=>$item['harga'],
                    'dpp'=>$item['dpp'],
                    'ppn'=>$item['ppn'],
                    'ppnpersen'=>null_is_nol(@$item['ppnpersen']),
                    'pph21'=>$item['pph21'],
                    'pph22'=>$item['pph22'],
                    'pph23'=>$item['pph23'],
                    'pph24'=>$item['pph24'],
                    'pph25'=>$item['pph25'],
                    'pph26'=>$item['pph26'],
                    'pph21persen'=>null_is_nol(@$item['pph21persen']),
                    'pph22persen'=>null_is_nol(@$item['pph22persen']),
                    'pph23persen'=>null_is_nol(@$item['pph23persen']),
                    'pph24persen'=>null_is_nol(@$item['pph24persen']),
                    'pph25persen'=>null_is_nol(@$item['pph25persen']),
                    'pph26persen'=>null_is_nol(@$item['pph26persen']),
                    'diskon'=>$item['diskon'],
                    'created_by'=>$this->user['id'],
                    'created_at'=>date('Y-m-d H:i:s+07'),
                    'status'=>false,
                )
            );
        }
        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return array('success',"$nota");
        }
    }
    public function permintaan_pembelian_update($id,$post){
        $this->db->where('id',$id);
        $raw=$this->db->get('pembelian.trpermintaanorderd')->result_array();
        $olditem=array();
        foreach ($raw as $i) {
            $olditem[$i['mstitem']]=$i;
        }
        $this->db->trans_begin();
        $this->db->where('id',$id);
        $this->db->update('pembelian.trpermintaanorder',
            array(
                'request_by'=>strlen($post['request_by'])>30?$post['request_by']:null,
                'request_unit'=>strlen($post['request_unit'])>30?$post['request_unit']:null,
                'modified_by'=>$this->user['id'],
                'modified_at'=>date('Y-m-d H:i:s+07'),
            )
        );
        foreach ($post['items'] as $v){
            $items=json_decode($v,TRUE);
            $item=array();
            foreach ($items as $d) {
                $item[$d['name']]=$d['value'];
            }
            if(isset($olditem[$item['mstitem']])) {
                $this->db->where('status',false);
                $this->db->where('id',$id);
                $this->db->where('mstitem',$item['mstitem']);
                $this->db->update('pembelian.trpermintaanorderd',
                    array(
                        'jumlah'=>$item['jumlah'],
                        'note'=>$item['note'],
                        'diperlukan'=>format_waktu($item['diperlukan'],true),
                        'modified_by'=>$this->user['id'],
                        'modified_at'=>date('Y-m-d H:i:s+07')
                    )
                );
                unset($olditem[$item['mstitem']]);
            }else{
                $this->db->where('id',$item['mstitem']);
                $data=$this->db->get('main.mstitem')->row_array();
                $this->db->insert('pembelian.trpermintaanorderd',
                    array(
                        'id'=>$id,
                        'mstitem'=>$data['id'],
                        'nama'=>$data['nama'],
                        'jumlah'=>$item['jumlah'],
                        'note'=>$item['note'],
                        'diperlukan'=>format_waktu($item['diperlukan'],true),
                        'created_by'=>$this->user['id'],
                        'created_at'=>date('Y-m-d H:i:s+07'),
                        'status'=>false,
                    )
                );
            }
        }
        foreach ($olditem as $i){
            $this->db->where('status',false);
            $this->db->where('id',$i['id']);
            $this->db->where('mstitem',$i['mstitem']);
            $this->db->delete('pembelian.trpermintaanorderd');
        }
        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }
    public function permintaan_pembelian($post){
        $date=explode("/",$post['tanggal']);
        $this->db->where('extract(month from tanggal)=',$date[1]);
        $this->db->where('extract(year from tanggal)=',$date[2]);
        $increment=$this->db->get('pembelian.trpermintaanorder')->num_rows();
        $nota=str_pad($increment+1,3,"0",STR_PAD_LEFT)."/PPB/".romanic_number($date[1])."/$date[2]";
        $this->load->library('uuid');
        $id = $this->uuid->v4();
        $this->db->trans_begin();
        $this->db->insert('pembelian.trpermintaanorder',
            array(
                'id'=>$id,
                'nota'=>$nota,
                'tanggal'=>format_waktu($post['tanggal'],true),
                'request_by'=>strlen($post['request_by'])>30?$post['request_by']:null,
                'request_unit'=>strlen($post['request_unit'])>30?$post['request_unit']:null,
                'status'=>true,
                'closed'=>false,
                'editable'=>true,
                'created_by'=>$this->user['id'],
                'created_at'=>date('Y-m-d H:i:s+07'),
            )
        );
        foreach ($post['items'] as $v){
            $items=json_decode($v,TRUE);
            $item=array();
            foreach ($items as $d) {
                $item[$d['name']]=$d['value'];
            }
            $this->db->where('id',$item['mstitem']);
            $data=$this->db->get('main.mstitem')->row_array();
            $this->db->insert('pembelian.trpermintaanorderd',
                array(
                    'id'=>$id,
                    'mstitem'=>$data['id'],
                    'nama'=>$data['nama'],
                    'jumlah'=>$item['jumlah'],
                    'note'=>$item['note'],
                    'diperlukan'=>format_waktu($item['diperlukan'],true),
                    'created_by'=>$this->user['id'],
                    'created_at'=>date('Y-m-d H:i:s+07'),
                    'status'=>false,
                )
            );
        }
        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return array('success',"$nota");
        }
    }

}