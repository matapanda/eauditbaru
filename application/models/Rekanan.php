<?php

/**
 * Created by PhpStorm.
 * User: gradindesign4
 * Date: 07/11/2016
 * Time: 10.40
 */
defined('BASEPATH') OR exit('No direct script access allowed');
class rekanan extends CI_Model {
    public function get_customer($id){
        $this->db->where('id',$id);
        return $this->db->get("main.mstcustomer")->row_array();
    }
    public function get_supplier($id){
        $this->db->where('id',$id);
        return $this->db->get("main.mstsupplier")->row_array();
    }
}