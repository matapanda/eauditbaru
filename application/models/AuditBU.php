<?php

/**
 * Created by PhpStorm.
 * User: erick
 * Date: 7/23/17
 * Time: 9:39 PM
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Audit extends CI_Model
{
    private $user;

    public function __construct()
    {
        parent::__construct();
        $this->user = $this->session->userdata('user');
    }

    public function hapus_perencanaan($id)
    {
        $this->db->where('id', $id);
        $this->db->where('draft', true);
        $data = $this->db->get('interpro.perencanaan')->row_array();
        if (!isset($data['id'])) {
            return false;
        } else {
            $this->db->trans_begin();
            $this->db->where('id', $id);
            $this->db->delete('interpro.perencanaan_detail');
            $this->db->where('id', $id);
            $this->db->delete('interpro.perencanaan_pedoman');
            $this->db->where('id', $id);
            $this->db->delete('interpro.perencanaan_aturan');
            $this->db->where('id', $id);
            $this->db->delete('interpro.perencanaan_tujuan');
            $this->db->where('id', $id);
            $this->db->delete('interpro.perencanaan_sasaran');
            $this->db->where('id', $id);
            $this->db->delete('interpro.perencanaan');
            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
                return false;
            } else {
                $this->db->trans_commit();
                return true;
            }
        }
    }

    public function perencanaan()
    {
        error_reporting(0);
        $postdata = file_get_contents("php://input");
        $data = json_decode($postdata, true);
        $form = array();
        foreach ($data['form'] as $i => $d) {
            if (strpos($d['name'], '[]') == false) {
                $column = str_replace(array('header[', ']'), '', $d['name']);
                $form['header'][$column] = $d['value'];
            } else {
                $column = str_replace('[]', '', $d['name']);
                $form['detail'][$column][] = $d['value'];
            }
        }
        $form['detail']['aturan'] = $data['aturan'];
        $form['detail']['pedoman'] = $data['pedoman'];
        $form['detail']['tao'] = $data['tao'];

        $this->load->library('uuid');
        $id = $this->uuid->v4();
        $form['header']['start_date'] = format_waktu($form['header']['start_date'], true);
        $form['header']['end_date'] = format_waktu($form['header']['end_date'], true);
        $form['header']['spt_date'] = format_waktu($form['header']['spt_date'], true);
        $form['header']['id'] = $id;
        $form['header']['jml_hari'] = blank_is_null($form['header']['jml_hari']);
        $form['header']['periode'] = blank_is_null($form['header']['periode']);
        $form['header']['jenis'] = blank_is_null($form['header']['jenis']);
        $form['header']['satker'] = blank_is_null($form['header']['satker']);
        $form['header']['ketuatim'] = blank_is_null($form['header']['ketuatim']);
        $form['header']['irban'] = blank_is_null($form['header']['irban']);
        $form['header']['dalnis'] = blank_is_null($form['header']['dalnis']);
        $form['header']['tim'] = json_encode(is_array(@$data['tim']) ? $data['tim'] : []);
        $form['header']['created_by'] = $this->user['id'];
        $form['header']['created_at'] = date('Y-m-d H:i:s+07');
        $form['header']['closed'] = false;
        $form['header']['status'] = true;
        $form['header']['draft'] = $data['draft'] == '1' ? true : false;
        $this->db->trans_begin();
        $this->db->insert('interpro.perencanaan', $form['header']);
        if (is_array(@$form['detail']['sasaran'])) {
            $sasaran = array();
            foreach ($form['detail']['sasaran'] as $i => $f) {
                if (strlen($f) < 30) {
                    continue;
                }
                $sasaran[] = $f;
                $this->db->insert('interpro.perencanaan_sasaran', array(
                    'id' => $id,
                    'sasaran' => $f
                ));
            }
        }
        if (is_array(@$form['detail']['tujuan']))
            foreach ($form['detail']['tujuan'] as $i => $f) {
                if (strlen($f) < 30) {
                    continue;
                }
                $this->db->insert('interpro.perencanaan_tujuan', array(
                    'id' => $id,
                    'tujuan' => $f
                ));
            }
        if (is_array(@$form['detail']['aturan']))
            foreach ($form['detail']['aturan'] as $i => $f) {
                if (@$f['checked']
                    && $form['header']['jenis'] == $f['jenis']
                    && in_array($f['sasaran'], $sasaran)
                ) {
                    $this->db->insert('interpro.perencanaan_aturan', array(
                        'id' => $id,
                        'aturan' => $f
                    ));
                }
            }
        if (is_array(@$form['detail']['pedoman']))
            foreach ($form['detail']['pedoman'] as $i => $f) {
                if (@$f['checked']
                    && $form['header']['jenis'] == $f['jenis']
                    && in_array($f['sasaran'], $sasaran)
                ) {
                    $this->db->insert('interpro.perencanaan_pedoman', array(
                        'id' => $id,
                        'pedoman' => $f
                    ));
                }
            }
        if (is_array(@$form['detail']['tao']))
            foreach ($form['detail']['tao'] as $i => $f) {
                if ($form['header']['jenis'] == $f['jenis']
                    && in_array($f['sasaran'], $sasaran)
                    && is_not_empty_string($f['pelaksana'])
                    && is_not_empty_string($f['pelaksanaan'])
                ) {
                    $f['pelaksanaan'] = format_waktu($f['pelaksanaan'], true);
                    $this->db->insert('interpro.perencanaan_detail', array(
                        'id' => $id,
                        'tao' => $f['id'],
                        'jumlah_hari' => $f['jumlah'],
                        'rencana_by' => $f['pelaksana'],
                        'rencana_date' => $f['pelaksanaan'],
                        'created_by' => $this->user['id'],
                        'created_at' => date('Y-m-d H:i:s+07'),
                        'status' => true
                    ));
                }
            }
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return array('success', "Nomor SPT: " . $form['header']['spt_no'] . " tersimpan");
        }
    }

    public function update_perencanaan($id)
    {
        error_reporting(0);
        $postdata = file_get_contents("php://input");
        $data = json_decode($postdata, true);
        $form = array();
        foreach ($data['form'] as $i => $d) {
            if (strpos($d['name'], '[]') == false) {
                $column = str_replace(array('header[', ']'), '', $d['name']);
                $form['header'][$column] = $d['value'];
            } else {
                $column = str_replace('[]', '', $d['name']);
                $form['detail'][$column][] = $d['value'];
            }
        }
        $form['detail']['aturan'] = $data['aturan'];
        $form['detail']['pedoman'] = $data['pedoman'];
        $form['detail']['tao'] = $data['tao'];

        $form['header']['start_date'] = format_waktu($form['header']['start_date'], true);
        $form['header']['end_date'] = format_waktu($form['header']['end_date'], true);
        $form['header']['spt_date'] = format_waktu($form['header']['spt_date'], true);
        $form['header']['jenis'] = blank_is_null($form['header']['jenis']);
        $form['header']['jml_hari'] = blank_is_null($form['header']['jml_hari']);
        $form['header']['periode'] = blank_is_null($form['header']['periode']);
        $form['header']['satker'] = blank_is_null($form['header']['satker']);
        $form['header']['ketuatim'] = blank_is_null($form['header']['ketuatim']);
        $form['header']['irban'] = blank_is_null($form['header']['irban']);
        $form['header']['dalnis'] = blank_is_null($form['header']['dalnis']);
        $form['header']['tim'] = json_encode(is_array(@$data['tim']) ? $data['tim'] : []);
        $form['header']['modified_by'] = $this->user['id'];
        $form['header']['modified_at'] = date('Y-m-d H:i:s+07');
        $form['header']['closed'] = false;
        $form['header']['status'] = true;
        $form['header']['draft'] = $data['draft'] == '1' ? true : false;
        $total_hari=$form['header']['total_hari'];
        if (!$form['header']['draft']) {
            $form['header']['proses'] = 1;
        }
        $this->db->where('id', $id);
        $this->db->where('proses <= ', 1);
        $rencana = $this->db->get('interpro.perencanaan')->row_array();
        if (!isset($rencana['id'])) {
            return false;
        }
        $this->db->trans_begin();
        unset($form['header']['total_hari']);
        $this->db->where('id', $id);
        $this->db->update('interpro.perencanaan', $form['header']);

        $this->db->where('id', $id);
        $this->db->delete('interpro.perencanaan_sasaran');

        if (is_array(@$form['detail']['sasaran'])) {
            $sasaran = array();
            foreach ($form['detail']['sasaran'] as $i => $f) {
                if (strlen($f) < 30) {
                    continue;
                }
                $sasaran[] = $f;
                $this->db->insert('interpro.perencanaan_sasaran', array(
                    'id' => $id,
                    'sasaran' => $f
                ));
            }
        }

        $this->db->where('id', $id);
        $this->db->delete('interpro.perencanaan_tujuan');

        if (is_array(@$form['detail']['tujuan']))
            foreach ($form['detail']['tujuan'] as $i => $f) {
                if (strlen($f) < 30) {
                    continue;
                }
                $this->db->insert('interpro.perencanaan_tujuan', array(
                    'id' => $id,
                    'tujuan' => $f
                ));
            }

        $this->db->where('id', $id);
        $this->db->delete('interpro.perencanaan_aturan');

        if (is_array(@$form['detail']['aturan']))
            foreach ($form['detail']['aturan'] as $i => $f) {
                if (@$f['checked']
                    && $form['header']['jenis'] == $f['jenis']
                    && in_array($f['sasaran'], $sasaran)
                ) {
                    $this->db->insert('interpro.perencanaan_aturan', array(
                        'id' => $id,
                        'aturan' => $f
                    ));
                }
            }

        $this->db->where('id', $id);
        $this->db->delete('interpro.perencanaan_pedoman');

        if (is_array(@$form['detail']['pedoman']))
            foreach ($form['detail']['pedoman'] as $i => $f) {
                if (@$f['checked']
                    && $form['header']['jenis'] == $f['jenis']
                    && in_array($f['sasaran'], $sasaran)
                ) {
                    $this->db->insert('interpro.perencanaan_pedoman', array(
                        'id' => $id,
                        'pedoman' => $f
                    ));
                }
            }

        $this->db->where('id', $id);
        $this->db->delete('interpro.perencanaan_detail');

        if (is_array(@$form['detail']['tao']))
            foreach ($form['detail']['tao'] as $i => $f) {
                if ($form['header']['jenis'] == $f['jenis']
                    && in_array($f['sasaran'], $sasaran)
                    && is_not_empty_string($f['pelaksana'])
                    && is_not_empty_string($f['pelaksanaan'])
                ) {
                    $f['pelaksanaan'] = format_waktu($f['pelaksanaan'], true);
                    $this->db->insert('interpro.perencanaan_detail', array(
                        'id' => $id,
                        'tao' => $f['id'],
                        'jumlah_hari' => $f['jumlah'],
                        'rencana_by' => $f['pelaksana'],
                        'rencana_date' => $f['pelaksanaan'],
                        'created_by' => $this->user['id'],
                        'created_at' => date('Y-m-d H:i:s+07'),
                        'status' => true
                    ));
                }
            }
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return array('success', "Nomor SPT: " . $form['header']['spt_no'] . " diperbarui");
        }
    }
    public function update_temuan($input)
    {
        $temuan=json_decode($this->input->get_post('temuans'),true);
        $this->db->where('id_rencana', $this->input->get_post('id_test'));
        $rencana = $this->db->get('interpro.tindak_lanjut')->row_array();
        $user=$this->session->userdata('user');
        $this->db->trans_begin();
        if (isset($rencana['id'])) {
            $this->db->where('id', $this->input->get_post('id_test'));
            $a = $this->db->get('interpro.perencanaan')->row_array();
            $this->db->where('id_rencana', $this->input->get_post('id_test'));
            $this->db->update('interpro.tindak_lanjut',array(
                'id_rencana'=>$a['id'],
                'modified_at'=>date('Y-m-d H:i:s'),
                'modified_by'=>$user['id'],
                'judul'=>$a['judul'],
                'spt_no'=>$a['spt_no'],
                'skpd'=>$a['judul'],
                'no_lhp'=>$a['no_laporan'],
                'thn_lhp'=>$a['thn_laporan'],
                'pkpt_no'=>$a['pkpt_no'],
                'spt_date'=>$a['spt_date'],
            ));
            $this->db->where('id', $rencana['id']);
            $this->db->delete('interpro.tindak_lanjutd');

            foreach ($temuan as $t){
                $this->db->insert('interpro.tindak_lanjutd',array(
                    'id'=>$rencana['id'],
                    'kode_temuan'=>$t['id_t'],
                    'kode_rekom'=>$t['id_r'],
                    'negara'=>$t['nk1'],
                    'daerah'=>$t['nk2'],
                    'nd'=>$t['nd'],
                    'tindak_lanjut'=>$t['tl'],
                    'hasil_rekom'=>$t['kode_hr'],
                    'nilai_rekom'=>$t['nilai_r'],
                    'keterangan'=>$t['keterangan'],
                ));
            }
        }
        else{
            $this->load->library('uuid');
            $id = $this->uuid->v4();
            $this->db->where('id', $this->input->get_post('id_test'));
            $a = $this->db->get('interpro.perencanaan')->row_array();
            $this->db->insert('interpro.tindak_lanjut',array(
                'id'=>$id,
                'id_rencana'=>$a['id'],
                'created_at'=>date('Y-m-d H:i:s'),
                'created_by'=>$user['id'],
                'status'=>TRUE,
                'judul'=>$a['judul'],
                'spt_no'=>$a['spt_no'],
                'pkpt_no'=>$a['pkpt_no'],
                'spt_date'=>$a['spt_date'],
//                'skpd'=>$a['judul'],
//                'no_lhp'=>$a['no_laporan'],
//                'thn_lhp'=>$a['thn_laporan'],
            ));
            foreach ($temuan as $t){
                $this->db->insert('interpro.tindak_lanjutd',array(
                    'id'=>$id,
                    'kode_temuan'=>$t['id_t'],
                    'kode_rekom'=>$t['id_r'],
                    'negara'=>$t['nk1'],
                    'daerah'=>$t['nk2'],
                    'nd'=>$t['nd'],
                    'akibat'=>$t['akibat'],
                    'penyebab'=>$t['penyebab'],
                    'kriteria'=>$t['kriteria'],
                    'kondisi'=>$t['kondisi'],
                    'tujuan_tl'=>$t['tujuan_tl'],
                    'sasaran_tl'=>$t['sasaran_tl'],
                    'judul_tl'=>$t['judul_tl'],
                    'evaluasi'=>$t['evaluasi'],
                    'tanggapan'=>$t['tanggapan'],
                    'ruang'=>$t['ruang'],
                    'obrik'=>$t['obrik'],
                    'tindak_lanjut'=>$t['tindak_lanjut'],
                    'tindak_lanjut_s'=>$t['tindak_lanjut_s'],
                    'hasil_rekom'=>$t['kode_hr'],
                    'nilai_rekom'=>$t['nilai_r'],
                    'keterangan'=>$t['keterangan'],
                ));
            }
    }

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {

            $this->db->where('id', $this->input->get_post('id_test'));
            $this->db->update('interpro.perencanaan',array(
                'status_tl'=>'t'
            ));
            $this->db->trans_commit();
            return array('success', "Temuan berhasil disimpan");
        }
    }
    public function insert_temuan($input)
    {

        $postdata = file_get_contents("php://input");
        $data = json_decode($postdata, true);
        $form = array();
        foreach ($data['form'] as $d) {
                $column = $d['name'];
                $form[$column] = $d['value'];
            }
        $temuan=json_decode($data['temuans'],true);
//        echo json_encode($form);die();
        $user=$this->session->userdata('user');
        $this->db->trans_begin();
            $this->load->library('uuid');
            $id = $this->uuid->v4();
        $date=explode('/',$form['tgl_lhp']);
        $tgl=$date[2].'-'.$date[1].'-'.$date[0];
            $this->db->insert('interpro.tindak_lanjut',array(
                'id'=>$id,
                'created_at'=>date('Y-m-d H:i:s'),
                'created_by'=>$user['id'],
                'status'=>TRUE,
                'skpd'=>$form['judul_spt'],
                'no_lhp'=>$form['spt_no'],
                'thn_lhp'=>@$form['spt_date'],
                'tgl_lhp'=>@$tgl,
                'pelaksana'=>$form['pelaksana'],
            ));
            foreach ($temuan as $t){
                $this->db->insert('interpro.tindak_lanjutd',array(
                    'id'=>$id,
                    'kode_temuan'=>$t['id_t'],
                    'kode_rekom'=>$t['id_r'],
                    'negara'=>$t['nk1'],
                    'daerah'=>$t['nk2'],
                    'nd'=>$t['nd'],
                    'akibat'=>$t['akibat'],
                    'penyebab'=>$t['penyebab'],
                    'kriteria'=>$t['kriteria'],
                    'kondisi'=>$t['kondisi'],
                    'tujuan_tl'=>$t['tujuan_tl'],
                    'sasaran_tl'=>$t['sasaran_tl'],
                    'judul_tl'=>$t['judul_tl'],
                    'evaluasi'=>$t['evaluasi'],
                    'tanggapan'=>$t['tanggapan'],
                    'ruang'=>$t['ruang'],
                    'obrik'=>$t['obrik'],
                    'tindak_lanjut'=>$t['tindak_lanjut'],
                    'tindak_lanjut_s'=>$t['tindak_lanjut_s'],
                    'hasil_rekom'=>$t['kode_hr'],
                    'nilai_rekom'=>$t['nilai_r'],
                    'uraian_rekom'=>$t['rekom'],
                    'keterangan'=>$t['keterangan'],
                ));
            }

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            echo 'f';die();
            return array('danger', "Temuan gagal disimpan");
        } else {
            $this->db->trans_commit();
            echo 'yeay';die();
            return array('success', "Temuan berhasil disimpan");
        }
    }

    public function pelaksanaan($id, $input)
    {
        $this->db->where('id', $id);
        $this->db->where('progres <=', 1);
        $this->db->where('tao', $input['id']);
        $sebelumnya = $this->db->get('interpro.perencanaan_detail')->row_array();
        if(!isset($sebelumnya['id'])){
            return array('error', 'Data realisasi gagal tersimpan');
        }

        $kk = json_decode($sebelumnya['file'], TRUE);
        if(!is_array($kk)){
            $kk=array();
        }
        $input['isian']=$kk;
        for ($i = 0; $i < count($_FILES['file']['name']); $i++) :
            $_FILES['userfile']['name']     = $_FILES['file']['name'][$i];
            $_FILES['userfile']['type']     = $_FILES['file']['type'][$i];
            $_FILES['userfile']['tmp_name'] = $_FILES['file']['tmp_name'][$i];
            $_FILES['userfile']['error']    = $_FILES['file']['error'][$i];
            $_FILES['userfile']['size']     = $_FILES['file']['size'][$i];

            $config['upload_path'] = './img/';
            $config['allowed_types'] = '*';
            $config['max_size'] = 1024 * 8;
            $config['encrypt_name'] = TRUE;
            $this->load->library('upload', $config);
            if (!$this->upload->do_upload('userfile')) {
                return array('danger', $this->upload->display_errors());
            } else {
                $file = $this->upload->data();
                $input['isian'][] = array('nama' => $_FILES['userfile']['name'], 'file' => $file['file_name']);
            }
            unset($this->upload);
            unset($_FILES['userfile']);
        endfor;
        if(count($input['isian'])>0) {
            $this->db->trans_begin();
            $this->db->where('id', $id);
            $this->db->where('tao', $input['id']);
            $update = array(
                'realisasi_by' => $input['realisasi_by'],
                'realisasi_date' => format_waktu($input['realisasi_date'], true),
                'file' => json_encode($input['isian']),
                'progres' => 1,
            );
            if ($input['kesimpulan']) {
                $update['kesimpulan'] = $input['kesimpulan'];
            }
            $this->db->update('interpro.perencanaan_detail', $update);
            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
                return array('danger', 'Data realisasi gagal tersimpan');
            } else {
                $this->db->trans_commit();
                return array('success', 'Data realisasi berhasil tersimpan');
            }
        }else{
            return array('danger', 'Berkas belum terpilih');
        }
    }
}