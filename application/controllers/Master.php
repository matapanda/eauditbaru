<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Master extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        check_access('f10293a6-79c7-41e2-840e-a597e5ace426');
        $this->template->set_view('master');
    }

    public function index()
    {
        redirect(base_url());
    }

    public function group()
    {
        $this->load->model('user');
        $menu = '6ea5af6e-aeb0-4bde-9e9b-8dd58f322c75';
        $data['access'] = list_access($menu);
        if ($this->input->get_post('pk')) {
            check_access($menu, 'u');
            $id = $this->input->get_post('pk');
            $value = $this->input->get_post('value');
            $this->db->where('id', $id);
            $this->db->update('public.role', array('name' => $value));
            return;
        } elseif ($this->input->get_post('s')) {
            check_access($menu, 'u');
            $data['id'] = $this->input->get_post('s');
            $data['group'] = $this->user->get_group_access(array('id' => $data['id']));
            if (!isset($data['group'][0]['name'])) {
                die("403");
            }
            $data['menu'] = $this->user->get_access_rights($data['id']);
            $this->template->user('accessrights', $data, array('title' => $data['group'][0]['name'], 'breadcrumbs' => array('Master', 'User', 'Group Access')));
            return;
        } elseif ($this->input->get_post('menu') && $this->input->get_post('attribute') && $this->input->get_post('role')) {
            check_access($menu, 'u');
            $this->user->set_rolemenu_status($this->input->get_post('menu'), $this->input->get_post('attribute'), $this->input->get_post('role'));
            return;
        } elseif ($this->input->get_post('status')) {
            check_access($menu, 'u');
            $this->user->set_group_status($this->input->get_post('status'));
            return;
        } elseif ($this->input->get_post('name')) {
            check_access($menu, 'c');
            $this->db->set('id', 'uuid_generate_v4()', FALSE);
            $data = $this->db->insert('public.role',
                array(
                    'name' => $this->input->get_post('name'),
                    'status' => TRUE,
                )
            );
            $this->session->set_flashdata('status_update', $data);
            redirect(base_url('master/group'));
            return;
        }
        if ($this->session->flashdata('status_update')) {
            $data['status_update'] = $this->session->flashdata('status_update');
        }
        $data['group'] = $this->user->get_group_access();
        $this->template->user('group', $data, array('title' => 'Group Access', 'breadcrumbs' => array('Master', 'User')));
    }

    public function test(){
        $this->template->user('excel','', array('title' => 'Group Access', 'breadcrumbs' => array('Master', 'User')));
    }
    public function upload(){
        $this->db->trans_begin();
            $config['upload_path'] = './img/';
            $config['allowed_types'] = 'xls|xlsx';
            $config['max_size'] = 1024 * 8;
            $config['encrypt_name'] = TRUE;
            $this->load->library('upload', $config);
            if (!$this->upload->do_upload("file")) {
                $data['status_update'] = 'error';
                $data['status_message'] =  $this->upload->display_errors('', '');
            } else if($this->upload->do_upload("file")){
                $file = $this->upload->data();
                $full_path=$file["full_path"];

                $this->load->library('PHPExcel');
                $objPHPExcel = PHPExcel_IOFactory::load($full_path);
                $max=$objPHPExcel->getActiveSheet(0)->getHighestRow();
                set_time_limit(0);
                for($no=1;$no<$max;$no++){
                    $row['A']=$objPHPExcel->getActiveSheet()->getCell("A".$no)->getValue();//kode
                    $row['B']=$objPHPExcel->getActiveSheet()->getCell("B".$no)->getValue();//nama
                        $raw = $this->db->query("SELECT uuid_generate_v4() as id,NOW() as tstamp")->row_array();
                        $id=$raw['id'];
                    $this->db->insert('master.satker',array(
                        'id'=>$id,
                        'kode'=>$row['A'],
                        'nama'=>$row['B'],
                        'status'=>'t',
                    ));
                }
            }
        if ($this->db->trans_status()){
            $this->db->trans_commit();
          echo "ok";
        }else{
            echo "fail";
        }
        redirect(base_url());
    }
    public function user()
    {
        $menu = 'd66ab356-6792-451d-8f42-84f07e48b9e7';
        $data['access'] = list_access($menu);
        $this->load->model('user');
        if ($this->input->get_post('u')) {
            check_access($menu, 'u');
            $data['id'] = $this->input->get_post('u');
            $this->db->where('status','t');
            $data['jabatan']=$this->db->get('master.jabatan')->result_array();
            $this->db->where('status','t');
            $data['golongan']=$this->db->get('master.golongan')->result_array();
            $data['user'] = $this->user->get_user_by_id($data['id']);
            if (!isset($data['user']['name'])) {
                die("403");
            }
            if ($this->input->get_post('name') && $this->input->get_post('email')) {
                $data['status_update'] = $this->user->update_user($this->input->get_post(null), $data['id']);
                $data['user'] = $this->user->get_user_by_id($data['id']);
                if ($data['status_update']) {
                    $this->session->set_flashdata('status_update', true);
                }
                redirect(base_url('master/user'));
            }
            $data['role'] = $this->user->get_group_access(array('status' => true));
            $data['roleusers'] = $this->user->get_users_group($data['id']);
            $this->template->user('updateuser', $data, array('title' => $data['user']['name'], 'breadcrumbs' => array('Master', 'User', 'Perbarui')));
            return;
        } elseif ($this->input->get_post('i')) {
            check_access($menu, 'c');
            if ($this->input->get_post('name')) {
                $data['status_update'] = $this->user->insert($this->input->get_post(null));
                if ($data['status_update']) {
                    $this->session->set_flashdata('status_update', true);
                }
                redirect(base_url('master/user'));
            }
            $data['role'] = $this->user->get_group_access(array('status' => true));
            $this->template->user('adduser', $data, array('title' => 'Tambah Data', 'breadcrumbs' => array('Master', 'User')));
            return;
        } elseif ($this->input->get_post('status')) {
            check_access($menu, 'u');
            $this->user->set_user_status($this->input->get_post('status'));
            return;
        } elseif ($this->input->get_post('is_exist')) {
            $this->db->like('username', $this->input->get_post('is_exist'));
            $data['jumlah'] = $this->db->get('users')->num_rows();
            echo json_encode($data);
            return;
        }
        $data['user'] = $this->user->get_list_user();
        $this->template->user('user', $data, array('title' => 'User Access', 'breadcrumbs' => array('Master', 'User')));
    }


    //PKPT e-audit Banyuwangi
    public function pkpt()
    {
        $menu = '2a5a511b-7bd4-424f-8b38-252f6115640f';
        $data['access'] = list_access($menu);
        $this->load->model('user');
        $this->db->order_by('nama','desc');
        $this->db->where('status','t');
        $this->db->select('nama as tahun');
        $data['list_tahun'] = $this->db->get('public.periode')->result_array();
        $this->db->order_by('name');
        $data['tim'] = $this->db->get('users')->result_array();
        if ($this->input->get_post('pk')) {
            check_access($menu, 'u');
            $id = $this->input->get_post('pk');
            $value = $this->input->get_post('value');
            $this->db->where('id', $id);
            $this->db->update('master.pkpt', array($this->input->get_post('name') => $value));
            return;
        }
        elseif ($this->input->get_post('i')) {
            check_access($menu, 'c');
            if($this->input->get_post('tema')) {
                check_access($menu, 'c');
                $data['status_update'] = $this->user->insert_pkpt($this->input->get_post(null));
                if ($data['status_update']) {
                    $this->session->set_flashdata('status_update', true);
                }
                redirect(base_url('master/pkpt'));
            }
            $this->db->order_by('sasaran.order,sasaran.kode', 'asc');
            $this->db->where('status', true);
            $data['sasaran'] = $this->db->get('master.sasaran')->result_array();

            $this->db->order_by('order,kode', 'asc');
            $this->db->where('status', true);
            $data['jenis'] = $this->db->get('master.jenis')->result_array();

            $this->db->order_by('tujuan.order,tujuan.kode', 'asc');
            $this->db->where('tujuan.status', true);
            $data['tujuan'] = $this->db->get('master.tujuan')->result_array();

            $data['role'] = $this->user->get_group_access(array('status' => true));
            $this->template->user('updatepkpt', $data, array('title' => 'Tambah Data PKPT', 'breadcrumbs' => array('Master', 'PKPT')));
            return;
        }
        elseif ($this->input->get_post('e')) {
            check_access($menu, 'u');
            $data['id'] = $this->input->get_post('e');
            $data['user'] = $this->user->get_pkpt_by_id($data['id']);
            if($this->input->get_post('tema')) {
                check_access($menu, 'c');
                $data['status_update'] = $this->user->update_pkpt($this->input->get_post(null),$data['id']);
                if ($data['status_update']) {
                    $this->session->set_flashdata('status_update', true);
                }
                redirect(base_url('master/pkpt'));
            }

            $this->db->order_by('sasaran.order,sasaran.kode', 'asc');
            $this->db->where('status', true);
            $data['sasaran'] = $this->db->get('master.sasaran')->result_array();

            $this->db->order_by('order,kode', 'asc');
            $this->db->where('status', true);
            $data['jenis'] = $this->db->get('master.jenis')->result_array();

            $this->db->order_by('tujuan.order,tujuan.kode', 'asc');
            $this->db->where('tujuan.status', true);
            $data['tujuan'] = $this->db->get('master.tujuan')->result_array();
            $this->template->user('updatepkpt', $data, array('title' => $data['user']['no'], 'breadcrumbs' => array('Master', 'PKPT', 'Perbarui')));
            return;
        }
        elseif ($this->input->get_post('status')) {
            check_access($menu, 'u');
            $this->user->set_pkpt_status($this->input->get_post('status'));
            return;
        }
        $data['closed'] = $this->input->get_post('closed') ? $this->input->get_post('closed') : '';
        $data['search'] = $this->input->get_post('search') ? strtolower($this->input->get_post('search')) : '';
        if ($data['search'] <> '') {
            $this->db->like('lower(master.pkpt.nama)', $data['search']);
            $this->db->or_like('lower(master.pkpt.hp)', $data['search']);
            $this->db->or_like('lower(master.pkpt.kegiatan)', $data['search']);
            $this->db->or_like('lower(master.pkpt.rmp)', $data['search']);
            $this->db->or_like('lower(master.pkpt.tema)', $data['search']);
            $this->db->or_like('lower(master.pkpt.rpl)', $data['search']);
            $this->db->or_like('lower(master.pkpt.dana)', $data['search']);
            $this->db->or_like('lower(master.pkpt.risiko)', $data['search']);
            $this->db->or_like('lower(master.pkpt.keterangan)', $data['search']);
        }
        if ($data['closed'] <> '') {
            $this->db->where('master.pkpt.status', $data['closed']);
        }
        if ($this->input->get_post('tahun')) {
            $data['tahun'] = $this->input->get_post('tahun');
        } else {
            $data['tahun'] = @$data['list_tahun'][0]['tahun'];
        }
        $data['pkpt'] = $this->user->get_list_pkpt($data['tahun']);
        $this->template->user('pkpt', $data, array('title' => 'PKPT', 'breadcrumbs' => array('Master')));
    }

    //JABATAN e-audit Banyuwangi
    public function jabatan()
    {
        $menu = 'acda06a9-1a27-45f0-bceb-1b77c81b8749';
        $data['access'] = list_access($menu);
        $this->load->model('user');
        if ($this->input->get_post('pk')) {
            check_access($menu, 'u');
            $id = $this->input->get_post('pk');
            $value = $this->input->get_post('value');
            $this->db->where('id', $id);
            $this->db->update('master.jabatan', array($this->input->get_post('name') => $value));
            return;
        } elseif ($this->input->get_post('ket')) {
            check_access($menu, 'c');
            $data['status_update'] = $this->user->insert_jabatan($this->input->get_post(null));
            if ($data['status_update']) {
                $this->session->set_flashdata('status_update', true);
            }
            redirect(base_url('master/jabatan'));
        } elseif ($this->input->get_post('status')) {
            check_access($menu, 'u');
            $this->user->set_jabatan_status($this->input->get_post('status'));
            return;
        } elseif ($this->input->get_post('is_exist')) {
            $this->db->like('username', $this->input->get_post('is_exist'));
            $data['jumlah'] = $this->db->get('users')->num_rows();
            echo json_encode($data);
            return;
        }
        $data['closed'] = $this->input->get_post('closed') ? $this->input->get_post('closed') : '';
        $data['search'] = $this->input->get_post('search') ? strtolower($this->input->get_post('search')) : '';
        if ($data['search'] <> '') {
            $this->db->like('lower(master.jabatan.ket)', $data['search']);
        }
        if ($data['closed'] <> '') {
            $this->db->where('master.jabatan.status', $data['closed']);
        }
        $data['user'] = $this->user->get_list_jabatan();
        $this->template->user('jabatan', $data, array('title' => 'Jabatan', 'breadcrumbs' => array('Master')));
    }
    public function tipe_r()
    {
        $menu = '340671b5-d101-46ec-b86f-6dc06e9d5668';
        $data['access'] = list_access($menu);
        $this->load->model('user');
        if ($this->input->get_post('pk')) {
            check_access($menu, 'u');
            $id = $this->input->get_post('pk');
            $value = $this->input->get_post('value');
            $this->db->where('id', $id);
            $this->db->update('master.tipe_resiko', array($this->input->get_post('name') => $value));
            return;
        } elseif ($this->input->get_post('nama')) {
            check_access($menu, 'c');
            $data['status_update'] = $this->user->insert_tipe_resiko($this->input->get_post(null));
            if ($data['status_update']) {
                $this->session->set_flashdata('status_update', true);
            }
            redirect(base_url('master/tipe_r'));
        } elseif ($this->input->get_post('status')) {
            check_access($menu, 'u');
            $this->user->set_tipe_resiko_status($this->input->get_post('status'));
            return;
        }
        else{
        $data['closed'] = $this->input->get_post('closed') ? $this->input->get_post('closed') : '';
        $data['search'] = $this->input->get_post('search') ? strtolower($this->input->get_post('search')) : '';
        if ($data['search'] <> '') {
            $this->db->like('lower(master.tipe_resiko.nama)', $data['search']);
        }
        if ($data['closed'] <> '') {
            $this->db->where('master.tipe_resiko.status', $data['closed']);
        }
        $data['user'] = $this->user->get_list_tipe_resiko();
        $this->template->user('tipe_resiko', $data, array('title' => 'Tipe Resiko', 'breadcrumbs' => array('Master')));
    }
    }
    public function penyebab()
    {
        $menu = 'c43ac70d-a037-4148-827d-d5c0a8b00b97';
        $data['access'] = list_access($menu);
        $this->load->model('user');
        if ($this->input->get_post('pk')) {
            check_access($menu, 'u');
            $id = $this->input->get_post('pk');
            $value = $this->input->get_post('value');
            $this->db->where('id', $id);
            $this->db->update('master.penyebab', array($this->input->get_post('name') => $value));
            return;
        } elseif ($this->input->get_post('nama')) {
            check_access($menu, 'c');
            $data['status_update'] = $this->user->insert_penyebab($this->input->get_post(null));
            if ($data['status_update']) {
                $this->session->set_flashdata('status_update', true);
            }
            redirect(base_url('master/penyebab'));
        } elseif ($this->input->get_post('status')) {
            check_access($menu, 'u');
            $this->user->set_penyebab_status($this->input->get_post('status'));
            return;
        }
        else{
        $data['closed'] = $this->input->get_post('closed') ? $this->input->get_post('closed') : '';
        $data['search'] = $this->input->get_post('search') ? strtolower($this->input->get_post('search')) : '';
        if ($data['search'] <> '') {
            $this->db->like('lower(master.penyebab.nama)', $data['search']);
        }
        if ($data['closed'] <> '') {
            $this->db->where('master.penyebab.status', $data['closed']);
        }
        $data['user'] = $this->user->get_list_penyebab();
        $this->template->user('penyebab', $data, array('title' => 'Penyebab', 'breadcrumbs' => array('Master')));
    }
    }
    public function dampak()
    {
        $menu = '1541696d-a165-4a06-9181-88472bd61294';
        $data['access'] = list_access($menu);
        $this->load->model('user');
        if ($this->input->get_post('pk')) {
            check_access($menu, 'u');
            $id = $this->input->get_post('pk');
            $value = $this->input->get_post('value');
            $this->db->where('id', $id);
            $this->db->update('master.dampak', array($this->input->get_post('name') => $value));
            return;
        } elseif ($this->input->get_post('nama')) {
            check_access($menu, 'c');
            $data['status_update'] = $this->user->insert_dampak($this->input->get_post(null));
            if ($data['status_update']) {
                $this->session->set_flashdata('status_update', true);
            }
            redirect(base_url('master/dampak'));
        } elseif ($this->input->get_post('status')) {
            check_access($menu, 'u');
            $this->user->set_dampak_status($this->input->get_post('status'));
            return;
        }
        else{
        $data['closed'] = $this->input->get_post('closed') ? $this->input->get_post('closed') : '';
        $data['search'] = $this->input->get_post('search') ? strtolower($this->input->get_post('search')) : '';
        if ($data['search'] <> '') {
            $this->db->like('lower(master.dampak.nama)', $data['search']);
        }
        if ($data['closed'] <> '') {
            $this->db->where('master.dampak.status', $data['closed']);
        }
        $data['user'] = $this->user->get_list_dampak();
        $this->template->user('dampak', $data, array('title' => 'Dampak', 'breadcrumbs' => array('Master')));
    }
    }
    public function akibat()
    {
        $menu = '2a5a511b-7bd4-424f-8b38-252f6115640d';
        $data['access'] = list_access($menu);
        $this->load->model('user');
        if ($this->input->get_post('pk')) {
            check_access($menu, 'u');
            $id = $this->input->get_post('pk');
            $value = $this->input->get_post('value');
            $this->db->where('id', $id);
            $this->db->update('master.akibat', array($this->input->get_post('name') => $value));
            return;
        } elseif ($this->input->get_post('nama')) {
            check_access($menu, 'c');
            $data['status_update'] = $this->user->insert_akibat($this->input->get_post(null));
            if ($data['status_update']) {
                $this->session->set_flashdata('status_update', true);
            }
            redirect(base_url('master/akibat'));
        } elseif ($this->input->get_post('status')) {
            check_access($menu, 'u');
            $this->user->set_akibat_status($this->input->get_post('status'));
            return;
        }
        else{
        $data['closed'] = $this->input->get_post('closed') ? $this->input->get_post('closed') : '';
        $data['search'] = $this->input->get_post('search') ? strtolower($this->input->get_post('search')) : '';
        if ($data['search'] <> '') {
            $this->db->like('lower(master.akibat.nama)', $data['search']);
        }
        if ($data['closed'] <> '') {
            $this->db->where('master.akibat.status', $data['closed']);
        }
        $data['user'] = $this->user->get_list_akibat();
        $this->template->user('akibat', $data, array('title' => 'Akibat', 'breadcrumbs' => array('Master')));
    }
    }
    public function tk()
    {
        $menu = '23ca0398-00b3-4547-9312-63633c0e4a10';
        $data['access'] = list_access($menu);
        $this->load->model('user');
        if ($this->input->get_post('pk')) {
            check_access($menu, 'u');
            $id = $this->input->get_post('pk');
            $value = $this->input->get_post('value');
            $this->db->where('id', $id);
            $this->db->update('master.tindak', array($this->input->get_post('name') => $value));
            return;
        } elseif ($this->input->get_post('nama')) {
            check_access($menu, 'c');
            $data['status_update'] = $this->user->insert_tk($this->input->get_post(null));
            if ($data['status_update']) {
                $this->session->set_flashdata('status_update', true);
            }
            redirect(base_url('master/tk'));
        } elseif ($this->input->get_post('status')) {
            check_access($menu, 'u');
            $this->user->set_tk_status($this->input->get_post('status'));
            return;
        }
        else{
        $data['closed'] = $this->input->get_post('closed') ? $this->input->get_post('closed') : '';
        $data['search'] = $this->input->get_post('search') ? strtolower($this->input->get_post('search')) : '';
        if ($data['search'] <> '') {
            $this->db->like('lower(master.tindak.nama)', $data['search']);
        }
        if ($data['closed'] <> '') {
            $this->db->where('master.tindak.status', $data['closed']);
        }
        $data['user'] = $this->user->get_list_tk();
        $this->template->user('tk', $data, array('title' => 'Data Tindak Terkait', 'breadcrumbs' => array('Master')));
    }
    }
    public function tindak_lanjut()
    {
        $menu = 'e36e779a-e2aa-468e-a11c-972f75c1c052';
        $data['access'] = list_access($menu);
        $this->load->model('user');
        if ($this->input->get_post('pk')) {
            check_access($menu, 'u');
            $id = $this->input->get_post('pk');
            $value = $this->input->get_post('value');
            $this->db->where('id', $id);
            $this->db->update('master.tindak_lanjut', array($this->input->get_post('name') => $value));
            return;
        } elseif ($this->input->get_post('nama')) {
            check_access($menu, 'c');
            $data['status_update'] = $this->user->insert_mtindak_lanjut($this->input->get_post(null));
            if ($data['status_update']) {
                $this->session->set_flashdata('status_update', true);
            }
            redirect(base_url('master/tindak_lanjut'));
        } elseif ($this->input->get_post('status')) {
            check_access($menu, 'u');
            $this->user->set_tindak_lanjut_status($this->input->get_post('status'));
            return;
        }
        else{
        $data['closed'] = $this->input->get_post('closed') ? $this->input->get_post('closed') : '';
        $data['search'] = $this->input->get_post('search') ? strtolower($this->input->get_post('search')) : '';
        if ($data['search'] <> '') {
            $this->db->like('lower(master.tindak_lanjut.nama)', $data['search']);
        }
        if ($data['closed'] <> '') {
            $this->db->where('master.tindak_lanjut.status', $data['closed']);
        }
        $data['user'] = $this->user->get_list_mtindak_lanjut();
        $this->template->user('tindak_lanjut', $data, array('title' => 'Tindak Lanjut', 'breadcrumbs' => array('Master')));
    }
    }
    public function kendali()
    {
        $menu = 'bfa6447a-7dc4-4495-9397-db2e5865ade1';
        $data['access'] = list_access($menu);
        $this->load->model('user');
        if ($this->input->get_post('pk')) {
            check_access($menu, 'u');
            $id = $this->input->get_post('pk');
            $value = $this->input->get_post('value');
            $this->db->where('id', $id);
            $this->db->update('master.kendali', array($this->input->get_post('name') => $value));
            return;
        } elseif ($this->input->get_post('nama')) {
            check_access($menu, 'c');
            $data['status_update'] = $this->user->insert_kendali($this->input->get_post(null));
            if ($data['status_update']) {
                $this->session->set_flashdata('status_update', true);
            }
            redirect(base_url('master/kendali'));
        } elseif ($this->input->get_post('status')) {
            check_access($menu, 'u');
            $this->user->set_kendali_status($this->input->get_post('status'));
            return;
        }
        else{
        $data['closed'] = $this->input->get_post('closed') ? $this->input->get_post('closed') : '';
        $data['search'] = $this->input->get_post('search') ? strtolower($this->input->get_post('search')) : '';
        if ($data['search'] <> '') {
            $this->db->like('lower(master.kendali.nama)', $data['search']);
        }
        if ($data['closed'] <> '') {
            $this->db->where('master.kendali.status', $data['closed']);
        }
        $data['user'] = $this->user->get_list_kendali();
        $this->template->user('kendali', $data, array('title' => 'Kendali', 'breadcrumbs' => array('Master')));
    }
    }
    public function prosedur_audit()
    {
        $menu = '72e9e5f5-9b6c-4213-9c23-85588627d997';
        $data['access'] = list_access($menu);
        $this->load->model('user');
        if ($this->input->get_post('pk')) {
            check_access($menu, 'u');
            $id = $this->input->get_post('pk');
            $value = $this->input->get_post('value');
            $this->db->where('id', $id);
            $this->db->update('master.prosedur_audit', array($this->input->get_post('name') => $value));
            return;
        } elseif ($this->input->get_post('nama')) {
            check_access($menu, 'c');
            $data['status_update'] = $this->user->insert_prosedur($this->input->get_post(null));
            if ($data['status_update']) {
                $this->session->set_flashdata('status_update', true);
            }
            redirect(base_url('master/prosedur_audit'));
        } elseif ($this->input->get_post('status')) {
            check_access($menu, 'u');
            $this->user->set_prosedur_status($this->input->get_post('status'));
            return;
        }
        $data['closed'] = $this->input->get_post('closed') ? $this->input->get_post('closed') : '';
        $data['search'] = $this->input->get_post('search') ? strtolower($this->input->get_post('search')) : '';
        if ($data['search'] <> '') {
            $this->db->like('lower(master.prosedur_audit.nama)', $data['search']);
        }
        if ($data['closed'] <> '') {
            $this->db->where('master.prosedur_audit.status', $data['closed']);
        }
        $data['user'] = $this->user->get_list_prosedur_audit();
        $this->template->user('prosedur_audit', $data, array('title' => 'Prosedur Audit', 'breadcrumbs' => array('Master')));
    }
    //JABATAN e-audit Banyuwangi
    public function conf_spt()
    {
        $menu = '292e5ff3-1095-49b3-aafd-565794ee545e';
        $data['access'] = list_access($menu);
        $this->load->model('user');
        if ($this->input->get_post('pk')) {
            check_access($menu, 'u');
            $id = $this->input->get_post('pk');
            $value = $this->input->get_post('value');
            $this->db->where('id', $id);
            $this->db->update('master.conf_spt', array($this->input->get_post('name') => $value));
            return;
        } elseif ($this->input->get_post('ket')) {
            check_access($menu, 'c');
            $data['status_update'] = $this->user->insert_conf($this->input->get_post(null));
            if ($data['status_update']) {
                $this->session->set_flashdata('status_update', true);
            }
            redirect(base_url('master/conf_spt'));
        } elseif ($this->input->get_post('status')) {
            check_access($menu, 'u');
            $this->user->set_conf_spt_status($this->input->get_post('status'));
            return;
        }
        $data['closed'] = $this->input->get_post('closed') ? $this->input->get_post('closed') : '';
        $data['search'] = $this->input->get_post('search') ? strtolower($this->input->get_post('search')) : '';
        if ($data['search'] <> '') {
            $this->db->like('lower(master.conf_spt.ket)', $data['search']);
        }
        if ($data['closed'] <> '') {
            $this->db->where('master.conf_spt.status', $data['closed']);
        }
        $data['user'] = $this->user->get_list_conf_spt();
        $this->template->user('conf_spt', $data, array('title' => 'Config SPT', 'breadcrumbs' => array('Master')));
    }

    //WILAYAH e-audit Banyuwangi
    public function wilayah()
    {
        $menu = '92559c74-927e-4edf-85c8-203447141840';
        $data['access'] = list_access($menu);
        $this->load->model('user');
        if ($this->input->get_post('pk')) {
            check_access($menu, 'u');
            $id = $this->input->get_post('pk');
            $value = $this->input->get_post('value');
            $this->db->where('id', $id);
            $this->db->update('support.wilayah', array($this->input->get_post('name') => $value));
            return;
        } elseif ($this->input->get_post('new')) {
            check_access($menu, 'c');
            $data['status_update'] = $this->user->insert_wilayah($this->input->get_post(null));
            if ($data['status_update']) {
                $this->session->set_flashdata('status_update', true);
            }
            redirect(base_url('master/wilayah'));
        } elseif ($this->input->get_post('status')) {
            check_access($menu, 'u');
            $this->user->set_wilayah_status($this->input->get_post('status'));
            return;
        }
        $data['closed'] = $this->input->get_post('closed') ? $this->input->get_post('closed') : '';
        $data['search'] = $this->input->get_post('search') ? strtolower($this->input->get_post('search')) : '';
        if ($data['search'] <> '') {
            $this->db->like('lower(public.users.name)', $data['search']);
            $this->db->or_like('lower(support.wilayah.wilayah)', $data['search']);
        }
        if ($data['closed'] <> '') {
            $this->db->where('support.wilayah.status', $data['closed']);
        }
        $data['user'] = $this->user->get_list_wilayah();
        $this->db->where('public.roleusers.role', '2332800e-edb0-49cd-92d7-1abc3d981207');
        $this->db->join('public.roleusers', 'public.roleusers.user=public.users.id');
        $data['irban'] = $this->db->get('public.users')->result_array();
        $data['list'] = array();
        foreach ($data['irban'] as $d) {
            $data['list'][] = array('id' => $d['id'], 'text' => $d['name']);
        }
        $data['irban2'] = array();
        foreach ($data['irban'] as $d) {
            $data['irban2'][] = array('value' => $d['id'], 'text' => $d['name']);
        }
        $this->template->user('wilayah', $data, array('title' => 'Wilayah', 'breadcrumbs' => array('Master')));
    }

    //TIM e-audit Banyuwangi
    public function tim()
    {
        $menu = '36fbf7b0-d4ba-49d1-af67-1e0af4a05766';
        $data['access'] = list_access($menu);
        $this->load->model('user');
        if ($this->input->get_post('pk')) {
            check_access($menu, 'u');
            $id = $this->input->get_post('pk');
            $value = $this->input->get_post('value');
            $this->db->where('id', $id);
            $this->db->update('support.tim', array($this->input->get_post('name') => $value));
            return;
        }
        elseif ($this->input->get_post('e')) {
            check_access($menu, 'u');
            $data['id'] = $this->input->get_post('e');
            $data['user'] = $this->user->get_tim_by_id($data['id']);
            if ($this->input->get_post('tim') && $this->input->get_post('wilayah')) {
                $data['status_update'] = $this->user->update_tim($this->input->get_post(null), $data['id']);
                if ($data['status_update']) {
                    $this->session->set_flashdata('status_update', true);
                }
                $data['user'] = $this->user->get_tim_by_id($data['id']);
            }
            $data['dalnis'] = array();
            $this->db->where('public.roleusers.role', '108ce450-d740-4bcb-9aaa-be9504bf94d8');
            $this->db->join('public.roleusers', 'public.roleusers.user=public.users.id');
            $dalnis = $this->db->get('public.users')->result_array();

            foreach ($dalnis as $d) {
                $data['dalnis'][] = array('id' => $d['id'], 'text' => $d['name']);
            }

            $this->db->where('public.roleusers.role', '97cc7404-490c-4827-beba-425c85acef5d');
            $this->db->join('public.roleusers', 'public.roleusers.user=public.users.id');
            $ketua = $this->db->get('public.users')->result_array();

            $data['ketua'] = array();
            foreach ($ketua as $d) {
                $data['ketua'][] = array('id' => $d['id'], 'text' => $d['name']);
            }

            $wilayah = $this->db->get('support.wilayah')->result_array();

            $data['wilayah'] = array();
            foreach ($wilayah as $d) {
                $data['wilayah'][] = array('id' => $d['id'], 'text' => $d['wilayah']);
            }

//            $this->db->where('public.roleusers.role', '9d0e2ca0-f4fd-4b2b-9938-4cf5cade7f26');
            $this->db->join('public.roleusers', 'public.roleusers.user=public.users.id');
            $this->db->select('users.id,users.name');
            $this->db->group_by('users.id,users.name');
            $anggota = $this->db->get('public.users')->result_array();

            $data['anggota'] = array();
            foreach ($anggota as $d) {
                $data['anggota'][] = array('id' => $d['id'], 'text' => $d['name']);
            }

            $data['anggotatim'] = $this->user->get_tim_group($data['id']);
            $this->template->user('updatetim', $data, array('title' => $data['user']['tim'], 'breadcrumbs' => array('Master', 'Tim', 'Perbarui')));
            return;
        } elseif ($this->input->get_post('new')) {
            check_access($menu, 'c');
            $data['status_update'] = $this->user->insert_tim($this->input->get_post(null));
            if ($data['status_update']) {
                $this->session->set_flashdata('status_update', true);
            }
            redirect(base_url('master/tim'));
        } elseif ($this->input->get_post('status')) {
            check_access($menu, 'u');
            $this->user->set_tim_status($this->input->get_post('status'));
            return;
        }

        $data['wilayahnew'] = $this->db->get('support.wilayah')->result_array();

        $this->db->where('public.roleusers.role', '108ce450-d740-4bcb-9aaa-be9504bf94d8');
        $this->db->join('public.roleusers', 'public.roleusers.user=public.users.id');
        $data['dalnisnew'] = $this->db->get('public.users')->result_array();

        $this->db->where('public.roleusers.role', '97cc7404-490c-4827-beba-425c85acef5d');
        $this->db->join('public.roleusers', 'public.roleusers.user=public.users.id');
        $data['ketuanew'] = $this->db->get('public.users')->result_array();

        $this->db->where('public.roleusers.role', '108ce450-d740-4bcb-9aaa-be9504bf94d8');
        $this->db->join('public.roleusers', 'public.roleusers.user=public.users.id');
        $dalnis = $this->db->get('public.users')->result_array();

        $data['dalnis'] = array();
        foreach ($dalnis as $d) {
            $data['dalnis'][] = array('id' => $d['id'], 'text' => $d['name']);
        }

        $this->db->where('public.roleusers.role', '97cc7404-490c-4827-beba-425c85acef5d');
        $this->db->join('public.roleusers', 'public.roleusers.user=public.users.id');
        $ketua = $this->db->get('public.users')->result_array();

        $data['ketua'] = array();
        foreach ($ketua as $d) {
            $data['ketua'][] = array('id' => $d['id'], 'text' => $d['name']);
        }

        $wilayah = $this->db->get('support.wilayah')->result_array();

        $data['wilayah'] = array();
        foreach ($wilayah as $d) {
            $data['wilayah'][] = array('id' => $d['kode'], 'text' => $d['wilayah']);
        }
        $data['closed'] = $this->input->get_post('closed') ? $this->input->get_post('closed') : '';
        $data['search'] = $this->input->get_post('search') ? strtolower($this->input->get_post('search')) : '';
        if ($data['search'] <> '') {
            $this->db->like('lower(support.wilayah.wilayah)', $data['search']);
            $this->db->or_like('lower(support.tim.tim)', $data['search']);
            $this->db->or_like('lower(t1.name)', $data['search']);
            $this->db->or_like('lower(t2.name)', $data['search']);
        }
        if ($data['closed'] <> '') {
            $this->db->where('support.tim.status', $data['closed']);
        }
        $data['user'] = $this->user->get_list_tim();
        $this->template->user('tim', $data, array('title' => 'Tim', 'breadcrumbs' => array('Master')));
    }

    //get irban
    public function get_irban()
    {
        $this->db->where('id', '52e68b85-90c1-4910-b9b0-df9a3b3574c4');
        $data['irban'] = $this->db->get('public.users')->result_array();
        foreach ($data['irban'] as $d) {
            $output[] = array('id' => $d['id'], 'text' => $d['name']);
        }
        echo json_encode($output);
    }

    //JENIS e-audit Banyuwangi
    public function jenis()
    {
        $menu = 'd66ab356-6792-451d-8f42-84f07e48b9e7';
        $data['access'] = list_access($menu);
        $this->load->model('user');
        if ($this->input->get_post('pk')) {
            check_access($menu, 'u');
            $id = $this->input->get_post('pk');
            $value = $this->input->get_post('value');
            $this->db->where('id', $id);
            $this->db->update('master.jenis', array($this->input->get_post('name') => $value));
            return;
        } elseif ($this->input->get_post('ket')) {
            check_access($menu, 'c');
            $data['status_update'] = $this->user->insert_jenis($this->input->get_post(null));
            if ($data['status_update']) {
                $this->session->set_flashdata('status_update', true);
            }
            redirect(base_url('master/jenis'));
        } elseif ($this->input->get_post('status')) {
            check_access($menu, 'u');
            $this->user->set_jenis_status($this->input->get_post('status'));
            return;
        }
        $data['closed'] = $this->input->get_post('closed') ? $this->input->get_post('closed') : '';
        $data['search'] = $this->input->get_post('search') ? strtolower($this->input->get_post('search')) : '';
        if ($data['search'] <> '') {
            $this->db->like('lower(master.jenis.ket)', $data['search']);
            $this->db->or_like('lower(master.jenis.kode)', $data['search']);
        }
        if ($data['closed'] <> '') {
            $this->db->where('master.jenis.status', $data['closed']);
        }
        $data['jenis'] = $this->user->get_list_jenis();
        $this->template->user('jenis', $data, array('title' => 'Jenis Audit', 'breadcrumbs' => array('Master')));
    }

    //Pedoman e-audit Banyuwangi
    public function pedoman()
    {
        $menu = '7a2eee70-8883-4c74-9a9b-e820f2249b4b';
        $data['access'] = list_access($menu);
        $jenis = $this->db->get('master.jenis')->result_array();

        $data['jenis'] = array();
        foreach ($jenis as $d) {
            $data['jenis'][] = array('id' => $d['id'], 'text' => $d['kode'] . "/" . $d['ket']);
        }

        $sasaran = $this->db->get('master.sasaran')->result_array();

        $data['sasaran'] = array();
        foreach ($sasaran as $d) {
            $data['sasaran'][] = array('id' => $d['id'], 'text' => $d['kode'] . "/" . $d['ket']);
        }

        $this->load->model('user');
        if ($this->input->get_post('pk')) {
            check_access($menu, 'u');
            $id = $this->input->get_post('pk');
            $value = $this->input->get_post('value');
            $this->db->where('id', $id);
            $this->db->update('support.pedoman', array($this->input->get_post('name') => $value));
            return;
        } elseif ($this->input->get_post('i')) {
            check_access($menu, 'c');
            if (isset($_FILES['file']) && $this->input->get_post(null)) {
                check_access($menu, 'u');
                $data['status_update'] = $this->user->insert_pedoman($this->input->get_post(null));
                if ($data['status_update']) {
                    $this->session->set_flashdata('status_update', true);
                }
                redirect(base_url('master/pedoman'));
            }
            $this->db->where('id', $this->input->get_post('e'));
            $this->db->select('file');
            $data['jumlah'] = $this->db->get('support.pedoman')->num_rows();
            $this->template->user('updatepedoman', $data, array('title' => 'Data Baru', 'breadcrumbs' => array('Master', 'Pedoman')));
            return;
        } elseif ($this->input->get_post('e')) {
            check_access($menu, 'c');
            if (isset($_FILES['file']) && $this->input->get_post(null)) {
                check_access($menu, 'u');
                $data['status_update'] = $this->user->upload_pedoman($this->input->get_post(null));
                if ($data['status_update']) {
                    $this->session->set_flashdata('status_update', true);
                }
                redirect(base_url('master/pedoman'));
            }
            $this->db->where('id', $this->input->get_post('e'));
            $this->db->select('file');
            $data['jumlah'] = $this->db->get('support.pedoman')->num_rows();
            $data['user'] = $this->user->get_pedoman_by_id($this->input->get_post('e'));
            $this->template->user('updatepedoman', $data, array('title' => $data['user']['kode'], 'breadcrumbs' => array('Master', 'Pedoman', 'Perbarui')));
            return;
        } elseif ($this->input->get_post('status')) {
            check_access($menu, 'u');
            $this->user->set_pedoman_status($this->input->get_post('status'));
            return;
        }
        $data['closed'] = $this->input->get_post('closed') ? $this->input->get_post('closed') : '';
        $data['search'] = $this->input->get_post('search') ? strtolower($this->input->get_post('search')) : '';
        if ($data['search'] <> '') {
            $this->db->like('lower(master.jenis.ket)', $data['search']);
            $this->db->or_like('lower(master.sasaran.ket)', $data['search']);
            $this->db->or_like('lower(support.pedoman.kode)', $data['search']);
        }
        if ($data['closed'] <> '') {
            $this->db->where('support.pedoman.status', $data['closed']);
        }
        $data['pedoman'] = $this->user->get_list_pedoman();
        $this->template->user('pedoman', $data, array('title' => 'Pedoman Audit', 'breadcrumbs' => array('Master')));
    }

    //Detail TAO e-audit Banyuwangi
    public function detail_tao($id = null)
    {
        $this->db->where('id',$id);
        $header=$this->db->get("support.v_tao_header")->row_array();
        if (!isset($header['id'])) {
            redirect(base_url('master/tao'));
        }
        $data['header'] = $header;
        $menu = '2f4e5764-01c0-49f5-b178-555f814a015a';
        $data['access'] = list_access($menu);
        $this->load->model('user');
        if ($this->input->get_post('status')) {
            check_access($menu, 'u');
            $this->user->set_detail_tao_status($this->input->get_post('status'));
            return;
        }elseif ($this->input->get_post('pk')) {
            check_access($menu, 'u');
            $id = $this->input->get_post('pk');
            $value = $this->input->get_post('value');
            $this->db->where('id', $id);
            $this->db->update('support.tao_detail', array($this->input->get_post('name') => $value));
            return;
        } elseif ($this->input->get_post('i')) {
            check_access($menu, 'c');
            $data['id_detail'] = $id;
            if (isset($_FILES['file_download']) && $this->input->get_post(null)) {
                check_access($menu, 'u');
                $data['status_update'] = $this->user->insert_detail_tao($id,$this->input->get_post(null));
                $this->session->set_flashdata('status_update', $data['status_update']);
                redirect(base_url('master/detail_tao/' . $data['id_detail']));
            }
            $this->template->user('updatedetailtao', $data, array('title' => 'Input Detail Baru', 'breadcrumbs' => array('Master', 'TAO', 'Audit Program')));
            return;
        } elseif ($this->input->get_post('e')) {
            check_access($menu, 'c');
            $data['id_detail'] = $id;
            $data['id'] = $this->input->get_post('e');
            if (isset($_FILES['file_download']) && $this->input->get_post(null)) {
                check_access($menu, 'u');
                $data['status_update'] = $this->user->update_detail_tao($id,$this->input->get_post(null));
                $this->session->set_flashdata('status_update', $data['status_update']);
                redirect(base_url('master/detail_tao/' . $data['id_detail']));
            }
            $data['user'] = $this->user->get_tao_detail_by_id($this->input->get_post('e'));
            $this->template->user('updatedetailtao', $data, array('title' => 'Perbarui data TAO', 'breadcrumbs' => array('Master', 'TAO', 'Audit Program')));
            return;
        }

        $data['id_tao'] = $id;
        $this->db->order_by('kertas_kerja');
        $this->db->where('id_tao', $data['id_tao']);
        $data['detail'] = $this->db->get('support.tao_detail')->result_array();
        $this->template->user('detailtao', $data, array('title' => "$header[kode] - $header[langkah]", 'breadcrumbs' => array('Master', 'TAO')));
        return;
    }

    //TAO e-audit Banyuwangi
    public function tao()
    {
        $menu = '2f4e5764-01c0-49f5-b178-555f814a015a';
        $data['access'] = list_access($menu);
        $jenis = $this->db->get('master.jenis')->result_array();

        $data['jenis'] = array();
        foreach ($jenis as $d) {
            $data['jenis'][] = array('id' => $d['id'], 'text' => $d['kode'] . " - " . $d['ket']);
        }

        $sasaran = $this->db->get('master.sasaran')->result_array();

        $data['sasaran'] = array();
        foreach ($sasaran as $d) {
            $data['sasaran'][] = array('id' => $d['id'], 'text' => $d['kode'] . " - " . $d['ket']);
        }

        $tujuan = $this->db->get('master.tujuan')->result_array();

        $data['tujuan'] = array();
        foreach ($tujuan as $d) {
            $data['tujuan'][] = array('id' => $d['id'], 'text' => $d['kode'] . " - " . $d['ket']);
        }

        $tahapan = $this->db->get('master.tahapan_audit')->result_array();
        $data['tahapan'] = array();

        $this->load->model('user');
        if ($this->input->get_post('pk')) {
            check_access($menu, 'u');
            $id = $this->input->get_post('pk');
            $value = $this->input->get_post('value');
            $this->db->where('id', $id);
            $this->db->update('support.tao', array($this->input->get_post('name') => $value));
            return;
        } elseif ($this->input->get_post('i')) {
            check_access($menu, 'c');
            if ($this->input->get_post(null)) {
                check_access($menu, 'u');
                $data['status_update'] = $this->user->insert_tao($this->input->get_post(null));
                if ($data['status_update']) {
                    $this->session->set_flashdata('status_update', true);
                }
                redirect(base_url('master/tao'));
            }
            foreach ($tahapan as $d) {
                $data['tahapan'][] = array('id' => $d['kode'], 'text' => $d['tahapan']);
            }
            $this->template->user('updatetao', $data, array('title' => 'Tambah Data TAO', 'breadcrumbs' => array('Master', 'TAO')));
            return;
        } elseif ($this->input->get_post('e')) {
            check_access($menu, 'c');
            $data['id'] = $this->input->get_post('e');
            if ($this->input->get_post(null)) {
                check_access($menu, 'u');
                $data['status_update'] = $this->user->update_tao($data['id'], $this->input->get_post(null));
                if ($data['status_update']) {
                    $this->session->set_flashdata('status_update', true);
                }
                redirect(base_url('master/tao'));
            }

            foreach ($tahapan as $d) {
                $data['tahapan'][] = array('id' => $d['kode'], 'text' => $d['tahapan']);
            }
            $data['user'] = $this->user->get_tao_by_id($this->input->get_post('e'));
            $this->template->user('updatetao', $data, array('title' => $data['user']['kode'], 'breadcrumbs' => array('Master', 'TAO', 'Perbarui')));
            return;
        } elseif ($this->input->get_post('status')) {
            check_access($menu, 'u');
            $this->user->set_tao_status($this->input->get_post('status'));
            return;
        }
        foreach ($tahapan as $d) {
            $data['tahapan'][$d['kode']] = $d['tahapan'];
        }
        $data['closed'] = $this->input->get_post('closed') ? $this->input->get_post('closed') : '';
        $data['search'] = $this->input->get_post('search') ? strtolower($this->input->get_post('search')) : '';
        $data['thp'] = $this->input->get_post('thp') ? strtolower($this->input->get_post('thp')) : '';
        $this->db->order_by('tahapan,kode');
        if ($data['search'] <> '') {
            $this->db->or_like('lower(supprort.tao.langkah)', $data['search']);
        }
        if ($data['closed'] <> '') {
            $this->db->where('support.tao.status', $data['closed']);
        }
        if ($data['thp'] <> '') {
            $this->db->where('lower(support.tao.tahapan)', $data['thp']);
        }
        $data['tao'] = $this->db->get('support.v_tao_header')->result_array();
        $this->template->user('tao', $data, array('title' => 'TAO', 'breadcrumbs' => array('Master')));
    }

    //KM3 laporan
    public function km3()
    {
        $data = "";
        $this->load->library('pdf');
        $html = $this->load->view('private/pdf/km3', $data, true);
        ini_set('memory_limit', '32M');
        $pdf = $this->pdf->load(array('mode' => 'UTf-8', 'format' => 'A4', 'FontFamily' => 'roboto'));
        $pdf->WriteHTML($html);
        $filename = "img/test" . time() . ".pdf";
        $pdf->Output($filename);
    }

//Aturan e-audit Banyuwangi
    public function aturan()
    {
        $menu = '1541696d-a165-4a06-9181-88472bd61293';
        $data['access'] = list_access($menu);
        $jenis = $this->db->get('master.jenis')->result_array();

        $data['jenis'] = array();
        foreach ($jenis as $d) {
            $data['jenis'][] = array('id' => $d['id'], 'text' => $d['kode'] . "/" . $d['ket']);
        }

        $sasaran = $this->db->get('master.sasaran')->result_array();

        $data['sasaran'] = array();
        foreach ($sasaran as $d) {
            $data['sasaran'][] = array('id' => $d['id'], 'text' => $d['kode'] . "/" . $d['ket']);
        }

        $this->load->model('user');
        if ($this->input->get_post('pk')) {
            check_access($menu, 'u');
            $id = $this->input->get_post('pk');
            $value = $this->input->get_post('value');
            $this->db->where('id', $id);
            $this->db->update('support.aturan', array($this->input->get_post('name') => $value));
            return;
        } elseif ($this->input->get_post('i')) {
            check_access($menu, 'c');
            if (isset($_FILES['file']) && $this->input->get_post(null)) {
                check_access($menu, 'u');
                $data['status_update'] = $this->user->insert_aturan($this->input->get_post(null));
                if ($data['status_update']) {
                    $this->session->set_flashdata('status_update', true);
                }
                redirect(base_url('master/aturan'));
            }
            $this->db->where('id', $this->input->get_post('e'));
            $this->db->select('file');
            $data['jumlah'] = $this->db->get('support.aturan')->num_rows();
            $this->template->user('updateaturan', $data, array('title' => 'Data Baru', 'breadcrumbs' => array('Master', 'Aturan')));
            return;
        } elseif ($this->input->get_post('e')) {
            check_access($menu, 'c');
            if (isset($_FILES['file']) && $this->input->get_post(null)) {
                check_access($menu, 'u');
                $data['status_update'] = $this->user->upload_aturan($this->input->get_post(null));
                if ($data['status_update']) {
                    $this->session->set_flashdata('status_update', true);
                }
                redirect(base_url('master/aturan'));
            }
            $this->db->where('id', $this->input->get_post('e'));
            $this->db->select('file');
            $data['jumlah'] = $this->db->get('support.aturan')->num_rows();
            $data['user'] = $this->user->get_aturan_by_id($this->input->get_post('e'));
            $this->template->user('updateaturan', $data, array('title' => $data['user']['kode'], 'breadcrumbs' => array('Master', 'Aturan', 'Perbarui')));
            return;
        } elseif ($this->input->get_post('status')) {
            check_access($menu, 'u');
            $this->user->set_aturan_status($this->input->get_post('status'));
            return;
        }
        $data['closed'] = $this->input->get_post('closed') ? $this->input->get_post('closed') : '';
        $data['search'] = $this->input->get_post('search') ? strtolower($this->input->get_post('search')) : '';
        if ($data['search'] <> '') {
            $this->db->like('lower(master.jenis.ket)', $data['search']);
            $this->db->or_like('lower(master.sasaran.ket)', $data['search']);
        }
        if ($data['closed'] <> '') {
            $this->db->where('support.aturan.status', $data['closed']);
        }
        $data['aturan'] = $this->user->get_list_aturan();
        $this->template->user('aturan', $data, array('title' => 'Aturan Audit', 'breadcrumbs' => array('Master')));
    }

    // Nama Satker E-Audit
    public function satker()
    {
        $menu = '8a1dfd71-6c85-412f-91de-970ca913dc60';
        $data['access'] = list_access($menu);
        $this->load->model('user');
        if ($this->input->get_post('pk')) {
            check_access($menu, 'u');
            $id = $this->input->get_post('pk');
            $value = $this->input->get_post('value');
            $this->db->where('id', $id);
            $this->db->update('master.satker', array($this->input->get_post('name') => $value));
            return;
        }
        else if($this->input->get_post('satker')){
            check_access($menu, 'c');
//            echo $this->input->get_post('tot_lp');
//            die();
            $tot=null_is_nol($this->input->get_post('tot_lp'))+null_is_nol($this->input->get_post('tot_km'))+null_is_nol($this->input->get_post('tot_im'))+null_is_nol($this->input->get_post('tot_ko'))+null_is_nol($this->input->get_post('tot_si'));

            $this->db->trans_begin();
            $raw = $this->db->query("SELECT uuid_generate_v4() as id,NOW() as tstamp")->row_array();
            $this->db->where('id_satker', $this->input->get_post('satker'));
            $this->db->where('periode', $this->input->get_post('periode_h'));
            $a=$this->db->get('master.resiko_satker')->row_array();
            if($a['id']==''){
                $data['status_update']=$this->db->insert('master.resiko_satker',array(
                    'id'=>$raw['id'],
                    'id_satker'=>$this->input->get_post('satker'),
                    'periode'=>$this->input->get_post('periode_h'),
                    'ket_resiko'=>'',
                    'resiko'=>$tot,
                    'jumlah_anggaran'=>$this->input->get_post('ja'),
                    'nilai_aset'=>$this->input->get_post('na'),
                    'jumlah_pegawai'=>$this->input->get_post('jp'),
                    'jumlah_unit'=>$this->input->get_post('ju'),
                    'jumlah_jenis_keg'=>$this->input->get_post('jk'),
                    'jumlah_pemeriksaan'=>$this->input->get_post('jp5'),
                    'nilai_sakip'=>$this->input->get_post('ns'),
                    'nilai_spip'=>$this->input->get_post('nsp'),
                    'tertib_adm'=>$this->input->get_post('ta'),
                    'jumlah_pengaduan'=>$this->input->get_post('jpm'),
                    'jumlah_jabatan'=>$this->input->get_post('jj'),
                    'hasil_monitoring'=>$this->input->get_post('hm'),
                    'lp'=>$this->input->get_post('lp'),
                    'lp2'=>$this->input->get_post('lp2'),
                    'lp3'=>$this->input->get_post('lp3'),
                    'lp4'=>$this->input->get_post('lp4'),
                    'lp5'=>$this->input->get_post('lp5'),
                    'tot_lp'=>$this->input->get_post('tot_lp'),
                    'km'=>$this->input->get_post('km'),
                    'km2'=>$this->input->get_post('km2'),
                    'km3'=>$this->input->get_post('km3'),
                    'km4'=>$this->input->get_post('km4'),
                    'km5'=>$this->input->get_post('km5'),
                    'tot_km'=>$this->input->get_post('tot_km'),
                    'im'=>$this->input->get_post('im'),
                    'im2'=>$this->input->get_post('im2'),
                    'im3'=>$this->input->get_post('im3'),
                    'im4'=>$this->input->get_post('im4'),
                    'im5'=>$this->input->get_post('im5'),
                    'tot_im'=>$this->input->get_post('tot_im'),
                    'ko'=>$this->input->get_post('ko'),
                    'ko2'=>$this->input->get_post('ko2'),
                    'ko3'=>$this->input->get_post('ko3'),
                    'ko4'=>$this->input->get_post('ko4'),
                    'ko5'=>$this->input->get_post('ko5'),
                    'tot_ko'=>$this->input->get_post('tot_ko'),
                    'si'=>$this->input->get_post('si'),
                    'si2'=>$this->input->get_post('si2'),
                    'si3'=>$this->input->get_post('si3'),
                    'si4'=>$this->input->get_post('si4'),
                    'si5'=>$this->input->get_post('si5'),
                    'tot_si'=>$this->input->get_post('tot_si'),
                ));
            }
            else{
                $this->db->where('id_satker', $this->input->get_post('satker'));
                $this->db->where('periode', $this->input->get_post('periode_h'));
                $data['status_update']= $this->db->update('master.resiko_satker',array(
                    'id_satker'=>$this->input->get_post('satker'),
                    'periode'=>$this->input->get_post('periode_h'),
                    'ket_resiko'=>'',
                    'resiko'=>$tot,
                    'jumlah_anggaran'=>$this->input->get_post('ja'),
                    'nilai_aset'=>$this->input->get_post('na'),
                    'jumlah_pegawai'=>$this->input->get_post('jp'),
                    'jumlah_unit'=>$this->input->get_post('ju'),
                    'jumlah_jenis_keg'=>$this->input->get_post('jk'),
                    'jumlah_pemeriksaan'=>$this->input->get_post('jp5'),
                    'nilai_sakip'=>$this->input->get_post('ns'),
                    'nilai_spip'=>$this->input->get_post('nsp'),
                    'tertib_adm'=>$this->input->get_post('ta'),
                    'jumlah_pengaduan'=>$this->input->get_post('jpm'),
                    'jumlah_jabatan'=>$this->input->get_post('jj'),
                    'hasil_monitoring'=>$this->input->get_post('hm'),
                    'lp'=>$this->input->get_post('lp'),
                    'lp2'=>$this->input->get_post('lp2'),
                    'lp3'=>$this->input->get_post('lp3'),
                    'lp4'=>$this->input->get_post('lp4'),
                    'lp5'=>$this->input->get_post('lp5'),
                    'tot_lp'=>$this->input->get_post('tot_lp'),
                    'km'=>$this->input->get_post('km'),
                    'km2'=>$this->input->get_post('km2'),
                    'km3'=>$this->input->get_post('km3'),
                    'km4'=>$this->input->get_post('km4'),
                    'km5'=>$this->input->get_post('km5'),
                    'tot_km'=>$this->input->get_post('tot_km'),
                    'im'=>$this->input->get_post('im'),
                    'im2'=>$this->input->get_post('im2'),
                    'im3'=>$this->input->get_post('im3'),
                    'im4'=>$this->input->get_post('im4'),
                    'im5'=>$this->input->get_post('im5'),
                    'tot_im'=>$this->input->get_post('tot_im'),
                    'ko'=>$this->input->get_post('ko'),
                    'ko2'=>$this->input->get_post('ko2'),
                    'ko3'=>$this->input->get_post('ko3'),
                    'ko4'=>$this->input->get_post('ko4'),
                    'ko5'=>$this->input->get_post('ko5'),
                    'tot_ko'=>$this->input->get_post('tot_ko'),
                    'si'=>$this->input->get_post('si'),
                    'si2'=>$this->input->get_post('si2'),
                    'si3'=>$this->input->get_post('si3'),
                    'si4'=>$this->input->get_post('si4'),
                    'si5'=>$this->input->get_post('si5'),
                    'tot_si'=>$this->input->get_post('tot_si'),
                ));
            }
            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
                if ($data['status_update']) {
                    $this->session->set_flashdata('status_update', true);
                }
                redirect(base_url('master/satker'));
                return false;
            } else {
                $this->db->trans_commit();
                if ($data['status_update']) {
                    $this->session->set_flashdata('status_update', true);
                }
                redirect(base_url('master/satker'));
                return true;
            }
        }
        elseif ($this->input->get_post('kode')) {
            check_access($menu, 'c');
            $data['status_update'] = $this->user->insert_satker($this->input->get_post(null));
            if ($data['status_update']) {
                $this->session->set_flashdata('status_update', true);
            }
            redirect(base_url('master/satker'));
        }
        elseif ($this->input->get_post('status')) {
            check_access($menu, 'u');
            $this->user->set_satker_status($this->input->get_post('status'));
            return;
        }
        $data['closed'] = $this->input->get_post('closed') ? $this->input->get_post('closed') : '';
        $data['search'] = $this->input->get_post('search') ? strtolower($this->input->get_post('search')) : '';
        if ($data['search'] <> '') {
            $this->db->like('lower(master.satker.kode)', $data['search']);
            $this->db->or_like('lower(master.satker.ket)', $data['search']);
            $this->db->or_like('lower(master.satker.nama)', $data['search']);
            $this->db->or_like('lower(master.satker.alamat)', $data['search']);
            $this->db->or_like('lower(master.satker.email)', $data['search']);
        }
        if($this->input->get_post('p')){
            $this->db->where('status','t');
            $this->db->where('resiko','0');
            $data['satker']=$this->db->get('master.satker')->result_array();
            $data['tipe']='p';
            $data['periode']=$this->db->get('periode')->result_array();
            $this->template->user('resiko_satker', $data, array('title' => 'Resiko Satker2', 'breadcrumbs' => array('Master')));
        return;
        }
        else if($this->input->get_post('e')){
            $data['tipe']='e';
            $data['satker']=$this->db->get('master.satker')->result_array();
            $data['id_satker']=$this->input->get_post('e');

            $this->db->where('nama',date('Y'));
            $thn=$this->db->get('periode')->row_array();
            $data['per']=$this->input->get_post('periode') !=''?$this->input->get_post('periode'):$thn['id'];

            $this->db->where('periode',$data['per']);
            $this->db->where('id_satker',$this->input->get_post('e'));
            $data['edit']=$this->db->get('master.resiko_satker')->row_array();

            $data['periode']=$this->db->get('periode')->result_array();
            $this->template->user('resiko_satker', $data, array('title' => 'Resiko Satker', 'breadcrumbs' => array('Master')));
        return;
        }
        else if($this->input->get_post('cek_periode')){
            $data['satker']=$this->db->get('master.satker')->result_array();
            $data['id_satker']=$this->input->get_post('id_h');
//echo 'f';die();
            $data['per']=$this->input->get_post('periode');

            $this->db->where('periode',$this->input->get_post('periode'));
            $this->db->where('id_satker',$this->input->get_post('id_h'));
            $data['edit']=$this->db->get('master.resiko_satker')->row_array();
//            echo $data['edit'];die();
            $data['periode']=$this->db->get('periode')->result_array();
            $this->template->user('resiko_satker', $data, array('title' => 'Resiko Satker', 'breadcrumbs' => array('Master')));
        return;
        }
        if ($data['closed'] <> '') {
            $this->db->where('master.satker.status', $data['closed']);
        }
        $data['satker'] = $this->user->get_list_satker();
        $this->template->user('satker', $data, array('title' => 'Satker', 'breadcrumbs' => array('Master')));
    }
    public function kode_temuan($page=0){
        $menu = 'c6fabac7-e2b3-4245-a69a-84992efa4bc1';
        $data['access'] = list_access($menu);
        $this->load->model('user');

        $perpage=1000;
    if($this->input->get_post('i')){
            check_access($menu,'c');
            if($this->input->get_post('status')){
                $this->akuntansi->set_coa_status($this->input->get_post('status'));
            }else{
                if($this->input->get_post('kode')){
                    $data['form']=$this->input->get_post(null);
                    $data['status_update']=$this->user->add_kode_temuan($data['form']);
                    if($data['status_update']){
                        $this->session->set_flashdata('status_update', true);
                        redirect(base_url('master/kode_temuan'));
                    }
                }
                $this->db->order_by('k1,k2,k3,k4','asc');
                $this->db->where('level <> ',4);
                $this->db->where('k1 <> ','9');
                $this->db->where('status',true);
                $this->db->where('deleted',false);
                $this->db->select("support.kode_temuan.id,support.kode_temuan.nama,(k1||k2||k3||k4) as kode");
                $data['parent']=$this->db->get('support.kode_temuan')->result_array();
                $this->template->user('addkt',$data,array('title'=>'Tambah Data','breadcrumbs'=>array('Master','Kode Temuan')));
//           echo json_encode($data['status_update']);
//                die();
            }
            return;
        }
        elseif($this->input->get_post('delete')){
        check_access($menu,'d');
        echo $this->user->set_coa_delete($this->input->get_post('delete'));
        return;
    }
    elseif($this->input->get_post('e')){
        check_access($menu,'u');
        if($this->input->get_post('status')){
            $this->user->set_coa_status($this->input->get_post('status'));
        }else{
            if($this->input->get_post('level')){
                $data['status_update']=$this->user->update_coa($this->input->get_post(null));
            }
            $this->db->where('id',$this->input->get_post('e'));
            $data['data']=$this->db->get('support.kode_temuan')->row_array();
            $this->template->user('updatecoa',$data,array('title'=>'Perbarui Data','breadcrumbs'=>array('Master','Chart Of Account')));
        }
        return;
    }
    elseif($this->input->get_post('status')){
            check_access($menu,'d');
            echo $this->user->set_kode_delete($this->input->get_post('delete'));
            return;
        }
        $data['search']=$this->input->get_post('search');
        $data['type']=$this->input->get_post('type');
        $data['status']=$this->input->get_post('status')?$this->input->get_post('status'):(isset($_GET['status'])?'':'t');
        if($this->input->get_post('search')){
            $this->db->like('lower(nama)',strtolower($data['search']));
            $this->db->or_where('lower(k1||k2||k3||k4)',strtolower(str_replace(".","",$data['search'])));
        }
        if($data['status']){
            $this->db->where('status',$data['status']);
        }
        $this->db->where('deleted',false);
        $total=$this->db->get('support.kode_temuan')->num_rows();
        $this->db->limit($perpage,$page);
        $this->db->order_by('k1,k2,k3,k4','asc');
        if($this->input->get_post('search')){
            $this->db->like('lower(nama)',strtolower($data['search']));
            $this->db->or_where('lower(k1||k2||k3||k4)',strtolower(str_replace(".","",$data['search'])));
        }
        if($data['status']){
            $this->db->where('status',$data['status']);
        }
        $this->db->where('deleted',false);
        $this->db->where('k1 <> ','9');
        $this->db->select("kode_temuan.*,(k1||'.'||k2||'.'||k3||'.'||k4) as kode");
        $data['data']=$this->db->get('support.kode_temuan')->result_array();
        $data['pagination']=$this->template->pagination('master/kode_temuan',$total,$perpage);
        $data['page']=$page;

        $this->template->user('kode_temuan', $data, array('title' => 'Kode Temuan', 'breadcrumbs' => array('Master')));
        return;
    }
    public function kode_rekom($page=0){
        $menu = 'c6fabac7-e2b3-4245-a69a-84992efa4bc1';
        $data['access'] = list_access($menu);
        $this->load->model('user');

        $perpage=1000;
    if($this->input->get_post('i')){
            check_access($menu,'c');
            if($this->input->get_post('status')){
                $this->akuntansi->set_coa_status($this->input->get_post('status'));
            }else{
                if($this->input->get_post('parent')){
                    $data['form']=$this->input->get_post(null);
                    $data['status_update']=$this->user->add_kode_temuan($data['form']);
                    if($data['status_update']){
                        $this->session->set_flashdata('status_update', true);
                        redirect(base_url('master/kode_temuan'));
                    }
                }
                $this->db->order_by('k1,k2,k3,k4','asc');
                $this->db->where('k1','9');
                $this->db->where('level <> ',4);
                $this->db->where('status',true);
                $this->db->where('deleted',false);
                $this->db->select("support.kode_temuan.id,support.kode_temuan.nama,(k1||k2||k3||k4) as kode");
                $data['parent']=$this->db->get('support.kode_temuan')->result_array();
                $this->template->user('addkt',$data,array('title'=>'Tambah Data','breadcrumbs'=>array('Master','Kode Temuan')));
//           echo json_encode($data['status_update']);
//                die();
            }
            return;
        }elseif($this->input->get_post('delete')){
        check_access($menu,'d');
        echo $this->user->set_coa_delete($this->input->get_post('delete'));
        return;
    }
    elseif($this->input->get_post('e')){
        check_access($menu,'u');
        if($this->input->get_post('status')){
            $this->user->set_coa_status($this->input->get_post('status'));
        }else{
            if($this->input->get_post('level')){
                $data['status_update']=$this->user->update_coa($this->input->get_post(null));
            }
            $this->db->where('id',$this->input->get_post('e'));
            $data['data']=$this->db->get('support.kode_temuan')->row_array();
            $this->template->user('updatecoa',$data,array('title'=>'Perbarui Data','breadcrumbs'=>array('Master','Chart Of Account')));
        }
        return;
    }
    elseif($this->input->get_post('status')){
            check_access($menu,'d');
            echo $this->user->set_kode_delete($this->input->get_post('delete'));
            return;
        }
        $data['search']=$this->input->get_post('search');
        $data['type']=$this->input->get_post('type');
        $data['status']=$this->input->get_post('status')?$this->input->get_post('status'):(isset($_GET['status'])?'':'t');
        if($this->input->get_post('search')){
            $this->db->like('lower(nama)',strtolower($data['search']));
            $this->db->or_where('lower(k1||k2||k3||k4)',strtolower(str_replace(".","",$data['search'])));
        }
        if($data['status']){
            $this->db->where('status',$data['status']);
        }
        $this->db->where('deleted',false);
        $total=$this->db->get('support.kode_temuan')->num_rows();
        $this->db->limit($perpage,$page);
        $this->db->order_by('k1,k2,k3,k4','asc');
        if($this->input->get_post('search')){
            $this->db->like('lower(nama)',strtolower($data['search']));
            $this->db->or_where('lower(k1||k2||k3||k4)',strtolower(str_replace(".","",$data['search'])));
        }
        if($data['status']){
            $this->db->where('status',$data['status']);
        }
        $this->db->where('deleted',false);
        $this->db->where('k1','9');
        $this->db->select("kode_temuan.*,(k1||'.'||k2||'.'||k3||'.'||k4) as kode");
        $data['data']=$this->db->get('support.kode_temuan')->result_array();
        $data['pagination']=$this->template->pagination('master/kode_temuan',$total,$perpage);
        $data['page']=$page;

        $this->template->user('kode_temuan', $data, array('title' => 'Kode Rekomendasi', 'breadcrumbs' => array('Master')));
        return;
    }
    // Periode E-Audit
    public function periode()
    {
        $menu = 'b0014449-4706-48e1-b81d-d758057ef0d0';
        $data['access'] = list_access($menu);
        $this->load->model('user');
        if($this->input->get_post('id')=='tambah'){
            check_access($menu, 'c');
            $data['status_update'] = $this->user->insert_periode($this->input->get_post(null));
           $status_update=explode(',',$data['status_update'][0]);
//            echo $status_update[0];die();
            if ($status_update[0]=='true') {
                $this->session->set_flashdata('status_update', true);
                $this->db->where('nama',$this->input->get_post('nama'));
                $this->db->where('start',$this->input->get_post('sd'));
                $id=$this->db->get('periode')->row_array();
            redirect(base_url("master/periode?e=$id[id]"));
            }
            else{
                $this->session->set_flashdata('status_update', $data['status_update']);
            redirect(base_url('master/periode'));
            }
        }
        else if($this->input->get_post('holidayadd')){
            check_access($menu, 'c');
            $this->db->where('id',$this->input->get_post('id'));
            $this->db->where('holiday',$this->input->get_post('hari'));
            $num=$this->db->get('periode_holiday');
            if($num->num_rows()>0){
                $this->db->where('id',$this->input->get_post('id'));
                $this->db->where('holiday',$this->input->get_post('hari'));
                $this->db->update('periode_holiday',array('ket'=>$this->input->get_post('ket')));
            }
            else{
                $this->db->insert('periode_holiday',array('ket'=>$this->input->get_post('ket'),'id'=>$this->input->get_post('id'),'holiday'=>$this->input->get_post('hari')));
            }
            $data=array('id'=>$this->input->get_post('id'),'start'=>$this->input->get_post('hari'),'end'=>$this->input->get_post('hari'),'ket'=>$this->input->get_post('ket'));
            echo json_encode($data);
            return;
        }
        else if($this->input->get_post('deletes')){
            check_access($menu, 'c');
            $this->db->where('id',$this->input->get_post('id'));
            $this->db->where('holiday',$this->input->get_post('hari'));
            $this->db->delete('periode_holiday');
            return;
        }
        else if($this->input->get_post('id')!='tambah'&&$this->input->get_post('id')!=''){
            check_access($menu, 'u');
            $data['status_update'] = $this->user->edit_periode($this->input->get_post(null));
            if ($data['status_update']) {
                $this->session->set_flashdata('status_update', true);
            }
            redirect(base_url('master/periode'));
        }
        elseif ($this->input->get_post('status')) {
            check_access($menu, 'u');
            $this->user->set_periode_status($this->input->get_post('status'));
            return;
        }
        elseif ($this->input->get_post('cek')) {
            check_access($menu, 'r');
            $this->db->where('holiday',$this->input->get_post('hari'));
            $this->db->where('id',$this->input->get_post('edit'));
            $holiday=$this->db->get('periode_holiday')->row_array();
            echo json_encode($holiday);
            return;
        }
        else if($this->input->get_post('e')){
            $lh=$this->db->get('periode_holiday')->result_array();
            $data['listholiday']=array();
            foreach($lh as $l){
            $data['listholiday'][]=$l['holiday'];
            }
            $this->db->where('id',$this->input->get_post('e'));
            $data['list_h']=$this->db->get('periode_holiday')->result_array();
            $this->db->where('id',$this->input->get_post('e'));
            $data['edit']=$this->db->get('periode')->row_array();
            $start_date = $data['edit']['start'];
            $end_date = $data['edit']['end'];

            $data['range_date'] = array();
            for ($i = strtotime($start_date); $i <= strtotime($end_date); $i = strtotime('+1 day', $i))
            {
                $data['range_date'][]=$i;
            }
            $data['starts'] = $data['edit']['start']?$data['edit']['start']:date('Y-m-d');
            $data['ends'] =  $data['edit']['end']?$data['edit']['end']:date('Y-m-d');
            $s = explode('-', $data['starts']);
            $d = explode('-', $data['ends']);
            $data['daterange'] = $s[2] . '/' . $s[1] . '/' . $s[0] . ' - ' . $d[2] . '/' . $d[1 ] . '/' . $d[0];

            $this->template->user('addperiode', $data, array('title' => 'Edit Periode', 'breadcrumbs' => array('Master')));
        return;
        }
        $data['starts'] =date('Y').'-'.'01'.'-'.'01';
        $data['ends'] =  date('Y').'-'.'12'.'-'.'31';
        $s = explode('-', $data['starts']);
        $d = explode('-', $data['ends']);
        $data['daterange'] = $s[2] . '/' . $s[1] . '/' . $s[0] . ' - ' . $d[2] . '/' . $d[1 ] . '/' . $d[0];

        $data['closed'] = $this->input->get_post('closed') ? $this->input->get_post('closed') : '';
        if ($data['closed'] <> '') {
            $this->db->where('periode.status', $data['closed']);
        }
        $data['periode'] = $this->user->get_list_periode();
        $this->template->user('periode', $data, array('title' => 'Periode', 'breadcrumbs' => array('Master')));
    }

    // Sasaran E-Audit
    public function sasaran()
    {
        $menu = 'd66ab356-6792-451d-8f42-84f07e48b9e7';
        $data['access'] = list_access($menu);
        $this->load->model('user');
        if ($this->input->get_post('pk')) {
            check_access($menu, 'u');
            $id = $this->input->get_post('pk');
            $value = $this->input->get_post('value');
            $this->db->where('id', $id);
            $this->db->update('master.sasaran', array($this->input->get_post('name') => $value));
            return;
        } elseif ($this->input->get_post('ket')) {
            check_access($menu, 'c');
            $data['status_update'] = $this->user->insert_sasaran($this->input->get_post(null));
            if ($data['status_update']) {
                $this->session->set_flashdata('status_update', true);
            }
            redirect(base_url('master/sasaran'));
        } elseif ($this->input->get_post('status')) {
            check_access($menu, 'u');
            $this->user->set_sasaran_status($this->input->get_post('status'));
            return;
        }
        $data['closed'] = $this->input->get_post('closed') ? $this->input->get_post('closed') : '';
        $data['search'] = $this->input->get_post('search') ? strtolower($this->input->get_post('search')) : '';
        if ($data['search'] <> '') {
            $this->db->like('lower(master.sasaran.kode)', $data['search']);
            $this->db->or_like('lower(master.sasaran.ket)', $data['search']);

        }
        if ($data['closed'] <> '') {
            $this->db->where('master.sasaran.status', $data['closed']);
        }
        $data['sasaran'] = $this->user->get_list_sasaran();
        $this->template->user('sasaran', $data, array('title' => 'Sasaran Audit', 'breadcrumbs' => array('Master')));
    }

    // Tujuan E-Audit
    public function tujuan()
    {
        $menu = '9312a5c1-7991-4eb1-be6d-05fef767935b';
        $data['access'] = list_access($menu);
        $this->load->model('user');
        if ($this->input->get_post('pk')) {
            check_access($menu, 'u');
            $id = $this->input->get_post('pk');
            $value = $this->input->get_post('value');
            $this->db->where('id', $id);
            $this->db->update('master.tujuan', array($this->input->get_post('name') => $value));
            return;
        } elseif ($this->input->get_post('ket')) {
            check_access($menu, 'c');
            $data['status_update'] = $this->user->insert_tujuan($this->input->get_post(null));
            if ($data['status_update']) {
                $this->session->set_flashdata('status_update', true);
            }
            redirect(base_url('master/tujuan'));
        } elseif ($this->input->get_post('status')) {
            check_access($menu, 'u');
            $this->user->set_tujuan_status($this->input->get_post('status'));
            return;
        }
        $data['closed'] = $this->input->get_post('closed') ? $this->input->get_post('closed') : '';
        $data['search'] = $this->input->get_post('search') ? strtolower($this->input->get_post('search')) : '';
        if ($data['search'] <> '') {
            $this->db->like('master.tujuan.kode', $data['search']);
            $this->db->or_like('lower(master.tujuan.ket)', $data['search']);

        }
        if ($data['closed'] <> '') {
            $this->db->where('master.tujuan.status', $data['closed']);
        }
        $data['tujuan'] = $this->user->get_list_tujuan();
        $this->template->user('tujuan', $data, array('title' => 'Tujuan Audit', 'breadcrumbs' => array('Master')));
    }

    // Peran E-Audit
    public function peran()
    {
        $menu = 'd66ab356-6792-451d-8f42-84f07e48b9e7';
        $data['access'] = list_access($menu);
        $this->load->model('user');
        if ($this->input->get_post('pk')) {
            check_access($menu, 'u');
            $id = $this->input->get_post('pk');
            $value = $this->input->get_post('value');
            $this->db->where('id', $id);
            $this->db->update('master.peran', array($this->input->get_post('name') => $value));
            return;
        } elseif ($this->input->get_post('ket')) {
            check_access($menu, 'c');
            $data['status_update'] = $this->user->insert_peran($this->input->get_post(null));
            if ($data['status_update']) {
                $this->session->set_flashdata('status_update', true);
            }
            redirect(base_url('master/peran'));
        } elseif ($this->input->get_post('status')) {
            check_access($menu, 'u');
            $this->user->set_peran_status($this->input->get_post('status'));
            return;
        }
        $data['peran'] = $this->user->get_list_peran();
        $this->template->user('peran', $data, array('title' => 'Peran Audit', 'breadcrumbs' => array('Master')));
    }

    // Golongan E-Audit
    public function golongan()
    {
        $menu = 'e89cdedb-32f8-47fd-bbfc-960c894dae01';
        $data['access'] = list_access($menu);
        $this->load->model('user');
        if ($this->input->get_post('pk')) {
            check_access($menu, 'u');
            $id = $this->input->get_post('pk');
            $value = $this->input->get_post('value');
            $this->db->where('id', $id);
            $this->db->update('master.golongan', array($this->input->get_post('name') => $value));
            return;
        } elseif ($this->input->get_post('ket')) {
            check_access($menu, 'c');
            $data['status_update'] = $this->user->insert_golongan($this->input->get_post(null));
            if ($data['status_update']) {
                $this->session->set_flashdata('status_update', true);
            }
            redirect(base_url('master/golongan'));
        } elseif ($this->input->get_post('status')) {
            check_access($menu, 'u');
            $this->user->set_golongan_status($this->input->get_post('status'));
            return;
        }
        $data['closed'] = $this->input->get_post('closed') ? $this->input->get_post('closed') : '';
        $data['search'] = $this->input->get_post('search') ? strtolower($this->input->get_post('search')) : '';
        if ($data['search'] <> '') {
            $this->db->like('lower(master.golongan.ket)', $data['search']);

        }
        if ($data['closed'] <> '') {
            $this->db->where('master.golongan.status', $data['closed']);
        }
        $data['golongan'] = $this->user->get_list_golongan();
        $this->template->user('golongan', $data, array('title' => 'Golongan', 'breadcrumbs' => array('Master')));
    }

    public function session()
    {
        $menu = 'd66ab356-6792-451d-8f42-84f07e48b9e7';
        check_access($menu, 'd');
        if ($this->input->get_post('clear')) {
            $this->db->truncate('sessions');
            redirect(base_url('master/session'));
        } elseif ($this->input->get_post('c')) {
            $this->db->where('id', $this->input->get_post('c'));
            $this->db->delete('sessions');
            redirect(base_url('master/session'));
        }
        $this->db->order_by('timestamp');
        $this->db->where('timestamp >= ', (time() - 7200));
        $data['session'] = $this->db->get('sessions')->result_array();
        $this->template->user('session', $data, array('title' => 'Session Active', 'breadcrumbs' => array('Master')));
    }
}
