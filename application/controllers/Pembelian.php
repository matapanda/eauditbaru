<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pembelian extends CI_Controller {
    public function __construct()
    {
        parent::__construct();
        check_access('3d4ca5e7-8f17-4537-8d2a-4b0f02c3edf7');
        $this->template->set_view('pembelian');
    }

    public function index()
    {
        $this->permintaan();
    }
    public function permintaan($page=0)
    {
        $menu='f577d9b2-3ac6-43af-8c55-81f0652ffd72';
        $data['access']=list_access($menu);
        $this->load->model('barang');
        if($this->input->get_post('e')){
            check_access($menu,'u');
            $id=$this->input->get_post('e');
            if($this->input->get_post('updateppb')==$id){
                $data['status_update']=$this->barang->permintaan_pembelian_update($id,$this->input->get_post(null));
            }
            $this->db->order_by('kode,nama');
            $this->db->where('status',TRUE);
            $data['unit']=$this->db->get('main.mstunit')->result_array();
            $this->db->order_by('kode,nama');
            $this->db->where('status',TRUE);
            $data['barang']=$this->db->get('main.mstitem')->result_array();

            $this->db->where('id',$id);
            $this->db->where('editable',true);
            $data['header']=$this->db->get('pembelian.vpermintaanorder')->row_array();
            $this->db->where('header',$data['header']['id']);
            $data['detail']=$this->db->get('pembelian.vpermintaanorderd')->result_array();
            $this->template->user('updatepermintaanpembelian',$data,array('title'=>'Update Data','breadcrumbs'=>array('Pembelian','Permintaan')));
        }elseif($this->input->get_post('list')){
            $data=$this->session->userdata('csrf');
            echo json_encode($data);
            echo "<hr>";
//            check_csrf(false,'9249a7cbf8a2d12f0570e58406f96bd1');
            $data=$this->session->userdata('csrf');
            echo json_encode($data);die();
        }elseif(in_array(strtolower($this->input->get_post('new')),array('jasa','barang'))){
            check_access($menu,'c');
            if($this->input->get_post('items')){
                if(check_csrf(false,$this->input->get_post('csrf'))){
                    $data['status_update']=$this->barang->permintaan_pembelian($this->input->get_post(null));
                }else{
                    $data['status_update']=false;
                }
            }
            $data['type']=strtolower($this->input->get_post('new'));
            $this->db->order_by('kode,nama');
            $this->db->where('status',TRUE);
            $data['unit']=$this->db->get('main.mstunit')->result_array();
            $this->template->user('addpermintaanpembelian',$data,array('title'=>'Tambah Data','breadcrumbs'=>array('Pembelian','Permintaan')));
        }elseif($this->input->get_post('d')){
            $this->db->where('id',$this->input->get_post('d'));
            $item=$this->db->get('pembelian.trpermintaanorderd')->result_array();
            foreach ($item as $no=>$d){
                echo "<tr><td class='center'>".($no+1)."</td><td>$d[nama]</td><td class='center'>$d[jumlah]</td><td>$d[note]</td><td class='center'>".format_waktu($d['diperlukan'])."</td><td class='center'>".getLabelProcess($d['status'],false)."</td></tr>";
            }
            return;
        }elseif($this->input->get_post('query')){
            $this->db->limit(10);
            if($this->input->get_post('k')){
                $this->db->order_by('nik,nama','asc');
                $this->db->like('lower(nama)',strtolower($this->input->get_post('query')));
                $this->db->or_where('lower(nik)',strtolower($this->input->get_post('query')));
                $this->db->where('status',TRUE);
                $this->db->select("id as id,nik||' - '||nama as label");
                $data=$this->db->get('main.mstkaryawan')->result_array();
            }elseif($this->input->get_post('bj')){
                $this->db->order_by('kode,nama','asc');
                $this->db->where('status',TRUE);
                $this->db->where('type',($this->input->get_post('bj')=='b'?TRUE:FALSE));
                $this->db->like('lower(nama)',strtolower($this->input->get_post('query')));
                $this->db->or_where('lower(kode)',strtolower($this->input->get_post('query')));
                $this->db->select("id as id,kode||' - '||nama as label");
                $data=$this->db->get('main.mstitem')->result_array();
            }
            echo json_encode($data);
            return;
        }else{
            $data['nota']=$this->input->get_post('nota');
            $data['closed']=$this->input->get_post('closed')?$this->input->get_post('closed'):(isset($_GET['closed'])?'':'f');
            if($this->input->get_post('start') && $this->input->get_post('end')){
                $data['start']=$this->input->get_post('start');
                $data['end']=$this->input->get_post('end');
            }else{
                $data['start']=date('d/m/Y',strtotime('-1 months'));;
                $data['end']=date('d/m/Y',strtotime('+1 months'));
            }
            $start=format_waktu($data['start'],true);
            $end=format_waktu($data['end'],true);
            $perpage=10;
            if($this->input->get_post('nota')){
                $this->db->where('lower(nota)',strtolower($data['nota']));
            }
            if($data['closed']){
                $this->db->where('closed',$data['closed']);
            }
            $this->db->where('tanggal >= ',$start);
            $this->db->where('tanggal <= ',$end);
            $total=$this->db->get('pembelian.vpermintaanorder')->num_rows();
            $this->db->limit($perpage,$page);
            $this->db->order_by('tanggal,nota');
            if($this->input->get_post('nota')){
                $this->db->where('lower(nota)',strtolower($data['nota']));
            }
            if($data['closed']){
                $this->db->where('closed',$data['closed']);
            }
            $this->db->where('tanggal >= ',$start);
            $this->db->where('tanggal <= ',$end);
            $data['data']=$this->db->get('pembelian.vpermintaanorder')->result_array();
            $data['pagination']=$this->template->pagination('pembelian/permintaan',$total,$perpage);
            $data['page']=$page;
            $this->template->user('permintaanpembelian',$data,array('title'=>'Permintaan','breadcrumbs'=>array('Pembelian')));
        }
    }
    public function order($page=0){
        $menu='2f4e5764-01c0-49f5-b178-555f814a015a';
        $data['access']=list_access($menu);
        $this->load->model('barang');
        if($this->session->flashdata('status_update')){
            $data['status_update'] = $this->session->flashdata('status_update');
        }
        if($this->input->get_post('save')){
            check_access($menu,'c');
            $this->session->set_flashdata('status_update', $this->barang->order_pembelian());
        }elseif($this->input->get_post('update')){
            check_access($menu,'u');
            $this->session->set_flashdata('status_update', $this->barang->order_pembelian_update());
        }elseif($this->input->get_post('e')){
            exit('belum');
            check_access($menu,'u');
            $id=$this->input->get_post('e');
            $this->db->order_by('kode,nama');
            $this->db->where('status',TRUE);
            $data['unit']=$this->db->get('main.mstunit')->result_array();
            $this->db->order_by('kode,nama');
            $this->db->where('status',TRUE);
            $data['barang']=$this->db->get('main.mstitem')->result_array();

            $this->db->where('id',$id);
            $this->db->where('editable',true);
            $data['header']=$this->db->get('pembelian.vpembelianorder')->row_array();

            $this->db->where('id',$data['header']['id']);
            $data['data']=$this->db->get('pembelian.trpembelianorderd')->result_array();

            $this->db->order_by('kode,nama');
            $this->db->where('status',TRUE);
            $data['supplier']=$this->db->get('main.mstsupplier')->result_array();
            $this->template->user('updateorderpembelian',$data,array('title'=>'Form','breadcrumbs'=>array('Pembelian','Order')));
        }elseif($this->input->get_post('new')){
            check_access($menu,'c');
            if($this->input->get_post('permintaan')){
                $permintaan=$this->input->get_post('permintaan');
                $this->db->order_by('diperlukan,tanggal,nota');
                $this->db->where('status',false);
                $this->db->where('closed',false);
                $this->db->where_in('header||\'_\'||mstitem',$permintaan);
                $data['data']=$this->db->get('pembelian.vpermintaanorderdd')->result_array();
            }else{
                $data['data']=array();
            }
            if(count($data['data'])==0){
                redirect(base_url());
            }
            $this->db->order_by('kode,nama');
            $this->db->where('status',TRUE);
            $data['supplier']=$this->db->get('main.mstsupplier')->result_array();
            $this->load->model('akuntansi');
            $data['metode']=$this->akuntansi->get_coa_payment();
            $this->template->user('addorderpembelian',$data,array('title'=>'Form','breadcrumbs'=>array('Pembelian','Order')));
        }elseif($this->input->get_post('permintaan')){
            $this->db->order_by('diperlukan,tanggal,nota');
            $this->db->where('status',false);
            $this->db->where('closed',false);
            $data['data']=$this->db->get('pembelian.vpermintaanorderdd')->result_array();
            $data['page']=$page;
            $this->template->user('orderfrompermintaan',$data,array('title'=>'Order dari daftar permintaan','breadcrumbs'=>array('Pembelian','Order')));
        }elseif($this->input->get_post('d')){
            header("Access-Control-Allow-Orgin: *");
            header("Access-Control-Allow-Methods: *");
            header("Content-Type: application/json");
            $this->db->where('id',$this->input->get_post('d'));
            $item=$this->db->get('pembelian.vpembelianorderd')->result_array();
            $data['data']='';
            foreach ($item as $no=>$d){
                $data['data'].="<tr><td class='center'>$d[nota]</td><td>$d[nama]</td><td class='center'>$d[qty]</td><td class='right'>".format_uang($d['hargabeli'])."</td><td class='right'>".format_uang($d['diskon'])."</td><td class='right'>".format_uang($d['dpp'])."</td><td class='right'>".format_uang($d['ppn'])."</td><td class='right'>".format_uang($d['pph'])."</td><td class='right'>".format_uang($d['grandtotal'])."</td><td class='center'>".($d['type']=='t'?'BARANG':'JASA')."</td><td class='center'>".getLabelProcess($d['status'],false)."</td></tr>";
            }
            $this->db->where('truangmuka.refid',$this->input->get_post('d'));
            $this->db->where('truangmuka.reftrans','pembelian.trpembelianorder');
            $header=$this->db->get('main.truangmuka')->row_array();
            $data['uangmuka']=isset($header['nota'])?($header['nota']." - ".format_uang($header['uangmuka'])):"-";
            echo json_encode($data);
            return;
        }elseif($this->input->get_post('query')){
            $this->db->limit(10);
            if($this->input->get_post('k')){
                $this->db->order_by('nik,nama','asc');
                $this->db->like('lower(nama)',strtolower($this->input->get_post('query')));
                $this->db->or_where('lower(nik)',strtolower($this->input->get_post('query')));
                $this->db->where('status',TRUE);
                $this->db->select("id as id,nik||' - '||nama as label");
                $data=$this->db->get('main.mstkaryawan')->result_array();
            }elseif($this->input->get_post('bj')){
                $this->db->order_by('kode,nama','asc');
                $this->db->where('status',TRUE);
                $this->db->like('lower(nama)',strtolower($this->input->get_post('query')));
                $this->db->or_where('lower(kode)',strtolower($this->input->get_post('query')));
                $this->db->select("id as id,kode||' - '||nama as label");
                $data=$this->db->get('main.mstitem')->result_array();
            }
            echo json_encode($data);
            return;
        }else{
            $data['nota']=$this->input->get_post('nota');
            $data['closed']=$this->input->get_post('closed')?$this->input->get_post('closed'):(isset($_GET['closed'])?'':'f');
            if($this->input->get_post('start') && $this->input->get_post('end')){
                $data['start']=$this->input->get_post('start');
                $data['end']=$this->input->get_post('end');
            }else{
                $data['start']=date('d/m/Y',strtotime('-1 months'));;
                $data['end']=date('d/m/Y',strtotime('+1 months'));
            }
            $start=format_waktu($data['start'],true);
            $end=format_waktu($data['end'],true);
            $perpage=10;
            if($this->input->get_post('nota')){
                $this->db->where('lower(nota)',strtolower($data['nota']));
            }
            if($data['closed']){
                $this->db->where('closed',$data['closed']);
            }
            $this->db->where('tanggal >= ',$start);
            $this->db->where('tanggal <= ',$end);
            $total=$this->db->get('pembelian.vpembelianorder')->num_rows();
            $this->db->limit($perpage,$page);
            $this->db->order_by('tanggal,nota');
            if($this->input->get_post('nota')){
                $this->db->where('lower(nota)',strtolower($data['nota']));
            }
            if($data['closed']){
                $this->db->where('closed',$data['closed']);
            }
            $this->db->where('tanggal >= ',$start);
            $this->db->where('tanggal <= ',$end);
            $data['data']=$this->db->get('pembelian.vpembelianorder')->result_array();
            $data['pagination']=$this->template->pagination('pembelian/order',$total,$perpage);
            $data['page']=$page;
            $this->template->user('orderpembelian',$data,array('title'=>'Order','breadcrumbs'=>array('Pembelian')));
        }
    }
    public function umk($page=0){
        $menu='2f4e5764-01c0-49f5-b178-555f814a015a';
        $data['access']=list_access($menu);
        $this->load->model('barang');
        if($this->session->flashdata('status_update')){
            $data['status_update'] = $this->session->flashdata('status_update');
        }
        if($this->input->get_post('save')){
            check_access($menu,'c');
            $this->session->set_flashdata('status_update', $this->barang->pembayaran_umk());
        }elseif($this->input->get_post('nota_id_id')){
            check_access($menu,'c');
            $id=$this->input->get_post('nota_id_id');
            $this->db->where('truangmuka.closed',false);
            $this->db->where('truangmuka.masterid',$id);
            $this->db->where('truangmuka.jurnal_id is null',null,false);
            $data['data']=$this->db->get('main.truangmuka')->result_array();
            if(!isset($data['data'][0]['masterid'])){
                $this->session->set_flashdata('status_update', array('warning','Uangmuka dari supplier tidak ada'));
                redirect(base_url('pembelian/umk'));
            }
            $this->db->where('id',$id);
            $data['header']=$this->db->get('main.mstsupplier')->row_array();
            $this->load->model('akuntansi');
            $data['coa']=$this->akuntansi->get_coa_umk('pembelian',false);
            $data['metode']=$this->akuntansi->get_coa_payment();
            $this->template->user('umkbysupplier',$data,array('title'=>'Pembayaran Uang Muka','breadcrumbs'=>array('Pembelian')));
        }elseif($this->input->get_post('query')){
            $this->db->limit(10);
            if($this->input->get_post('k')){
                $this->db->order_by('nik,nama','asc');
                $this->db->like('lower(nama)',strtolower($this->input->get_post('query')));
                $this->db->or_where('lower(nik)',strtolower($this->input->get_post('query')));
                $this->db->where('status',TRUE);
                $this->db->select("id as id,nik||' - '||nama as label");
                $data=$this->db->get('main.mstkaryawan')->result_array();
            }elseif($this->input->get_post('kode')){
                $this->db->order_by('kode','asc');
                $this->db->where('status',true);
                $this->db->like('lower(kode||\' \'||nama)',strtolower($this->input->get_post('query')));
                $this->db->select("id as id,kode||' - '||nama as label");
                $data=$this->db->get('main.mstsupplier')->result_array();
            }
            echo json_encode($data);
            return;
        }else{
            $data['nota']=$this->input->get_post('nota');
            $data['closed']=$this->input->get_post('closed')?$this->input->get_post('closed'):(isset($_GET['closed'])?'':'f');
            if($this->input->get_post('id') && $this->input->get_post('debet') && $this->input->get_post('kredit')){
                $debet=explode(';',$this->input->get_post('debet'));
                $kredit=explode(';',$this->input->get_post('kredit'));
                $this->db->where('closed','f');
                $this->db->where('id',$this->input->get_post('id'));
                $data['end']=$this->db->update('main.truangmuka',array(
                    'debet'=>$debet[0],
                    'metode_d'=>'',
                    'kredit'=>$kredit[0],
                    'metode_k'=>$kredit[1],
                ));
            }
            if($this->input->get_post('start') && $this->input->get_post('end')){
                $data['start']=$this->input->get_post('start');
                $data['end']=$this->input->get_post('end');
            }else{
                $data['start']=date('d/m/Y',strtotime('-1 months'));;
                $data['end']=date('d/m/Y',strtotime('+1 months'));
            }
            $start=format_waktu($data['start'],true);
            $end=format_waktu($data['end'],true);
            $perpage=10;
            if($this->input->get_post('nota')){
                $this->db->where('lower(nota)',strtolower($data['nota']));
            }
            if($data['closed']){
                $this->db->where('closed',$data['closed']);
            }
            $this->db->where('jurnal_id is not null',null,false);
            $this->db->where('tanggal >= ',$start);
            $this->db->where('tanggal <= ',$end);
            $total=$this->db->get('main.truangmuka')->num_rows();
            $this->db->limit($perpage,$page);
            $this->db->order_by('truangmuka.tanggal,truangmuka.nota');
            if($this->input->get_post('nota')){
                $this->db->where('lower(truangmuka.nota)',strtolower($data['nota']));
            }
            if($data['closed']){
                $this->db->where('truangmuka.closed',$data['closed']);
            }
            $this->db->where('jurnal_id is not null',null,false);
            $this->db->where('truangmuka.tanggal >= ',$start);
            $this->db->where('truangmuka.tanggal <= ',$end);
            $this->db->join('main.mstsupplier','mstsupplier.id=truangmuka.masterid');
            $this->db->select('truangmuka.*,mstsupplier.kode||\' - \'||mstsupplier.nama as supplier');
            $data['data']=$this->db->get('main.truangmuka')->result_array();
            $this->load->model('akuntansi');
            $data['pagination']=$this->template->pagination('pembelian/umk',$total,$perpage);
            $data['page']=$page;
            $this->template->user('umk',$data,array('title'=>'Pembayaran Uang Muka','breadcrumbs'=>array('Pembelian')));
        }
    }
    public function penerimaan($page=0){
        $menu='9be5fdc8-3b57-4822-8840-3e2a9944385c';
        $data['access']=list_access($menu);
        $this->load->model('barang');
        if($this->session->flashdata('status_update')){
            $data['status_update'] = $this->session->flashdata('status_update');
        }
        if($this->input->get_post('save')){
            check_access($menu,'c');
            $this->session->set_flashdata('status_update', $this->barang->penerimaan_pembelian());
        }elseif($this->input->get_post('new') && $this->input->get_post('nota_id')){
            check_access($menu,'c');
            $id=$this->input->get_post('nota_id_id');
            $this->db->where('jumlah > diterima',null,FALSE);
            $this->db->where('type',true);
            $this->db->where('id',$id);
            $data['data']=$this->db->get('pembelian.trpembelianorderd')->result_array();
            if(!isset($data['data'][0]['id'])){
                $this->session->set_flashdata('status_update', array('warning','OPB tidak valid'));
                redirect(base_url('pembelian/penerimaan'));
            }
            $this->db->where('id',$id);
            $data['header']=$this->db->get('pembelian.vpembelianorder')->row_array();
            $this->template->user('addpenerimaan',$data,array('title'=>'Form Penerimaan','breadcrumbs'=>array('Pembelian','Penerimaan')));
        }elseif($this->input->get_post('d')){
            header("Access-Control-Allow-Orgin: *");
            header("Access-Control-Allow-Methods: *");
            header("Content-Type: application/json");
            $this->db->where('id',$this->input->get_post('d'));
            $item=$this->db->get('pembelian.trpenerimaand')->result_array();
            $data['data']='';
            foreach ($item as $no=>$d){
                $data['data'].="<tr><td class='center'>$d[nota]</td><td>$d[nama]</td><td class='center'>$d[jumlah]</td><td style='white-space: pre-wrap'>".htmlveryspecialchars($d['note'])."</td></tr>";
            }
            echo json_encode($data);
            return;
        }elseif($this->input->get_post('query')){
            $this->db->limit(10);
            if($this->input->get_post('k')){
                $this->db->order_by('nik,nama','asc');
                $this->db->like('lower(nama)',strtolower($this->input->get_post('query')));
                $this->db->or_where('lower(nik)',strtolower($this->input->get_post('query')));
                $this->db->where('status',TRUE);
                $this->db->select("id as id,nik||' - '||nama as label");
                $data=$this->db->get('main.mstkaryawan')->result_array();
            }elseif($this->input->get_post('opb')){
                $this->db->group_by('trpembelianorder.id,trpembelianorder.nota','asc');
                $this->db->order_by('trpembelianorder.tanggal,trpembelianorder.nota','asc');
                $this->db->like('lower(trpembelianorder.nota)',strtolower($this->input->get_post('query')));
                $this->db->join("pembelian.trpembelianorderd",'trpembelianorder.id=trpembelianorderd.id AND trpembelianorderd.type=\'t\' AND trpembelianorderd.diterima<trpembelianorderd.jumlah');
                $this->db->select("trpembelianorder.id as id,trpembelianorder.nota as label");
                $data=$this->db->get('pembelian.trpembelianorder')->result_array();
            }
            echo json_encode($data);
            return;
        }else{
            $data['nota']=$this->input->get_post('nota');
            $data['closed']=$this->input->get_post('closed')?$this->input->get_post('closed'):(isset($_GET['closed'])?'':'f');
            if($this->input->get_post('start') && $this->input->get_post('end')){
                $data['start']=$this->input->get_post('start');
                $data['end']=$this->input->get_post('end');
            }else{
                $data['start']=date('d/m/Y',strtotime('-1 months'));;
                $data['end']=date('d/m/Y',strtotime('+1 months'));
            }
            $start=format_waktu($data['start'],true);
            $end=format_waktu($data['end'],true);
            $perpage=10;
            if($this->input->get_post('nota')){
                $this->db->where('lower(nota)',strtolower($data['nota']));
            }
            if($data['closed']){
                $this->db->where('closed',$data['closed']);
            }
            $this->db->where('tanggal >= ',$start);
            $this->db->where('tanggal <= ',$end);
            $total=$this->db->get('pembelian.vpenerimaanbarang')->num_rows();
            $this->db->limit($perpage,$page);
            $this->db->order_by('tanggal,nota');
            if($this->input->get_post('nota')){
                $this->db->where('lower(nota)',strtolower($data['nota']));
            }
            if($data['closed']){
                $this->db->where('closed',$data['closed']);
            }
            $this->db->where('tanggal >= ',$start);
            $this->db->where('tanggal <= ',$end);
            $data['data']=$this->db->get('pembelian.vpenerimaanbarang')->result_array();
            $data['pagination']=$this->template->pagination('pembelian/penerimaan',$total,$perpage);
            $data['page']=$page;
            $this->template->user('penerimaan',$data,array('title'=>'Penerimaan Barang','breadcrumbs'=>array('Pembelian')));
        }
    }
    public function penyelesaian($page=0){
        $menu='9be5fdc8-3b57-4822-8840-3e2a9944385c';
        $data['access']=list_access($menu);
        $this->load->model('barang');
        if($this->session->flashdata('status_update')){
            $data['status_update'] = $this->session->flashdata('status_update');
        }
        if($this->input->get_post('save')){
            check_access($menu,'c');
            $this->session->set_flashdata('status_update', $this->barang->penyelesaian_pembelian());
        }elseif($this->input->get_post('new') && $this->input->get_post('nota_id')){
            check_access($menu,'c');
            $id=$this->input->get_post('nota_id_id');
            $this->db->where('jumlah > diterima',null,FALSE);
            $this->db->where('type',false);
            $this->db->where('id',$id);
            $data['data']=$this->db->get('pembelian.trpembelianorderd')->result_array();

            if(!isset($data['data'][0]['id'])){
                $this->session->set_flashdata('status_update', array('warning','OPB tidak valid'));
                redirect(base_url('pembelian/penyelesaian'));
            }
            $permintaan=$data['data'][0]['permintaan'];
            $this->db->where('permintaan',$permintaan);
            $penerimaan=$this->db->get('pembelian.trpenerimaand')->result_array();
            $data['penerimaan']=array();
            foreach ($penerimaan as $i) {
                $data['penerimaan'][$i['mstitem']]=$i['beban'];
            }

            $this->db->where('id',$id);
            $data['header']=$this->db->get('pembelian.vpembelianorder')->row_array();

            $this->load->model('akuntansi');
            $data['beban']=$this->akuntansi->get_coa(array('9be41de3-c573-4e2f-b9de-bcdde8658614','1'));

            $this->template->user('addpenyelesaian',$data,array('title'=>'Form Penyelesaian Jasa','breadcrumbs'=>array('Pembelian','Penerimaan')));
        }elseif($this->input->get_post('d')){
            header("Access-Control-Allow-Orgin: *");
            header("Access-Control-Allow-Methods: *");
            header("Content-Type: application/json");
            $this->db->where('id',$this->input->get_post('d'));
            $item=$this->db->get('pembelian.trpenerimaand')->result_array();
            $data['data']='';
            foreach ($item as $no=>$d){
                $data['data'].="<tr><td class='center'>$d[nota]</td><td>$d[nama]</td><td class='center'>$d[jumlah]</td><td style='white-space: pre-wrap'>".htmlveryspecialchars($d['note'])."</td></tr>";
            }
            echo json_encode($data);
            return;
        }elseif($this->input->get_post('query')){
            $this->db->limit(10);
            if($this->input->get_post('k')){
                $this->db->order_by('nik,nama','asc');
                $this->db->like('lower(nama)',strtolower($this->input->get_post('query')));
                $this->db->or_where('lower(nik)',strtolower($this->input->get_post('query')));
                $this->db->where('status',TRUE);
                $this->db->select("id as id,nik||' - '||nama as label");
                $data=$this->db->get('main.mstkaryawan')->result_array();
            }elseif($this->input->get_post('opb')){
                $this->db->group_by('trpembelianorder.id,trpembelianorder.nota','asc');
                $this->db->order_by('trpembelianorder.tanggal,trpembelianorder.nota','asc');
                $this->db->like('lower(trpembelianorder.nota)',strtolower($this->input->get_post('query')));
                $this->db->join("pembelian.trpembelianorderd",'trpembelianorder.id=trpembelianorderd.id AND trpembelianorderd.type=\'f\' AND trpembelianorderd.diterima<trpembelianorderd.jumlah');
                $this->db->select("trpembelianorder.id as id,trpembelianorder.nota as label");
                $data=$this->db->get('pembelian.trpembelianorder')->result_array();
            }
            echo json_encode($data);
            return;
        }else{
            $data['nota']=$this->input->get_post('nota');
            $data['closed']=$this->input->get_post('closed')?$this->input->get_post('closed'):(isset($_GET['closed'])?'':'f');
            if($this->input->get_post('start') && $this->input->get_post('end')){
                $data['start']=$this->input->get_post('start');
                $data['end']=$this->input->get_post('end');
            }else{
                $data['start']=date('d/m/Y',strtotime('-1 months'));;
                $data['end']=date('d/m/Y',strtotime('+1 months'));
            }
            $start=format_waktu($data['start'],true);
            $end=format_waktu($data['end'],true);
            $perpage=10;
            if($this->input->get_post('nota')){
                $this->db->where('lower(nota)',strtolower($data['nota']));
            }
            if($data['closed']){
                $this->db->where('closed',$data['closed']);
            }
            $this->db->where('tanggal >= ',$start);
            $this->db->where('tanggal <= ',$end);
            $total=$this->db->get('pembelian.vpenyelesaianjasa')->num_rows();
            $this->db->limit($perpage,$page);
            $this->db->order_by('tanggal,nota');
            if($this->input->get_post('nota')){
                $this->db->where('lower(nota)',strtolower($data['nota']));
            }
            if($data['closed']){
                $this->db->where('closed',$data['closed']);
            }
            $this->db->where('tanggal >= ',$start);
            $this->db->where('tanggal <= ',$end);
            $data['data']=$this->db->get('pembelian.vpenyelesaianjasa')->result_array();
            $data['pagination']=$this->template->pagination('pembelian/penyelesaian',$total,$perpage);
            $data['page']=$page;
            $this->template->user('penyelesaian',$data,array('title'=>'Penyelesaian Jasa','breadcrumbs'=>array('Pembelian')));
        }
    }
    public function retur($page=0){
        $menu='7a2eee70-8883-4c74-9a9b-e820f2249b4b';
        $data['access']=list_access($menu);
        $this->load->model('barang');
        if($this->input->get_post('save')){
            check_access($menu,'c');
            $this->session->set_flashdata('status_update', $this->barang->retur_pembelian());
        }elseif($this->input->get_post('delete')){
            check_access($menu,'d');
            $this->db->where('id',$this->input->get_post('delete'));
            $this->db->where('closed',false);
            $this->db->where('cancel',false);
            $this->db->where('editable',true);
            $this->db->update('pembelian.trretur',array(
                'closed'=>true,
                'editable'=>false,
                'cancel'=>true,
                'status'=>false,
            ));
        }elseif($this->input->get_post('new') && $this->input->get_post('nota_id')){
            check_access($menu,'c');
            $id=$this->input->get_post('nota_id_id');
            $this->db->where('jumlah > retur+terjual',null,FALSE);
            $this->db->where('id',$id);
            $data['data']=$this->db->get('pembelian.trpenerimaand')->result_array();
            if(!isset($data['data'][0]['id'])){
                $this->session->set_flashdata('status_update', array('warning','FPB tidak valid'));
                redirect(base_url('pembelian/retur'));
            }
            $this->db->where('id',$id);
            $data['header']=$this->db->get('pembelian.vpenerimaanbarang')->row_array();
            $this->template->user('addretur',$data,array('title'=>'Form Retur Pembelian','breadcrumbs'=>array('Pembelian','Retur')));
        }elseif($this->input->get_post('d')){
            header("Access-Control-Allow-Orgin: *");
            header("Access-Control-Allow-Methods: *");
            header("Content-Type: application/json");
            $this->db->where('id',$this->input->get_post('d'));
            $item=$this->db->get('pembelian.trreturd')->result_array();
            $data['data']='';
            foreach ($item as $no=>$d){
                $data['data'].="<tr><td class='center'>$d[nota]</td><td>$d[nama]</td><td class='center'>$d[jumlah]</td><td style='white-space: pre-wrap'>".htmlveryspecialchars($d['note'])."</td></tr>";
            }
            echo json_encode($data);
            return;
        }elseif($this->input->get_post('query')){
            $this->db->limit(10);
            if($this->input->get_post('k')){
                $this->db->order_by('nik,nama','asc');
                $this->db->like('lower(nama)',strtolower($this->input->get_post('query')));
                $this->db->or_where('lower(nik)',strtolower($this->input->get_post('query')));
                $this->db->where('status',TRUE);
                $this->db->select("id as id,nik||' - '||nama as label");
                $data=$this->db->get('main.mstkaryawan')->result_array();
            }elseif($this->input->get_post('fpb')){
                $this->db->order_by('h.tanggal,h.nota','asc');
                $this->db->group_by('h.tanggal,h.id,h.nota');
                $this->db->having('SUM(d.jumlah) > SUM(d.retur+d.terjual)');
                $this->db->like('lower(h.nota)',strtolower($this->input->get_post('query')));
                $this->db->select("h.id as id,h.nota as label");
                $this->db->join("pembelian.trpenerimaand d","d.id=h.id");
                $data=$this->db->get('pembelian.trpenerimaan h')->result_array();
            }
            echo json_encode($data);
            return;
        }else{
            $data['nota']=$this->input->get_post('nota');
            $data['closed']=$this->input->get_post('closed')?$this->input->get_post('closed'):(isset($_GET['closed'])?'':'f');
            if($this->input->get_post('start') && $this->input->get_post('end')){
                $data['start']=$this->input->get_post('start');
                $data['end']=$this->input->get_post('end');
            }else{
                $data['start']=date('d/m/Y',strtotime('-1 months'));;
                $data['end']=date('d/m/Y',strtotime('+1 months'));
            }
            $start=format_waktu($data['start'],true);
            $end=format_waktu($data['end'],true);
            $perpage=10;
            if($this->input->get_post('nota')){
                $this->db->where('lower(nota)',strtolower($data['nota']));
            }
            if($data['closed']){
                $this->db->where('closed',$data['closed']);
            }
            $this->db->where('tanggal >= ',$start);
            $this->db->where('tanggal <= ',$end);
            $total=$this->db->get('pembelian.trretur')->num_rows();
            $this->db->limit($perpage,$page);
            $this->db->order_by('tanggal,nota');
            if($this->input->get_post('nota')){
                $this->db->where('lower(nota)',strtolower($data['nota']));
            }
            if($data['closed']){
                $this->db->where('closed',$data['closed']);
            }
            $this->db->where('tanggal >= ',$start);
            $this->db->where('tanggal <= ',$end);
            $data['data']=$this->db->get('pembelian.vretur')->result_array();
            $data['pagination']=$this->template->pagination('pembelian/retur',$total,$perpage);
            $data['page']=$page;
            $this->template->user('retur',$data,array('title'=>'Retur Barang','breadcrumbs'=>array('Pembelian')));
        }
    }
    public function tagihan($page=0){
        $menu='1541696d-a165-4a06-9181-88472bd61293';
        $data['access']=list_access($menu);
        $this->load->model('barang');
        if($this->session->flashdata('status_update')){
            $data['status_update'] = $this->session->flashdata('status_update');
        }
        if($this->input->get_post('save')){
            check_access($menu,'c');
            $this->session->set_flashdata('status_update', $this->barang->tagihan_pembelian());
        }elseif($this->input->get_post('nota_id_id')){
            check_access($menu,'c');
            $id=$this->input->get_post('nota_id_id');
            $this->db->where('supplier',$id);
            $data['data']=$this->db->get('pembelian.vlisttagihan')->result_array();
            if(!isset($data['data'][0]['supplier'])){
                $this->session->set_flashdata('status_update', array('warning','Tagihan dari supplier tidak ada'));
                redirect(base_url('pembelian/tagihan'));
            }
            $this->db->where('id',$id);
            $data['header']=$this->db->get('main.mstsupplier')->row_array();

            $this->template->user('tagihanbysupplier',$data,array('title'=>'Tagihan Supplier','breadcrumbs'=>array('Pembelian')));
        }elseif($this->input->get_post('d')){
            header("Access-Control-Allow-Orgin: *");
            header("Access-Control-Allow-Methods: *");
            header("Content-Type: application/json");
            $this->db->where('id',$this->input->get_post('d'));
            $this->db->select('orderdate,orderno,nota,tanggal,harga,dpp,ppn,pph,diskon');
            $item=$this->db->get('pembelian.trtagihansupplierd')->result_array();
            $data['data']='';
            foreach ($item as $no=>$d){
                $data['data'].="<tr><td class='center'>$d[orderno]</td><td class='center'>".format_waktu($d['orderdate'])."</td><td class='center'>$d[nota]</td><td class='center'>".format_waktu($d['tanggal'])."</td><td class='right'>".format_uang($d['dpp'])."</td><td class='right'>".format_uang($d['ppn'])."</td><td class='right'>".format_uang($d['pph'])."</td><td class='right'>".format_uang($d['diskon'])."</td><td class='right'>".format_uang($d['harga'])."</td></tr>";
            }
            echo json_encode($data);
            return;
        }elseif($this->input->get_post('query')){
            $this->db->limit(10);
            if($this->input->get_post('k')){
                $this->db->order_by('nik,nama','asc');
                $this->db->like('lower(nama)',strtolower($this->input->get_post('query')));
                $this->db->or_where('lower(nik)',strtolower($this->input->get_post('query')));
                $this->db->where('status',TRUE);
                $this->db->select("id as id,nik||' - '||nama as label");
                $data=$this->db->get('main.mstkaryawan')->result_array();
            }elseif($this->input->get_post('kode')){
                $this->db->order_by('kode','asc');
                $this->db->where('status',true);
                $this->db->like('lower(kode||\' \'||nama)',strtolower($this->input->get_post('query')));
                $this->db->select("id as id,kode||' - '||nama as label");
                $data=$this->db->get('main.mstsupplier')->result_array();
            }
            echo json_encode($data);
            return;
        }else{
            $data['nota']=$this->input->get_post('nota');
            $data['closed']=$this->input->get_post('closed')?$this->input->get_post('closed'):(isset($_GET['closed'])?'':'f');
            if($this->input->get_post('start') && $this->input->get_post('end')){
                $data['start']=$this->input->get_post('start');
                $data['end']=$this->input->get_post('end');
            }else{
                $data['start']=date('d/m/Y',strtotime('-1 months'));;
                $data['end']=date('d/m/Y',strtotime('+1 months'));
            }
            $start=format_waktu($data['start'],true);
            $end=format_waktu($data['end'],true);
            $perpage=10;
            if($this->input->get_post('nota')){
                $this->db->where('lower(nota)',strtolower($data['nota']));
            }
            if($data['closed']){
                $this->db->where('closed',$data['closed']);
            }
            $this->db->where('tanggal >= ',$start);
            $this->db->where('tanggal <= ',$end);
            $total=$this->db->get('pembelian.trtagihansupplier')->num_rows();
            $this->db->limit($perpage,$page);
            $this->db->order_by('tanggal,nota');
            if($this->input->get_post('nota')){
                $this->db->where('lower(nota)',strtolower($data['nota']));
            }
            if($data['closed']){
                $this->db->where('closed',$data['closed']);
            }
            $this->db->where('tanggal >= ',$start);
            $this->db->where('tanggal <= ',$end);
            $data['data']=$this->db->get('pembelian.vtagihan')->result_array();
            $data['pagination']=$this->template->pagination('pembelian/tagihan',$total,$perpage);

//            $this->db->order_by('supplier_id');
//            $this->db->group_by('supplier,supplier_id,supplier_name,hutangpiutang');
//            $this->db->select('supplier,supplier_id,supplier_name,hutangpiutang,SUM(harga-diskon) as tagihan');
//            $data['pending']=$this->db->get('pembelian.vlisttagihan')->result_array();

            $data['page']=$page;
            $this->template->user('tagihan',$data,array('title'=>'Tagihan Supplier','breadcrumbs'=>array('Pembelian')));
        }
    }
    public function pembayaran($page=0){
        $menu='36fbf7b0-d4ba-49d1-af67-1e0af4a05766';
        $data['access']=list_access($menu);
        $this->load->model('barang');
        if($this->session->flashdata('status_update')){
            $data['status_update'] = $this->session->flashdata('status_update');
        }
        if($this->input->get_post('save')){
            check_access($menu,'c');
            $this->session->set_flashdata('status_update', $this->barang->pembayaran_pembelian());
        }elseif($this->input->get_post('nota_id_id')){
            check_access($menu,'c');
            $id=$this->input->get_post('nota_id_id');
            $this->db->where('supplier',$id);
            $this->db->where('closed',false);
            $data['data']=$this->db->get('pembelian.vtagihan')->result_array();
            if(!isset($data['data'][0]['supplier'])){
                $this->session->set_flashdata('status_update', array('warning','Tagihan dari supplier tidak ada'));
                redirect(base_url('pembelian/pembayaran'));
            }
            $this->db->where('id',$id);
            $data['header']=$this->db->get('main.mstsupplier')->row_array();
            $this->load->model('akuntansi');
            $data['metode']=$this->akuntansi->get_coa_payment();
            $this->template->user('bayarbysupplier',$data,array('title'=>'Pembayaran Utang Usaha','breadcrumbs'=>array('Pembelian')));
        }elseif($this->input->get_post('d')){
            header("Access-Control-Allow-Orgin: *");
            header("Access-Control-Allow-Methods: *");
            header("Content-Type: application/json");
            $this->db->where('id',$this->input->get_post('d'));
            $this->db->select('nota,harga,ppn,pph,diskon,(harga-diskon) as tagihan');
            $item=$this->db->get('pembelian.trpembayaransupplierd')->result_array();
            $data['data']='';
            foreach ($item as $no=>$d){
                $data['data'].="<tr><td class='center'>$d[nota]</td><td class='right'>".format_uang($d['ppn'])."</td><td class='right'>".format_uang($d['pph'])."</td><td class='right'>".format_uang($d['harga'])."</td><td class='right'>".format_uang($d['diskon'])."</td><td class='right'>".format_uang($d['tagihan'])."</td></tr>";
            }
            echo json_encode($data);
            return;
        }elseif($this->input->get_post('query')){
            $this->db->limit(10);
            if($this->input->get_post('k')){
                $this->db->order_by('nik,nama','asc');
                $this->db->like('lower(nama)',strtolower($this->input->get_post('query')));
                $this->db->or_where('lower(nik)',strtolower($this->input->get_post('query')));
                $this->db->where('status',TRUE);
                $this->db->select("id as id,nik||' - '||nama as label");
                $data=$this->db->get('main.mstkaryawan')->result_array();
            }elseif($this->input->get_post('kode')){
                $this->db->order_by('kode','asc');
                $this->db->where('status',true);
                $this->db->like('lower(kode||\' \'||nama)',strtolower($this->input->get_post('query')));
                $this->db->select("id as id,kode||' - '||nama as label");
                $data=$this->db->get('main.mstsupplier')->result_array();
            }
            echo json_encode($data);
            return;
        }else{
            $data['nota']=$this->input->get_post('nota');
            if($this->input->get_post('start') && $this->input->get_post('end')){
                $data['start']=$this->input->get_post('start');
                $data['end']=$this->input->get_post('end');
            }else{
                $data['start']=date('d/m/Y',strtotime('-1 months'));;
                $data['end']=date('d/m/Y',strtotime('+1 months'));
            }
            $start=format_waktu($data['start'],true);
            $end=format_waktu($data['end'],true);
            $perpage=10;
            if($this->input->get_post('nota')){
                $this->db->where('lower(nota)',strtolower($data['nota']));
            }
            $this->db->where('tanggal >= ',$start);
            $this->db->where('tanggal <= ',$end);
            $total=$this->db->get('pembelian.vpembayaran')->num_rows();
            $this->db->limit($perpage,$page);
            $this->db->order_by('tanggal,nota');
            if($this->input->get_post('nota')){
                $this->db->where('lower(nota)',strtolower($data['nota']));
            }
            $this->db->where('tanggal >= ',$start);
            $this->db->where('tanggal <= ',$end);
            $data['data']=$this->db->get('pembelian.vpembayaran')->result_array();
            $data['pagination']=$this->template->pagination('pembelian/pembayaran',$total,$perpage);

            $this->db->order_by('supplier_id');
            $this->db->group_by('supplier,supplier_id,supplier_name,hutangpiutang');
            $this->db->where('closed',false);
            $this->db->select('supplier,supplier_id,supplier_name,hutangpiutang,SUM(harga-diskon-diskontambahan) as tagihan');
            $data['pending']=$this->db->get('pembelian.vtagihan')->result_array();
            $data['page']=$page;
            $this->template->user('pembayaran',$data,array('title'=>'Pembayaran Utang Usaha','breadcrumbs'=>array('Pembelian')));
        }
    }
}
