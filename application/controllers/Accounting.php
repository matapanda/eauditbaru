<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Accounting extends CI_Controller {
    public function __construct()
    {
        parent::__construct();
        check_access('fb7ca00c-ef46-4dc1-bf9f-5d00358213a0');
        $this->template->set_view('akuntansi');
    }

    public function index()
    {
        redirect(base_url());
    }
    public function detailjurnal(){
        if($this->input->get_post('d')){
            header("Access-Control-Allow-Orgin: *");
            header("Access-Control-Allow-Methods: *");
            header("Content-Type: application/json");
            $this->db->order_by('jurnald.no','asc');
            $this->db->where('jurnald.id',$this->input->get_post('d'));
            $this->db->join('akuntansi.coa','coa.id=jurnald.coa');
            $this->db->select("jurnald.*,coa.nama,(k1||'.'||k2||'.'||k3||'.'||k4) as kode");
            $item=$this->db->get('akuntansi.jurnald')->result_array();
            $data['data']='';
            foreach ($item as $no=>$d){
                $data['data'].="<tr><td class='center'>$d[no]</td><td>".format_coa($d['kode'])." - $d[nama]</td>";
                if($d['posisi']=="D"){
                    $data['data'].="<td class='right'>".format_uang($d['jumlah'])."</td>";
                    $data['data'].="<td class='right'></td>";
                }else{
                    $data['data'].="<td class='right'></td>";
                    $data['data'].="<td class='right'>".format_uang($d['jumlah'])."</td>";
                }
                $data['data'].="<td>$d[note]</td><td class='center'>$d[nobukti]</td>";
                $data['data'].="<td class='center'>$d[refnota]</td></tr>";
            }
            echo json_encode($data);
            return;
        }
    }
    public function jurnal($page=0)
    {
        $menu='da53f223-2170-4c58-b561-aa1af6721541';
        $data['access']=list_access($menu);
        $this->load->model('akuntansi');
        $data['nota']=$this->input->get_post('nota');
        $data['closed']=$this->input->get_post('closed')?$this->input->get_post('closed'):(isset($_GET['closed'])?'':'t');
        if($this->input->get_post('start') && $this->input->get_post('end')){
            $data['start']=$this->input->get_post('start');
            $data['end']=$this->input->get_post('end');
        }else{
            $data['start']=date('d/m/Y',strtotime('-1 months'));;
            $data['end']=date('d/m/Y',strtotime('+1 months'));
        }
        $start=format_waktu($data['start'],true);
        $end=format_waktu($data['end'],true);
        $perpage=10;
        if($this->input->get_post('nota')){
            $this->db->where('lower(nota)',strtolower($data['nota']));
        }
        if($data['closed']){
            $this->db->where('status',$data['closed']);
        }
        $this->db->where('tanggal >= ',$start);
        $this->db->where('tanggal <= ',$end);
        $total=$this->db->get('akuntansi.v_jurnal')->num_rows();
        $this->db->limit($perpage,$page);
        if($this->input->get_post('sort_by')=='type'){
            $this->db->order_by($this->input->get_post('sort_by').',tanggal,created_at,nota');
        }else{
            $this->db->order_by('tanggal,created_at,nota');
        }
        if($this->input->get_post('nota')){
            $this->db->where('lower(nota)',strtolower($data['nota']));
        }
        if($data['closed']){
            $this->db->where('status',$data['closed']);
        }
        $this->db->where('tanggal >= ',$start);
        $this->db->where('tanggal <= ',$end);
        $data['data']=$this->db->get('akuntansi.v_jurnal')->result_array();
        $data['pagination']=$this->template->pagination('accounting/jurnal',$total,$perpage);
        $data['page']=$page;
        unset($data['access']['c']);
        unset($data['access']['u']);
        unset($data['access']['d']);
        $data['jurnal']='jurnal';
        $this->template->user('jurnal',$data,array('title'=>'Jurnal','breadcrumbs'=>array('Akuntansi')));
    }
    public function memorial($page=0)
    {
        $menu='ca530176-384e-4c7d-8377-1171ec3a8d05';
        $data['access']=list_access($menu);
        $this->load->model('akuntansi');
        if($this->input->get_post('save')){
            check_access($menu,'c');
            $this->session->set_flashdata('status_update', $this->akuntansi->save_jurnal());
        }elseif($this->input->get_post('delete')){
            check_access($menu,'d');

        }elseif($this->input->get_post('e')){
            check_access($menu,'u');

        }elseif($this->input->get_post('i')){
            check_access($menu,'c');
            if ($this->session->flashdata('status_update')){
                $data['status_update']=$this->session->flashdata('status_update');
            }
            $data['coa']=$this->akuntansi->get_coa_option();
            $this->template->user('addjurnal',$data,array('title'=>'Tambah Data','breadcrumbs'=>array('Akuntansi','Jurnal')));
        }else{
            $data['nota']=$this->input->get_post('nota');
            $data['closed']=$this->input->get_post('closed')?$this->input->get_post('closed'):(isset($_GET['closed'])?'':'t');
            if($this->input->get_post('start') && $this->input->get_post('end')){
                $data['start']=$this->input->get_post('start');
                $data['end']=$this->input->get_post('end');
            }else{
                $data['start']=date('d/m/Y',strtotime('-1 months'));;
                $data['end']=date('d/m/Y',strtotime('+1 months'));
            }
            $start=format_waktu($data['start'],true);
            $end=format_waktu($data['end'],true);
            $perpage=10;
            if($this->input->get_post('nota')){
                $this->db->where('lower(nota)',strtolower($data['nota']));
            }
            if($data['closed']){
                $this->db->where('status',$data['closed']);
            }
            $this->db->where('tanggal >= ',$start);
            $this->db->where('tanggal <= ',$end);
            $this->db->where('type','0a');
            $total=$this->db->get('akuntansi.v_jurnal')->num_rows();
            $this->db->limit($perpage,$page);
            $this->db->order_by('tanggal,nota');
            if($this->input->get_post('nota')){
                $this->db->where('lower(nota)',strtolower($data['nota']));
            }
            if($data['closed']){
                $this->db->where('status',$data['closed']);
            }
            $this->db->where('tanggal >= ',$start);
            $this->db->where('tanggal <= ',$end);
            $this->db->where('type','0a');
            $data['data']=$this->db->get('akuntansi.v_jurnal')->result_array();
            $data['pagination']=$this->template->pagination('accounting/memorial',$total,$perpage);
            $data['page']=$page;
            $data['jurnal']='memorial';
            $this->template->user('jurnal',$data,array('title'=>'Jurnal Memorial','breadcrumbs'=>array('Akuntansi')));
        }
    }
    public function kas($page=0)
    {
        $menu='74fa394a-0fee-4b9d-827c-91a09f5c68f5';
        $data['access']=list_access($menu);
        $this->load->model('akuntansi');
        if($this->input->get_post('save')){
            check_access($menu,'c');
            $this->session->set_flashdata('status_update', $this->akuntansi->save_jurnal_kbg());
        }elseif($this->input->get_post('delete')){
            check_access($menu,'d');

        }elseif($this->input->get_post('e')){
            check_access($menu,'u');

        }elseif($this->input->get_post('i')){
            check_access($menu,'c');
            $data['kbg_value']='kas';
            $data['kbg_title']=ucfirst($data['kbg_value']);
            $data['kbg']=$this->akuntansi->get_coa($data['kbg_value']);
            if ($this->session->flashdata('status_update')){
                $data['status_update']=$this->session->flashdata('status_update');
            }
            $data['coa']=$this->akuntansi->get_coa_option();
            $this->template->user('addkbg',$data,array('title'=>'Tambah Data','breadcrumbs'=>array('Akuntansi','Jurnal',$data['kbg_title'])));
        }else{
            $data['nota']=$this->input->get_post('nota');
            $data['closed']=$this->input->get_post('closed')?$this->input->get_post('closed'):(isset($_GET['closed'])?'':'t');
            if($this->input->get_post('start') && $this->input->get_post('end')){
                $data['start']=$this->input->get_post('start');
                $data['end']=$this->input->get_post('end');
            }else{
                $data['start']=date('d/m/Y',strtotime('-1 months'));
                $data['end']=date('d/m/Y',strtotime('+1 months'));
            }
            $start=format_waktu($data['start'],true);
            $end=format_waktu($data['end'],true);
            $perpage=10;
            if($this->input->get_post('nota')){
                $this->db->where('lower(nota)',strtolower($data['nota']));
            }
            if($data['closed']){
                $this->db->where('status',$data['closed']);
            }
            $this->db->where('tanggal >= ',$start);
            $this->db->where('tanggal <= ',$end);
            $this->db->where('type','0b');
            $total=$this->db->get('akuntansi.v_jurnal')->num_rows();
            $this->db->limit($perpage,$page);
            $this->db->order_by('tanggal,nota');
            if($this->input->get_post('nota')){
                $this->db->where('lower(nota)',strtolower($data['nota']));
            }
            if($data['closed']){
                $this->db->where('status',$data['closed']);
            }
            $this->db->where('tanggal >= ',$start);
            $this->db->where('tanggal <= ',$end);
            $this->db->where('type','0b');
            $data['data']=$this->db->get('akuntansi.v_jurnal')->result_array();
            $data['pagination']=$this->template->pagination('accounting/kas',$total,$perpage);
            $data['page']=$page;
            $data['jurnal']='kas';
            $this->template->user('jurnal',$data,array('title'=>'Jurnal Kas','breadcrumbs'=>array('Akuntansi')));
        }
    }
    public function bank($page=0)
    {
        $menu='fe012b79-adf5-42f4-bdbd-5ffd5417289c';
        $data['access']=list_access($menu);
        $this->load->model('akuntansi');
        if($this->input->get_post('save')){
            check_access($menu,'c');
            $this->session->set_flashdata('status_update', $this->akuntansi->save_jurnal_kbg());
        }elseif($this->input->get_post('delete')){
            check_access($menu,'d');

        }elseif($this->input->get_post('e')){
            check_access($menu,'u');

        }elseif($this->input->get_post('i')){
            check_access($menu,'c');
            $data['kbg_value']='bank';
            $data['kbg_title']=ucfirst($data['kbg_value']);
            $data['kbg']=$this->akuntansi->get_coa($data['kbg_value']);
            if ($this->session->flashdata('status_update')){
                $data['status_update']=$this->session->flashdata('status_update');
            }
            $data['coa']=$this->akuntansi->get_coa_option();
            $this->template->user('addkbg',$data,array('title'=>'Tambah Data','breadcrumbs'=>array('Akuntansi','Jurnal',$data['kbg_title'])));
        }else{
            $data['nota']=$this->input->get_post('nota');
            $data['closed']=$this->input->get_post('closed')?$this->input->get_post('closed'):(isset($_GET['closed'])?'':'t');
            if($this->input->get_post('start') && $this->input->get_post('end')){
                $data['start']=$this->input->get_post('start');
                $data['end']=$this->input->get_post('end');
            }else{
                $data['start']=date('d/m/Y',strtotime('-1 months'));;
                $data['end']=date('d/m/Y',strtotime('+1 months'));
            }
            $start=format_waktu($data['start'],true);
            $end=format_waktu($data['end'],true);
            $perpage=10;
            if($this->input->get_post('nota')){
                $this->db->where('lower(nota)',strtolower($data['nota']));
            }
            if($data['closed']){
                $this->db->where('status',$data['closed']);
            }
            $this->db->where('tanggal >= ',$start);
            $this->db->where('tanggal <= ',$end);
            $this->db->where('type','0c');
            $total=$this->db->get('akuntansi.v_jurnal')->num_rows();
            $this->db->limit($perpage,$page);
            $this->db->order_by('tanggal,nota');
            if($this->input->get_post('nota')){
                $this->db->where('lower(nota)',strtolower($data['nota']));
            }
            if($data['closed']){
                $this->db->where('status',$data['closed']);
            }
            $this->db->where('tanggal >= ',$start);
            $this->db->where('tanggal <= ',$end);
            $this->db->where('type','0c');
            $data['data']=$this->db->get('akuntansi.v_jurnal')->result_array();
            $data['pagination']=$this->template->pagination('accounting/bank',$total,$perpage);
            $data['page']=$page;
            $data['jurnal']='bank';
            $this->template->user('jurnal',$data,array('title'=>'Jurnal Bank','breadcrumbs'=>array('Akuntansi')));
        }
    }
    public function giro($page=0)
    {
        $menu='4938d59a-801c-44b9-adea-20ebdc9e6343';
        $data['access']=list_access($menu);
        $this->load->model('akuntansi');
        if($this->input->get_post('save')){
            check_access($menu,'c');
            $this->session->set_flashdata('status_update', $this->akuntansi->save_jurnal_kbg());
        }elseif($this->input->get_post('delete')){
            check_access($menu,'d');

        }elseif($this->input->get_post('e')){
            check_access($menu,'u');

        }elseif($this->input->get_post('entry')){
            check_access($menu,'c');
            $data['kbg_value']='giro';
            $data['kbg_title']=ucfirst($data['kbg_value']);
            $data['kbg']=$this->akuntansi->get_coa($data['kbg_value']);
            if ($this->session->flashdata('status_update')){
                $data['status_update']=$this->session->flashdata('status_update');
            }
            $data['coa']=$this->akuntansi->get_coa_option();
            $this->template->user('addkbg',$data,array('title'=>'Tambah Data','breadcrumbs'=>array('Akuntansi','Jurnal',$data['kbg_title'])));
        }else{
            $data['nota']=$this->input->get_post('nota');
            $data['closed']=$this->input->get_post('closed')?$this->input->get_post('closed'):(isset($_GET['closed'])?'':'t');
            if($this->input->get_post('start') && $this->input->get_post('end')){
                $data['start']=$this->input->get_post('start');
                $data['end']=$this->input->get_post('end');
            }else{
                $data['start']=date('d/m/Y',strtotime('-1 months'));;
                $data['end']=date('d/m/Y',strtotime('+1 months'));
            }
            $start=format_waktu($data['start'],true);
            $end=format_waktu($data['end'],true);
            $perpage=10;
            if($this->input->get_post('nota')){
                $this->db->where('lower(nota)',strtolower($data['nota']));
            }
            if($data['closed']){
                $this->db->where('status',$data['closed']);
            }
            $this->db->where('tanggal >= ',$start);
            $this->db->where('tanggal <= ',$end);
            $this->db->where('type','0d');
            $total=$this->db->get('akuntansi.v_jurnal')->num_rows();
            $this->db->limit($perpage,$page);
            $this->db->order_by('tanggal,nota');
            if($this->input->get_post('nota')){
                $this->db->where('lower(nota)',strtolower($data['nota']));
            }
            if($data['closed']){
                $this->db->where('status',$data['closed']);
            }
            $this->db->where('tanggal >= ',$start);
            $this->db->where('tanggal <= ',$end);
            $this->db->where('type','0d');
            $data['data']=$this->db->get('akuntansi.v_jurnal')->result_array();
            $data['pagination']=$this->template->pagination('accounting/giro',$total,$perpage);
            $data['page']=$page;
            $data['jurnal']='giro';
            $this->template->user('jurnal',$data,array('title'=>'Jurnal Giro','breadcrumbs'=>array('Akuntansi')));
        }
    }
}
