<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Internalproses extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        check_access('fb7ca00c-ef46-4dc1-bf9f-5d00358213a0');
        $this->template->set_view('internalproses');
        $this->load->model('audit');
    }

    public function index()
    {
        redirect(base_url());
    }

    public function persetujuan_r($page = 0)
    {
        $menu = 'e3033e60-42d8-47fd-8c8a-6e51cebaba33';
        $dalnis = '';
        $irban = '';
        $raw = $this->db->query("SELECT NOW() as tstamp")->row_array();
        $data['access'] = list_access($menu);
        $data['dalnis'] = '';
        $data['irban'] = '';
        $sessid = $this->session->userdata('user');


        $this->db->where('user', $sessid['id']);
        $this->db->where('role', '108ce450-d740-4bcb-9aaa-be9504bf94d8');
        $this->db->join('role', 'roleusers.role=role.id');
        $this->db->select('roleusers.role');
        $cekroledalnis = $this->db->get('roleusers')->num_rows();

        $this->db->where('user', $sessid['id']);
        $this->db->where('role', '2332800e-edb0-49cd-92d7-1abc3d981207');
        $this->db->join('role', 'roleusers.role=role.id');
        $this->db->select('roleusers.role');
        $cekroleirban = $this->db->get('roleusers')->num_rows();

        if ($cekroledalnis > 0) {
            $dalnis = true;
        }
        if ($cekroleirban > 0) {
            $irban = true;
        }
        $perpage = 100;
        $total = $this->db->get('interpro.perencanaan')->num_rows();

        if ($dalnis == true && $irban == false) {
            $data['irban'] = false;
            $data['dalnis'] = true;
        } elseif ($irban == true && $dalnis == false) {
            $data['irban'] = true;
            $data['dalnis'] = false;
        } elseif ($irban == true && $dalnis == true) {
            $data['dalnis'] = true;
            $data['irban'] = true;

        }

        $this->db->limit($perpage, $page);
        $this->db->order_by('created_at,spt_date,spt_no');

        $this->db->where('draft', false);
        $this->db->where('proses > 0 and proses < 3', null, false);
        $this->db->like('tim', $sessid['id']);
        $data['rencana'] = $this->db->get('interpro.v_perencanaan')->result_array();

        if ($this->input->get_post('v')) {
            $id_rencana = $this->input->get_post('v');

            $this->db->where('id', $id_rencana);
            $data['rencana'] = $this->db->get('interpro.perencanaan')->row_array();

            $this->db->where('id', $data['rencana']['app_sv1']);
            $data['dalnis_name'] = $this->db->get('public.users')->row_array();

            $this->db->where('id', $data['rencana']['app_sv2']);
            $data['irban_name'] = $this->db->get('public.users')->row_array();

            $this->db->where('id', $id_rencana);
            $this->db->select('array_to_json(array_agg(sasaran)) as v');
            $raw = $this->db->get('interpro.perencanaan_sasaran')->row_array();
            $data['rencana']['sasaran'] = $raw['v'];

            $this->db->where('id', $id_rencana);
            $this->db->select('array_to_json(array_agg(tujuan)) as v');
            $raw = $this->db->get('interpro.perencanaan_tujuan')->row_array();
            $data['rencana']['tujuan'] = $raw['v'];

            $this->db->order_by('no', 'asc');
            $this->db->where('status', true);
            $data['pkpt'] = $this->db->get('master.pkpt')->result_array();

            $this->db->order_by('order,kode', 'asc');
            $this->db->where('status', true);
            $data['jenis'] = $this->db->get('master.jenis')->result_array();

            $this->db->order_by('order,kode', 'asc');
            $this->db->where('status', true);
            $data['satker'] = $this->db->get('master.satker')->result_array();

            $this->db->order_by('sasaran.order,sasaran.kode', 'asc');
            $this->db->where('sasaran.status', true);
            $data['sasaran'] = $this->db->get('master.sasaran')->result_array();

            $this->db->order_by('tujuan.order,tujuan.kode', 'asc');
            $this->db->where('tujuan.status', true);
            $data['tujuan'] = $this->db->get('master.tujuan')->result_array();

            $this->db->order_by('kode', 'asc');
            $this->db->where('status', true);
            $data['aturan'] = $this->db->get('support.aturan')->result_array();

            $this->db->order_by('kode', 'asc');
            $this->db->where('status', true);
            $data['pedoman'] = $this->db->get('support.pedoman')->result_array();

            $this->db->order_by('v_tao.tahapan,v_tao.kode_kk', 'asc');
            $this->db->where('v_tao.status_tao', true);
            $this->db->where('v_tao.status_kk', true);
            $this->db->join('interpro.perencanaan_detail pd', "v_tao.id=pd.tao AND pd.id='$id_rencana'", 'left');
            $this->db->select("v_tao.*,pd.rencana_by as pelaksana,to_char(pd.rencana_date,'dd/mm/yyyy') as pelaksanaan");
            $data['tao'] = $this->db->get('support.v_tao')->result_array();

            $this->db->order_by('kode', 'asc');
            $data['tahapan'] = $this->db->get('master.tahapan_audit')->result_array();

            $data['tim'] = $this->db->get('support.tim()')->result_array();

            $this->db->select('id,name as nama');
            $this->db->where('status', true);
            $data['listtim'] = $this->db->get('public.users')->result_array();
            $this->template->user('viewpersetujuan_r', $data, array('title' => 'Detail Persetujuan', 'breadcrumbs' => array('Internal Proses', 'Rencana Audit')));
            return;
        }
        if ($this->input->get_post('persetujuanD')) {
            $proses = ($this->input->get_post('persetujuanD') == 't' ? '2' : '0');
            $insert = array(
                'app_sv1_note' => $this->input->get_post('note'),
                'app_sv1' => $sessid['id'],
                'app_sv1_status' => $this->input->get_post('persetujuanD'),
                'app_sv1_tstamp' => $raw['tstamp'],
                'modified_at' => $raw['tstamp'],
                'modified_by' => $sessid['id'],
                'proses' => $proses
            );
            $this->db->trans_begin();
            $this->db->where('id', $this->input->get_post('id'));
            $this->db->update('interpro.perencanaan', $insert);
            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
                $data['status_update'] = false;
            } else {
                $this->db->trans_commit();
                $data['status_update'] = true;
            }
            if ($data['status_update']) {
                $this->session->set_flashdata('status_update', true);
            }
            redirect(base_url('internalproses/persetujuan_r'));
            return;
        }
        if ($this->input->get_post('persetujuanI')) {
            $proses = ($this->input->get_post('persetujuanI') == 't' ? '3' : '0');
            $insert = array(
                'app_sv2_note' => $this->input->get_post('noteI'),
                'app_sv2' => $sessid['id'],
                'app_sv2_status' => $this->input->get_post('persetujuanI'),
                'app_sv2_tstamp' => $raw['tstamp'],
                'modified_at' => $raw['tstamp'],
                'modified_by' => $sessid['id'],
                'proses' => $proses
            );
            $this->db->trans_begin();
            $this->db->where('id', $this->input->get_post('id'));
            $this->db->update('interpro.perencanaan', $insert);
            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
                $data['status_update'] = false;
            } else {
                $this->db->trans_commit();
                $data['status_update'] = true;
            }
            if ($data['status_update']) {
                $this->session->set_flashdata('status_update', true);
            }
            redirect(base_url('internalproses/persetujuan_r'));
        }
        if ($this->input->get_post('terima')) {
            check_access($menu, 'u');
            $stat = array(
                'app_sv1' => $sessid['id'],
                'app_sv1_status' => 't',
                'app_sv1_tstamp' => $raw['tstamp'],
                'modified_at' => $raw['tstamp'],
                'modified_by' => $sessid['id'],
                'proses' => '2'
            );
            $this->db->trans_begin();
            $this->db->where('id', $this->input->get_post('terima'));
            $this->db->update('interpro.perencanaan', $stat);
            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
                $data['status_update'] = false;
            } else {
                $this->db->trans_commit();
                $data['status_update'] = true;
            }
            if ($data['status_update']) {
                $this->session->set_flashdata('status_update', true);
            }
            redirect(base_url('internalproses/persetujuan_r'));
        } elseif ($this->input->get_post('tolak')) {
            check_access($menu, 'u');
            $stat = array(
                'app_sv1' => $sessid['id'],
                'app_sv1_status' => 'f',
                'app_sv1_tstamp' => $raw['tstamp'],
                'modified_at' => $raw['tstamp'],
                'modified_by' => $sessid['id'],
            );
            $this->db->trans_begin();
            $this->db->where('id', $this->input->get_post('tolak'));
            $this->db->update('interpro.perencanaan', $stat);
            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
                $data['status_update'] = false;
            } else {
                $this->db->trans_commit();
                $data['status_update'] = true;
            }
            if ($data['status_update']) {
                $this->session->set_flashdata('status_update', true);
            }
            redirect(base_url('internalproses/persetujuan_r'));
        }

        if ($this->input->get_post('terimaI')) {
            check_access($menu, 'u');
            $stat = array(
                'app_sv2' => $sessid['id'],
                'app_sv2_status' => 't',
                'app_sv2_tstamp' => $raw['tstamp'],
                'modified_at' => $raw['tstamp'],
                'modified_by' => $sessid['id'],
                'proses' => '3',
            );
            $this->db->trans_begin();
            $this->db->where('id', $this->input->get_post('terimaI'));
            $this->db->update('interpro.perencanaan', $stat);
            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
                $data['status_update'] = false;
            } else {
                $this->db->trans_commit();
                $data['status_update'] = true;
            }
            if ($data['status_update']) {
                $this->session->set_flashdata('status_update', true);
            }
            redirect(base_url('internalproses/persetujuan_r'));
        } elseif ($this->input->get_post('tolakI')) {
            check_access($menu, 'u');
            $stat = array(
                'app_sv2' => $sessid['id'],
                'app_sv2_status' => 'f',
                'app_sv2_tstamp' => $raw['tstamp'],
                'modified_at' => $raw['tstamp'],
                'modified_by' => $sessid['id'],
                'proses' => '1'
            );
            $this->db->trans_begin();
            $this->db->where('id', $this->input->get_post('tolakI'));
            $this->db->update('interpro.perencanaan', $stat);
            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
                $data['status_update'] = false;
            } else {
                $this->db->trans_commit();
                $data['status_update'] = true;
            }
            if ($data['status_update']) {
                $this->session->set_flashdata('status_update', true);
            }
            redirect(base_url('internalproses/persetujuan_r'));
        }

        $data['pagination'] = $this->template->pagination('internalproses/rencana', $total, $perpage);
        $data['page'] = $page;
        $this->template->user('persetujuan_r', $data, array('title' => 'Persetujuan Rencana Audit', 'breadcrumbs' => array('Internal Proses')));

    }

    public function rencana($page = 0)
    {
        $menu = '74fa394a-0fee-4b9d-827c-91a09f5c68f5';
        $data['access'] = list_access($menu);
        if ($this->input->get_post('save')) {
            check_access($menu, 'c');
            $status = $this->audit->perencanaan();
            if (!$status) {
                echo 500;
            } else {
                $this->session->set_flashdata('status_update', $status);
                echo 200;
            }
        } elseif ($this->input->get_post('update')) {
            check_access($menu, 'u');
            $status = $this->audit->update_perencanaan($this->input->get_post('update'));
            if (!$status) {
                echo 500;
            } else {
                $this->session->set_flashdata('status_update', $status);
                echo 200;
            }
        } elseif ($this->input->get_post('delete')) {
            check_access($menu, 'd');
            $status = $this->audit->hapus_perencanaan($this->input->get_post('delete'));
            if (!$status) {
                echo 500;
            } else {
                echo 200;
            }
        } elseif ($this->input->get_post('tao')) {
            $this->db->where('tao.id', $this->input->get_post('tao'));
            $this->db->join('master.jenis', 'jenis.id=tao.jenis');
            $this->db->join('master.sasaran', 'sasaran.id=tao.sasaran');
            $this->db->join('master.tujuan', 'tujuan.id=tao.tujuan');
            $this->db->select("tao.id,tao.kode_tao,tao.tao,tao.langkah,sasaran.kode||' '||sasaran.ket as sasaran,tujuan.kode||' '||tujuan.ket as tujuan,jenis.kode||' '||jenis.ket as jenis");
            $data = $this->db->get('support.v_tao tao')->row_array();

            $this->db->order_by('kertas_kerja', true);
            $this->db->where('id', $data['id']);
            $detail = $this->db->get('support.tao_detail')->row_array();

            echo "<table class='table'>";
            echo "<tr><td class='right'>JENIS AUDIT</td><td>:</td>";
            echo "<td>$data[jenis]</td></tr>";
            echo "<tr><td class='right'>SASARAN AUDIT</td><td>:</td>";
            echo "<td>$data[sasaran]</td></tr>";
            echo "<tr><td class='right'>TUJUAN AUDIT</td><td>:</td>";
            echo "<td>$data[tujuan]</td></tr>";
            echo "<tr><td class='right'>TAO</td><td>:</td>";
            echo "<td>$data[kode_tao] $data[tao]</td></tr>";
            echo "<tr><td class='right'></td><td></td>";
            echo "<td>" . "<a href=\"" . (@$detail['file_download'] ? base_url("img/$detail[file_download]") : "#") . "\" class=\"btn btn-sm btn-primary\" target=\"_blank\"><i class=\"fa fa-file-text\"></i> KERTAS KERJA</a>" . "</td></tr>";
            echo "</table>";
        } elseif ($this->input->get_post('periode')) {
            $start = explode('/', $this->input->get_post('a'));
            $tgl_s = $start['2'] . '-' . $start['1'] . '-' . $start[0];
            $end = explode('/', $this->input->get_post('e'));
            $tgl_e = $end['2'] . '-' . $end['1'] . '-' . $end[0];

            $this->db->where('id', $this->input->get_post('periode'));
            $this->db->where('holiday >=', $tgl_s);
            $this->db->where('holiday <=', $tgl_e);
            $r = $this->db->get('periode_holiday')->num_rows();

            $diff = abs(strtotime($tgl_e) - strtotime($tgl_s));

            $years = floor($diff / (365 * 60 * 60 * 24));
            $months = floor(($diff - $years * 365 * 60 * 60 * 24) / (30 * 60 * 60 * 24));
            $total = floor(($diff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24) / (60 * 60 * 24));
            $total = $total - $r;
            echo $total . ' Hari';
            return;
        } elseif ($this->input->get_post('periodes')) {
            $this->db->order_by('no', 'asc');
            $this->db->where('status', true);
            $this->db->like("no", $this->input->get_post('periodes'), 'after');
            $data['pkpt'] = $this->db->get('master.pkpt')->result_array();
            echo '<option value="">pilih nomor PKPT</option>';
            $optgroup = null;
            foreach ($data['pkpt'] as $v):
                $v = purify_token($v);
                if ($optgroup != $v['tema']) {
                    if ($optgroup != null) {
                        echo "</optgroup>";
                    }
                    $optgroup = $v['tema'];
                    echo '<optgroup label="' . $v['tema'] . '">';
                }
                echo '<option value="' . $v['id'] . '" >' . $v['no'] . ' - ' . $v['nama'] . '</option>';
            endforeach;
            if ($optgroup != null) {
                echo "</optgroup>";
            }
            return;
        }elseif ($this->input->get_post('no_pkpt')) {
            $this->db->where('id', $this->input->get_post('no_pkpt'));
            $pkpt = $this->db->get('master.pkpt')->row_array();
            echo json_encode($pkpt);
            return;
        } elseif ($this->input->get_post('i')) {
            check_access($menu, 'c');

//            $this->db->order_by('no', 'asc');
//            $this->db->where('status', true);
//            $this->db->where('id not in (select pkpt_no::uuid from interpro.perencanaan WHERE pkpt_no <> \'\')', null, false);
//            $data['pkpt'] = $this->db->get('master.pkpt')->result_array();

            $data['selected_pkpt'] = $this->input->get_post('i');

            $this->db->order_by('order,kode', 'asc');
            $this->db->where('status', true);
            $data['jenis'] = $this->db->get('master.jenis')->result_array();

            $this->db->where('start <=', date('Y-m-d'));
            $data['per_no'] = $this->db->get('public.periode')->row_array();

            $this->db->order_by('nama', 'desc');
            $data['periode'] = $this->db->get('public.periode')->result_array();

            $this->db->order_by('order,kode', 'asc');
            $this->db->where('status', true);
            $data['satker'] = $this->db->get('master.satker')->result_array();

            $this->db->order_by('order,kode', 'asc');
            $this->db->where('status', true);
            $data['sasaran'] = $this->db->get('master.sasaran')->result_array();

            $this->db->order_by('order,kode', 'asc');
            $this->db->where('status', true);
            $data['tujuan'] = $this->db->get('master.tujuan')->result_array();

            $this->db->order_by('kode', 'asc');
            $this->db->where('status', true);
            $data['aturan'] = $this->db->get('support.aturan')->result_array();

            $this->db->order_by('kode', 'asc');
            $this->db->where('status', true);
            $data['pedoman'] = $this->db->get('support.pedoman')->result_array();

            $this->db->order_by('tahapan,kode_kk', 'asc');
            $this->db->where('status_tao', true);
            $this->db->where('status_kk', true);
            $this->db->select("*,'' as pelaksana,'' as pelaksanaan");
            $data['tao'] = $this->db->get('support.v_tao')->result_array();

            $this->db->order_by('kode', 'asc');
            $data['tahapan'] = $this->db->get('master.tahapan_audit')->result_array();

            $data['tim'] = $this->db->get('support.tim()')->result_array();

            $this->db->select('id,name as nama');
            $this->db->where('status', true);
            $data['listtim'] = $this->db->get('public.users')->result_array();
            $this->template->user('addrencana', $data, array('title' => 'Form Isian', 'breadcrumbs' => array('Internal Proses', 'Rencana Audit')));
        } elseif ($this->input->get_post('e')) {
            $id_rencana = $this->input->get_post('e');

            $this->db->where('id', $id_rencana);
            $data['rencana'] = $this->db->get('interpro.perencanaan')->row_array();

            $this->db->where('id', $id_rencana);
            $this->db->select('array_to_json(array_agg(sasaran)) as v');
            $raw = $this->db->get('interpro.perencanaan_sasaran')->row_array();
            $data['rencana']['sasaran'] = $raw['v'];

            $this->db->where('id', $id_rencana);
            $this->db->select('array_to_json(array_agg(tujuan)) as v');
            $raw = $this->db->get('interpro.perencanaan_tujuan')->row_array();
            $data['rencana']['tujuan'] = $raw['v'];
//echo json_encode($data['rencana']);
//            die();
            $this->db->order_by('no', 'asc');
            $this->db->where('status', true);
            $data['pkpt'] = $this->db->get('master.pkpt')->result_array();

            $this->db->order_by('order,kode', 'asc');
            $this->db->where('status', true);
            $data['jenis'] = $this->db->get('master.jenis')->result_array();

            $this->db->order_by('order,kode', 'asc');
            $this->db->where('status', true);
            $data['satker'] = $this->db->get('master.satker')->result_array();

            $this->db->order_by('sasaran.order,sasaran.kode', 'asc');
            $this->db->where('sasaran.status', true);
            $data['sasaran'] = $this->db->get('master.sasaran')->result_array();

            $this->db->order_by('tujuan.order,tujuan.kode', 'asc');
            $this->db->where('tujuan.status', true);
            $data['tujuan'] = $this->db->get('master.tujuan')->result_array();

            $this->db->order_by('kode', 'asc');
            $this->db->where('status', true);
            $data['aturan'] = $this->db->get('support.aturan')->result_array();

            $this->db->order_by('kode', 'asc');
            $this->db->where('status', true);
            $data['pedoman'] = $this->db->get('support.pedoman')->result_array();

            $this->db->order_by('v_tao.tahapan,v_tao.kode_kk', 'asc');
            $this->db->where('v_tao.status_tao', true);
            $this->db->where('v_tao.status_kk', true);
            $this->db->join('interpro.perencanaan_detail pd', "v_tao.id=pd.tao AND pd.id='$id_rencana'", 'left');
            $this->db->select("v_tao.*,pd.jumlah_hari as jumlah,pd.rencana_by as pelaksana,to_char(pd.rencana_date,'dd/mm/yyyy') as pelaksanaan");
            $data['tao'] = $this->db->get('support.v_tao')->result_array();

            $this->db->where('start <=', date('Y-m-d'));
            $data['per_no'] = $this->db->get('public.periode')->row_array();

            $this->db->where('status', true);
            $data['periode'] = $this->db->get('public.periode')->result_array();

            $this->db->order_by('kode', 'asc');
            $data['tahapan'] = $this->db->get('master.tahapan_audit')->result_array();

            $data['tim'] = $this->db->get('support.tim()')->result_array();

            $this->db->select('id,name as nama');
            $this->db->where('status', true);
            $data['listtim'] = $this->db->get('public.users')->result_array();

            $this->template->user('updaterencana', $data, array('title' => 'Form Isian', 'breadcrumbs' => array('Internal Proses', 'Rencana Audit')));
        } elseif ($this->input->get_post('v')) {
            $id_rencana = $this->input->get_post('v');

            $this->db->where('id', $id_rencana);
            $data['rencana'] = $this->db->get('interpro.perencanaan')->row_array();

            $this->db->where('id', $id_rencana);
            $this->db->select('array_to_json(array_agg(sasaran)) as v');
            $raw = $this->db->get('interpro.perencanaan_sasaran')->row_array();
            $data['rencana']['sasaran'] = $raw['v'];

            $this->db->where('id', $id_rencana);
            $this->db->select('array_to_json(array_agg(tujuan)) as v');
            $raw = $this->db->get('interpro.perencanaan_tujuan')->row_array();
            $data['rencana']['tujuan'] = $raw['v'];

            $this->db->order_by('no', 'asc');
            $this->db->where('status', true);
            $data['pkpt'] = $this->db->get('master.pkpt')->result_array();

            $this->db->order_by('order,kode', 'asc');
            $this->db->where('status', true);
            $data['jenis'] = $this->db->get('master.jenis')->result_array();

            $this->db->order_by('order,kode', 'asc');
            $this->db->where('status', true);
            $data['satker'] = $this->db->get('master.satker')->result_array();

            $this->db->order_by('sasaran.order,sasaran.kode', 'asc');
            $this->db->where('sasaran.status', true);
            $data['sasaran'] = $this->db->get('master.sasaran')->result_array();

            $this->db->order_by('tujuan.order,tujuan.kode', 'asc');
            $this->db->where('tujuan.status', true);
            $data['tujuan'] = $this->db->get('master.tujuan')->result_array();

            $this->db->order_by('kode', 'asc');
            $this->db->where('status', true);
            $data['aturan'] = $this->db->get('support.aturan')->result_array();

            $this->db->order_by('kode', 'asc');
            $this->db->where('status', true);
            $data['pedoman'] = $this->db->get('support.pedoman')->result_array();

            $this->db->order_by('v_tao.tahapan,v_tao.kode_kk', 'asc');
            $this->db->where('v_tao.status_tao', true);
            $this->db->where('v_tao.status_kk', true);
            $this->db->join('interpro.perencanaan_detail pd', "v_tao.id=pd.tao AND pd.id='$id_rencana'", 'left');
            $this->db->select("v_tao.*,pd.jumlah_hari as jumlah ,pd.rencana_by as pelaksana,to_char(pd.rencana_date,'dd/mm/yyyy') as pelaksanaan");
            $data['tao'] = $this->db->get('support.v_tao')->result_array();

            $this->db->order_by('kode', 'asc');
            $data['tahapan'] = $this->db->get('master.tahapan_audit')->result_array();

            $data['tim'] = $this->db->get('support.tim()')->result_array();

            $this->db->select('id,name as nama');
            $this->db->where('status', true);
            $data['listtim'] = $this->db->get('public.users')->result_array();

            $this->template->user('viewrencana', $data, array('title' => 'Form Isian', 'breadcrumbs' => array('Internal Proses', 'Rencana Audit')));
        } else {
            $data['closed'] = $this->input->get_post('closed') ? $this->input->get_post('closed') : (isset($_GET['closed']) ? '' : 'f');
            if ($this->input->get_post('start') && $this->input->get_post('end')) {
                $data['start'] = $this->input->get_post('start');
                $data['end'] = $this->input->get_post('end');
            } else {
                $data['start'] = date('d/m/Y', strtotime('-1 months'));;
                $data['end'] = date('d/m/Y', strtotime('+1 months'));
            }
            $start = format_waktu($data['start'], true);
            $end = format_waktu($data['end'], true);
            $perpage = 100;
            if ($this->input->get_post('nota')) {
                $this->db->where('lower(nota)', strtolower($data['nota']));
            }
            if ($data['closed']) {
                $this->db->where('closed', $data['closed']);
            }
            $this->db->where('spt_date >= ', $start);
            $this->db->where('spt_date <= ', $end);
            $total = $this->db->get('interpro.perencanaan')->num_rows();
            $this->db->limit($perpage, $page);
            $this->db->order_by('created_at,spt_date,spt_no');
            if ($this->input->get_post('spt_no')) {
                $this->db->where('lower(spt_no)', strtolower($data['spt_no']));
            }
            if ($data['closed']) {
                $this->db->where('closed', $data['closed']);
            }
            $this->db->where('spt_date >= ', $start);
            $this->db->where('spt_date <= ', $end);
            $data['rencana'] = $this->db->get('interpro.v_perencanaan')->result_array();
            $data['pagination'] = $this->template->pagination('internalproses/rencana', $total, $perpage);
            $data['page'] = $page;
            $this->template->user('rencana', $data, array('title' => 'Rencana Audit', 'breadcrumbs' => array('Internal Proses')));
        }
    }

    public function tindak_lanjut($page = 0)
    {
        $menu = 'c34d2968-b466-4482-9803-d85a69a73d06';
        $data['access'] = list_access($menu);
        if ($this->input->get_post('save')) {
            check_access($menu, 'c');
            $status = $this->audit->tindak_lanjut();
            if (!$status) {
                echo 500;
            } else {
                $this->session->set_flashdata('status_update', $status);
                echo 200;
            }
        }
        elseif ($this->input->get_post('id_test')) {
            check_access($menu, 'u');
            $status = $this->audit->update_temuan($this->input->get_post(null));
            if ($status) {
                $this->session->set_flashdata('status_update', $status);
            }
            redirect(base_url('internalproses/tindak_lanjut'));
            return;
        }
        elseif ($this->input->get_post('export')) {
            header("Content-Type: application/vnd.msword");
            header("Expires: 0");//no-cache
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");//no-cache
            header("content-disposition: attachment;filename=sampleword.doc");
            $this->db->where('id', $this->input->get_post('export'));
            $data=$this->db->get('interpro.tindak_lanjut')->row_array();
            $this->db->where('d.id', $this->input->get_post('export'));
//            $this->db->join('suppot.kode_temuan k', 'd.kode_temuan=k.id');
//            $this->db->select('d.*, k.nama as jdl_temuan');
            $array=$this->db->get('interpro.tindak_lanjutd d')->result_array();
            $doc_body ="
<div style='width: 100%; text-align: center'>
 <h1>POKOK-POKOK HASIL PEMERIKSAAN (P2HP) INSPEKTORAT KABUPATEN BANYUAWANGI TERHADAP ".strtoupper($data['skpd'])."</h1>
</div>

 <table style='width: 100%;border:1px solid'>
 <tr style='border:1px solid'>
 <th style='width: 5%;border:1px solid;text-align: center'>No</th>
 <th style='width: 45%;border:1px solid;text-align: center'>Temuan Hasil Pemeriksaan</th>
 <th style='width: 50%;border:1px solid;text-align: center'>Komentar Pejabat yang diperiksa</th>
</tr>";
            $no=1;
            foreach($array as $a){
                $doc_body .="<tr style='border:1px solid;'>
<td rowspan='2' style='border:1px solid;text-align: center;'>$no</td>
<td style='border:1px solid;'><b>".strtoupper($a['judul_tl'])."</b></td>
<td rowspan='2' style='border:1px solid;'></td>
</tr><tr style='border:1px solid;'>
<td style='border:1px solid;'>$a[uraian_rekom]</td>
</tr>";
           $no++;
            }
            $doc_body .="</table>";
            echo "<html>";
            echo "$doc_body";
            echo "</html>";

            return;
        }
        elseif ($this->input->get_post('baru')) {
            check_access($menu, 'u');
            $status = $this->audit->insert_temuan($this->input->get_post(null));
            if ($status) {
                $this->session->set_flashdata('status_update', $status);
            }
            redirect(base_url('internalproses/tindak_lanjut'));
            return;
        }
        elseif ($this->input->get_post('update')) {
            check_access($menu, 'u');
            $status = $this->audit->update_perencanaan($this->input->get_post('update'));
            if (!$status) {
                echo 500;
            } else {
                $this->session->set_flashdata('status_update', $status);
                echo 200;
            }
        }
        elseif ($this->input->get_post('delete')) {
            check_access($menu, 'd');
            $status = $this->audit->hapus_perencanaan($this->input->get_post('delete'));
            if (!$status) {
                echo 500;
            } else {
                echo 200;
            }
        }
        elseif ($this->input->get_post('tao')) {
            $this->db->where('tao.id', $this->input->get_post('tao'));
            $this->db->join('master.jenis', 'jenis.id=tao.jenis');
            $this->db->join('master.sasaran', 'sasaran.id=tao.sasaran');
            $this->db->join('master.tujuan', 'tujuan.id=tao.tujuan');
            $this->db->select("tao.id,tao.kode_tao,tao.tao,tao.langkah,sasaran.kode||' '||sasaran.ket as sasaran,tujuan.kode||' '||tujuan.ket as tujuan,jenis.kode||' '||jenis.ket as jenis");
            $data = $this->db->get('support.v_tao tao')->row_array();

            $this->db->order_by('kertas_kerja', true);
            $this->db->where('id', $data['id']);
            $detail = $this->db->get('support.tao_detail')->row_array();

            echo "<table class='table'>";
            echo "<tr><td class='right'>JENIS AUDIT</td><td>:</td>";
            echo "<td>$data[jenis]</td></tr>";
            echo "<tr><td class='right'>SASARAN AUDIT</td><td>:</td>";
            echo "<td>$data[sasaran]</td></tr>";
            echo "<tr><td class='right'>TUJUAN AUDIT</td><td>:</td>";
            echo "<td>$data[tujuan]</td></tr>";
            echo "<tr><td class='right'>TAO</td><td>:</td>";
            echo "<td>$data[kode_tao] $data[tao]</td></tr>";
            echo "<tr><td class='right'></td><td></td>";
            echo "<td>" . "<a href=\"" . (@$detail['file_download'] ? base_url("img/$detail[file_download]") : "#") . "\" class=\"btn btn-sm btn-primary\" target=\"_blank\"><i class=\"fa fa-file-text\"></i> KERTAS KERJA</a>" . "</td></tr>";
            echo "</table>";
        }
        elseif ($this->input->get_post('i')) {
            check_access($menu, 'c');

            $this->db->order_by('no', 'asc');
            $this->db->where('status', true);
            $this->db->where('id not in (select pkpt_no::uuid from interpro.perencanaan WHERE pkpt_no <> \'\')', null, false);
            $data['pkpt'] = $this->db->get('master.pkpt')->result_array();

            $data['selected_pkpt'] = $this->input->get_post('i');

            $this->db->order_by('order,kode', 'asc');
            $this->db->where('status', true);
            $data['jenis'] = $this->db->get('master.jenis')->result_array();

            $this->db->order_by('order,kode', 'asc');
            $this->db->where('status', true);
            $data['satker'] = $this->db->get('master.satker')->result_array();

            $this->db->order_by('order,kode', 'asc');
            $this->db->where('status', true);
            $data['sasaran'] = $this->db->get('master.sasaran')->result_array();

            $this->db->order_by('order,kode', 'asc');
            $this->db->where('status', true);
            $data['tujuan'] = $this->db->get('master.tujuan')->result_array();

            $this->db->order_by('kode', 'asc');
            $this->db->where('status', true);
            $data['aturan'] = $this->db->get('support.aturan')->result_array();

            $this->db->order_by('kode', 'asc');
            $this->db->where('status', true);
            $data['pedoman'] = $this->db->get('support.pedoman')->result_array();

            $this->db->order_by('tahapan,kode_kk', 'asc');
            $this->db->where('status_tao', true);
            $this->db->where('status_kk', true);
            $this->db->select("*,'' as pelaksana,'' as pelaksanaan");
            $data['tao'] = $this->db->get('support.v_tao')->result_array();

            $this->db->order_by('kode', 'asc');
            $data['tahapan'] = $this->db->get('master.tahapan_audit')->result_array();


            $this->db->order_by('k1,k2,k3,k4', 'asc');
            $this->db->where('level', 4);
            $this->db->where('k1 <>', '9');
            $this->db->where('status', true);
            $this->db->where('deleted', false);
            $this->db->select("support.kode_temuan.id,support.kode_temuan.nama,(k1||'.'||k2||'.'||k3||'.'||k4) as kode");
            $data['parent'] = $this->db->get('support.kode_temuan')->result_array();

            $this->db->order_by('k1,k2,k3,k4', 'asc');
            $this->db->where('level', 4);
            $this->db->where('k1', '9');
            $this->db->where('status', true);
            $this->db->where('deleted', false);
            $this->db->select("support.kode_temuan.id,support.kode_temuan.nama,(k1||'.'||k2||'.'||k3||'.'||k4) as kode");
            $data['parent2'] = $this->db->get('support.kode_temuan')->result_array();


            $this->template->user('addtl', $data, array('title' => 'Tambah Temuan', 'breadcrumbs' => array('Internal Proses', 'Rencana Audit')));
        }
        elseif($this->input->get_post('reviu1')||$this->input->get_post('reviu2')){
                check_access($menu, 'u');
            $user = $this->session->userdata('user');
            $this->db->trans_begin();
            $status_a='';
            if(isset($_FILES['file'])&&$_FILES['file']['size']>0){
                $_FILES['userfile']['name']     = $_FILES['file']['name'];
                $_FILES['userfile']['type']     = $_FILES['file']['type'];
                $_FILES['userfile']['tmp_name'] = $_FILES['file']['tmp_name'];
                $_FILES['userfile']['error']    = $_FILES['file']['error'];
                $_FILES['userfile']['size']     = $_FILES['file']['size'];

                $config['upload_path'] = './img/';
                $config['allowed_types'] = '*';
                $config['max_size'] = 1024 * 8;
                $config['encrypt_name'] = TRUE;
                $this->load->library('upload', $config);
                if (!$this->upload->do_upload('userfile')) {
                    return array('danger', $this->upload->display_errors());
                } else {
                    $file = $this->upload->data();
                    $input['isian'][] = array('nama' => $_FILES['userfile']['name'], 'file' => $file['file_name']);
                }
                unset($this->upload);
                unset($_FILES['userfile']);
            }
if(@$this->input->get_post('reviu1')){
    $update = array(
        'app_sv1' => $user['id'],
        'app_sv1_note' => $this->input->get_post('reviu1'),
        'app_sv1_file' => (@$input['isian']?json_encode(@$input['isian']):null),
        'app_sv1_tstamp' => date('Y-m-d H:i:s'),
        'app_sv1_status' => $this->input->get_post('persetujuanD'),
        'proses' => $this->input->get_post('persetujuanD')=='f'?4:1,
        'status' => $this->input->get_post('persetujuanD')=='f'?'f':'t',
    );
    $status_a=$this->input->get_post('persetujuanD');
}else{

    $update = array(
        'app_sv2' => $user['id'],
        'app_sv2_note' => $this->input->get_post('reviu2'),
        'app_sv2_file' => (@$input['isian']?json_encode(@$input['isian']):null),
        'app_sv2_tstamp' => date('Y-m-d H:i:s'),
        'app_sv2_status' => $this->input->get_post('persetujuanI'),
        'proses' => $this->input->get_post('persetujuanI')=='f'?4:2,
        'status' => $this->input->get_post('persetujuanI')=='f'?'f':'t',
    );
    $status_a=$this->input->get_post('persetujuanI');
}
            $this->db->where('id', $this->input->get_post('id_rencana'));
            $this->db->update('interpro.perencanaan',array(
                'status_tl'=>$status_a,
            ));
            $this->db->where('id_rencana', $this->input->get_post('id_rencana'));
            $a=$this->db->get('interpro.tindak_lanjut')->row_array();
            if($a['id']==''){
                $this->db->where('id', $this->input->get_post('id_rencana'));
            }else{
                $this->db->where('id_rencana', $this->input->get_post('id_rencana'));
            }
            $this->db->update('interpro.tindak_lanjut', $update);
            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
                redirect(base_url('internalproses/tindak_lanjut'));
                return array('danger', 'Reviu Temuan gagal tersimpan');
            } else {
                $this->db->trans_commit();
                redirect(base_url('internalproses/tindak_lanjut'));
                return array('success', 'Reviu Temuan berhasil tersimpan');
            }
                return;
        }
        elseif ($this->input->get_post('e')) {
            $id_rencana = $this->input->get_post('e');

            $this->db->where('id', $id_rencana);
            $data['rencana'] = $this->db->get('interpro.perencanaan')->row_array();

            $this->db->where('interpro.perencanaan.id', $id_rencana);
            $this->db->join('users', 'interpro.perencanaan.dalnis=users.id');
            $data['dalnis_nama'] = $this->db->get('interpro.perencanaan')->row('name');

            $this->db->where('interpro.perencanaan.id', $id_rencana);
            $this->db->join('users', 'interpro.perencanaan.ketuatim=users.id');
            $data['ketuatim_nama'] = $this->db->get('interpro.perencanaan')->row('name');

            $this->db->where('interpro.perencanaan.id', $id_rencana);
            $this->db->join('users', 'interpro.perencanaan.irban=users.id');
            $data['irban_nama'] = $this->db->get('interpro.perencanaan')->row('name');

            $this->db->where('id', $id_rencana);
            $this->db->select('array_to_json(array_agg(sasaran)) as v');
            $raw = $this->db->get('interpro.perencanaan_sasaran')->row_array();
            $data['rencana']['sasaran'] = $raw['v'];

            $this->db->where('id', $id_rencana);
            $this->db->select('array_to_json(array_agg(tujuan)) as v');
            $raw = $this->db->get('interpro.perencanaan_tujuan')->row_array();
            $data['rencana']['tujuan'] = $raw['v'];

            $this->db->order_by('no', 'asc');
            $this->db->where('status', true);
            $data['pkpt'] = $this->db->get('master.pkpt')->result_array();

            $this->db->order_by('order,kode', 'asc');
            $this->db->where('status', true);
            $data['jenis'] = $this->db->get('master.jenis')->result_array();

            $this->db->order_by('order,kode', 'asc');
            $this->db->where('status', true);
            $data['satker'] = $this->db->get('master.satker')->result_array();

            $this->db->order_by('sasaran.order,sasaran.kode', 'asc');
            $this->db->where('sasaran.status', true);
            $data['sasaran'] = $this->db->get('master.sasaran')->result_array();

            $this->db->order_by('tujuan.order,tujuan.kode', 'asc');
            $this->db->where('tujuan.status', true);
            $data['tujuan'] = $this->db->get('master.tujuan')->result_array();

            $this->db->order_by('kode', 'asc');
            $this->db->where('status', true);
            $data['aturan'] = $this->db->get('support.aturan')->result_array();

            $this->db->order_by('kode', 'asc');
            $this->db->where('status', true);
            $data['pedoman'] = $this->db->get('support.pedoman')->result_array();

            $this->db->order_by('v_tao.tahapan,v_tao.kode_kk', 'asc');
            $this->db->where('v_tao.status_tao', true);
            $this->db->where('v_tao.status_kk', true);
            $this->db->join('interpro.perencanaan_detail pd', "v_tao.id=pd.tao AND pd.id='$id_rencana'", 'left');
            $this->db->select("v_tao.*,pd.jumlah_hari as jumlah,pd.rencana_by as pelaksana,to_char(pd.rencana_date,'dd/mm/yyyy') as pelaksanaan");
            $data['tao'] = $this->db->get('support.v_tao')->result_array();

            $this->db->order_by('kode', 'asc');
            $data['tahapan'] = $this->db->get('master.tahapan_audit')->result_array();

            $data['tim'] = $this->db->get('support.tim()')->result_array();

            $this->db->select('id,name as nama');
            $this->db->where('status', true);
            $data['listtim'] = $this->db->get('public.users')->result_array();

            $this->db->where('status', true);
            $data['periode'] = $this->db->get('public.periode')->result_array();


            $this->db->where('tl.id_rencana', $id_rencana);
            $this->db->order_by('tld.id', 'asc');
            $this->db->join('support.kode_temuan kt', 'tld.kode_temuan=kt.id');
            $this->db->join('support.kode_temuan kt2', 'tld.kode_rekom=kt2.id');
            $this->db->join('interpro.tindak_lanjut tl', 'tld.id=tl.id', 'left');
            $this->db->select("kt.id as id_t,kt2.id as id_r,(kt.k1||'.'||kt.k2||'.'||kt.k3||'.'||kt.k4) as kode_t,(kt2.k1||'.'||kt2.k2||'.'||kt2.k3||'.'||kt2.k4) as kode_r,kt.nama as temuan,kt2.nama as rekom, tld.tindak_lanjut as tl,  tld.hasil_rekom as kode_hr,  tld.keterangan as keterangan,  tld.nilai_rekom as nilai_r, ,  CASE WHEN tld.hasil_rekom=1 THEN 'Sesuai' WHEN tld.hasil_rekom=2 THEN 'Belum Sesuai' WHEN tld.hasil_rekom=3 THEN 'Belum Ditindaklanjuti' ELSE 'Tidak Dapat Ditindaklanjuti' END as hasil_r, tld.negara as nk1, tld.daerah as nk2, tld.nd as nd");
            $data['temuan'] = $this->db->get('interpro.tindak_lanjutd tld')->result_array();

            $this->db->order_by('k1,k2,k3,k4', 'asc');
            $this->db->where('level', 4);
            $this->db->where('k1 <>', '9');
            $this->db->where('status', true);
            $this->db->where('deleted', false);
            $this->db->select("support.kode_temuan.id,support.kode_temuan.nama,(k1||'.'||k2||'.'||k3||'.'||k4) as kode");
            $data['parent'] = $this->db->get('support.kode_temuan')->result_array();

            $this->db->order_by('k1,k2,k3,k4', 'asc');
            $this->db->where('level', 4);
            $this->db->where('k1', '9');
            $this->db->where('status', true);
            $this->db->where('deleted', false);
            $this->db->select("support.kode_temuan.id,support.kode_temuan.nama,(k1||'.'||k2||'.'||k3||'.'||k4) as kode");
            $data['parent2'] = $this->db->get('support.kode_temuan')->result_array();

            $this->template->user('updatetl', $data, array('title' => 'Tambah Temuan', 'breadcrumbs' => array('Internal Proses', 'Temuan & Tindak Lanjut')));
        }
        elseif ($this->input->get_post('v')) {
            $id_rencana = $this->input->get_post('v');

            $user = $this->session->userdata('user');
            $this->db->where('id', $id_rencana);
            $data['rencana'] = $this->db->get('interpro.perencanaan')->row_array();
            $id_rencana2 = '';
            if($data['rencana']['id']==''){
                $this->db->where('id', $id_rencana);
                $data['rencana'] = $this->db->get('interpro.tindak_lanjut')->row_array();
                $id_rencana2 = $id_rencana;
            }
//            echo json_encode($data['rencana']);die();

            $this->db->where('user', $user['id']);
            $this->db->where('role', '108ce450-d740-4bcb-9aaa-be9504bf94d8');
            $this->db->join('role', 'roleusers.role=role.id');
            $this->db->select('roleusers.role');
            $data['dalnis'] = $this->db->get('roleusers')->num_rows();
            $this->db->where('user', $user['id']);
            $this->db->where('role', '2332800e-edb0-49cd-92d7-1abc3d981207');
            $this->db->join('role', 'roleusers.role=role.id');
            $this->db->select('roleusers.role');
            $data['irban'] = $this->db->get('roleusers')->num_rows();

            if($id_rencana2!=''){

                $this->db->where('t.id', $id_rencana);
            }else{
                $this->db->where('id_rencana', $id_rencana);
            }
            $this->db->join('users u', 't.app_sv1=u.id','left');
            $this->db->join('users u2', 't.app_sv2=u2.id','left');
            $this->db->select('t.*,u.name as dalnis_n,u2.name as irban_n');
            $data['tl'] = $this->db->get('interpro.tindak_lanjut t')->row_array();
//            echo json_encode($data['tl']);
//            echo $data['irban'];
//            die();
            $this->db->where('interpro.perencanaan.id', $id_rencana);
            $this->db->join('users', 'interpro.perencanaan.dalnis=users.id');
            $data['dalnis_nama'] = $this->db->get('interpro.perencanaan')->row('name');
            $this->db->where('interpro.perencanaan.id', $id_rencana);
            $this->db->join('users', 'interpro.perencanaan.ketuatim=users.id');
            $data['ketuatim_nama'] = $this->db->get('interpro.perencanaan')->row('name');
            $this->db->where('interpro.perencanaan.id', $id_rencana);
            $this->db->join('users', 'interpro.perencanaan.irban=users.id');
            $data['irban_nama'] = $this->db->get('interpro.perencanaan')->row('name');

            $this->db->where('id', $id_rencana);
            $this->db->select('array_to_json(array_agg(sasaran)) as v');
            $raw = $this->db->get('interpro.perencanaan_sasaran')->row_array();
            $data['rencana']['sasaran'] = $raw['v'];

            $this->db->where('id', $id_rencana);
            $this->db->select('array_to_json(array_agg(tujuan)) as v');
            $raw = $this->db->get('interpro.perencanaan_tujuan')->row_array();
            $data['rencana']['tujuan'] = $raw['v'];

            $this->db->order_by('no', 'asc');
            $this->db->where('status', true);
            $data['pkpt'] = $this->db->get('master.pkpt')->result_array();

            $this->db->order_by('order,kode', 'asc');
            $this->db->where('status', true);
            $data['jenis'] = $this->db->get('master.jenis')->result_array();

            $this->db->order_by('order,kode', 'asc');
            $this->db->where('status', true);
            $data['satker'] = $this->db->get('master.satker')->result_array();

            $this->db->order_by('sasaran.order,sasaran.kode', 'asc');
            $this->db->where('sasaran.status', true);
            $data['sasaran'] = $this->db->get('master.sasaran')->result_array();

            $this->db->order_by('tujuan.order,tujuan.kode', 'asc');
            $this->db->where('tujuan.status', true);
            $data['tujuan'] = $this->db->get('master.tujuan')->result_array();

            $this->db->order_by('kode', 'asc');
            $this->db->where('status', true);
            $data['aturan'] = $this->db->get('support.aturan')->result_array();

            $this->db->order_by('kode', 'asc');
            $this->db->where('status', true);
            $data['pedoman'] = $this->db->get('support.pedoman')->result_array();

        //            $this->db->order_by('v_tao.tahapan,v_tao.kode_kk', 'asc');
        //            $this->db->where('v_tao.status_tao', true);
        //            $this->db->where('v_tao.status_kk', true);
        //            $this->db->join('interpro.perencanaan_detail pd', "v_tao.id=pd.tao AND pd.id='$id_rencana'", 'left');
        //            $this->db->select("v_tao.*,pd.jumlah_hari as jumlah,pd.rencana_by as pelaksana,to_char(pd.rencana_date,'dd/mm/yyyy') as pelaksanaan");
        //            $data['tao'] = $this->db->get('support.v_tao')->result_array();

            $this->db->order_by('kode', 'asc');
            $data['tahapan'] = $this->db->get('master.tahapan_audit')->result_array();

            $data['tim'] = $this->db->get('support.tim()')->result_array();

            $this->db->select('id,name as nama');
            $this->db->where('status', true);
            $data['listtim'] = $this->db->get('public.users')->result_array();

            $this->db->where('status', true);
            $data['periode'] = $this->db->get('public.periode')->result_array();
//echo $id_rencana2;die();

            if($id_rencana2!=''){

            $this->db->where('tl.id', $id_rencana);
            }else{
                $this->db->where('tl.id_rencana', $id_rencana);
            }
            $this->db->order_by('tld.id', 'asc');
            $this->db->join('support.kode_temuan kt', 'tld.kode_temuan=kt.id');
            $this->db->join('support.kode_temuan kt2', 'tld.kode_rekom=kt2.id');
            $this->db->join('interpro.tindak_lanjut tl', 'tld.id=tl.id', 'left');
            $this->db->select("kt.id as id_t,kt2.id as id_r,(kt.k1||'.'||kt.k2||'.'||kt.k3||'.'||kt.k4) as kode_t,(kt2.k1||'.'||kt2.k2||'.'||kt2.k3||'.'||kt2.k4) as kode_r,kt.nama as temuan,kt2.nama as rekom, tld.tindak_lanjut as tl,  tld.hasil_rekom as kode_hr,  tld.keterangan as keterangan,  tld.nilai_rekom as nilai_r, ,  CASE WHEN tld.hasil_rekom=1 THEN 'Sesuai' WHEN tld.hasil_rekom=2 THEN 'Belum Sesuai' WHEN tld.hasil_rekom=3 THEN 'Belum Ditindaklanjuti' ELSE 'Tidak Dapat Ditindaklanjuti' END as hasil_r, tld.negara as nk1, tld.daerah as nk2, tld.nd as nd, tld.obrik, tld.akibat, tld.penyebab, tld.kondisi, tld.kriteria, tld.tujuan_tl, tld.evaluasi, tld.ruang, tld.tanggapan, tld.tindak_lanjut_s, tld.judul_tl, tld.sasaran_tl");
            $data['temuan'] = $this->db->get('interpro.tindak_lanjutd tld')->result_array();

            $this->db->order_by('k1,k2,k3,k4', 'asc');
            $this->db->where('level', 4);
            $this->db->where('k1 <>', '9');
            $this->db->where('status', true);
            $this->db->where('deleted', false);
            $this->db->select("support.kode_temuan.id,support.kode_temuan.nama,(k1||'.'||k2||'.'||k3||'.'||k4) as kode");
            $data['parent'] = $this->db->get('support.kode_temuan')->result_array();

            $this->db->order_by('k1,k2,k3,k4', 'asc');
            $this->db->where('level', 4);
            $this->db->where('k1', '9');
            $this->db->where('status', true);
            $this->db->where('deleted', false);
            $this->db->select("support.kode_temuan.id,support.kode_temuan.nama,(k1||'.'||k2||'.'||k3||'.'||k4) as kode");
            $data['parent2'] = $this->db->get('support.kode_temuan')->result_array();

            $this->template->user('viewtl', $data, array('title' => 'Lihat Temuan', 'breadcrumbs' => array('Internal Proses', 'Temuan & Tindak Lanjut')));
        }
        else if(isset($_FILES['file'])&&$_FILES['file']['size']>0){
            $_FILES['userfile']['name']     = $_FILES['file']['name'];
            $_FILES['userfile']['type']     = $_FILES['file']['type'];
            $_FILES['userfile']['tmp_name'] = $_FILES['file']['tmp_name'];
            $_FILES['userfile']['error']    = $_FILES['file']['error'];
            $_FILES['userfile']['size']     = $_FILES['file']['size'];

            $config['upload_path'] = './img/';
            $config['allowed_types'] = '*';
            $config['max_size'] = 1024 * 8;
            $config['encrypt_name'] = TRUE;
            $this->load->library('upload', $config);
            if (!$this->upload->do_upload('userfile')) {
                redirect(base_url('internalproses/tindak_lanjut'));
                return array('danger', $this->upload->display_errors());
            } else {
                $file = $this->upload->data();
                $input['isian'][] = array('nama' => $_FILES['userfile']['name'], 'file' => $file['file_name']);

                $update = array(
                    'lap_akir' => (@$input['isian']?json_encode(@$input['isian']):null),
                    'proses' => 3,
                );
                $this->db->where('id', $this->input->get_post('id'));
                $this->db->update('interpro.tindak_lanjut', $update);
                $status=array('success', 'File berhasil tersimpan');
                $this->session->set_flashdata('status_update', true);
                redirect(base_url('internalproses/tindak_lanjut'));
                return ;
            }
        }
        else {
            $data['closed'] = $this->input->get_post('closed') ? $this->input->get_post('closed') : (isset($_GET['closed']) ? '' : 'f');
            if ($this->input->get_post('start') && $this->input->get_post('end')) {
                $data['start'] = $this->input->get_post('start');
                $data['end'] = $this->input->get_post('end');
            } else {
                $data['start'] = date('d/m/Y', strtotime('-1 months'));;
                $data['end'] = date('d/m/Y', strtotime('+1 months'));
            }
            $start = format_waktu($data['start'], true);
            $end = format_waktu($data['end'], true);
            $perpage = 100;
            if ($this->input->get_post('nota')) {
                $this->db->where('lower(interpro.perencanaan.nota)', strtolower($data['nota']));
            }
            if ($data['closed']) {
                $this->db->where('closed', $data['closed']);
            }
            $this->db->where('interpro.perencanaan.status', 't');
            $this->db->where('interpro.perencanaan.proses', 4);
            $this->db->where('interpro.perencanaan.created_at >= ', $start);
            $this->db->where('interpro.perencanaan.created_at <= ', $end);
            $this->db->join('interpro.tindak_lanjut','interpro.perencanaan.id=interpro.tindak_lanjut.id_rencana','left outer');
            $total = $this->db->get('interpro.perencanaan')->num_rows();
            $this->db->limit($perpage, $page);
            $this->db->order_by('interpro.perencanaan.spt_date');
            if ($this->input->get_post('spt_no')) {
                $this->db->where('lower(interpro.perencanaan.spt_no)', strtolower($data['spt_no']));
            }
            if ($data['closed']) {
                $this->db->where('closed', $data['closed']);
            }

//            $data['rencana'] =$this->db->query('
//            SELECT "interpro"."perencanaan".id as id, "interpro"."tindak_lanjut"."judul", "interpro"."tindak_lanjut"."skpd", "interpro"."tindak_lanjut"."thn_lhp", "interpro"."tindak_lanjut"."no_lhp"
//            FROM "interpro"."perencanaan" LEFT JOIN "interpro"."tindak_lanjut" ON "interpro"."perencanaan"."id"="interpro"."tindak_lanjut"."id_rencana"
//            WHERE "interpro"."perencanaan"."proses" = 4 AND (("interpro"."perencanaan"."created_at" >= \''.$start.'\' AND "interpro"."perencanaan"."created_at" <= \''.$start.'\') OR ("interpro"."tindak_lanjut"."created_at" >= \''.$end.'\' AND "interpro"."tindak_lanjut"."created_at" <= \''.$end.'\'))
//            UNION
//            SELECT "interpro"."tindak_lanjut"."id" as id, "interpro"."tindak_lanjut"."judul","interpro"."tindak_lanjut"."skpd", "interpro"."tindak_lanjut"."thn_lhp", "interpro"."tindak_lanjut"."no_lhp"
//          FROM "interpro"."tindak_lanjut"
//           WHERE "interpro"."tindak_lanjut"."created_at" >= \''.$start.'\' AND "interpro"."tindak_lanjut"."created_at" <= \''.$end.'\'
//           ;
//');
            $this->db->where('interpro.perencanaan.status', 't');
            $this->db->where('interpro.perencanaan.proses', 4);
            $this->db->where("interpro.perencanaan.created_at >= '$start' OR interpro.tindak_lanjut.created_at >= '$start'",null,true );
            $this->db->where("interpro.perencanaan.created_at <= '$end' OR interpro.tindak_lanjut.created_at <= '$end'",null,true );
            $this->db->select('interpro.perencanaan.*,interpro.tindak_lanjut.skpd,interpro.tindak_lanjut.thn_lhp,interpro.tindak_lanjut.no_lhp');
            $this->db->join('interpro.tindak_lanjut','interpro.perencanaan.id=interpro.tindak_lanjut.id_rencana','left outer');
            $data['rencana2'] = $this->db->get('interpro.perencanaan')->result_array();

            $data['rencana'] = $this->db->get("interpro.tindak_lanjut_v4('$start ','$end')")->result_array();

//echo json_encode($data['rencana']);die();
            $data['pagination'] = $this->template->pagination('internalproses/tindak_lanjut', $total, $perpage);
            $data['page'] = $page;
            $this->template->user('tindak_lanjut', $data, array('title' => 'Temuan & Tindak Lanjut', 'breadcrumbs' => array('Internal Proses')));
        }
    }

    public function pelaksanaan($page = 0)
    {
        $menu = 'ca530176-384e-4c7d-8377-1171ec3a8d05';
        $data['access'] = list_access($menu);
        if ($this->input->get_post('save')) {
            check_access($menu, 'c');
            $status = $this->audit->perencanaan();
            if (!$status) {
                echo 500;
            } else {
                $this->session->set_flashdata('status_update', $status);
                echo 200;
            }
        } elseif ($this->input->get_post('tao') && $this->input->get_post('idt')) {

            $id_perencanaan_detail = $this->input->get_post('idt');
            $id_tao_detail = $this->input->get_post('tao');

            $this->db->where('pd.tao', $id_tao_detail);
            $this->db->where('pd.id', $id_perencanaan_detail);
            $this->db->join('public.users u1', "u1.id=pd.app_sv1", 'left');
            $this->db->join('public.users u2', "u2.id=pd.app_sv2", 'left');
            $this->db->join('public.users u3', "u3.id=pd.app_sv3", 'left');
            $this->db->select("pd.kesimpulan, pd.progres,pd.app_sv1_tstamp,pd.app_sv2_tstamp,pd.app_sv3_tstamp, u1.name as app_sv1,u2.name as app_sv2,u3.name as app_sv3,pd.app_sv1_status,pd.app_sv2_status,pd.app_sv3_status,pd.file as isian");
            $detail = $this->db->get('interpro.perencanaan_detail pd')->row_array();

            echo '<div>
                    <div class="form-group row">
                        <label for="inputName" class="col-sm-4 form-control-label">Berkas</label>
                        <div class="col-sm-8">';
            $kk = json_decode($detail['isian'], TRUE);
            echo '<label id="file_r" class="col-sm-8 form-control-label">';
            foreach ($kk as $d):
                echo '<a href= ' . base_url('img/' . $d['file']) . ' class="btn btn-sm btn-primary" target="_blank"><i class="fa fa-file-text"></i> ' . $d['nama'] . '</a>';
            endforeach;
            echo '</label>';
            echo '            </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputName" class="col-sm-4 form-control-label">Kesimpulan</label>
                        <div class="col-sm-8">
                            ' . "<pre>$detail[kesimpulan]</pre>" . '
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputName" class="col-sm-4 form-control-label">Persetujuan Dalnis</label>
                        <div class="col-sm-8">';

            if ($detail['progres'] <> 1 && $detail['app_sv1_status'] == 't') {
                echo '<label for="inputName" class="form-control-label">Disetujui oleh ' . $detail['app_sv1'] . '</label><br>
            <label for="inputName" class="form-control-label">Pada ' . format_waktu($detail['app_sv1_tstamp']) . '</label>
            ';
            } elseif ($detail['progres'] == 0 && $detail['app_sv1_status'] == 'f') {
                echo '<label for="inputName" class="form-control-label">Tidak Disetujui oleh ' . $detail['app_sv1'] . '</label><br>
            <label for="inputName" class="form-control-label">Pada ' . format_waktu($detail['app_sv1_tstamp']) . '</label>
            ';
            } else {
                echo '<label for="inputName" class="form-control-label">Menunggu Persetujuan</label>';
            }
            echo '</div></div>';

            //irban
            echo ' <div class="form-group row">
                        <label for="inputName" class="col-sm-4 form-control-label">Persetujuan Irban</label>
                        <div class="col-sm-8">';
            if ($detail['progres'] <> 1 && $detail['app_sv2_status'] == 't') {
                echo '<label for="inputName" class="form-control-label">Disetujui oleh ' . $detail['app_sv2'] . '</label><br>
            <label for="inputName" class="form-control-label">Pada ' . format_waktu($detail['app_sv2_tstamp']) . '</label>
            ';
            } elseif ($detail['progres'] == 0 && $detail['app_sv2_status'] == 'f') {
                echo '<label for="inputName" class="form-control-label">Tidak Disetujui oleh ' . $detail['app_sv2'] . '</label><br>
            <label for="inputName" class="form-control-label">Pada ' . format_waktu($detail['app_sv2_tstamp']) . '</label>
            ';
            } elseif ($detail['progres'] == 2 && $detail['app_sv2_status'] == null) {
                echo '<label for="inputName" class="form-control-label">Menunggu Persetujuan</label>';
            } else {
                echo '<label for="inputName" class="form-control-label">-</label>';
            }
            echo '</div></div>';

            //inspektur
            echo ' <div class="form-group row">
                        <label for="inputName" class="col-sm-4 form-control-label">Persetujuan Inspektur</label>
                        <div class="col-sm-8">';
            if ($detail['progres'] <> 1 && $detail['app_sv3_status'] == 't') {
                echo '<label for="inputName" class="form-control-label">Disetujui oleh ' . $detail['app_sv3'] . '</label><br>
            <label for="inputName" class="form-control-label">Pada ' . format_waktu($detail['app_sv3_tstamp']) . '</label>
            ';
            } elseif ($detail['progres'] == 0 && $detail['app_sv3_status'] == 'f') {
                echo '<label for="inputName" class="form-control-label">Tidak Disetujui oleh ' . $detail['app_sv3'] . '</label><br>
            <label for="inputName" class="form-control-label">Pada ' . format_waktu($detail['app_sv3_tstamp']) . '</label>
            ';
            } elseif ($detail['progres'] == 3 && $detail['app_sv3_status'] == null) {
                echo '<label for="inputName" class="form-control-label">Menunggu Persetujuan</label>';
            } else {
                echo '<label for="inputName" class="form-control-label">-</label>';
            }

            echo '</div></div>';

            echo '</div>';
        } elseif ($this->input->get_post('tao')) {
            $this->db->where('tao.id', $this->input->get_post('tao'));
            $this->db->join('master.jenis', 'jenis.id=tao.jenis');
            $this->db->join('master.sasaran', 'sasaran.id=tao.sasaran');
            $this->db->join('master.tujuan', 'tujuan.id=tao.tujuan');
            $this->db->select("tao.id,tao.kode_tao,tao.tao,tao.langkah,sasaran.kode||' '||sasaran.ket as sasaran,tujuan.kode||' '||tujuan.ket as tujuan,jenis.kode||' '||jenis.ket as jenis");
            $data = $this->db->get('support.v_tao tao')->row_array();

            $this->db->order_by('kertas_kerja', true);
            $this->db->where('id', $data['id']);
            $detail = $this->db->get('support.tao_detail')->row_array();

            echo "<table class='table'>";
            echo "<tr><td class='right'>JENIS AUDIT</td><td>:</td>";
            echo "<td>$data[jenis]</td></tr>";
            echo "<tr><td class='right'>SASARAN AUDIT</td><td>:</td>";
            echo "<td>$data[sasaran]</td></tr>";
            echo "<tr><td class='right'>TUJUAN AUDIT</td><td>:</td>";
            echo "<td>$data[tujuan]</td></tr>";
            echo "<tr><td class='right'>TAO</td><td>:</td>";
            echo "<td>$data[kode_tao] $data[tao]</td></tr>";
            echo "<tr><td class='right'></td><td></td>";
            echo "<td>" . "<a href=\"" . (@$detail['file_download'] ? base_url("img/$detail[file_download]") : "#") . "\" class=\"btn btn-sm btn-primary\" target=\"_blank\"><i class=\"fa fa-file-text\"></i> KERTAS KERJA</a>" . "</td></tr>";
            echo "</table>";
        } elseif ($this->input->get_post('i')) {
            $id_rencana = $this->input->get_post('i');
            if (isset($_FILES['file']) && $this->input->get_post(null)) {
                check_access($menu, 'u');
                $data = $this->audit->pelaksanaan($id_rencana, $this->input->get_post(null));
                if ($data) {
                    $this->session->set_flashdata('status_update', $data);
                }
                redirect(base_url('internalproses/pelaksanaan?i=' . $id_rencana));
                return;
            }
            $this->db->where('id', $id_rencana);
            $data['rencana'] = $this->db->get('interpro.perencanaan')->row_array();

            $this->db->where('id', $id_rencana);
            $data['rencana']['sasaran'] = $this->db->get('interpro.perencanaan_sasaran')->result_array();

            $this->db->where('id', $id_rencana);
            $data['rencana']['tujuan'] = $this->db->get('interpro.perencanaan_tujuan')->result_array();

            $this->db->order_by('v_tao_pelaksanaan.tahapan,v_tao_pelaksanaan.kode_kk', 'asc');
            $this->db->join('interpro.perencanaan_detail pd', "v_tao_pelaksanaan.id=pd.tao AND pd.id='$id_rencana'");
            $this->db->join('public.users u1', "u1.id=pd.rencana_by", 'left');
            $this->db->join('public.users u2', "u2.id=pd.realisasi_by", 'left');
            $this->db->select("v_tao_pelaksanaan.*,u1.name as rencana_by,to_char(pd.rencana_date,'dd/mm/yyyy') as rencana_date,u2.name as realisasi_by,to_char(pd.realisasi_date,'dd/mm/yyyy') as realisasi_date,pd.progres,pd.file as isian");
            $data['tao'] = $this->db->get('support.v_tao_pelaksanaan')->result_array();

            $this->db->where_in('id', json_decode($data['rencana']['tim'], true));
            $this->db->select('id,name');
            $data['listtim'] = $this->db->get('public.users')->result_array();

            $this->template->user('inputpelaksanaan', $data, array('title' => $data['rencana']['spt_no'] . " " . $data['rencana']['judul'], 'breadcrumbs' => array('Internal Proses', 'Pelaksanaan Audit')));
        } elseif ($this->input->get_post('v')) {
            $id_rencana = $this->input->get_post('v');

            $this->db->where('id', $id_rencana);
            $data['rencana'] = $this->db->get('interpro.perencanaan')->row_array();

            $this->db->where('id', $id_rencana);
            $this->db->select('array_to_json(array_agg(sasaran)) as v');
            $raw = $this->db->get('interpro.perencanaan_sasaran')->row_array();
            $data['rencana']['sasaran'] = $raw['v'];

            $this->db->where('id', $id_rencana);
            $this->db->select('array_to_json(array_agg(tujuan)) as v');
            $raw = $this->db->get('interpro.perencanaan_tujuan')->row_array();
            $data['rencana']['tujuan'] = $raw['v'];

            $this->db->order_by('no', 'asc');
            $this->db->where('status', true);
            $data['pkpt'] = $this->db->get('master.pkpt')->result_array();

            $this->db->order_by('order,kode', 'asc');
            $this->db->where('status', true);
            $data['jenis'] = $this->db->get('master.jenis')->result_array();

            $this->db->order_by('order,kode', 'asc');
            $this->db->where('status', true);
            $data['satker'] = $this->db->get('master.satker')->result_array();

            $this->db->order_by('sasaran.order,sasaran.kode', 'asc');
            $this->db->where('sasaran.status', true);
            $data['sasaran'] = $this->db->get('master.sasaran')->result_array();

            $this->db->order_by('tujuan.order,tujuan.kode', 'asc');
            $this->db->where('tujuan.status', true);
            $data['tujuan'] = $this->db->get('master.tujuan')->result_array();

            $this->db->order_by('kode', 'asc');
            $this->db->where('status', true);
            $data['aturan'] = $this->db->get('support.aturan')->result_array();

            $this->db->order_by('kode', 'asc');
            $this->db->where('status', true);
            $data['pedoman'] = $this->db->get('support.pedoman')->result_array();

            $this->db->order_by('v_tao.tahapan,v_tao.kode_kk', 'asc');
            $this->db->where('v_tao.status_tao', true);
            $this->db->where('v_tao.status_kk', true);
            $this->db->join('interpro.perencanaan_detail pd', "v_tao.id=pd.tao AND pd.id='$id_rencana'", 'left');
            $this->db->select("v_tao.*,pd.rencana_by as pelaksana,to_char(pd.rencana_date,'dd/mm/yyyy') as pelaksanaan");
            $data['tao'] = $this->db->get('support.v_tao')->result_array();

            $this->db->order_by('kode', 'asc');
            $data['tahapan'] = $this->db->get('master.tahapan_audit')->result_array();

            $data['tim'] = $this->db->get('support.tim()')->result_array();

            $this->db->select('id,name as nama');
            $this->db->where('status', true);
            $data['listtim'] = $this->db->get('public.users')->result_array();

            $this->template->user('viewrencana', $data, array('title' => 'Form Isian', 'breadcrumbs' => array('Internal Proses', 'Pelaksanaan Audit')));
        } elseif ($this->input->get_post('files') && $this->input->get_post('tao_files')) {
            $this->db->where('id', $this->input->get_post('tao_files'));
            $this->db->where('tao', $this->input->get_post('files'));
            $data = $this->db->get('interpro.perencanaan_detail')->row_array();
            $kk = json_decode($data['file'], TRUE);
            if (is_array($kk)) {
                foreach ($kk as $d):
                    if (strlen($d['nama']) > 30) {
                        $d['nama'] = substr($d['nama'], 0, 30);
                    }
                    echo "<div class=\"row list-upload\">
                        <div class=\"col-xs-10\">
                            " . '<a href= ' . base_url('img/' . $d['file']) . ' class="btn btn-sm btn-primary" target="_blank"><i class="fa fa-file-text"></i> ' . $d['nama'] . '</a>' . "
                        </div>
                            <div class=\"col-xs-2\"><button type=\"button\" data-id='$d[file]' class=\"btn btn-xs btn-danger remove-past-upload\"><i class=\"fa fa-remove\"></i></button>
                        </div>
                    </div>";
                endforeach;
            }
        } elseif ($this->input->get_post('remove_files') && $this->input->get_post('remove_id') && $this->input->get_post('remove_tao')) {
            $this->db->where('id', $this->input->get_post('remove_id'));
            $this->db->where('tao', $this->input->get_post('remove_tao'));
            $data = $this->db->get('interpro.perencanaan_detail')->row_array();
            $kk = json_decode($data['file'], TRUE);
            $rf = $this->input->get_post('remove_files');
            if (is_array($kk)) {
                foreach ($kk as $i => $k) {
                    if ($k['file'] == $rf) {
                        $path = BASEPATH . "../img/$rf";
                        if (file_exists($path)) {
                            unlink($path);
                        }
                        unset($kk[$i]);
                        $this->db->where('id', $this->input->get_post('remove_id'));
                        $this->db->where('tao', $this->input->get_post('remove_tao'));
                        $this->db->update('interpro.perencanaan_detail', array('file' => json_encode($kk)));
                        return;
                    }
                }
            }
        } else {
            $perpage = 100;
            $total = $this->db->get('interpro.v_pelaksanaan')->num_rows();
            $this->db->limit($perpage, $page);
            $this->db->order_by('created_at,spt_date,spt_no');
            if ($this->input->get_post('spt_no')) {
                $this->db->where('lower(spt_no)', strtolower($data['spt_no']));
            }
            $user = $this->session->userdata('user');
            $this->db->where('proses > 0 and proses >= 3', null, false);
            $this->db->like('tim', $user['id']);
            $data['rencana'] = $this->db->get('interpro.v_pelaksanaan')->result_array();
            $data['pagination'] = $this->template->pagination('internalproses/pelaksanaan', $total, $perpage);
            $data['page'] = $page;
            $this->template->user('pelaksanaan', $data, array('title' => 'Pelaksanaan Audit', 'breadcrumbs' => array('Internal Proses')));
        }
    }

    public function persetujuan_p($page = 0)
    {
        $menu = 'ca530176-384e-4c7d-8377-1171ec3a8d05';
        $data['access'] = list_access($menu);
        if ($this->input->get_post('save')) {
            check_access($menu, 'c');
            $status = $this->audit->perencanaan();
            if (!$status) {
                echo 500;
            } else {
                $this->session->set_flashdata('status_update', $status);
                echo 200;
            }
        }
        elseif ($this->input->get_post('persetujuanD')) {
            $progres = ($this->input->get_post('persetujuanD') == 't' ? '2' : '0');
            $sessid = $this->session->userdata('user');

            $raw = $this->db->query("SELECT NOW() as tstamp")->row_array();
            $insert = array(
                'app_sv1_note' => $this->input->get_post('note'),
                'app_sv1' => $sessid['id'],
                'app_sv1_status' => $this->input->get_post('persetujuanD'),
                'app_sv1_tstamp' => $raw['tstamp'],
                'modified_at' => $raw['tstamp'],
                'modified_by' => $sessid['id'],
                'progres' => $progres
            );
            $this->db->trans_begin();
            $this->db->where('interpro.perencanaan_detail.id', $this->input->get_post('id_perencanaan_detail'));
            $this->db->where('interpro.perencanaan_detail.tao', $this->input->get_post('id_tao_detail'));
            $this->db->update('interpro.perencanaan_detail', $insert);
            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
                $data['status_update'] = false;
            } else {
                $this->db->trans_commit();
                $data['status_update'] = true;
            }
            if ($data['status_update']) {
                $this->session->set_flashdata('status_update', true);
            }
            redirect(base_url('internalproses/persetujuan_p?v=' . $this->input->get_post('id_perencanaan_detail')));
        }
        elseif ($this->input->get_post('persetujuanI')) {
            $progres = ($this->input->get_post('persetujuanI') == 't' ? '3' : '0');
            $sessid = $this->session->userdata('user');

            $raw = $this->db->query("SELECT NOW() as tstamp")->row_array();
            $insert = array(
                'app_sv2_note' => $this->input->get_post('noteI'),
                'app_sv2' => $sessid['id'],
                'app_sv2_status' => $this->input->get_post('persetujuanI'),
                'app_sv2_tstamp' => $raw['tstamp'],
                'modified_at' => $raw['tstamp'],
                'modified_by' => $sessid['id'],
                'progres' => $progres
            );
            $this->db->trans_begin();
            $this->db->where('interpro.perencanaan_detail.id', $this->input->get_post('id_perencanaan_detail'));
            $this->db->where('interpro.perencanaan_detail.tao', $this->input->get_post('id_tao_detail'));
            $this->db->update('interpro.perencanaan_detail', $insert);
            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
                $data['status_update'] = false;
            } else {
                $this->db->trans_commit();
                $data['status_update'] = true;
            }
            if ($data['status_update']) {
                $this->session->set_flashdata('status_update', true);
            }
            redirect(base_url('internalproses/persetujuan_p?v=' . $this->input->get_post('id_perencanaan_detail')));
        }
        elseif ($this->input->get_post('persetujuanIn')) {
            $progres = ($this->input->get_post('persetujuanIn') == 't' ? '4' : '0');
            $sessid = $this->session->userdata('user');

            $raw = $this->db->query("SELECT NOW() as tstamp")->row_array();
            $insert = array(
                'app_sv3_note' => $this->input->get_post('noteIn'),
                'app_sv3' => $sessid['id'],
                'app_sv3_status' => $this->input->get_post('persetujuanIn'),
                'app_sv3_tstamp' => $raw['tstamp'],
                'modified_at' => $raw['tstamp'],
                'modified_by' => $sessid['id'],
                'progres' => $progres
            );
            $this->db->trans_begin();
            $this->db->where('interpro.perencanaan_detail.id', $this->input->get_post('id_perencanaan_detail'));
            $this->db->where('interpro.perencanaan_detail.tao', $this->input->get_post('id_tao_detail'));
            $this->db->update('interpro.perencanaan_detail', $insert);
            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
                $data['status_update'] = false;
            } else {
                $this->db->trans_commit();
                $data['status_update'] = true;
            }
            if ($data['status_update']) {
                $this->session->set_flashdata('status_update', true);
            }
            redirect(base_url('internalproses/persetujuan_p?v=' . $this->input->get_post('id_perencanaan_detail')));
        }
        elseif ($this->input->get_post('tao') && $this->input->get_post('idt')) {

            $id_perencanaan_detail = $this->input->get_post('idt');
            $id_tao_detail = $this->input->get_post('tao');

            $data['dalnis'] = false;
            $data['irban'] = false;
            $data['inspektur'] = false;
            $sessid = $this->session->userdata('user');

            $this->db->where('user', $sessid['id']);
            $this->db->where('role', '108ce450-d740-4bcb-9aaa-be9504bf94d8');
            $this->db->join('role', 'roleusers.role=role.id');
            $this->db->select('roleusers.role');
            $cekroledalnis = $this->db->get('roleusers')->num_rows();

            $this->db->where('user', $sessid['id']);
            $this->db->where('role', '6a45efd2-7007-4a43-8bda-09e26135e1ac');
            $this->db->join('role', 'roleusers.role=role.id');
            $this->db->select('roleusers.role');
            $cekroleinsp = $this->db->get('roleusers')->num_rows();

            $this->db->where('user', $sessid['id']);
            $this->db->where('role', '2332800e-edb0-49cd-92d7-1abc3d981207');
            $this->db->join('role', 'roleusers.role=role.id');
            $this->db->select('roleusers.role');
            $cekroleirban = $this->db->get('roleusers')->num_rows();

            if ($cekroledalnis > 0) {
                $data['dalnis'] = true;
            }
            if ($cekroleirban > 0) {
                $data['irban'] = true;
            }
            if ($cekroleinsp > 0) {
                $data['inspektur'] = true;
            }

            $this->db->where('pd.tao', $id_tao_detail);
            $this->db->where('pd.id', $id_perencanaan_detail);
            $this->db->join('public.users u1', "u1.id=pd.app_sv1", 'left');
            $this->db->join('public.users u2', "u2.id=pd.app_sv2", 'left');
            $this->db->join('public.users u3', "u3.id=pd.app_sv3", 'left');
            $this->db->select("pd.kesimpulan,pd.progres,pd.app_sv1_tstamp,pd.app_sv2_tstamp,pd.app_sv3_tstamp, u1.name as app_sv1,u2.name as app_sv2,u3.name as app_sv3,pd.app_sv1_status,pd.app_sv2_status,pd.app_sv3_status,pd.file as isian");
            $detail = $this->db->get('interpro.perencanaan_detail pd')->row_array();

            echo '<form role="form" method="post" enctype="multipart/form-data" onsubmit="$(\'#preloader\').show();">
                    <input type="hidden" name="id_perencanaan_detail" value="' . $id_perencanaan_detail . '">
                    <input type="hidden" name="id_tao_detail" value="' . $id_tao_detail . '">
                   <div class="form-group row">
                        <label for="inputName" class="col-sm-4 form-control-label">Berkas</label>
                        <div class="col-sm-8">';
            $kk = json_decode($detail['isian'], TRUE);
            echo '<label id="file_r" class="col-sm-8 form-control-label">';
            foreach ($kk as $d):
                echo '<a href= ' . base_url('img/' . $d['file']) . ' class="btn btn-sm btn-primary" target="_blank"><i class="fa fa-file-text"></i> ' . $d['nama'] . '</a>';
            endforeach;
            echo '</label>';
            echo '</div>
                    </div>
                    <div class="form-group row">
                        <label for="inputName" class="col-sm-4 form-control-label">Kesimpulan</label>
                        <div class="col-sm-8">
                            <pre>' . $detail['kesimpulan'] . '</pre>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputName" class="col-sm-4 form-control-label">Persetujuan Dalnis</label>
                        <div class="col-sm-8">';
            if ($detail['progres'] == 1 && $data['dalnis'] == true) {
                echo '<div class="radio radio-success radio-inline">
                            <input type="radio" id="inlineRadioDalnis1" value="t" name="persetujuanD" checked="">
                            <label for="inlineRadioDalnis1"> Setuju </label>
                        </div>
                        <div class="radio radio-success radio-inline">
                            <input type="radio" id="inlineRadioDalnis2" value="f" name="persetujuanD">
                            <label for="inlineRadioDalnis2"> Tidak Setuju </label>
                        </div>
                        <textarea class=" form-control" name="note" placeholder="Catatan"></textarea><br>
                        <input type="submit" class="btn btn-inverse waves-effect waves-light" value="Persetujuan">';
            } else if ($detail['progres'] <> 1 && $detail['app_sv1_status'] == 't') {
                echo '<label for="inputName" class="form-control-label">Disetujui oleh ' . $detail['app_sv1'] . '</label><br>
            <label for="inputName" class="form-control-label">Pada ' . format_waktu($detail['app_sv1_tstamp']) . '</label>
            ';
            } elseif ($detail['progres'] == 0 && $detail['app_sv1_status'] == 'f') {
                echo '<label for="inputName" class="form-control-label">Tidak Disetujui oleh ' . $detail['app_sv1'] . '</label><br>
            <label for="inputName" class="form-control-label">Pada ' . format_waktu($detail['app_sv1_tstamp']) . '</label>
            ';
            } else {
                echo '<label for="inputName" class="form-control-label">Menunggu Persetujuan</label>';
            }
            echo '</div></div>';

            //irban
            echo ' <div class="form-group row">
                        <label for="inputName" class="col-sm-4 form-control-label">Persetujuan Irban</label>
                        <div class="col-sm-8">';
            if ($detail['progres'] == 2 && $data['irban'] == true) {
                echo '<div class="radio radio-success radio-inline">
                            <input type="radio" id="inlineRadioDalnis1" value="t" name="persetujuanI" checked="">
                            <label for="inlineRadioDalnis1"> Setuju </label>
                        </div>
                        <div class="radio radio-success radio-inline">
                            <input type="radio" id="inlineRadioDalnis2" value="f" name="persetujuanI">
                            <label for="inlineRadioDalnis2"> Tidak Setuju </label>
                        </div>
                        <textarea class=" form-control" name="note" placeholder="Catatan"></textarea><br>
                        <input type="submit" class="btn btn-inverse waves-effect waves-light" value="Persetujuan">';
            } elseif ($detail['progres'] <> 1 && $detail['app_sv2_status'] == 't') {
                echo '<label for="inputName" class="form-control-label">Disetujui oleh ' . $detail['app_sv2'] . '</label><br>
            <label for="inputName" class="form-control-label">Pada ' . format_waktu($detail['app_sv2_tstamp']) . '</label>
            ';
            } elseif ($detail['progres'] == 0 && $detail['app_sv2_status'] == 'f') {
                echo '<label for="inputName" class="form-control-label">Tidak Disetujui oleh ' . $detail['app_sv2'] . '</label><br>
            <label for="inputName" class="form-control-label">Pada ' . format_waktu($detail['app_sv2_tstamp']) . '</label>
            ';
            } elseif ($detail['progres'] == 2 && $detail['app_sv2_status'] == null) {
                echo '<label for="inputName" class="form-control-label">Menunggu Persetujuan</label>';
            } else {
                echo '<label for="inputName" class="form-control-label">-</label>';
            }
            echo '</div></div>';

            //inspektur
            echo ' <div class="form-group row">
                        <label for="inputName" class="col-sm-4 form-control-label">Persetujuan Inspektur</label>
                        <div class="col-sm-8">';
            if ($detail['progres'] == 3 && $data['inspektur'] == true) {
                echo '<div class="radio radio-success radio-inline">
                            <input type="radio" id="inlineRadioDalnis1" value="t" name="persetujuanIn" checked="">
                            <label for="inlineRadioDalnis1"> Setuju </label>
                        </div>
                        <div class="radio radio-success radio-inline">
                            <input type="radio" id="inlineRadioDalnis2" value="f" name="persetujuanIn">
                            <label for="inlineRadioDalnis2"> Tidak Setuju </label>
                        </div>
                        <textarea class=" form-control" name="note" placeholder="Catatan"></textarea><br>
                        <input type="submit" class="btn btn-inverse waves-effect waves-light" value="Persetujuan">';
            } elseif ($detail['progres'] <> 1 && $detail['app_sv3_status'] == 't') {
                echo '<label for="inputName" class="form-control-label">Disetujui oleh ' . $detail['app_sv3'] . '</label><br>
            <label for="inputName" class="form-control-label">Pada ' . format_waktu($detail['app_sv3_tstamp']) . '</label>
            ';
            } elseif ($detail['progres'] == 0 && $detail['app_sv3_status'] == 'f') {
                echo '<label for="inputName" class="form-control-label">Tidak Disetujui oleh ' . $detail['app_sv3'] . '</label><br>
            <label for="inputName" class="form-control-label">Pada ' . format_waktu($detail['app_sv3_tstamp']) . '</label>
            ';
            } elseif ($detail['progres'] == 3 && $detail['app_sv3_status'] == null) {
                echo '<label for="inputName" class="form-control-label">Menunggu Persetujuan</label>';
            } else {
                echo '<label for="inputName" class="form-control-label">-</label>';
            }

            echo '</div></div>';

            echo '</div></form>';

        }
        elseif ($this->input->get_post('v')) {
            $id_rencana = $this->input->get_post('v');
            if (isset($_FILES['file']) && $this->input->get_post(null)) {
                check_access($menu, 'u');
                $data = $this->audit->pelaksanaan($id_rencana, $this->input->get_post(null));
                if ($data) {
                    $this->session->set_flashdata('status_update', $data);
                }
                redirect(base_url('internalproses/pelaksanaan?i=' . $id_rencana));
                return;
            }
            $this->db->where('id', $id_rencana);
            $data['rencana'] = $this->db->get('interpro.perencanaan')->row_array();

            $this->db->where('id', $id_rencana);
            $data['rencana']['sasaran'] = $this->db->get('interpro.perencanaan_sasaran')->result_array();

            $this->db->where('id', $id_rencana);
            $data['rencana']['tujuan'] = $this->db->get('interpro.perencanaan_tujuan')->result_array();

            $this->db->order_by('v_tao_pelaksanaan.tahapan,v_tao_pelaksanaan.kode_kk', 'asc');
            $this->db->join('interpro.perencanaan_detail pd', "v_tao_pelaksanaan.id=pd.tao AND pd.id='$id_rencana'");
            $this->db->join('public.users u1', "u1.id=pd.rencana_by", 'left');
            $this->db->join('public.users u2', "u2.id=pd.realisasi_by", 'left');
            $this->db->select("v_tao_pelaksanaan.*,u1.name as rencana_by,to_char(pd.rencana_date,'dd/mm/yyyy') as rencana_date,u2.name as realisasi_by,to_char(pd.realisasi_date,'dd/mm/yyyy') as realisasi_date,pd.progres,pd.file as isian");
            $data['tao'] = $this->db->get('support.v_tao_pelaksanaan')->result_array();

            $this->db->where_in('id', json_decode($data['rencana']['tim'], true));
            $this->db->select('id,name');
            $data['listtim'] = $this->db->get('public.users')->result_array();

            $this->template->user('viewpersetujuan_p', $data, array('title' => $data['rencana']['spt_no'] . " " . $data['rencana']['judul'], 'breadcrumbs' => array('Internal Proses', 'Pelaksanaan Audit')));
        } else {
            $user = $this->session->userdata('user');
            $this->db->where('user', $user['id']);
            $this->db->where('role', '6a45efd2-7007-4a43-8bda-09e26135e1ac');
            $this->db->join('role', 'roleusers.role=role.id');
            $this->db->select('roleusers.role');
            $cekroleinsp = $this->db->get('roleusers')->num_rows();

            $perpage = 100;
            $total = $this->db->get('interpro.v_pelaksanaan')->num_rows();
            $this->db->limit($perpage, $page);
            $this->db->where('proses > 0 and proses >= 3', null, false);
            if ($cekroleinsp > 0) {
                $data['rencana'] = $this->db->get('interpro.v_pelaksanaan')->result_array();
            } else {
                $this->db->like('tim', $user['id']);
                $this->db->where('proses > 0 and proses >= 3', null, false);
                $data['rencana'] = $this->db->get('interpro.v_pelaksanaan')->result_array();
            }
            $data['pagination'] = $this->template->pagination('internalproses/pelaksanaan', $total, $perpage);
            $data['page'] = $page;
            $this->template->user('persetujuan_p', $data, array('title' => 'Pelaksanaan Audit', 'breadcrumbs' => array('Internal Proses')));
        }
    }

    public function km3()
    {
        $id = $this->input->get_post('id');
        $this->db->where('id', $id);
        $this->db->join('master.satker', 'interpro.perencanaan.satker=master.satker.id');
        $this->db->join('interpro.perencanaan_detail', 'interpro.perencanaan.id=interpro.perencanaan_detail.id');
        $this->db->join('interpro.perencanaan_detail', 'interpro.perencanaan.id=interpro.perencanaan_detail.id');
        $this->db->get('interpro.perencanaan')->result_array();
    }

    public function km9()
    {
        $id_rencana = $this->input->get_post('v');

        $this->db->order_by('v_tao_pelaksanaan.tahapan,v_tao_pelaksanaan.kode_kk', 'asc');
        $this->db->join('interpro.perencanaan_detail pd', "v_tao_pelaksanaan.id=pd.tao AND pd.id='$id_rencana'");
        $this->db->join('public.users u1', "u1.id=pd.rencana_by", 'left');
        $this->db->join('public.users u2', "u2.id=pd.realisasi_by", 'left');
        $this->db->select("v_tao_pelaksanaan.*,u1.name as rencana_by,to_char(pd.rencana_date,'dd/mm/yyyy') as rencana_date,u2.name as realisasi_by,to_char(pd.realisasi_date,'dd/mm/yyyy') as realisasi_date,pd.progres,pd.file as isian");
        $data['tao'] = $this->db->get('support.v_tao_pelaksanaan')->result_array();

        $this->db->order_by('v_tao.tahapan,v_tao.kode_kk', 'asc');
        $this->db->where('v_tao.status_tao', true);
        $this->db->where('v_tao.status_kk', true);
        $this->db->join('public.users u1', "u1.id=pd.rencana_by", 'left');
        $this->db->join('public.users u2', "u2.id=pd.realisasi_by", 'left');
        $this->db->join('interpro.perencanaan_detail pd', "v_tao_pelaksanaan.id=pd.tao AND pd.id='$id_rencana'");
        $this->db->join('interpro.perencanaan_detail pd', "v_tao.id=pd.tao AND pd.id='$id_rencana'", 'left');
        $this->db->select("v_tao.*,pd.rencana_by as pelaksana,to_char(pd.rencana_date,'dd/mm/yyyy') as pelaksanaan");
        $data['tao'] = $this->db->get('support.v_tao')->result_array();


    }

    public function rot_tugas()
    {
        $tahun = 2017;
        $this->db->group_by('interpro.perencanaan.spt_no,interpro.perencanaan.spt_date,u4.name,u1.name,u2.name,interpro.perencanaan.app_sv1_tstamp,interpro.perencanaan.app_sv2_tstamp,interpro.perencanaan.created_at');
        $this->db->select('interpro.perencanaan.spt_no,interpro.perencanaan.spt_date,u4.name as created_by,interpro.perencanaan.created_at,u1.name as dalnis,u2.name as irban, interpro.perencanaan.app_sv1_tstamp, interpro.perencanaan.app_sv2_tstamp');
        $this->db->where('EXTRACT(year FROM interpro.perencanaan.spt_date) =', $tahun);
        $this->db->join('interpro.perencanaan_detail pd', "interpro.perencanaan.id=interpro.perencanaan.id");
        $this->db->join('public.users u4', "u4.id=interpro.perencanaan.created_by");
        $this->db->join('public.users u2', "u2.id=interpro.perencanaan.app_sv2");
        $this->db->join('public.users u1', "u1.id=interpro.perencanaan.app_sv1");
        $data['tao'] = $this->db->get('interpro.perencanaan')->result_array();
        $this->template->user('rot_tugas', $data, array('title' => 'ROT TUGAS', 'breadcrumbs' => array('Pelaporan')));
    }

    public function proses($page = 0)
    {
        $menu = 'fe012b79-adf5-42f4-bdbd-5ffd5417289c';
        $dalnis = '';
        $irban = '';
        $raw = $this->db->query("SELECT NOW() as tstamp")->row_array();
        $data['access'] = list_access($menu);
        $data['dalnis'] = '';
        $data['irban'] = '';
        $sessid = $this->session->userdata('user');


        $this->db->where('user', $sessid['id']);
        $this->db->where('role', '108ce450-d740-4bcb-9aaa-be9504bf94d8');
        $this->db->join('role', 'roleusers.role=role.id');
        $this->db->select('roleusers.role');
        $cekroledalnis = $this->db->get('roleusers')->num_rows();

        $this->db->where('user', $sessid['id']);
        $this->db->where('role', '2332800e-edb0-49cd-92d7-1abc3d981207');
        $this->db->join('role', 'roleusers.role=role.id');
        $this->db->select('roleusers.role');
        $cekroleirban = $this->db->get('roleusers')->num_rows();

        if ($cekroledalnis > 0) {
            $dalnis = true;
        }
        if ($cekroleirban > 0) {
            $irban = true;
        }
        $perpage = 100;
        $total = $this->db->get('interpro.perencanaan')->num_rows();

        if ($dalnis == true && $irban == false) {
            $data['irban'] = false;
            $data['dalnis'] = true;
        } elseif ($irban == true && $dalnis == false) {
            $data['irban'] = true;
            $data['dalnis'] = false;
        } elseif ($irban == true && $dalnis == true) {
            $data['dalnis'] = true;
            $data['irban'] = true;

        }
        if ($this->input->get_post('v')) {
            $id_rencana = $this->input->get_post('v');

            $this->db->where('interpro.perencanaan.id', $id_rencana);
            $this->db->select('interpro.perencanaan.*,support.tao.kode');
            $this->db->group_by('perencanaan.id,interpro.perencanaan_detail.id,support.tao.kode');
            $this->db->join('support.tao_detail', 'support.tao_detail.id=interpro.perencanaan_detail.tao');
            $this->db->join('support.tao', 'support.tao_detail.id_tao=support.tao.id');
            $this->db->join('interpro.perencanaan', 'interpro.perencanaan_detail.id=interpro.perencanaan.id');
            $data['tao'] = $this->db->get('interpro.perencanaan_detail')->result_array();

            $this->db->where('id_tao', $id_rencana);
            $data['kesimpulan'] = $this->db->get('interpro.perencanaan_hasil')->num_rows;

            $this->db->where('id', $id_rencana);
            $data['rencana'] = $this->db->get('interpro.perencanaan')->row_array();


            $this->template->user('viewproses', $data, array('title' => 'Detail Persetujuan', 'breadcrumbs' => array('Internal Proses', 'Rencana Audit')));
            return;
        } elseif ($this->input->get_post('idk')) {
            $nomor = $this->input->get_post('nomor');
            $tahun = $this->input->get_post('tahun');
            $kesimpulan = $this->input->get_post('kesimpulan');
            $this->db->trans_begin();
            $raw = $this->db->query("SELECT uuid_generate_v4() as id,NOW() as tstamp")->row_array();
            $this->db->insert('interpro.perencanaan_hasil', array(
                'id' => $raw['id'],
                'created_at' => $raw['tstamp'],
                'modified_at' => $raw['tstamp'],
                'modified_by' => $sessid['id'],
                'created_by' => $sessid['id'],
                'id_tao' => $this->input->get_post('idk'),
                'kesimpulan' => $kesimpulan
            ));
            $this->db->where('id', $this->input->get_post('idk'));
            $this->db->update('interpro.perencanaan', array(
                'created_at' => $raw['tstamp'],
                'modified_at' => $raw['tstamp'],
                'modified_by' => $sessid['id'],
                'created_by' => $sessid['id'],
                'kesimpulan' => $kesimpulan,
                'no_laporan' => $nomor,
                'thn_laporan' => $tahun,
            ));
            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
                $data['status_update'] = false;
            } else {
                $this->db->trans_commit();
                $data['status_update'] = true;
            }
            if ($data['status_update']) {
                $this->session->set_flashdata('status_update', true);
            }
            redirect(base_url('internalproses/persetujuan_r'));
            return;
        } elseif ($this->input->get_post('id_laporan')) {
            $nomor = $this->input->get_post('no_laporan');
            $tahun = $this->input->get_post('tahun');
            $this->db->trans_begin();
            $raw = $this->db->query("SELECT uuid_generate_v4() as id,NOW() as tstamp")->row_array();
            $this->db->where('id', $this->input->get_post('id_laporan'));
            $this->db->update('interpro.perencanaan', array(
                'created_at' => $raw['tstamp'],
                'modified_at' => $raw['tstamp'],
                'modified_by' => $sessid['id'],
                'created_by' => $sessid['id'],
                'no_laporan' => $nomor,
                'thn_laporan' => $tahun,
            ));
            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
                $data['status_update'] = false;
            } else {
                $this->db->trans_commit();
                $data['status_update'] = true;
            }
            if ($data['status_update']) {
                $this->session->set_flashdata('status_update', true);
            }
            redirect(base_url('internalproses/proses'));
            return;
        } elseif ($this->input->get_post('kesimpulan')) {
            $year = date("Y");
            $nextyear = $year + 1;
            $beforeyear = $year - 1;
            echo '<form role="form" method="post" action="?">';
            echo "<table class='table'>";
            echo "<tr><td class='right'>KESIMPULAN **</td><td>:</td>";
            echo "<td><input type='hidden' name ='idk' value='" . $this->input->get_post('kesimpulan') . "'><input type='text' class='form-control' name='kesimpulan' required></td></tr>";
            echo "<tr><td></td><td></td><td><input type='submit' value='Simpan' class='btn btn-inverse'> </td></tr>";
            echo "</table>";
            echo "</form>";
            return;
        } elseif ($this->input->get_post('cek')) {
            $this->db->where('id', $this->input->get_post('cek'));
            $this->db->where('proses', 4);
            $hasil = $this->db->get('interpro.perencanaan')->row_array();
            echo "<table class='table'>";
            echo "<tr><td class='left'>NOMOR LAPORAN</td><td class='pull-left'>:</td>";
            echo "<td class='left'>" . $hasil['no_laporan'] . "</td></tr>";
            echo "<tr><td class='left'>TAHUN LAPORAN</td><td class='pull-left'>:</td>";
            echo "<td class='left'>" . $hasil['thn_laporan'] . "</td></tr>";
            echo "<tr><td class='left'>KESIMPULAN</td><td class='pull-left'>:</td>";
            echo "<td class='left'>" . $hasil['kesimpulan'] . "</td></tr>";
            echo "</table>";
            return;
        }

        $this->db->limit($perpage, $page);
        $this->db->order_by('created_at,spt_date,spt_no');
        $this->db->where('proses >= 4', null, false);
        $this->db->like('tim', $sessid['id']);
        $this->db->join('interpro.perencanaan_hasil', 'interpro.v_perencanaan.id = interpro.perencanaan_hasil.id', 'left');
        $this->db->select('interpro.perencanaan_hasil.app_sv1 as app_sv1, interpro.perencanaan_hasil.app_sv2 as app_sv2, interpro.perencanaan_hasil.app_sv3 as app_sv3, interpro.perencanaan_hasil.app_sv1_status as app_sv1_status, interpro.perencanaan_hasil.app_sv2_status as app_sv2_status, interpro.perencanaan_hasil.app_sv3_status as app_sv3_status, interpro.perencanaan_hasil.app_sv1_note as app_sv1_note , interpro.perencanaan_hasil.app_sv2_note as app_sv2_note , interpro.perencanaan_hasil.app_sv3_note as app_sv3_note,interpro.v_perencanaan.*');
        $data['rencana'] = $this->db->get('interpro.v_perencanaan')->result_array();
        $this->template->user('proses', $data, array('title' => 'Proses Pelaporan', 'breadcrumbs' => array('Pelaporan Hasil Audit')));
    }
}
