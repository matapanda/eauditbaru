<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Init extends CI_Controller {
    public function index()
    {
        if(!$this->session->userdata('config')){
            $this->refresh(false);
        }
        redirect($this->agent->is_referral()?$this->agent->referrer():base_url());
    }
    public function refresh($redirect=true)
    {
        $this->session->set_userdata('config',array(
            'id'=>1,
            'a'=>'Pemerintah Kota Malang',
            'd'=>'Pemerintah Kota Malang',
            'company'=>'E-Audit - Pemerintah Kota Malang',
            'logotext'=>'E-Audit',
            'maintenance'=>'Pemerintah Kota Malang',
        ));
        if($redirect){
            redirect(base_url());
        }
    }
}
