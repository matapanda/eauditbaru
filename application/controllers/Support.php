<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Support extends CI_Controller {
    public function __construct()
    {
        parent::__construct();
        check_access('f10293a6-79c7-41e2-840e-a597e5ace426');
        $this->template->set_view('support');
    }

    public function index()
    {
        redirect(base_url());
    }
    public function program()
    {
        $menu='d66ab356-6792-451d-8f42-84f07e48b9e7';
        $data['access']=list_access($menu);
        $this->load->model('user');
        if($this->input->get_post('pk')){
            check_access($menu,'u');
            $id=$this->input->get_post('pk');
            $value=$this->input->get_post('value');
            $this->db->where('id',$id);
            $this->db->update('support.audit_program',array($this->input->get_post('name')=>$value));
            return;
        }elseif($this->input->get_post('i')){
            check_access($menu,'c');
            $this->template->user('program',$data,array('title'=>'Input Program','breadcrumbs'=>array('Master','Program','New')));
        }elseif($this->input->get_post('e')){
            check_access($menu,'c');
            $data['cek']=$this->user->get_data_program($this->input->get_post('e'));
            $this->template->user('program',$data,array('title'=>$data['cek']['langkah'],'breadcrumbs'=>array('Master','Program','Update')));
        }
        $data['user']=$this->user->get_list_program();
        $this->template->user('program',$data,array('title'=>'Program','breadcrumbs'=>array('Support')));
    }
}
