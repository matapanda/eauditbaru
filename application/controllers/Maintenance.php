<?php

/**
 * Created by PhpStorm.
 * User: gradindesign4
 * Date: 07/11/2016
 * Time: 15.46
 */
class Maintenance extends CI_Controller {
    public function index()
    {
        $config=$this->session->userdata('config');
        if($config['maintenance']){
            $this->template->guest('maintenance');
        }else{
            redirect(base_url());
        }
    }

}